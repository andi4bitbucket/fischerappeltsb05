@isTest
private class CO_CaseTriggerLogic_Test {

	@isTest static void testCO_CaseTriggerLogic() {
		Account acc = new Account();
		acc.Name = 'Test AQ';
		acc.BillingStreet = 'Högerdamm 41';
		acc.BillingCity = 'Hamburg';
		acc.BillingPostalCode = '20097';
		acc.BillingCountry = 'Germany';

		insert acc;

		Case__c tmpCase = new Case__c();
		tmpCase.accountname__c = acc.Id;
		tmpCase.Name = 'Test';
		tmpCase.branche__c = 'Test;Test2;';
		tmpCase.leistungssegmente__c = 'Test;Test2;';

		insert tmpCase;
	}
}