/**************************************************************************
Company: aquilliance GmbH
Author: Andreas Wolf
CreatedDate: 2017-1

**************************************************************************/
public with sharing class Salesflash_Ext {

	private final sObject mysObject;

	public Salesflash__c sf {get;set;}
	public String oppRTstandardId {get;set;}

	public Boolean notAllowed {get;set;}

    public Salesflash_Ext(ApexPages.StandardController stdController) {
        this.mysObject = (sObject)stdController.getRecord();
				sf = new Salesflash__c();
				notAllowed = false;

				try{sf=[SELECT Id, account__c, account__r.Name, beschreibung__c, opportunity__c, praedikat__c, status__c, OwnerId FROM Salesflash__c WHERE Id =: mysObject.Id];
			 	System.debug('');}catch(Exception e){System.debug('### -Salesflash_Ext- getSalesflashInfos: '+e);}
				try{oppRTstandardId=[SELECT Id FROM RecordType WHERE sObjectType = 'Opportunity__c' AND DeveloperName = 'Standard'].Id;
				System.debug('');}catch(Exception e){System.debug('### -Salesflash_Ext- getOppRTstandardId: '+e);}
    }

		public PageReference createOpportunity()
    {
      if(sf.opportunity__c == null && sf.Id != null)
      {
        Opportunity__c prep = new Opportunity__c();
				if(sf.account__r.Name != null)
        	prep.Name = sf.account__r.Name+' - '+Date.today().format();
				else
					prep.Name = 'Neu - '+Date.today().format();

				prep.account__c = sf.account__c;
				prep.projektstart__c = Date.today()+14;
				prep.projektende__c = Date.today()+28;
				prep.quelle__c = 'Salesflash';
				prep.quelle_person__c = sf.OwnerId;

        if(oppRTstandardId != '')
          prep.RecordTypeId = oppRTstandardId;


        try{insert prep;
        System.debug('');}catch(Exception e){System.debug('### -Salesflash_Ext- insertOpp: '+e);}

        if(prep.Id != null)
        {
          PageReference gotoPR = new ApexPages.StandardController(prep).view();
          gotoPR.setRedirect(true);

					// Follow opportunity
					EntitySubscription follow = new EntitySubscription (
																 parentId = sf.Id,
																 subscriberid = sf.OwnerId);

					// Link to Salesflash
					sf.opportunity__c = prep.Id;
					try{update sf; insert follow;
	        System.debug('');}catch(Exception e){System.debug('### -Salesflash_Ext- updateSalesflash: '+e);}

          return gotoPR;
        }else{
          return null;
        }
      }
      else
      {
        notAllowed = true;   // Shows PopUp Window with infotext that it isnt allowed to create opps for this account
        return null;
      }
    }

    // Method to go back to the account after the popup screen
    public PageReference backTo()
    {
      PageReference gotoPR = new ApexPages.StandardController(sf).view();
      gotoPR.setRedirect(true);

      return gotoPR;
    }
}