@IsTest(SeeAllData=true)
private class RecordAggregator_Test {

    @isTest static void positiveTest() {
        createSettings();
        Account LO_Account = createAccount();
        System.assertEquals( 0, LO_Account.NumberOfEmployees );
        Contact LO_Contact = createContact( LO_Account );
        Test.startTest();
        RecordAggregator.aggregate( new List< SObject >{ LO_Contact }, null );
        LO_Account = getAccount( LO_Account.Id );
        System.assertEquals( 1, LO_Account.NumberOfEmployees );
        Test.stopTest();
    }

    private static void createSettings(){
        AggregateSettings__c LO_AggregateSettings = new AggregateSettings__c();
        LO_AggregateSettings.Name                 = 'Account-Contact-1';
        LO_AggregateSettings.MasterObject__c      = 'Account';
        LO_AggregateSettings.SlaveObject__c       = 'Contact';
        LO_AggregateSettings.LookupField__c       = 'AccountId';
        LO_AggregateSettings.MasterFields__c      = 'NumberOfEmployees';
        LO_AggregateSettings.AggregateFields__c   = 'Count(Id)';
        LO_AggregateSettings.include_deleted__c   = true;
        LO_AggregateSettings.isActive__c          = true;
        insert( LO_AggregateSettings );


        AggregateSettings__c LO_AggregateSettings2 = new AggregateSettings__c();
        LO_AggregateSettings2.Name                 = 'Account-Contact-City';
        LO_AggregateSettings2.MasterObject__c      = 'Account';
        LO_AggregateSettings2.SlaveObject__c       = 'Contact';
        LO_AggregateSettings2.LookupField__c       = 'AccountId';
        LO_AggregateSettings2.MasterFields__c      = 'BillingCity';
        LO_AggregateSettings2.AggregateFields__c   = 'LAST(LastModifiedDate,MailingCity)';
        LO_AggregateSettings2.include_deleted__c   = true;
        LO_AggregateSettings2.isActive__c          = true;
        insert( LO_AggregateSettings2 );

    }

    private static Account createAccount()
    {
        Id acc_RecordTypeId = null;
        try{
                acc_RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'kunde' AND SobjectType='Account' LIMIT 1].Id;
            System.debug('');}catch(Exception e){System.debug('### error acc_RecordTypeId:'+e);}

        Account LO_Account                                        = new Account();
        Map<String, Schema.DescribeFieldResult> LO_RequiredFields = getRequiredFields( 'Account' );
        setRequiredFields( LO_Account, LO_RequiredFields, 0 );
        LO_Account.Name = 'AQ Test';
        LO_Account.NumberOfEmployees = 0;
        LO_Account.RecordTypeId = acc_RecordTypeId;
        LO_Account.sperrgrund__c = null;
        LO_Account.account_status__c = 'Aktiver Kunde';
        insert( LO_Account );
        return LO_Account;
    }

    private static Account getAccount( Id PV_AccountId ){
        Account LO_Account = [ SELECT Id, NumberOfEmployees FROM Account WHERE Id =: PV_AccountId ];
        return LO_Account;
    }

    private static Contact createContact( Account PO_Account ){
        Contact LO_Contact = new Contact();
        Map<String, Schema.DescribeFieldResult> LO_RequiredFields = getRequiredFields( 'Contact' );
        setRequiredFields( LO_Contact, LO_RequiredFields, 1 );
        LO_Contact.AccountId = PO_Account.Id;
        LO_Contact.Salutation = 'Mr';
        insert( LO_Contact );
        return LO_Contact;
    }

    //Get the required fields for the given object type
    private static Map< String, Schema.DescribeFieldResult > getRequiredFields( String PV_ObjectName ){
        Map< String, Schema.DescribeFieldResult > LM_RequiredFields = new Map< String, Schema.DescribeFieldResult >();
        Map< String, Schema.SObjectField > LM_ObjectFields = Schema.getGlobalDescribe().get( PV_ObjectName ).getDescribe().fields.getMap();
        for( String LV_FieldName: LM_ObjectFields.KeySet() ){
            Schema.SObjectField LO_Field = LM_ObjectFields.get( LV_FieldName );
            Schema.DescribeFieldResult LO_FieldDescribe = LO_Field.getDescribe();
            if ( ( !LO_FieldDescribe.isNillable() ) && ( !LO_FieldDescribe.isCalculated() ) && ( !LO_FieldDescribe.isAutonumber() ) && ( !LO_FieldDescribe.isDefaultedOnCreate() ) && ( !LO_FieldDescribe.isDeprecatedAndHidden() ) && ( LO_FieldDescribe.isUpdateable() ) ){
                LM_RequiredFields.put( LV_FieldName, LO_FieldDescribe );
            }
        }
        return LM_RequiredFields;
    }

    //Set the required fields for the given record
    private static void setRequiredFields( SObject PO_Record, Map< String, Schema.DescribeFieldResult > PM_RequiredFields, Integer PV_Number ){
        for( String LV_Field: PM_RequiredFields.KeySet() ){
            Schema.DescribeFieldResult LO_FieldDescribe = PM_RequiredFields.get( LV_Field );
            if ( LO_FieldDescribe.getType() == Schema.DisplayType.base64 ) {
                PO_Record.put( LV_Field, blob.valueOf( String.valueOf( PV_Number ) ) );
            } else if ( LO_FieldDescribe.getType() == Schema.DisplayType.Boolean ) {
                PO_Record.put( LV_Field, false );
            } else if ( LO_FieldDescribe.getType() == Schema.DisplayType.Combobox ) {
                PO_Record.put( LV_Field, String.valueOf( PV_Number ) );
            } else if ( LO_FieldDescribe.getType() == Schema.DisplayType.Currency ) {
                PO_Record.put( LV_Field, PV_Number );
            } else if ( LO_FieldDescribe.getType() == Schema.DisplayType.Date ) {
                PO_Record.put( LV_Field, Date.today() );
            } else if ( LO_FieldDescribe.getType() == Schema.DisplayType.DateTime ) {
                PO_Record.put( LV_Field, DateTime.now() );
            } else if ( LO_FieldDescribe.getType() == Schema.DisplayType.Double ) {
                PO_Record.put( LV_Field, PV_Number );
            } else if ( LO_FieldDescribe.getType() == Schema.DisplayType.Email ) {
                PO_Record.put( LV_Field, 'test' + String.valueOf( PV_Number ) + '@test.com' );
            } else if ( LO_FieldDescribe.getType() == Schema.DisplayType.EncryptedString ) {
                PO_Record.put( LV_Field, String.valueOf( PV_Number ) );
            } else if ( LO_FieldDescribe.getType() == Schema.DisplayType.Integer ) {
                PO_Record.put( LV_Field, PV_Number );
            } else if ( LO_FieldDescribe.getType() == Schema.DisplayType.MultiPicklist ) {
                PO_Record.put( LV_Field, String.valueOf( PV_Number ) );
            } else if ( LO_FieldDescribe.getType() == Schema.DisplayType.Percent ) {
                PO_Record.put(LV_Field, PV_Number );
            } else if ( LO_FieldDescribe.getType() == Schema.DisplayType.Phone ) {
                PO_Record.put( LV_Field, '123' );
            } else if ( LO_FieldDescribe.getType() == Schema.DisplayType.Picklist ) {
                PO_Record.put( LV_Field, String.valueOf( PV_Number ) );
            } else if ( LO_FieldDescribe.getType() == Schema.DisplayType.String ) {
                PO_Record.put( LV_Field, String.valueOf( PV_Number ) );
            } else if ( LO_FieldDescribe.getType() == Schema.DisplayType.TextArea ) {
                PO_Record.put( LV_Field, String.valueOf( PV_Number ) );
            } else if ( LO_FieldDescribe.getType() == Schema.DisplayType.Time ) {
                PO_Record.put( LV_Field, Time.newInstance( 0, 0, 0, 0 ) );
            } else if ( LO_FieldDescribe.getType() == Schema.DisplayType.URL ) {
                PO_Record.put( LV_Field, 'http://test' + String.valueOf( PV_Number ) + '.com' );
            } else {
                System.debug( 'Unhandled field type ' + LO_FieldDescribe.getType() );
            }
        }
    }

}