/**************************************************************************
Company: aquilliance GmbH
Author: Andreas Wolf
CreatedDate: 2017-1

**************************************************************************/
public with sharing class List_Management_Ext {

	private final sObject mysObject;
	public User u {get;set;}

	// List RecordTypes
	public String standardRT {get;set;}
	public String internRT {get;set;}
	public String externRT {get;set;}
	public String privateRT {get;set;}

	public Account a {get;set;}
	public Mandant__c m {get;set;}
	public Contact c {get;set;}


	public List<List__c> list_list {get;set;}
	public Map<String, List__c> map_folders {get;set;}
	// Account Team Info (Accounts where the current user is a member of the team)
	public Map<String, String> map_acc_team {get;set;}
	public Map<String, List<List__c>> intern_Lists {get;set;}
	public Map<String, List<List__c>> extern_Lists {get;set;}
	public Map<String, List<List__c>> private_Lists {get;set;}






    public List_Management_Ext(ApexPages.StandardController stdController) {
        this.mysObject = (sObject)stdController.getRecord();
				u = new User();
				try{u = [SELECT id, FirstName, LastName, Email, Phone, Profile.Name FROM User WHERE id =: UserInfo.getUserId() LIMIT 1];
				System.debug('');}catch(Exception e){System.debug('### -List_Management_Ext- getUserInfo: '+e);}

				String object_type = checkPrefixForObjectName(mysObject.Id);
				System.debug('### -List_Management_Ext- object_type: '+object_type);

				getRecordTypeIds();

				if(object_type == 'Account')
					run_Acc();
				if(object_type == 'Mandant__c')
					run_Mandant();
				if(object_type == 'Contact')
					run_Contact();
    }

		public static String checkPrefixForObjectName(String recordId)
		{
			String objectName;
			String myIdPrefix = String.valueOf(recordId).substring(0,3);

			Map<String, Schema.SObjectType> gd =  Schema.getGlobalDescribe();
			//Loop through all the sObject types returned by Schema
			for(Schema.SObjectType stype : gd.values())
			{
					Schema.DescribeSObjectResult r = stype.getDescribe();
					String prefix = r.getKeyPrefix();
					//System.debug('Prefix is ' + prefix);
					//Check if the prefix matches with requested prefix
					if(prefix!=null && prefix.equals(myIdPrefix)){
							objectName = r.getName();
							//System.debug('Object Name! ' + objectName);
							break;
					}
			}
			return objectName;
		}

		public void getRecordTypeIds(){
			try{standardRT = [SELECT id FROM RecordType WHERE sObjectType = 'List__c' AND DeveloperName = 'Standard' LIMIT 1].Id;
			System.debug('');}catch(Exception e){System.debug('### -List_Management_Ext- getStandardListRTid: '+e);}
			try{internRT = [SELECT id FROM RecordType WHERE sObjectType = 'List__c' AND DeveloperName = 'Intern' LIMIT 1].Id;
			System.debug('');}catch(Exception e){System.debug('### -List_Management_Ext- getInternListRTid: '+e);}
			try{externRT = [SELECT id FROM RecordType WHERE sObjectType = 'List__c' AND DeveloperName = 'Extern' LIMIT 1].Id;
			System.debug('');}catch(Exception e){System.debug('### -List_Management_Ext- getExternListRTid: '+e);}
			try{privateRT = [SELECT id FROM RecordType WHERE sObjectType = 'List__c' AND DeveloperName = 'Private' LIMIT 1].Id;
			System.debug('');}catch(Exception e){System.debug('### -List_Management- getPrivateListRTid: '+e);}
		}


		public void getRelevantAccountTeams(){
			List<Team_Member__c> list_tm = new List<Team_Member__c>();
			try{list_tm = [SELECT id, Name, Account__c FROM Team_Member__c WHERE User__c =: u.Id AND RecordType.DeveloperName = 'Customer_Team'];
			System.debug('');}catch(Exception e){System.debug('### -List_Management_Ext- getAccTeams: '+e);}
			for(Team_Member__c tmp : list_tm)
			{
				if(!map_acc_team.containsKey(tmp.Account__c))
				{
					map_acc_team.put(tmp.Account__c, tmp.Account__c);
				}
			}
		}

// ************************************** For Account ******************************************************
		public void run_Acc()
		{
			init_acc();
			getRelevantAccountTeams();
			getAllLists_Acc();
			prepareListsForVF_Acc();
		}

		public void init_acc()
		{
			a = new Account();
			a.Id = mysObject.ID;
			map_folders = new Map<String, List__c>();
			map_acc_team = new Map<String, String>();
			extern_Lists = new Map<String, List<List__c>>();
		}

		public void getAllLists_Acc(){

			List<List__c> list_folders = new List<List__c>();
			// Get All extern Folders for which the current user has access
			try{list_folders = [SELECT id, Name, RecordTypeId, Account__c, Account__r.Name, Mandant__c, Mandant__r.Name, Description__c
				FROM List__c
				WHERE RecordTypeId =: externRT AND Account__c IN: map_acc_team.keyset() AND Account__c =: a.Id
				ORDER By Name ASC];
			System.debug('');}catch(Exception e){System.debug('### -List_Management_Ext- getExternFolders_acc: '+e);}
			System.debug('### list_folders: '+list_folders);
			// prepare Folder Map
			for(List__c tmp : list_folders)
			{
				if(!map_folders.containsKey(tmp.Id))
				{
					map_folders.put(tmp.Id, tmp);
				}
			}

			list_list = new List<List__c>();
			// Get all Lists
			try{list_list = [SELECT id, Name, RecordTypeId, Folder__r.RecordTypeId, Folder__r.Name,
				OwnerId, Description__c
				FROM List__c
				WHERE RecordTypeId =: standardRT AND Folder__c IN: map_folders.keySet()
				ORDER BY Folder__r.Name ASC];
			System.debug('');}catch(Exception e){System.debug('### -List_Management_Ext- getLists_acc: '+e);}
		}

		public void prepareListsForVF_Acc(){
			for(List__c tmp : list_list)
			{
				// if LookUp to Folder isnt empty
				if(tmp.Folder__c != null)
				{
					// if user has access
					if(map_folders.containsKey(tmp.Folder__c))
					{
						//map_lists.put(tmp.Id, tmp.Name);
						// EXTERN
						if(tmp.Folder__r.RecordTypeId == externRT)
						{
							if(!extern_Lists.containskey(tmp.Folder__c))
							{
								List<List__c> tmp_list = new List<List__c>();
								tmp_list.add(tmp);
								extern_Lists.put(tmp.Folder__c, tmp_list);
							}else{
								List<List__c> tmp_list = extern_Lists.get(tmp.Folder__c);
								tmp_list.add(tmp);
								tmp_list.sort();
								extern_Lists.put(tmp.Folder__c, tmp_list);
							}
						}
					}
				}
			}
		}

		public PageReference goToListManagement_Acc(){

			String selected_list = Apexpages.currentPage().getParameters().get('list_id');
			return new PageReference('/apex/List_Management?activeListId='+selected_list);
		}



// ************************************** For Mandant ******************************************************
		public void run_Mandant()
		{
			init_Man();
			getAllLists_Man();
			prepareListsForVF_Man();
		}

		public void init_Man()
		{
			m = new Mandant__c();
			m.Id = mysObject.ID;
			map_folders = new Map<String, List__c>();
			intern_Lists = new Map<String, List<List__c>>();
		}

		public void getAllLists_Man(){

			List<List__c> list_folders = new List<List__c>();
			// Get All extern Folders for which the current user has access
			try{list_folders = [SELECT id, Name, RecordTypeId, Account__c, Account__r.Name, Mandant__c, Mandant__r.Name, Description__c
				FROM List__c
				WHERE RecordTypeId =: internRT AND Mandant__c =: m.Id
				ORDER By Name ASC];
			System.debug('');}catch(Exception e){System.debug('### -List_Management_Ext- getExternFolders_man: '+e);}
			System.debug('### list_folders: '+list_folders);
			// prepare Folder Map
			for(List__c tmp : list_folders)
			{
				if(!map_folders.containsKey(tmp.Id))
				{
					map_folders.put(tmp.Id, tmp);
				}
			}

			list_list = new List<List__c>();
			// Get all Lists
			try{list_list = [SELECT id, Name, RecordTypeId, Folder__r.RecordTypeId, Folder__r.Name,
				OwnerId, Description__c
				FROM List__c
				WHERE RecordTypeId =: standardRT AND Folder__c IN: map_folders.keySet()
				ORDER BY Folder__r.Name ASC];
			System.debug('');}catch(Exception e){System.debug('### -List_Management_Ext- getLists_man: '+e);}
		}

		public void prepareListsForVF_Man(){
			for(List__c tmp : list_list)
			{
				// if LookUp to Folder isnt empty
				if(tmp.Folder__c != null)
				{
					// if user has access
					if(map_folders.containsKey(tmp.Folder__c))
					{
						// INTERN
						if(tmp.Folder__r.RecordTypeId == internRT)
						{
							if(!intern_Lists.containskey(tmp.Folder__c))
							{
								List<List__c> tmp_list = new List<List__c>();
								tmp_list.add(tmp);
								intern_Lists.put(tmp.Folder__c, tmp_list);
							}else{
								List<List__c> tmp_list = intern_Lists.get(tmp.Folder__c);
								tmp_list.add(tmp);
								tmp_list.sort();
								intern_Lists.put(tmp.Folder__c, tmp_list);
							}
						}
					}
				}
			}
		}

		public PageReference goToListManagement_Man(){

			String selected_list = Apexpages.currentPage().getParameters().get('list_id');
			return new PageReference('/apex/List_Management?activeListId='+selected_list);
		}


// ************************************** For Contact ******************************************************
		public void run_Contact()
		{
			init_Con();
			getRelevantAccountTeams();
			getAllLists_Con();
			prepareListsForVF_Con();
		}

		public void init_Con()
		{
			c = new Contact();
			try{c = [SELECT Id, FirstName, LastName, Title, Department, Account.Name, Account.Industry FROM Contact WHERE Id =: mysObject.Id LIMIT 1];
      System.debug('');}catch(Exception e){System.debug('### -List_Management_Ext- getContact: '+e);}
			map_folders = new Map<String, List__c>();
			map_acc_team = new Map<String, String>();
			intern_Lists = new Map<String, List<List__c>>();
			extern_Lists = new Map<String, List<List__c>>();
			private_Lists = new Map<String, List<List__c>>();
		}

		public void getAllLists_Con(){

			List<List__c> list_folders = new List<List__c>();
			// Get All extern Folders for which the current user has access
			try{list_folders = [SELECT id, Name, RecordTypeId, Account__c, Account__r.Name, Mandant__c, Mandant__r.Name, Description__c
				FROM List__c
				WHERE RecordTypeId =: internRT
				OR (RecordTypeId =: externRT AND Account__c IN: map_acc_team.keyset())
				OR (RecordTypeId =: privateRT AND OwnerId =: u.Id)
				ORDER By Name ASC];
			System.debug('');}catch(Exception e){System.debug('### -List_Management_Ext- getExternFolders_con: '+e);}
			System.debug('### list_folders: '+list_folders);
			// prepare Folder Map
			for(List__c tmp : list_folders)
			{
				if(!map_folders.containsKey(tmp.Id))
				{
					map_folders.put(tmp.Id, tmp);
				}
			}

			List<List_Member__c> list_member = new List<List_Member__c>();
			Map<String, String> map_list_member = new  Map<String, String>();
			try{list_member = [SELECT id, List__c
				FROM List_Member__c WHERE Contact__c =: c.Id];
			System.debug('');}catch(Exception e){System.debug('### -List_Management_Ext- getMembersFromActiveList_con: '+e);}
			for(List_Member__c tmp : list_member)
			{
				map_list_member.put(tmp.List__c, tmp.List__c);
			}
			System.debug('### list_member: '+list_member);

			list_list = new List<List__c>();
			// Get all Lists
			try{list_list = [SELECT id, Name, RecordTypeId, Folder__r.RecordTypeId, Folder__r.Name,
				OwnerId, Description__c
				FROM List__c
				WHERE RecordTypeId =: standardRT AND Folder__c IN: map_folders.keySet() AND Id IN: map_list_member.keySet()
				ORDER BY Folder__r.Name ASC];
			System.debug('');}catch(Exception e){System.debug('### -List_Management_Ext- getLists_con: '+e);}
		}

		public void prepareListsForVF_Con(){
			for(List__c tmp : list_list)
			{
				// if LookUp to Folder isnt empty
				if(tmp.Folder__c != null)
				{
					// if user has access
					if(map_folders.containsKey(tmp.Folder__c))
					{
						// INTERN
						if(tmp.Folder__r.RecordTypeId == internRT)
						{
							if(!intern_Lists.containskey(tmp.Folder__c))
							{
								List<List__c> tmp_list = new List<List__c>();
								tmp_list.add(tmp);
								intern_Lists.put(tmp.Folder__c, tmp_list);
							}else{
								List<List__c> tmp_list = intern_Lists.get(tmp.Folder__c);
								tmp_list.add(tmp);
								tmp_list.sort();
								intern_Lists.put(tmp.Folder__c, tmp_list);
							}
						}
						// EXTERN
						if(tmp.Folder__r.RecordTypeId == externRT)
						{
							if(!extern_Lists.containskey(tmp.Folder__c))
							{
								List<List__c> tmp_list = new List<List__c>();
								tmp_list.add(tmp);
								extern_Lists.put(tmp.Folder__c, tmp_list);
							}else{
								List<List__c> tmp_list = extern_Lists.get(tmp.Folder__c);
								tmp_list.add(tmp);
								tmp_list.sort();
								extern_Lists.put(tmp.Folder__c, tmp_list);
							}
						}
						// PRIVATE
						if(tmp.Folder__r.RecordTypeId == privateRT)
						{
							if(!private_Lists.containskey(tmp.Folder__c))
							{
								List<List__c> tmp_list = new List<List__c>();
								tmp_list.add(tmp);
								private_Lists.put(tmp.Folder__c, tmp_list);
							}else{
								List<List__c> tmp_list = private_Lists.get(tmp.Folder__c);
								tmp_list.add(tmp);
								tmp_list.sort();
								private_Lists.put(tmp.Folder__c, tmp_list);
							}
						}
					}
				}
			}
		}

		public PageReference goToListManagement_Con(){
			String selected_list = Apexpages.currentPage().getParameters().get('list_id');

			String firstName = '';
			String lastName = '';
			String accName = '';
			String accBranch = '';
			String department = '';
			String function = '';

			if(c.FirstName != null)
				firstName = c.FirstName;
			if(c.LastName != null)
				lastName = c.LastName;
			if(c.Account.Name != null)
				accName = c.Account.Name;
			if(c.Account.Industry != null)
				accBranch = c.Account.Industry;
			if(c.Department != null)
				department = c.Department;
			if(c.Title != null)
				function = c.Title;

			return new PageReference('/apex/List_Management?first_name='+firstName+'&last_name='+lastName+'&depa='+department+'&func='+function+'&acc_name='+accName+'&acc_branch='+accBranch+'&search=true&activeListId='+selected_list);
		}
}