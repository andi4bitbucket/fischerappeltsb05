public with sharing class User_Mandant_Trigger {
	public User_Mandant_Trigger() {

	}

	public void check_for_doubles(List<User_Mandant__c> list_um)
	{
		String agenturRTid = '';
		try{agenturRTid = [SELECT id FROM RecordType WHERE sObjectType = 'user_mandant__c' AND DeveloperName = 'Agentur' LIMIT 1].Id;
		System.debug('');}catch(Exception e){System.debug('### -Listing_Check_Ext- getAgenturRTid: '+e);}


		Map<String, User_Mandant__c> map_um = new Map<String, User_Mandant__c>();
		for(User_Mandant__c tmp:list_um)
		{
			if(tmp.RecordTypeId == agenturRTid)
			{
				map_um.put(tmp.user__c, tmp);
			}
		}

		List<User_Mandant__c> list_check = new List<User_Mandant__c>();
		Map<String, User_Mandant__c> map_existing_um = new Map<String, User_Mandant__c>();
		try{list_check = [SELECT id, user__c FROM user_mandant__c WHERE User__c IN: map_um.Keyset() AND RecordTypeId =: agenturRTid];
		System.debug('');}catch(Exception e){System.debug('### -User_Mandant_Trigger-check_for_doubles- getUserMandantInfo: '+e);}
		for(User_Mandant__c tmp:list_check)
		{
			map_existing_um.put(tmp.User__c, tmp);
		}

		for(User_Mandant__c tmp:list_um)
		{
			if(tmp.RecordTypeId == agenturRTid)
			{
				if(map_existing_um.containskey(tmp.User__c))
				{
					tmp.addError('Für diesen User wurde bereits eine User-Mandanten Beziehung erstellt!');
				}
			}
		}
	}
}