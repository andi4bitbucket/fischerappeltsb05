/***************************************************************************
Company: aquilliance GmbH
Author: Mattis Weiler
CreatedDate: 2017-01
***************************************************************************/
global class Listing_Reminder_Scheduled implements Schedulable {
  /***********************************************
  to Start open execute anonymous

  Listing_Reminder_Scheduled asset_sched = new Listing_Reminder_Scheduled();
  system.schedule('Listing_Daily_Check', asset_sched.CRON_EXP_everyday_6am, asset_sched);
  ***********************************************/
  public String CRON_EXP_everyday_6am =  '0 0 6 * * ?';

  global void execute(SchedulableContext sc) {
    run();
  }

  public Listing_Reminder_Scheduled(){

  }

  public void run(){
    List<Listings__c> list_listing = new List<Listings__c>();
    try{list_listing = [SELECT id, zz_close_listing__c, zz_create_listing_check__c, zz_listing_has_ended__c, zz_listing_needs_approval__c FROM Listings__c WHERE zz_listing_needs_approval__c = true OR zz_listing_has_ended__c = true];
    System.debug('');}catch(Exception e){System.debug('### -Listing_Reminder_Scheduled- getOpps: '+e);}
    for(Listings__c tmp: list_listing)
    {
      if(tmp.zz_listing_has_ended__c)
        tmp.zz_close_listing__c = true;
      if(tmp.zz_listing_needs_approval__c)
        tmp.zz_create_listing_check__c = true;
    }

    // DML statement
    Database.SaveResult[] srList = Database.update(list_listing, false);

    // Iterate through each returned result
    for (Database.SaveResult sr : srList) {
        if (sr.isSuccess()) {
            // Operation was successful, so get the ID of the record that was processed
            System.debug('### Successfully updated listing. Listing ID: ' + sr.getId());
        }
        else {
            // Operation failed, so get all errors
            for(Database.Error err : sr.getErrors()) {
                System.debug('### The following error has occurred.');
                System.debug(err.getStatusCode() + ': ' + err.getMessage());
                System.debug('### Listing fields that affected this error: ' + err.getFields());
            }
        }
    }
  }
}