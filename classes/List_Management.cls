/**************************************************************************
Company: aquilliance GmbH
Author: Andreas Wolf
CreatedDate: 2017-1

**************************************************************************/
public with sharing class List_Management {

	public User u {get;set;}
	public List<List__c> list_list {get;set;}
	public List<List__c> list_folders {get;set;}
	public Map<String, List__c> map_folders {get;set;}
	public Map<String, String> map_lists {get;set;}

	// collapsed area
	public Boolean col_folders {get;set;}
	public Boolean col_search {get;set;}
	public Boolean col_member {get;set;}

	// Account Team Info (Accounts where the current user is a member of the team)
	public Map<String, String> map_acc_team {get;set;}

	// List RecordTypes
	public String standardRT {get;set;}
	public String internRT {get;set;}
	public String externRT {get;set;}
	public String privateRT {get;set;}

	// prepared Lists for VF View
	public Map<String, List<List__c>> intern_Lists {get;set;}
	public Map<String, List<List__c>> extern_Lists {get;set;}
	public Map<String, List<List__c>> private_Lists {get;set;}

	public Map<String, String> folder_names {get;set;}

	// active selected List
	public String active_list {get;set;}

	// List Member of active List
	public List<List_Member__c> list_member {get;set;}
	public Integer member_list_size {get;set;}
	public Map<String, String> map_member {get;set;}
	public Set<String> set_member {get;set;}
	public List<wrapper_Member> list_wm {get;set;}
	public Boolean allchecked2 {get;set;}
	public Boolean deleteStep {get;set;}

	// Search Vars
	public String search_firstname {get;set;}
	public String search_lastname {get;set;}
	public String search_account {get;set;}
	public List<String> search_branch {get;set;}
	public String search_function {get;set;}
	public String search_department {get;set;}

	public List<SelectOption> listAccountBranches {get;set;}

	// Searched Contacts List
	public List<Contact> searched_con_list {get;set;}
	public Integer searched_con_list_size {get;set;}
	public List<wrapper_Contact> list_wc {get;set;}
	public String changed_row_nr {get;set;}
	public Boolean allchecked {get;set;}

	// Add new list
	public Boolean addNewList {get;set;}
	public String newObject {get;set;} // shows wether its about a list or folder
	public Boolean createNew_step2 {get;set;}

	// New List Details
	public String list_name {get;set;}
	public String list_folder {get;set;}
	public String list_description {get;set;}
	public String list_type {get;set;}
	public List__c newList {get;set;}
	//New Folder Addition
	public List__c new_folder {get;set;}
	// Errors
	public String err_list_name {get;set;}
	public String err_list_folder {get;set;}
	public String err_folder_account {get;set;}
	public String err_folder_mandant {get;set;}

	// delete List
	public Boolean deleteList {get;set;}
	public String list_id_for_deletion {get;set;}

	public String theSelectedArea {get;set;}

	// defines wether member should be shown or not in search area
	public Boolean showMember {get;set;}

	// Add Member to other List
	public Boolean show_AddMemberToOtherList {get;set;}
	public List_Member__c newListMember {get;set;}
	public String err_selected_list {get;set;}
	public String addMember_selectedList {get;set;}

	// Button disable if no member picked
	public Integer selected_member_number {get;set;}
	// Button disable if no contact picked
	public Integer selected_contacts_number {get;set;}

	// Edit Folder
	public Boolean folderEditMode {get;set;}
	public String activeFolderId {get;set;}
	public List__c activeFolder {get;set;}
	public String activeFolderType {get;set;}

	public List_Management() {
		run();
		System.debug('### List_Management controller start!');
	}

	public void run(){
		System.debug('### run called!');
		init();

		try{u = [SELECT id, FirstName, LastName, Email, Phone, Profile.Name, Zz_openFoldersListManagement__c FROM User WHERE id =: UserInfo.getUserId() LIMIT 1];
		System.debug('');}catch(Exception e){System.debug('### -List_Management- getUserInfo: '+e);}

		getRecordTypeIds();
		getRelevantAccountTeams();
		getAllLists();
		prepareListsForVF();
	}

	public void init(){
		System.debug('### init called!');
		u = new User();
		list_list = new List<List__c>();
		standardRT = '';
		internRT = '';
		externRT = '';
		privateRT = '';
		folder_names = new Map<String, String>();
		intern_Lists = new Map<String, List<List__c>>();
		extern_Lists = new Map<String, List<List__c>>();
		private_Lists = new Map<String, List<List__c>>();
		map_folders = new Map<String, List__c>();
		list_folders = new List<List__c>();
		map_acc_team = new Map<String, String>();
		active_list = '';
		list_member = new List<List_Member__c>();
		search_firstname = '';
		search_lastname = '';
		search_account = '';
		search_branch = new List<String>();
		search_function = '';
		search_department = '';
		searched_con_list = new List<Contact>();
		searched_con_list_size = 0;
		addNewList = false;
		newObject = '';
		createNew_step2 = false;
		list_folder = '';
		list_name = '';
		list_description = '';
		err_list_folder = '';
		err_list_name = '';
		list_type = 'intern';
		new_folder = new List__c();
		err_folder_account = '';
		err_folder_mandant = '';
		col_folders = false;
		col_search = false;
		col_member = false;
		deleteList = false;
		list_id_for_deletion = '';
		map_lists = new Map<String, String>();
		map_member = new Map<String, String>();
		set_member = new Set<String>();
		list_wc = new List<wrapper_Contact>();
		allchecked = false;
		list_wm = new List<wrapper_Member>();
		member_list_size = 0;
		deleteStep = false;
		allchecked2 = false;
		theSelectedArea = '';
		showMember = true;
		show_AddMemberToOtherList = false;
		addMember_selectedList = '';
		selected_member_number = 0;
		selected_contacts_number = 0;
		limitSize = 50;
		totalPages = 0;
		currentPage = 0;
		selected_limit = '50';
		limitSize2 = 50;
		totalPages2 = 0;
		currentPage2 = 0;
		selected_limit2 = '50';
		folderEditMode = false;
		activeFolder = new List__c();
		listAccountBranches = prepareBranchSelectOptions();
		newList = new List__c();
		newListMember = new List_Member__c();
		err_selected_list = '';

		// receive Contact Info for initial search

		search_firstname = Apexpages.currentPage().getParameters().get('first_name');
		search_lastname = Apexpages.currentPage().getParameters().get('last_name');
		search_account = Apexpages.currentPage().getParameters().get('acc_name');
		search_function = Apexpages.currentPage().getParameters().get('func');
		search_department = Apexpages.currentPage().getParameters().get('depa');
		String stringBranches = '';
		stringBranches = Apexpages.currentPage().getParameters().get('acc_branch');
		if(stringBranches != null)
		{
			if(stringBranches.length()>0 && stringBranches.contains(';'))
			{
				for(String tmp:stringBranches.split(';'))
				{
					search_branch.add(tmp);
				}
			}else if(stringBranches.length()>0)
			{
				search_branch.add(stringBranches);
			}
		}



		String start_search = Apexpages.currentPage().getParameters().get('search');
		if(start_search == 'true')
		{
			search();
		}

		String activeListId = Apexpages.currentPage().getParameters().get('activeListId');
		if(activeListId != '' && activeListId != null)
		{
			setActiveList();
		}
	}

	public void addNewList_toggle(){
		System.debug('### addNewList_toggle called!');
		if(addNewList)
		{
			addNewList = false;
			createNew_step2 = false;
			newObject = '';
			list_folder = '';
			list_name = '';
			list_description = '';
			err_list_folder = '';
			err_list_name = '';
			err_folder_account = '';
			err_folder_mandant = '';
			list_type = 'intern';
		}else{
			addNewList = true;
			newList = new List__c();
		}
	}

	public void createNewListOrFolder(){
		System.debug('### createNewListOrFolder called!');
		createNew_step2 = true;
	}

	public void back(){
		System.debug('### back called!');
		createNew_step2 = false;
		newObject = '';
		list_folder = '';
		list_name = '';
		list_description = '';
		err_list_folder = '';
		err_list_name = '';
		err_folder_account = '';
		err_folder_mandant = '';
		list_type = 'intern';
	}

	public void createNewList(){
		System.debug('### createNewList called!');
		if(newObject == 'list')
		{
			//Validate
			if(newList.Name != '' && newList.Name != null)
			{
				err_list_name = '';
			}else{
				err_list_name = 'Please insert a name first.';
			}

			if(newList.Folder__c != null)
			{
				err_list_folder = '';
			}else{
				err_list_folder = 'Please choose a folder first.';
			}

			if(err_list_folder == '' && err_list_name == '')
			{
				// set Standard List RecordType
				newList.RecordTypeId = standardRT;

				try{
					insert newList;
					addNewList = false;
					createNew_step2 = false;
					newObject = '';
					run();
				System.debug('');}catch(Exception e){System.debug('### -list_management- insertNewList: '+e);}
			}
		}else{
			//Validate
			if(list_name == '')
			{
				err_list_name = 'Please insert a name first.';
			}else{
				err_list_name = '';
			}
			if(new_folder.Mandant__c == null && list_type == 'intern')
			{
				err_folder_mandant = 'Please choose a mandant first.';
			}else{
				err_folder_mandant = '';
			}
			if(new_folder.Account__c == null && list_type == 'extern')
			{
				err_folder_account = 'Please choose an account first.';
			}else{
				err_folder_account = '';
			}

			if(err_folder_account == '' && err_folder_mandant == '' && err_list_name == '')
			{
				// create new Folder
				new_folder.Name = list_name;
				new_folder.Description__c = list_description;
				if(list_type == 'intern'){
					new_folder.RecordTypeId = internRT;
				}
				else if(list_type == 'extern'){
					new_folder.RecordTypeId = externRT;
				}
				else{
					new_folder.RecordTypeId = privateRT;
				}
				try{
					insert new_folder;
					run();
				System.debug('');}catch(Exception e){System.debug('### -list_management- insertNewFolder: '+e);}
			}
		}
	}

	public void editFolder()
	{
		System.debug('### editFolder called!');
		System.debug('###-editFolder-map_folders.get(activeFolderId): '+map_folders.get(activeFolderId));

		List__c tmp = map_folders.get(activeFolderId);
		activeFolder.Id = tmp.Id;
		activeFolder.Name = tmp.Name;
		activeFolder.Account__c = tmp.Account__c;
		activeFolder.Mandant__c = tmp.Mandant__c;
		activeFolder.Description__c = tmp.Description__c;
		// activeFolder = map_folders.get(activeFolderId);
		System.debug('###-editFolder-activeFolder2: '+activeFolder);

		setActiveFolderType();
		folderEditMode = true;
	}

	public void closeEditActiveFolder()
	{
		System.debug('### Edit Folder Closed!');
		err_list_name = '';
		err_folder_account = '';
		err_folder_mandant = '';
		activeFolderType = '';
		folderEditMode = false;
		activeFolder = new List__c();
	}

	public void setActiveFolderType()
	{
		System.debug('### setActiveFolderType called!');
		if(activeFolder.Account__c != null)
			activeFolderType = 'extern';
		else if(activeFolder.Mandant__c != null)
			activeFolderType = 'intern';
		else
			activeFolderType = 'private';
	}

	public void updateActiveFolder()
	{
		System.debug('### activeFolderType: '+activeFolderType);
		System.debug('### activeFolder: '+activeFolder);
		err_list_name = '';
		err_folder_account = '';
		err_folder_mandant = '';

		if(activeFolder.Name == null)
			err_list_name = 'Please insert a name first.';
		else if(activeFolder.Account__c == null && activeFolderType == 'extern')
			err_folder_account = 'Please choose an account.';
		else if(activeFolder.Mandant__c == null && activeFolderType == 'intern')
			err_folder_mandant = 'Please choose a mandant.';
		else{
			Boolean updateSucceeded = false;
			try{
				if(activeFolderType == 'intern')
				{
					activeFolder.Account__c = null;
					activeFolder.RecordTypeId = internRT;
				}
				if(activeFolderType == 'extern')
				{
					activeFolder.Mandant__c = null;
					activeFolder.RecordTypeId = externRT;
				}
				if(activeFolderType == 'private')
				{
					activeFolder.Account__c = null;
					activeFolder.Mandant__c = null;
					activeFolder.RecordTypeId = privateRT;
				}
				update activeFolder;
				updateSucceeded = true;
			System.debug('');}catch(Exception e){
				System.debug('### -List_Management- updateFolder: '+e);
			}

			if(updateSucceeded)
			{
				run();
				// activeFolderType = '';
				// folderEditMode = false;
				// map_folders.put(activeFolder.Id, activeFolder);
				// activeFolder = new List__c();
			}
		}
	}

	public void getRecordTypeIds(){
		System.debug('### getRecordTypeIds called!');
		try{standardRT = [SELECT id FROM RecordType WHERE sObjectType = 'List__c' AND DeveloperName = 'Standard' LIMIT 1].Id;
		System.debug('');}catch(Exception e){System.debug('### -List_Management- getStandardListRTid: '+e);}
		try{internRT = [SELECT id FROM RecordType WHERE sObjectType = 'List__c' AND DeveloperName = 'Intern' LIMIT 1].Id;
		System.debug('');}catch(Exception e){System.debug('### -List_Management- getInternListRTid: '+e);}
		try{externRT = [SELECT id FROM RecordType WHERE sObjectType = 'List__c' AND DeveloperName = 'Extern' LIMIT 1].Id;
		System.debug('');}catch(Exception e){System.debug('### -List_Management- getExternListRTid: '+e);}
		try{privateRT = [SELECT id FROM RecordType WHERE sObjectType = 'List__c' AND DeveloperName = 'Private' LIMIT 1].Id;
		System.debug('');}catch(Exception e){System.debug('### -List_Management- getPrivateListRTid: '+e);}
	}

	public List<SelectOption> prepareBranchSelectOptions()
	{
		List<SelectOption> options = new List<SelectOption>();

		Schema.DescribeFieldResult fieldResult = Account.Branche__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

		for( Schema.PicklistEntry f : ple)
		{
			options.add(new SelectOption(f.getLabel(), f.getValue()));
		}
		options.sort();
		return options;
	}

	public void search(){
		System.debug('### search called!');
        // reset vars
				list_wc = new List<wrapper_Contact>();
				searched_con_list = new List<Contact>();
				selected_contacts_number = 0;
				allchecked = false;
        /*********************************
                    build query
        *********************************/
        String qString = '';
        qString  = 'SELECT Id, FirstName, LastName, Email, Phone, Account.Name, AccountId, Department ';
        qString += 'FROM Contact ';
        qString += 'WHERE Id !=\'\' ';

        if(search_account!=null && search_account!='')
        {
            qString+= ' AND Account.Name like \'%' + String.escapeSingleQuotes(search_account).replace(' ','%') + '%\' ';
        }
        if(search_firstname!=null && search_firstname!='')
        {
            qString+= ' AND FirstName like \'%' + String.escapeSingleQuotes(search_firstname).replace(' ','%') + '%\' ';
        }
				if(search_lastname!=null && search_lastname!='')
        {
            qString+= ' AND LastName like \'%' + String.escapeSingleQuotes(search_lastname).replace(' ','%') + '%\' ';
        }
				if(search_branch.size()>0)
				{
					Boolean first = true;
					for(String tmp:search_branch)
					{
						if(first)
						{
							qString+= ' AND (Account.zz_Branche__c like \'%' + String.escapeSingleQuotes(tmp).replace(' ','%') + '%\' ';
							first = false;
						}else{
							qString+= ' OR Account.zz_Branche__c like \'%' + String.escapeSingleQuotes(tmp).replace(' ','%') + '%\' ';
						}
					}
					qString+= ')';
				}
				if(search_function!=null && search_function!='')
        {
            qString+= ' AND Title like \'%' + String.escapeSingleQuotes(search_function).replace(' ','%') + '%\' ';
        }
				if(search_department!=null && search_department!='')
        {
            qString+= ' AND Department like \'%' + String.escapeSingleQuotes(search_department).replace(' ','%') + '%\' ';
        }

				// Sortieren nach Nachname
				qString += ' ORDER BY LastName ASC';


        // limit to 1000
        qString += ' LIMIT 500';
        system.debug('### qString:' +qString);
        /*********************************
                    call query
        *********************************/
        searched_con_list = database.query(qString);
        system.debug('####### searched_con_list:' +searched_con_list);
				searched_con_list_size = searched_con_list.size();
				for(Contact tmp:searched_con_list)
				{
					wrapper_Contact new_wc = new wrapper_Contact();
					new_wc.c = tmp;
					if(set_member.contains(tmp.Id)){
						new_wc.member = true;
					}else{
						new_wc.member = false;
					}
					new_wc.isChecked = false;
					list_wc.add(new_wc);
				}
				preparePagination();
    }

	public void setActiveList()
	{
		System.debug('### setActiveList called!');
		active_list = Apexpages.currentPage().getParameters().get('activeListId');
		selected_member_number = 0;
		allchecked2 = false;
		getAllListMembers();
		resetSearch();
	}

	public void resetSearch()
	{
		System.debug('### resetSearch called!');
		list_wc = new List<wrapper_Contact>();
		searched_con_list_size = 0;
		preparePagination();
	}

	public void getAllListMembers()
	{
		System.debug('### getAllListMembers called!');
		map_member = new Map<String, String>();
		set_member = new Set<String>();
		list_wm = new List<wrapper_Member>();
		//system.debug('### active_list:' +active_list);
		if(active_list != '')
		{
			try{list_member = [SELECT id, Name, Contact__c, Contact__r.FirstName,
				Contact__r.LastName, Contact__r.Email, Contact__r.Phone,
				Contact__r.Account.Name, Contact__r.AccountId, Contact__r.MailingCity, Contact__r.Department
				FROM List_Member__c WHERE List__c =: active_list]; member_list_size = list_member.size();
			System.debug('');}catch(Exception e){System.debug('### -List_Management- getMembersFromActiveList: '+e);}
		}
		for(List_Member__c tmp:list_member)
		{
			map_member.put(tmp.Id, tmp.Contact__r.FirstName+' '+tmp.Contact__r.LastName);
			set_member.add(tmp.Contact__c);
		}
		system.debug('### list_member:' +list_member);
		list_wc = new List<wrapper_Contact>();
		for(Contact tmp:searched_con_list)
		{
			wrapper_Contact new_wc = new wrapper_Contact();
			new_wc.c = tmp;
			if(set_member.contains(tmp.Id)){
				new_wc.member = true;
			}else{
				new_wc.member = false;
			}
			new_wc.isChecked = false;
			list_wc.add(new_wc);
		}
		for(List_Member__c tmp : list_member)
		{
			wrapper_Member new_wm = new wrapper_Member();
			new_wm.isChecked = false;
			new_wm.lm = tmp;

			list_wm.add(new_wm);
		}
		preparePagination2();
	}

	public void getRelevantAccountTeams(){
		System.debug('### getRelevantAccountTeams called!');
		List<Team_Member__c> list_tm = new List<Team_Member__c>();
		try{list_tm = [SELECT id, Name, Account__c FROM Team_Member__c WHERE User__c =: u.Id AND RecordType.DeveloperName = 'Customer_Team'];
		System.debug('');}catch(Exception e){System.debug('### -List_Management- getAccTeams: '+e);}
		for(Team_Member__c tmp : list_tm)
		{
			if(!map_acc_team.containsKey(tmp.Account__c))
			{
				map_acc_team.put(tmp.Account__c, tmp.Account__c);
			}
		}
	}

	public void getAllLists(){
		System.debug('### getAllLists called!');
		// Get all Lists
		try{list_list = [SELECT id, Name, RecordTypeId, Folder__r.RecordTypeId, Folder__r.Name,
			OwnerId, Description__c
			FROM List__c
			//WHERE RecordTypeId =: standardRT
			ORDER BY Folder__r.Name ASC];
		System.debug('');}catch(Exception e){System.debug('### -List_Management- getLists: '+e);}
		// Get All Folders for which the current user has access
		try{list_folders = [SELECT id, Name, RecordTypeId, Account__c, Account__r.Name, Mandant__c, Mandant__r.Name, Description__c, OwnerId
			FROM List__c
			WHERE RecordTypeId =: internRT
			OR (RecordTypeId =: externRT AND Account__c IN: map_acc_team.keyset())
			OR (RecordTypeId =: privateRT AND OwnerId =: u.Id)
			ORDER By Name ASC];
		System.debug('');}catch(Exception e){System.debug('### -List_Management- getFolders: '+e);}
		System.debug('### list_folders: '+list_folders);
		// prepare Folder Map
		for(List__c tmp : list_folders)
		{
			if(!map_folders.containsKey(tmp.Id))
			{
				map_folders.put(tmp.Id, tmp);
			}
		}
	}

	public void prepareListsForVF(){
		System.debug('### -prepareListsForVF- active_list: '+active_list);
		for(List__c tmp : list_list)
		{
			// if LookUp to Folder isnt empty && its a standard list (not a folder)
			if(tmp.Folder__c != null && tmp.RecordTypeId == standardRT)
			{
				// if user has access
				if(map_folders.containsKey(tmp.Folder__c))
				{
					map_lists.put(tmp.Id, tmp.Name);

					// INTERN
					if(tmp.Folder__r.RecordTypeId == internRT)
					{
						if(!intern_Lists.containskey(tmp.Folder__c))
						{
							List<List__c> tmp_list = new List<List__c>();
							tmp_list.add(tmp);
							intern_Lists.put(tmp.Folder__c, tmp_list);
						}else{
							List<List__c> tmp_list = intern_Lists.get(tmp.Folder__c);
							tmp_list.add(tmp);
							tmp_list.sort();
							intern_Lists.put(tmp.Folder__c, tmp_list);
						}
					}
					// EXTERN
					if(tmp.Folder__r.RecordTypeId == externRT)
					{
						if(!extern_Lists.containskey(tmp.Folder__c))
						{
							List<List__c> tmp_list = new List<List__c>();
							tmp_list.add(tmp);
							extern_Lists.put(tmp.Folder__c, tmp_list);
						}else{
							List<List__c> tmp_list = extern_Lists.get(tmp.Folder__c);
							tmp_list.add(tmp);
							tmp_list.sort();
							extern_Lists.put(tmp.Folder__c, tmp_list);
						}
					}
					// PRIVATE
					if(tmp.Folder__r.RecordTypeId == privateRT)
					{
						if(!private_Lists.containskey(tmp.Folder__c))
						{
							List<List__c> tmp_list = new List<List__c>();
							tmp_list.add(tmp);
							private_Lists.put(tmp.Folder__c, tmp_list);
						}else{
							List<List__c> tmp_list = private_Lists.get(tmp.Folder__c);
							tmp_list.add(tmp);
							tmp_list.sort();
							private_Lists.put(tmp.Folder__c, tmp_list);
						}
					}
				}
			}
			// Make empty Folder visible
			else
			{
				// if user has access
				if(map_folders.containsKey(tmp.Id))
				{
					// INTERN
					if(tmp.RecordTypeId == internRT)
					{
						if(!intern_Lists.containskey(tmp.Id))
						{
							List<List__c> tmp_list = new List<List__c>();
							intern_Lists.put(tmp.Id, tmp_list);
						}
					}
					// EXTERN
					if(tmp.RecordTypeId == externRT)
					{
						if(!extern_Lists.containskey(tmp.Id))
						{
							List<List__c> tmp_list = new List<List__c>();
							extern_Lists.put(tmp.Id, tmp_list);
						}
					}
					// PRIVATE
					if(tmp.RecordTypeId == privateRT)
					{
						if(!private_Lists.containskey(tmp.Id))
						{
							List<List__c> tmp_list = new List<List__c>();
							private_Lists.put(tmp.Id, tmp_list);
						}
					}
				}
			}
		}
	}

	public void set_collapsible_area()
	{
		if(!Test.isRunningTest())
			theSelectedArea = Apexpages.currentPage().getParameters().get('selected_area');

		if(theSelectedArea == 'folders')
		{
			if(col_folders){
				col_folders = false;
			}else{
				col_folders = true;
			}
		}
		if(theSelectedArea == 'search')
		{
			if(col_search){
				col_search = false;
			}else{
				col_search = true;
			}
		}
		if(theSelectedArea == 'member')
		{
			if(col_member){
				col_member = false;
			}else{
				col_member = true;
			}
		}
	}

	public void deleteList_toggle(){
		if(!Test.isRunningTest())
			list_id_for_deletion = Apexpages.currentPage().getParameters().get('list_id');

		if(deleteList)
		{
			deleteList = false;
		}else{
			deleteList = true;
		}
	}

	public void deleteList(){
		List__c delete_list = new List__c();
		delete_List.Id = list_id_for_deletion;
		try{delete delete_List; run();
		System.debug('');}catch(Exception e){System.debug('### -List_Management- deleteList: '+e);}
	}

	public void askForDeletion(){
			if(deleteStep)
			{
				deleteStep = false;
			}else{
				deleteStep = true;
			}
	}

	public void removeFromList(){
		if(deleteStep)
		{
			List<List_Member__c> delete_list_lm = new List<List_Member__c>();
			for(wrapper_Member tmp:list_wm)
			{
				if(tmp.isChecked)
				{
					List_Member__c del_lm = new List_Member__c();
					del_lm.Id = tmp.lm.Id;
					delete_list_lm.add(del_lm);
				}
			}
			try{
				delete delete_list_lm;
				getAllListMembers();
				deleteStep = false;
				allchecked2 = false;
				selected_member_number = 0;
			System.debug('');}catch(Exception e){System.debug('### -List_Management- delete_member_list: '+e);}
		}
	}

	public void change(){
		System.debug('### list_type: '+list_type);
		System.debug('### activeFolderType: '+activeFolderType);
	}

	public void rerender_contacts(){
		System.debug('### list_wc: '+list_wc);
		list_wc[integer.valueOf(changed_row_nr)].isChecked = true;
	}

	public void check_all(){
		selected_contacts_number = 0;
		//System.debug('### allchecked: '+allchecked);
		if(allchecked){
			allchecked = false;
		}else{
			allchecked = true;
		}
		for(wrapper_Contact tmp:list_wc)
		{
			//System.debug('### tmp.isChecked: '+tmp.isChecked);
			if(!tmp.member)
			{
				tmp.isChecked = allchecked;
				if(allchecked){
					selected_contacts_number += 1;
				}
			}
			//System.debug('### tmp.isChecked: '+tmp.isChecked);
		}
	}

	public void check_one(){
		//System.debug('### its changed_row_nr: '+changed_row_nr);
		selected_contacts_number = 0;
		if(changed_row_nr != '')
		{
			if(list_wc[Integer.valueOf(changed_row_nr)].isChecked)
			{
				list_wc[Integer.valueOf(changed_row_nr)].isChecked = false;
				allchecked = false;
				//System.debug('### its false!');
			}else{
				list_wc[Integer.valueOf(changed_row_nr)].isChecked = true;
				selected_contacts_number += 1;
				//System.debug('### its true!');
			}
		}
	}

	public void check_all2(){
		selected_member_number = 0;
		//System.debug('### allchecked: '+allchecked);
		if(allchecked2){
			allchecked2 = false;
		}else{
			allchecked2 = true;
		}
		for(wrapper_Member tmp:list_wm)
		{
			tmp.isChecked = allchecked2;
			if(allchecked2){
				selected_member_number += 1;
			}
		}
	}

	public void check_one2(){
		//System.debug('### its changed_row_nr: '+changed_row_nr);
		selected_member_number = 0;
		if(changed_row_nr != '')
		{
			if(list_wm[Integer.valueOf(changed_row_nr)].isChecked)
			{
				list_wm[Integer.valueOf(changed_row_nr)].isChecked = false;
				allchecked2 = false;
				//System.debug('### its false!');
			}else{
				list_wm[Integer.valueOf(changed_row_nr)].isChecked = true;
				//System.debug('### its true!');
				selected_member_number += 1;
			}
		}
	}

	public void addToList(){
		List<List_Member__c> insert_list_lm = new List<List_Member__c>();
		for(wrapper_Contact tmp : list_wc)
		{
			//System.debug('### checked: '+tmp.isChecked);
			if(tmp.isChecked){
				List_Member__c new_lm = new List_Member__c();
				new_lm.Contact__c = tmp.c.Id;
				new_lm.List__c = active_list;

				insert_list_lm.add(new_lm);
			}
		}

		try{
			insert insert_list_lm;
			getAllListMembers();
			allchecked = false;
			search();
		System.debug('');}catch(Exception e){System.debug('### -List_Management- insert_new_member: '+e);}
		preparePagination();
	}

	public void showMember_toggle(){
		if(showMember){
			showMember = false;
		}else{
			showMember = true;
		}
	}

	public void addToOtherList_toggle(){
		if(show_AddMemberToOtherList){
			show_AddMemberToOtherList = false;
		}else{
			show_AddMemberToOtherList = true;
			newListMember = new List_Member__c();
			err_selected_list = '';
		}
	}

	public void addMemberToOtherList(){
		System.debug('### addMemberToOtherList called!');
		System.debug('### newListMember.List__c: '+newListMember.List__c);
		if(newListMember.List__c != null)
		{
			err_selected_list = '';
		}else{
			err_selected_list = 'Please choose a list first.';
		}

		if(err_selected_list == '')
		{
			// get Member of selected List
			List<List_Member__c> existing_list_lm = new List<List_Member__c>();
			try{existing_list_lm = [SELECT id, Contact__c
				FROM List_Member__c WHERE List__c =: newListMember.List__c];
			System.debug('');}catch(Exception e){System.debug('### -List_Management- getMembersFromSelectedList: '+e);}
			Set<String> existing_member_ids = new Set<String>();
			for(List_Member__c tmp : existing_list_lm)
			{
				existing_member_ids.add(tmp.Contact__c);
			}

			List<List_Member__c> insert_list_lm = new List<List_Member__c>();
			for(wrapper_Member tmp : list_wm)
			{
				//System.debug('### checked: '+tmp.isChecked);
				if(tmp.isChecked && !existing_member_ids.contains(tmp.lm.Contact__c)){
					List_Member__c new_lm = new List_Member__c();
					new_lm.Contact__c = tmp.lm.Contact__c;
					new_lm.List__c = newListMember.List__c;

					insert_list_lm.add(new_lm);
				}
			}

			try{
				if(insert_list_lm.size()>0)
					insert insert_list_lm;
				allchecked2 = true;
				check_all2();
				selected_member_number = 0;
				show_AddMemberToOtherList = false;
			System.debug('');}catch(Exception e){System.debug('### -List_Management- insert_new_member_from_existing: '+e);}
		}
	}

	public class wrapper_Contact
	{
		public Contact c {get;set;}
		public Boolean member {get;set;}
		public Boolean isChecked {get;set;}
	}

	public class wrapper_Member
	{
		public List_Member__c lm {get;set;}
		public Boolean isChecked {get;set;}
	}


		// Pagination for CONTACT SEARCH
		//List to store all the contacts according to requirement
    //public List AllContacts = new list();

    //List to show the limited records on the page
    public List<wrapper_Contact> ContactsToShow {get;set;}

    //Navigation variables
    Integer counter = 0;								//TO track the number of records parsed
    public Integer limitSize {get;set;}		//Number of records to be displayed
    Integer totalSize =0; 						//To Store the total number of records available
		public Decimal totalPages {get;set;}
		public Decimal currentPage {get;set;}
		public String selected_limit {get;set;}

    //Constructor
    public void preparePagination(){

				totalPages = 0;
        ContactsToShow = new list<wrapper_Contact>();
				totalSize = searched_con_list_size;
				Decimal getTotalPages = searched_con_list_size / (Decimal) limitSize;
				totalPages = getTotalPages.round(System.RoundingMode.CEILING);
				currentPage = 1;

        //Intial adding of contacts to ContactsToShow
        //check the total records are more than limitSize and assign the records
        if((counter+limitSize) <= totalSize){
            for(Integer i=0;i<limitSize;i++){
                ContactsToShow.add(list_wc.get(i));
            }
        }else{
            for(Integer i=0;i<totalSize;i++){
                ContactsToShow.add(list_wc.get(i));
            }
        }
    }

		public List<SelectOption> getPagination_limit_options()
		{
					List<SelectOption> pagination_limit_options = new List<SelectOption>();
					pagination_limit_options.add(new SelectOption('10', '10'));
					pagination_limit_options.add(new SelectOption('25', '25'));
					pagination_limit_options.add(new SelectOption('50', '50'));
					pagination_limit_options.add(new SelectOption('100', '100'));
					return pagination_limit_options;
		}

		public void limit_change()
		{
			limitSize = Integer.valueOf(selected_limit);
			preparePagination();
		}

    //Navigation methods
	public void beginning(){
        ContactsToShow.clear();
        counter=0;
				currentPage = 1;
        if((counter + limitSize) <= totalSize){

            for(Integer i=0;i<limitSize;i++){
                ContactsToShow.add(list_wc.get(i));
            }

        } else{

            for(Integer i=0;i<totalSize;i++){
                ContactsToShow.add(list_wc.get(i));
            }
        }
    }

    public void next(){
        ContactsToShow.clear();
        counter=counter+limitSize;

				Decimal getCurrentPage = counter / (Decimal) limitSize;
				currentPage = getCurrentPage.round(System.RoundingMode.CEILING)+1;

        if((counter+limitSize) <= totalSize){
            for(Integer i=counter;i<(counter+limitSize);i++){
                ContactsToShow.add(list_wc.get(i));
            }
        } else{
            for(Integer i=counter;i<totalSize;i++){
                ContactsToShow.add(list_wc.get(i));
            }
        }
    }

    public void previous(){
        ContactsToShow.clear();
        counter=counter-limitSize;

				Decimal getCurrentPage = counter / (Decimal) limitSize;
				currentPage = getCurrentPage.round(System.RoundingMode.CEILING)+1;

        for(Integer i=counter;i<(counter+limitSize); i++){
            ContactsToShow.add(list_wc.get(i));
        }
    }

    public void last (){
        ContactsToShow.clear();
				currentPage = totalPages;
        if(math.mod(totalSize , limitSize) == 0){
            counter = limitSize * ((totalSize/limitSize)-1);
        } else if (math.mod(totalSize , limitSize) != 0){
            counter = limitSize * ((totalSize/limitSize));
        }

        for(Integer i=counter-1;i<totalSize-1;i++){
                ContactsToShow.add(list_wc.get(i));
        }
    }

    public Boolean getDisableNext(){
        if((counter + limitSize) >= totalSize )
            return true ;
        else
            return false ;
    }

    public Boolean getDisablePrevious(){
        if(counter == 0)
            return true ;
        else
            return false ;
    }

		// Pagination for MEMBER TABLE
		//List to store all the contacts according to requirement
    //public List AllContacts = new list();

    //List to show the limited records on the page
    public List<wrapper_Member> MemberToShow {get;set;}

    //Navigation variables
    Integer counter2 = 0;								//TO track the number of records parsed
    public Integer limitSize2 {get;set;}		//Number of records to be displayed
    Integer totalSize2 =0; 						//To Store the total number of records available
		public Decimal totalPages2 {get;set;}
		public Decimal currentPage2 {get;set;}
		public String selected_limit2 {get;set;}

    //Constructor
    public void preparePagination2(){

				totalPages2 = 0;
        MemberToShow = new list<wrapper_Member>();
				totalSize2 = member_list_size;
				Decimal getTotalPages2 = member_list_size / (Decimal) limitSize2;
				totalPages2 = getTotalPages2.round(System.RoundingMode.CEILING);
				currentPage2 = 1;

        //Intial adding of contacts to MemberToShow
        //check the total records are more than limitSize and assign the records
        if((counter2+limitSize2) <= totalSize2){
            for(Integer i=0;i<limitSize2;i++){
                MemberToShow.add(list_wm.get(i));
            }
        }else{
            for(Integer i=0;i<totalSize2;i++){
                MemberToShow.add(list_wm.get(i));
            }
        }
    }

		public List<SelectOption> getPagination_limit_options2()
		{
					List<SelectOption> pagination_limit_options = new List<SelectOption>();
					pagination_limit_options.add(new SelectOption('10', '10'));
					pagination_limit_options.add(new SelectOption('25', '25'));
					pagination_limit_options.add(new SelectOption('50', '50'));
					pagination_limit_options.add(new SelectOption('100', '100'));
					return pagination_limit_options;
		}

		public void limit_change2()
		{
			limitSize2 = Integer.valueOf(selected_limit2);
			preparePagination2();
		}

    //Navigation methods
	public void beginning2(){
        MemberToShow.clear();
        counter2=0;
				currentPage2 = 1;
        if((counter2 + limitSize2) <= totalSize2){

            for(Integer i=0;i<limitSize2;i++){
                MemberToShow.add(list_wm.get(i));
            }

        } else{

            for(Integer i=0;i<totalSize2;i++){
                MemberToShow.add(list_wm.get(i));
            }
        }
    }

    public void next2(){
        MemberToShow.clear();
        counter2=counter2+limitSize2;

				Decimal getCurrentPage2 = counter2 / (Decimal) limitSize2;
				currentPage2 = getCurrentPage2.round(System.RoundingMode.CEILING)+1;

        if((counter2+limitSize2) <= totalSize2){
            for(Integer i=counter2;i<(counter2+limitSize2);i++){
                MemberToShow.add(list_wm.get(i));
            }
        } else{
            for(Integer i=counter2;i<totalSize2;i++){
                MemberToShow.add(list_wm.get(i));
            }
        }
    }

    public void previous2(){
        MemberToShow.clear();
        counter2=counter2-limitSize2;

				Decimal getCurrentPage2 = counter2 / (Decimal) limitSize2;
				currentPage2 = getCurrentPage2.round(System.RoundingMode.CEILING)+1;

        for(Integer i=counter2;i<(counter2+limitSize2); i++){
            MemberToShow.add(list_wm.get(i));
        }
    }

    public void last2(){
        MemberToShow.clear();
				currentPage2 = totalPages2;
        if(math.mod(totalSize2 , limitSize2) == 0){
            counter2 = limitSize2 * ((totalSize2/limitSize2)-1);
        } else if (math.mod(totalSize2 , limitSize2) != 0){
            counter2 = limitSize2 * ((totalSize2/limitSize2));
        }

        for(Integer i=counter2-1;i<totalSize2-1;i++){
                MemberToShow.add(list_wm.get(i));
        }
    }

    public Boolean getDisableNext2(){
        if((counter2 + limitSize2) >= totalSize2 )
            return true ;
        else
            return false ;
    }

    public Boolean getDisablePrevious2(){
        if(counter2 == 0)
            return true ;
        else
            return false ;
    }

		// Collapse Folders
		public void saveFolderCollapseStatusForActiveUser()
		{
			if(u.Zz_openFoldersListManagement__c != null)
			{
				if(u.Zz_openFoldersListManagement__c.contains(activeFolderId))
				{
					u.Zz_openFoldersListManagement__c = u.Zz_openFoldersListManagement__c.replace(activeFolderId, '');
				}
				else
				{
					u.Zz_openFoldersListManagement__c += ' '+activeFolderId;
				}
			}else{
				u.Zz_openFoldersListManagement__c = activeFolderId;
			}

			updateActiveUser();
		}

		public void updateActiveUser()
		{
			try{
				update u;
			System.debug('');}catch(Exception e)
			{
				System.debug('###-List_Management-updateActiveUser: '+e);
			}
		}
}