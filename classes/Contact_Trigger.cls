/**************************************************************************
Company: aquilliance GmbH
Author: Andre Mergen
CreatedDate: 2017-01

**************************************************************************/
public with sharing class Contact_Trigger
{

	/***************************************************************************************
	set contact status to name
	***************************************************************************************
	public void set_name_status(List<Contact> new_contacts, MAP<Id, Contact> old_map_contacts)
	{
		String string_to_replace = '';
		for(Contact tmp_con:new_contacts)
		{
			/******************************************
				reset Contact Lastname Status
			******************************************
			if(old_map_contacts != null && old_map_contacts.containskey(tmp_con.Id))
			{
				string_to_replace = old_map_contacts.get(tmp_con.Id).kontakt_status__c;
				tmp_con.Lastname = tmp_con.Lastname.replace('['+string_to_replace+']','');
				tmp_con.Lastname = tmp_con.Lastname.trim();
			}


			/******************************************
				set kontakt_status status
			******************************************
			if(tmp_con.kontakt_status__c == 'Inaktiv' || tmp_con.kontakt_status__c == 'Gesperrt' || tmp_con.kontakt_status__c == 'Gesperrt durch Firma')
			{
				tmp_con.Lastname =tmp_con.Lastname + ' ['+tmp_con.kontakt_status__c+']';
			}
		}
	}

	/***************************************************************************************
	set sperrgrund to name
	***************************************************************************************/
	public void set_name_sperrgrund(List<Contact> new_contacts, MAP<Id, Contact> old_map_contacts)
	{
		String string_to_replace = '';
		for(Contact tmp_con:new_contacts)
		{
			/******************************************
				reset Contact Lastname Status
			******************************************/
			if(old_map_contacts != null && old_map_contacts.containskey(tmp_con.Id))
			{
				string_to_replace = old_map_contacts.get(tmp_con.Id).sperrgrund__c;
				tmp_con.Lastname = tmp_con.Lastname.replace(' ['+string_to_replace+']','');
				tmp_con.Lastname = tmp_con.Lastname.trim();
			}

			/******************************************
				set sperrgrund status
			******************************************/

			// Save Old value, falls der Kontakt wieder entsperrt wird
			if(tmp_con.kontakt_status__c != 'Gesperrt' && tmp_con.kontakt_status__c != 'Gesperrt durch Firma')
			{
				tmp_con.zz_status_vor_sperrung__c = tmp_con.kontakt_status__c;
			}

			if(tmp_con.sperrgrund__c != null && tmp_con.sperrgrund__c != '')
			{
				tmp_con.Lastname = tmp_con.Lastname + ' ['+ tmp_con.sperrgrund__c +']';

				tmp_con.kontakt_status__c = 'Gesperrt';
			}
			else
			{
				tmp_con.kontakt_status__c = tmp_con.zz_status_vor_sperrung__c;
			}
		}
	}

	/***************************************************************************************
	set email opt out
	***************************************************************************************/
	public void set_email_opt_out(List<Contact> new_contacts, MAP<Id, Contact> old_map_contacts)
	{
		for(Contact tmp_con:new_contacts)
		{

			/******************************************tmp_con.kontakt_status__c=='Gesperrt durch Firma'
				setzt HasOptedOutOfEmail zurück auf gespeicherten wert aus "optout_status_vor_sperrung__c"
			******************************************/
			if(old_map_contacts != null && old_map_contacts.containskey(tmp_con.Id))
			{
				if(((old_map_contacts.get(tmp_con.Id).kontakt_status__c=='Inaktiv' || old_map_contacts.get(tmp_con.Id).kontakt_status__c=='Gesperrt' || old_map_contacts.get(tmp_con.Id).kontakt_status__c=='Gesperrt durch Firma') && (tmp_con.kontakt_status__c!='Inaktiv' || tmp_con.kontakt_status__c!='Gesperrt' || tmp_con.kontakt_status__c!='Gesperrt durch Firma'))||((old_map_contacts.get(tmp_con.Id).sperrgrund__c!=null && old_map_contacts.get(tmp_con.Id).sperrgrund__c!='') && (tmp_con.sperrgrund__c==null || tmp_con.sperrgrund__c=='')))
				{
					if(tmp_con.optout_status_vor_sperrung__c == 'gesperrt')
					{
						tmp_con.HasOptedOutOfEmail = true;
					}
					if(tmp_con.optout_status_vor_sperrung__c == 'nicht gesperrt')
					{
						tmp_con.HasOptedOutOfEmail = false;
					}
				}
			}



			/******************************************
			wenn Inaktiv, Gesperrt oder
			sperrgrund__c gesetzt, keine Emails !
			******************************************/
			if(tmp_con.kontakt_status__c=='Inaktiv' || tmp_con.kontakt_status__c=='Gesperrt' || tmp_con.kontakt_status__c=='Gesperrt durch Firma' || (tmp_con.sperrgrund__c!=null && tmp_con.sperrgrund__c!=''))
			{
				tmp_con.HasOptedOutOfEmail = true;
			}


			/******************************************
			optout_status_vor_sperrung__c setzten,
			solange nicht gesperrt
			******************************************/
			if(tmp_con.kontakt_status__c!='Inaktiv' && tmp_con.kontakt_status__c!='Gesperrt' &&  tmp_con.kontakt_status__c!='Gesperrt durch Firma' && (tmp_con.sperrgrund__c==null || tmp_con.sperrgrund__c==''))
			{
				if(tmp_con.HasOptedOutOfEmail == true)
				{
					tmp_con.optout_status_vor_sperrung__c = 'gesperrt';
				}
				else
				{
					tmp_con.optout_status_vor_sperrung__c = 'nicht gesperrt';
				}
			}
		}
	}

}