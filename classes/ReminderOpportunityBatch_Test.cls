/**************************************************************************
Company: aquilliance GmbH
Author: Andreas Wolf
CreatedDate: 2017-8
**************************************************************************/
@isTest
private class ReminderOpportunityBatch_Test {

	@isTest static void test_method_one() {
		Account acc = new Account();
		acc.Name 								= 'Test AQ';
		acc.BillingStreet 			= 'Högerdamm 41';
		acc.BillingCity 				= 'Hamburg';
		acc.BillingPostalCode 	= '20097';
		acc.BillingCountry 			= 'Germany';
		acc.ShippingStreet 			= 'Högerdamm 41';
		acc.ShippingCity 				= 'Hamburg';
		acc.ShippingPostalCode 	= '20097';
		acc.ShippingCountry 		= 'Germany';
		insert acc;

		Contact con = new Contact();
		con.AccountId = acc.Id;
		con.Firstname = 'Chuck';
		con.Lastname = 'Blue';
		con.Salutation = 'Mr.';
		con.MailingStreet = 'Blumen Str. 1';
		con.MailingCity = 'Hamburg';
		con.MailingPostalCode = '20097';
		con.MailingCountry = 'Germany';
		con.Birthdate = Date.today().addDays(3);
		insert con;

		Opportunity__c opp = new Opportunity__c();
		opp.Name = 'MainTestOpp';
		opp.account__c = acc.Id;
		opp.Stage__c = 'open';
		opp.honorarumsatz__c = 200000.00;
		opp.Opportunity_Art__c = 'Neukundengeschäft';
		opp.fremdkosten__c = 100;
		opp.Owner__c = UserInfo.getUserId();
		opp.projektstart__c = Date.Today();
		opp.projektende__c = Date.Today()+95;
		opp.zz_stage_date__c = Date.Today()-95;
		opp.zz_open_activities__c = 0;
		opp.Next_Step__c = Date.Today().addDays(-7);
		opp.schlusstermin__c = Date.Today().addDays(14);
		opp.leistungssegmente__c = 'Test1; Test2;';

		insert opp;

		ReminderOpportunityBatch b2 = new ReminderOpportunityBatch();
		database.executebatch(b2);
	}
}