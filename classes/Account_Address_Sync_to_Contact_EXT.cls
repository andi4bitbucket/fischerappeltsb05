/**************************************************************************
Company: aquilliance GmbH
Author: Andre Mergen
CreatedDate: 2016-12

**************************************************************************/
public with sharing class Account_Address_Sync_to_Contact_EXT {

		public Account the_Acc {get;set;}
		public List<wrapper_contact> list_wrapper_contacts {get;set;}
		public Boolean show_conf_pop {get;set;}

    /******************************************
									constructor
		******************************************/
    public Account_Address_Sync_to_Contact_EXT(ApexPages.StandardController stdController)
		{
			init();

      the_Acc = (Account)stdController.getRecord();

			select_acc();
			select_contacts();
    }

		/******************************************
					initialize/reset the vars
		******************************************/
		public void init()
		{
			the_Acc = new Account();
			list_wrapper_contacts = new List<wrapper_contact>();
			show_conf_pop = false;
		}


		/******************************************
					select the Account Fields
		******************************************/
		public void select_acc()
		{
			the_Acc = [SELECT Id,
			Name,
			BillingStreet,
			BillingCity,
			BillingPostalCode,
			BillingCountry,
			BillingState,
			ShippingStreet,
			ShippingCity,
			ShippingPostalCode,
			ShippingCountry,
			ShippingState
			FROM Account WHERE Id =: the_Acc.Id];
		}

		/******************************************
					select the Contacts
		******************************************/
		public void select_contacts()
		{
			List<Contact> list_contacts = new List<Contact>();
			wrapper_contact tmp_wc = new wrapper_contact();

			list_contacts = [SELECT Id,
			Firstname,
			Lastname,
			MailingStreet,
			MailingCity,
			MailingPostalCode,
			MailingCountry,
			MailingState FROM Contact WHERE AccountId =: the_Acc.Id];

			for(Contact tmp_con:list_contacts)
			{
				tmp_wc = new wrapper_contact();
				tmp_wc.selected = false;
				tmp_wc.address_equal = false;
				tmp_wc.contact = tmp_con;

				/**************************
				if address equal to Acc
				Address set Boolean
				**************************/
				if(tmp_wc.contact.MailingStreet == the_Acc.ShippingStreet && tmp_wc.contact.MailingCity == the_Acc.ShippingCity && tmp_wc.contact.MailingPostalCode == the_Acc.ShippingPostalCode && tmp_wc.contact.MailingCountry == the_Acc.ShippingCountry &&  tmp_wc.contact.MailingState == the_Acc.ShippingState)
				{
					tmp_wc.address_equal = true;
				}

				list_wrapper_contacts.add(tmp_wc);
			}
		}

		/******************************************
					button to select all contacts
		******************************************/
		public void btn_select_all()
		{
			for(wrapper_contact tmp_wc:list_wrapper_contacts)
			{
				tmp_wc.selected = true;
			}
		}

		/******************************************
					button to deselect all contacts
		******************************************/
		public void btn_deselect_all()
		{
			for(wrapper_contact tmp_wc:list_wrapper_contacts)
			{
				tmp_wc.selected = false;
			}
		}

		/******************************************
			button to sync all selected contacts
		******************************************/
		public void btn_sync_selected()
		{
			List<Contact> list_upd_contact = new List<Contact>();

			for(wrapper_contact tmp_wc:list_wrapper_contacts)
			{
				// if the contact is selected, copy the Account Address
				if(tmp_wc.selected == true)
				{
					tmp_wc.contact.MailingStreet 			= the_Acc.ShippingStreet;
					tmp_wc.contact.MailingCity 				= the_Acc.ShippingCity;
					tmp_wc.contact.MailingPostalCode 	= the_Acc.ShippingPostalCode;
					tmp_wc.contact.MailingCountry 		= the_Acc.ShippingCountry;
					tmp_wc.contact.MailingState 			= the_Acc.ShippingState;

					// add to update list
					list_upd_contact.add(tmp_wc.contact);
				}
			}

			// update the selected contacts
			if(list_upd_contact.size()>0)
			{
				update list_upd_contact;
			}

			show_conf_pop = false;
			list_wrapper_contacts = new List<wrapper_contact>();
			select_contacts();
		}

		/******************************************
					button to goback (disable popup)
		******************************************/
		public void btn_nix()
		{
			show_conf_pop = false;
		}

		/******************************************
					button to show popup
		******************************************/
		public void btn_show_conf_pop()
		{
			show_conf_pop = true;
		}

		/******************************************
			wrapper class
			verwendet für list_wrapper_contacts
		******************************************/
		public class wrapper_contact
		{
			public boolean selected {get;set;}
			public boolean address_equal {get;set;}
			public Contact contact {get;set;}
		}
}