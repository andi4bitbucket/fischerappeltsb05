public class Image_Drop_ApexController {
	@AuraEnabled
    public static Attachment getProfilePicture(Id parentId) {

        // Attachment permissions are set in parent object (Contact)
        //if (!Schema.sObjectType.Contact.isAccessible()) {
        //    throw new System.NoAccessException();
        //    return null;
        //}
        String attName = 'record_logo_'+parentId;

        return [SELECT Id, Name, LastModifiedDate, ContentType FROM Attachment
            WHERE parentid=:ParentId AND Name =: attName AND ContentType IN ('image/png', 'image/jpeg', 'image/gif')
            ORDER BY LastModifiedDate DESC LIMIT 1];
    }

    @AuraEnabled
    public static Id saveAttachment(Id parentId, String fileName, String base64Data, String contentType) {

        // Edit permission on parent object (Contact) is required to add attachments
        // if (!Schema.sObjectType.Contact.isUpdateable()) {
        //     throw new System.NoAccessException();
        //     return null;
        // }
        
        String attName = 'record_logo_'+parentId;
        List<Attachment> list_att = new List<Attachment>();
        list_att = [SELECT Id, Name, LastModifiedDate, ContentType FROM Attachment
            WHERE parentid=:ParentId AND Name =: attName AND ContentType IN ('image/png', 'image/jpeg', 'image/gif')];

        if(list_att.size()>0)
        {
            delete list_att;
        }

        Attachment attachment = new Attachment();
        attachment.parentId = parentId;
        attachment.body = EncodingUtil.base64Decode(base64Data);
        attachment.name = 'record_logo_'+parentId;
		attachment.contentType = contentType;
        insert attachment;
        return attachment.id;
    }
}