/**************************************************************************
Company: aquilliance GmbH
Author: Andreas Wolf
CreatedDate: 2017-1

**************************************************************************/
@isTest
private class Opportunity_Create_From_Ext_Test {

	@isTest static void test_method_one() {
		Account tmp_acc = new Account();
		tmp_acc.Name = 'Test';
		tmp_acc.account_status__c = 'Gesperrter Kunde';

		insert tmp_acc;

		Contact tmp_con = new Contact();
		tmp_con.AccountId = tmp_acc.Id;
		tmp_con.FirstName = 'Max';
		tmp_con.LastName = 'Mustermann';
		tmp_con.Salutation = 'Mr.';

		insert tmp_con;

		ApexPages.StandardController standard1 = new ApexPages.StandardController(tmp_acc);
		Opportunity_Create_From_Ext con = new Opportunity_Create_From_Ext(standard1);
		con.backTo();
		con.createOpportunity_Acc();

		ApexPages.StandardController standard2 = new ApexPages.StandardController(tmp_con);
		Opportunity_Create_From_Ext con2 = new Opportunity_Create_From_Ext(standard2);
		con2.backTo();
		con2.createOpportunity_Acc();
	}

	@isTest static void test_method_oneSUCCESS() {
		Account tmp_acc = new Account();
		tmp_acc.Name = 'Test';

		insert tmp_acc;

		Contact tmp_con = new Contact();
		tmp_con.AccountId = tmp_acc.Id;
		tmp_con.FirstName = 'Max';
		tmp_con.LastName = 'Mustermann';
		tmp_con.Salutation = 'Mr.';

		insert tmp_con;

		ApexPages.StandardController standard1 = new ApexPages.StandardController(tmp_acc);
		Opportunity_Create_From_Ext con = new Opportunity_Create_From_Ext(standard1);
		con.selected_rt = con.standardRT.Id;
		con.createOpportunity_Acc();

		ApexPages.StandardController standard2 = new ApexPages.StandardController(tmp_con);
		Opportunity_Create_From_Ext con2 = new Opportunity_Create_From_Ext(standard2);
		con2.selected_rt = con2.ausschreibungRT.Id;
		con2.createOpportunity_Acc();

		//ApexPages.StandardController standard3 = new ApexPages.StandardController(tmp_acc);
		//Opportunity_Create_From_Ext con3 = new Opportunity_Create_From_Ext(standard3);
		con.selected_rt = con.pitchRT.Id;
		con.createOpportunity_Acc();
	}
}