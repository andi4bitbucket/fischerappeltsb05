/**************************************************************************
Company: aquilliance GmbH
Author: Andreas Wolf
CreatedDate: 2017-1

**************************************************************************/
@isTest
private class account_status_check_Test {

	@isTest static void test_method_AKTIV() {
		Account tmp_Acc = new Account();
		tmp_Acc.Name 								= 'Test AQ';
		tmp_Acc.BillingStreet 			= 'Högerdamm 41';
		tmp_Acc.BillingCity 				= 'Hamburg';
		tmp_Acc.BillingPostalCode 	= '20097';
		tmp_Acc.BillingCountry 			= 'Germany';
		tmp_Acc.ShippingStreet 			= 'Högerdamm 41';
		tmp_Acc.ShippingCity 				= 'Hamburg';
		tmp_Acc.ShippingPostalCode 	= '20097';
		tmp_Acc.ShippingCountry 		= 'Germany';
		insert tmp_Acc;

		Contact tmp_Con_1 = new Contact();
		tmp_Con_1.AccountId = tmp_Acc.Id;
		tmp_Con_1.Firstname = 'Chuck';
		tmp_Con_1.Lastname = 'Blue';
		tmp_Con_1.Salutation = 'Mr.';
		tmp_Con_1.MailingStreet = 'Blumen Str. 1';
		tmp_Con_1.MailingCity = 'Hamburg';
		tmp_Con_1.MailingPostalCode = '20097';
		tmp_Con_1.MailingCountry = 'Germany';
		insert tmp_Con_1;

		Mandant__c tmp_man = new Mandant__c();
		tmp_man.Name = 'Test';
		insert tmp_man;

		Vertragsbeziehung__c tmp_vb = new Vertragsbeziehung__c();
		tmp_vb.Mandant__c = tmp_man.Id;
		tmp_vb.Account__c = tmp_Acc.Id;
		tmp_vb.Vertragsbeziehung__c = 'Entwurf';
		insert tmp_vb;

		Vertragsbeziehung__c tmp_vb2 = new Vertragsbeziehung__c();
		tmp_vb2.Mandant__c = tmp_man.Id;
		tmp_vb2.Account__c = tmp_Acc.Id;
		tmp_vb2.Vertragsbeziehung__c = 'Entwurf';
		tmp_vb2.zz_anzahl_rechnungsempfaenger__c = 2;
		insert tmp_vb2;

		vertragsbeziehungs_rolle__c tmp_vbr = new vertragsbeziehungs_rolle__c();
		tmp_vbr.contact__c = tmp_Con_1.Id;
		tmp_vbr.Vertragsbeziehung__c = tmp_vb2.Id;
		tmp_vbr.rolle__c = 'Rechnungsempfänger';
		insert tmp_vbr;

		// tmp_vb2.Vertragsbeziehung__c = 'Genehmigungsprüfung';
		// tmp_vb2.zz_approved__c = false;
		// update tmp_vb2;
		// tmp_vb2.Vertragsbeziehung__c = 'Aktiv';
		// update tmp_vb2;

		Test.startTest();
		 account_status_check_schedulable con = new account_status_check_schedulable();

		 // Schedule the test job
		 String CRON_EXP = '0 0 0 15 3 ? 2022';
		 String jobId = System.schedule('TESTING 444xxx', CRON_EXP, con);

		 // Get the information from the CronTrigger API object
		 CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];

		 // Verify the expressions are the same
		 System.assertEquals(CRON_EXP, ct.CronExpression);

		 // Verify the job has not run
		 System.assertEquals(0, ct.TimesTriggered);

		 // Verify the next time the job will run
		 System.assertEquals('2022-03-15 00:00:00', String.valueOf(ct.NextFireTime));

		Test.stopTest();
	}

	@isTest static void test_method_INAKTIV() {
		Account tmp_Acc = new Account();
		tmp_Acc.Name 								= 'Test AQ';
		tmp_Acc.BillingStreet 			= 'Högerdamm 41';
		tmp_Acc.BillingCity 				= 'Hamburg';
		tmp_Acc.BillingPostalCode 	= '20097';
		tmp_Acc.BillingCountry 			= 'Germany';
		tmp_Acc.ShippingStreet 			= 'Högerdamm 41';
		tmp_Acc.ShippingCity 				= 'Hamburg';
		tmp_Acc.ShippingPostalCode 	= '20097';
		tmp_Acc.ShippingCountry 		= 'Germany';
		insert tmp_Acc;

		Mandant__c tmp_man = new Mandant__c();
		tmp_man.Name = 'Test';
		insert tmp_man;

		Vertragsbeziehung__c tmp_vb = new Vertragsbeziehung__c();
		tmp_vb.Mandant__c = tmp_man.Id;
		tmp_vb.Account__c = tmp_Acc.Id;
		tmp_vb.Vertragsbeziehung__c = 'Entwurf';
		insert tmp_vb;
		Vertragsbeziehung__c tmp_vb2 = new Vertragsbeziehung__c();
		tmp_vb2.Mandant__c = tmp_man.Id;
		tmp_vb2.Account__c = tmp_Acc.Id;
		tmp_vb2.Vertragsbeziehung__c = 'Entwurf';
		tmp_vb2.zz_anzahl_rechnungsempfaenger__c = 2;
		insert tmp_vb2;

		account_status_check_batch b = new account_status_check_batch();
		database.executebatch(b);
	}

	@isTest static void test_method_GESPERRT() {
		Account tmp_Acc = new Account();
		tmp_Acc.Name 								= 'Test AQ';
		tmp_Acc.BillingStreet 			= 'Högerdamm 41';
		tmp_Acc.BillingCity 				= 'Hamburg';
		tmp_Acc.BillingPostalCode 	= '20097';
		tmp_Acc.BillingCountry 			= 'Germany';
		tmp_Acc.ShippingStreet 			= 'Högerdamm 41';
		tmp_Acc.ShippingCity 				= 'Hamburg';
		tmp_Acc.ShippingPostalCode 	= '20097';
		tmp_Acc.ShippingCountry 		= 'Germany';
		tmp_Acc.sperrgrund__c = 'fusioniert';
		insert tmp_Acc;

		account_status_check_batch b = new account_status_check_batch();
		database.executebatch(b);
	}

	@isTest static void test_method_POTENZIELLER() {
		Account tmp_Acc = new Account();
		tmp_Acc.Name 								= 'Test AQ';
		tmp_Acc.BillingStreet 			= 'Högerdamm 41';
		tmp_Acc.BillingCity 				= 'Hamburg';
		tmp_Acc.BillingPostalCode 	= '20097';
		tmp_Acc.BillingCountry 			= 'Germany';
		tmp_Acc.ShippingStreet 			= 'Högerdamm 41';
		tmp_Acc.ShippingCity 				= 'Hamburg';
		tmp_Acc.ShippingPostalCode 	= '20097';
		tmp_Acc.ShippingCountry 		= 'Germany';
		insert tmp_Acc;

		account_status_check_batch b = new account_status_check_batch();
		database.executebatch(b);
	}

}