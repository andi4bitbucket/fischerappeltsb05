/**************************************************************************
Company: aquilliance GmbH
Author: Andreas Wolf
CreatedDate: 2017-1

**************************************************************************/
@isTest
private class List_Management_Test {

	@isTest static void test_method_one() {
		Account tmp_acc = new Account();
		tmp_acc.Name = 'Test';

		insert tmp_acc;

		Team_Member__c tmp_tm = new Team_Member__c();
		tmp_tm.Account__c = tmp_acc.Id;
		tmp_tm.User__c = UserInfo.getUserId();

		insert tmp_tm;

		Contact tmp_con = new Contact();
		tmp_con.AccountId = tmp_acc.Id;
		tmp_con.FirstName = 'Max';
		tmp_con.LastName = 'Mustermann';
		tmp_con.Salutation = 'Mr.';

		insert tmp_con;

		Contact tmp_con2 = new Contact();
		tmp_con2.AccountId = tmp_acc.Id;
		tmp_con2.FirstName = 'Max2';
		tmp_con2.LastName = 'Mustermann2';
		tmp_con2.Salutation = 'Mr.';

		insert tmp_con2;
		Contact tmp_con3 = new Contact();
		tmp_con3.AccountId = tmp_acc.Id;
		tmp_con3.FirstName = 'Max3';
		tmp_con3.LastName = 'Mustermann3';
		tmp_con3.Salutation = 'Mr.';

		insert tmp_con3;
		Contact tmp_con4 = new Contact();
		tmp_con4.AccountId = tmp_acc.Id;
		tmp_con4.FirstName = 'Max4';
		tmp_con4.LastName = 'Mustermann4';
		tmp_con4.Salutation = 'Mr.';

		insert tmp_con4;

		Contact tmp_con5 = new Contact();
		tmp_con5.AccountId = tmp_acc.Id;
		tmp_con5.FirstName = 'Max5';
		tmp_con5.LastName = 'Mustermann5';
		tmp_con5.Salutation = 'Mr.';

		insert tmp_con5;

		Contact tmp_con6 = new Contact();
		tmp_con6.AccountId = tmp_acc.Id;
		tmp_con6.FirstName = 'Max6';
		tmp_con6.LastName = 'Mustermann6';
		tmp_con6.Salutation = 'Mr.';

		insert tmp_con6;

		String standardRT = [SELECT id FROM RecordType WHERE sObjectType = 'List__c' AND DeveloperName = 'Standard' LIMIT 1].Id;
		String internRT = [SELECT id FROM RecordType WHERE sObjectType = 'List__c' AND DeveloperName = 'Intern' LIMIT 1].Id;
		String externRT = [SELECT id FROM RecordType WHERE sObjectType = 'List__c' AND DeveloperName = 'Extern' LIMIT 1].Id;
		String privateRT = [SELECT id FROM RecordType WHERE sObjectType = 'List__c' AND DeveloperName = 'Private' LIMIT 1].Id;

		List<List__c> list_folders = new List<List__c>();
		List__c tmp_list = new List__c();
		tmp_list.Name = 'Something';
		tmp_list.RecordTypeId = internRT;
		list_folders.add(tmp_list);
		List__c tmp_list2 = new List__c();
		tmp_list2.Name = 'Something2';
		tmp_list2.Account__c = tmp_acc.Id;
		tmp_list2.RecordTypeId = externRT;
		list_folders.add(tmp_list2);
		List__c tmp_list3 = new List__c();
		tmp_list3.Name = 'Something3';
		tmp_list3.RecordTypeId = privateRT;
		list_folders.add(tmp_list3);
		insert list_folders;

		List<List__c> list_lists = new List<List__c>();
		List__c tmp_list4 = new List__c();
		tmp_list4.Name = 'Aomething';
		tmp_list4.Folder__c = tmp_list.Id;
		tmp_list4.RecordTypeId = standardRT;
		list_lists.add(tmp_list4);
		List__c tmp_list4_2 = new List__c();
		tmp_list4_2.Name = 'Something';
		tmp_list4_2.Folder__c = tmp_list.Id;
		tmp_list4_2.RecordTypeId = standardRT;
		list_lists.add(tmp_list4_2);
		List__c tmp_list5 = new List__c();
		tmp_list5.Name = 'Something';
		tmp_list5.Folder__c = tmp_list2.Id;
		tmp_list5.RecordTypeId = standardRT;
		list_lists.add(tmp_list5);
		List__c tmp_list5_2 = new List__c();
		tmp_list5_2.Name = 'Something';
		tmp_list5_2.Folder__c = tmp_list2.Id;
		tmp_list5_2.RecordTypeId = standardRT;
		list_lists.add(tmp_list5_2);
		List__c tmp_list6 = new List__c();
		tmp_list6.Name = 'Something';
		tmp_list6.Folder__c = tmp_list3.Id;
		tmp_list6.RecordTypeId = standardRT;
		list_lists.add(tmp_list6);

		insert list_lists;

		List_Member__c tmp_lm = new List_Member__c();
		tmp_lm.Contact__c = tmp_con.Id;
		tmp_lm.List__c = tmp_list4.Id;

		insert tmp_lm;

		List_Member__c tmp_lm2 = new List_Member__c();
		tmp_lm2.Contact__c = tmp_con.Id;
		tmp_lm2.List__c = tmp_list5.Id;

		insert tmp_lm2;

		List_Member__c tmp_lm3 = new List_Member__c();
		tmp_lm3.Contact__c = tmp_con4.Id;
		tmp_lm3.List__c = tmp_list4.Id;

		insert tmp_lm3;

		List_Member__c tmp_lm4 = new List_Member__c();
		tmp_lm4.Contact__c = tmp_con3.Id;
		tmp_lm4.List__c = tmp_list4.Id;

		insert tmp_lm4;

		Mandant__c tmp_man = new Mandant__c();
		tmp_man.Name = 'Test';
		insert tmp_man;

		List_Management con = new List_Management();
		con.search_account = 'Test';
		con.search_firstname = 'Max';
		con.search_lastname = 'Mustermann';
		con.search();
		con.setActiveList();
		con.active_list = tmp_list.Id;
		con.getAllListMembers();
		con.addNewList_toggle();
		con.addNewList_toggle();
		con.back();
		con.createNewListOrFolder();
		// Create successfully new list
		con.newObject = 'list';
		con.list_name = 'Test';
		con.list_folder = tmp_list3.Id;
		con.createNewList();
		// Fail to Create new list
		con.newObject = 'list';
		con.list_name = '';
		con.list_folder = '';
		con.createNewList();
		// Create successfully new folder -intern
		con.newObject = 'folder';
		con.list_type = 'intern';
		con.list_name = 'Test';
		con.new_folder.Mandant__c = tmp_man.Id;
		con.createNewList();
		// Fail to Create new folder  -intern
		con.newObject = 'folder';
		con.list_type = 'intern';
		con.list_name = '';
		con.new_folder.Mandant__c = null;
		con.createNewList();
		// Create successfully new folder -extern
		con.newObject = 'folder';
		con.list_type = 'extern';
		con.list_name = 'Test';
		con.new_folder.Account__c = tmp_Acc.Id;
		con.createNewList();
		// Fail to Create new folder  -extern
		con.newObject = 'folder';
		con.list_type = 'extern';
		con.list_name = '';
		con.new_folder.Account__c = null;
		con.createNewList();
		// Create successfully new folder -private
		con.newObject = 'folder';
		con.list_type = 'private';
		con.list_name = 'Test';
		con.createNewList();
		// folders
		con.theSelectedArea = 'folders';
		con.set_collapsible_area();
		con.set_collapsible_area();
		// search
		con.theSelectedArea = 'search';
		con.set_collapsible_area();
		con.set_collapsible_area();
		// member
		con.theSelectedArea = 'member';
		con.set_collapsible_area();
		con.set_collapsible_area();

		con.deleteList_toggle();
		con.deleteList_toggle();
		con.list_id_for_deletion = tmp_list6.Id;
		con.deleteList();

		con.askForDeletion();
		con.askForDeletion();

		con.deleteStep = true;
		con.active_list = tmp_list5.Id;
		con.getAllListMembers();
		con.list_wm[0].isChecked = true;
		con.removeFromList();

		con.active_list = tmp_list4.Id;
		con.getAllListMembers();
		con.change();
		con.search();
		con.changed_row_nr = '0';
		con.rerender_contacts();

		con.check_all();
				con.addToList();
		con.check_all();
		con.check_all2();
		con.check_all2();
		con.check_one();
		con.check_one();
		con.check_one2();
		con.check_one2();

		con.showMember_toggle();
		con.showMember_toggle();

		con.addToOtherList_toggle();
		con.addToOtherList_toggle();
		con.addMember_selectedList = tmp_list4.Id;
		con.addMemberToOtherList();

		for(Integer i =0; i <11; i++){
			List_Management.wrapper_Contact wc_test = new List_Management.wrapper_Contact();
			con.list_wc.add(wc_test);
		}
		con.selected_limit = '1';
		List<SelectOption> test1 = con.getPagination_limit_options();
		con.searched_con_list_size = 10;
		con.limit_change();
		con.beginning();
		con.next();
		con.previous();
		con.last();
		Boolean bool_1 = con.getDisableNext();
		Boolean bool_2 = con.getDisablePrevious();


		for(Integer i =0; i <11; i++){
			List_Management.wrapper_Member wm_test = new List_Management.wrapper_Member();
			con.list_wm.add(wm_test);
		}
		con.selected_limit2 = '1';
		List<SelectOption> test2 = con.getPagination_limit_options2();
		con.member_list_size = 10;
		con.limit_change2();
		con.beginning2();
		con.next2();
		con.previous2();
		con.last2();
		Boolean bool_3 = con.getDisableNext2();
		Boolean bool_4 = con.getDisablePrevious2();


		// edit folder
		con.activeFolderId = tmp_list2.Id;
		con.editFolder();
		con.closeEditActiveFolder();
		con.activeFolderId = tmp_list2.Id;
		con.editFolder();
		con.updateActiveFolder();
	}
}