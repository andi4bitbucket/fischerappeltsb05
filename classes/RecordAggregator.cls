public class RecordAggregator{

    //Aggregate LL_SlaveRecords based on the Aggregation Settings
    public static void aggregate( List< SObject> PL_NewSlaveRecords, List< SObject> PL_OldSlaveRecords )
    {
        System.debug( 'Start RecordAggregator.aggregate | PL_NewSlaveRecords: ' + PL_NewSlaveRecords + ' | PL_OldSlaveRecords: ' + PL_NewSlaveRecords );
        List< SObject > LL_SlaveRecords = new List< SObject >();
        if( PL_NewSlaveRecords != null ){
            LL_SlaveRecords.addAll( PL_NewSlaveRecords );
        }
        if( PL_OldSlaveRecords != null ){
            LL_SlaveRecords.addAll( PL_OldSlaveRecords );
        }
        List< AggregateSettings__c > LL_RollupSettings = getRollupSettings( LL_SlaveRecords.get( 0 ) );
        Set< Id > LS_SlaveRecordIds = getSlaveRecordIds( LL_SlaveRecords );
        Set< Id > LS_MasterRecordIds;
        Map< String, Set< AggregateSettings__c > > LM_SlaveObjectFields = new Map< String, Set< AggregateSettings__c > >();
        for( AggregateSettings__c LO_RollupSettings: LL_RollupSettings ){
            String LV_MasterObjectName = LO_RollupSettings.MasterObject__c;
            String LV_LookupField = LO_RollupSettings.LookupField__c;
            if ( LM_SlaveObjectFields.get( LV_MasterObjectName ) == null ){
                LM_SlaveObjectFields.put( LV_MasterObjectName, new Set< AggregateSettings__c >() );
            }
            LM_SlaveObjectFields.get( LV_MasterObjectName ).add( LO_RollupSettings);
        }

        List< SObject > LL_UpdateRecords = new List< SObject >();
        for( Set< AggregateSettings__c > LL_SlaveObjectRollupSettings: LM_SlaveObjectFields.Values() )
        {
            for( AggregateSettings__c LO_SlaveObjectRollupSettings: LL_SlaveObjectRollupSettings )
            {
              List< AggregateResult > LL_Aggregates = new List< AggregateResult >();
              MAP<string,String> result_map = new MAP<string,String>();

              LS_MasterRecordIds = getMasterRecordIds( LO_SlaveObjectRollupSettings, LL_SlaveRecords );

              String LV_AggregateFields = LO_SlaveObjectRollupSettings.AggregateFields__c;
              String LV_AggregateFields_clean = LV_AggregateFields.remove(' ');




              /***********************************************
              Switch for last and first
              ***********************************************/
              if(LV_AggregateFields_clean.containsIgnoreCase('last(') || LV_AggregateFields_clean.containsIgnoreCase('first('))
              {
                System.debug('###Last');
                result_map = getAggregates_first_last_value( LO_SlaveObjectRollupSettings, LS_MasterRecordIds );

                // hier update
                updateMasterRecords_first_last( LO_SlaveObjectRollupSettings, LS_MasterRecordIds, result_map);
              }
              else
              {
                LL_Aggregates = getAggregates( LO_SlaveObjectRollupSettings, LS_MasterRecordIds );

                System.debug( 'Aggregates: ' + LL_Aggregates );
                LL_UpdateRecords.addAll( updateMasterRecords( LO_SlaveObjectRollupSettings, LS_MasterRecordIds, LL_Aggregates ) );
              }

              /***********************************************/
            }
        }

        System.debug( 'End RecordAggregator.aggregate' );
    }

    /***************************************************************************************
    Get the RollupSettings for the ObjetType of the given record
    ***************************************************************************************/
    private static List< AggregateSettings__c > getRollupSettings( SObject LO_Record ){
        System.debug( 'Start RecordAggregator.getRollupSettings | LO_Record: ' + LO_Record );
        String LV_ObjectName = getSObjectName( LO_Record );
        List< AggregateSettings__c > LL_RollupSettings = [ SELECT MasterObject__c, SlaveObject__c, LookupField__c, AggregateFields__c, MasterFields__c, Criteria__c, isActive__c, include_deleted__c FROM AggregateSettings__c WHERE ( SlaveObject__c =: LV_ObjectName ) AND isActive__c = true ];
        System.debug( 'End RecordAggregator.getRollupSettings' );
        return LL_RollupSettings;
    }


    /***************************************************************************************
    Get the SObject Name of the given record
    ***************************************************************************************/
    private static String getSObjectName( SObject LO_Record ){
        System.debug( 'Start RecordAggregator.getSObjectName | LO_Record: ' + LO_Record );
        Schema.SObjectType LO_SObjectType = LO_Record.Id.getSObjectType();
        Schema.DescribeSObjectResult LO_SObjectDescribe = LO_SObjectType.getDescribe();
        String LV_ObjectName = LO_SObjectDescribe.getName();
        System.debug( 'End RecordAggregator.getSObjectName' );
        return LV_ObjectName;
    }


    /***************************************************************************************
    Get a list of Ids for a given list of objects
    ***************************************************************************************/
    private static Set< Id > getSlaveRecordIds( List< SObject> LL_SlaveRecords ){
        System.debug( 'Start RecordAggregator.getSlaveRecordIds | LL_SlaveRecords: ' + LL_SlaveRecords );
        Set< Id > LS_SlaveRecordIds = new Set< Id >();
        for( SObject LO_SlaveRecord: LL_SlaveRecords ){
            LS_SlaveRecordIds.add( LO_SlaveRecord.Id );
        }
        System.debug( 'End RecordAggregator.getSlaveRecordIds' );
        return LS_SlaveRecordIds;
    }


    /***************************************************************************************
    Get the Ids of the Master Records referenced in the given list of Slave Records
    ***************************************************************************************/
    private static Set< Id > getMasterRecordIds( AggregateSettings__c LO_RollupSettings, List< SObject > LL_SlaveRecords ){
        System.debug( 'Start RecordAggregator.getMasterRecordIds | LO_RollupSettings : ' + LO_RollupSettings + ' | LL_SlaveRecords : ' + LL_SlaveRecords );
        String LV_MasterObjectName = LO_RollupSettings.MasterObject__c;
        String LV_SlaveObjectName = LO_RollupSettings.SlaveObject__c;
        String LV_LookupField = LO_RollupSettings.LookupField__c;

        Set< Id > LS_MasterRecordIds = new Set< Id>();
        //String LV_MasterRecordQuery = 'SELECT ' + LV_LookupField  + ' FROM ' + LV_SlaveObjectName + ' WHERE ( Id IN: LS_SlaveRecordIds )';
        //for( SObject LO_SlaveRecord: Database.query( LV_MasterRecordQuery  ) ){
        //    LS_MasterRecordIds.add( (Id) LO_SlaveRecord.get( LV_LookupField ) );
        //}
        for( SObject LO_SlaveRecord: LL_SlaveRecords ){
            if( LO_SlaveRecord.get( LV_LookupField ) != null ){
                LS_MasterRecordIds.add( (Id)LO_SlaveRecord.get( LV_LookupField ) );
            }
        }
        System.debug( 'End RecordAggregator.getMasterRecordIds' );
        return LS_MasterRecordIds;
    }


    /***************************************************************************************
    Get the aggregated values for the given Master Records and the Rollup Settings
    ***************************************************************************************/
    private static List< AggregateResult > getAggregates( AggregateSettings__c LO_RollupSettings, Set< Id > LS_MasterRecordIds ){
        System.debug( 'Start RecordAggregator.getAggregates | LO_RollupSettings : ' + LO_RollupSettings + ' | LS_MasterRecordIds : ' + LS_MasterRecordIds );
        String LV_SlaveObjectName = LO_RollupSettings.SlaveObject__c;
        String LV_LookupField = LO_RollupSettings.LookupField__c;
        String LV_AggregateFields = LO_RollupSettings.AggregateFields__c;
        String LV_MasterFields = LO_RollupSettings.MasterFields__c;
        String LV_Criteria = LO_RollupSettings.Criteria__c;
        String LV_include_deleted = '';
        If(LO_RollupSettings.include_deleted__c == true)
        {
          LV_include_deleted = ' ALL ROWS';
        }

        if( ( LV_Criteria != null ) && ( LV_Criteria != '' ) ){
            LV_Criteria = ' AND ( ' + LV_Criteria + ' )';
        }else{
            LV_Criteria = '';
        }

        List< String > LL_AggregateFields = LV_AggregateFields.split( ',' );
        List< String > LL_MasterFields = LV_MasterFields.split( ',' );

        String LV_AggregateFieldsQueryString = '';
        String LV_Seperator = '';
        for( Integer LV_Counter = 0; LV_Counter < LL_AggregateFields.size(); LV_Counter++ ){
            String LV_AggregateField = LL_AggregateFields.get( LV_Counter );
            String LV_MasterField = LL_MasterFields.get( LV_Counter );
            LV_AggregateFieldsQueryString += LV_Seperator + LV_AggregateField + LV_MasterField;
            LV_Seperator = ',';
        }

        String LV_AggregateSlaveRecordsQuery = 'SELECT ' + LV_LookupField  + ', ' + LV_AggregateFieldsQueryString + ' FROM ' + LV_SlaveObjectName + ' WHERE ( ' + LV_LookupField  + ' IN: LS_MasterRecordIds )' + LV_Criteria + ' GROUP BY ' + LV_LookupField + '' + LV_include_deleted;
        System.debug('### LV_AggregateSlaveRecordsQuery:'+LV_AggregateSlaveRecordsQuery);
        List< AggregateResult > LL_SlaveRecordAggregates = Database.query( LV_AggregateSlaveRecordsQuery  );
        System.debug('### End RecordAggregator.getAggregates' );
        return LL_SlaveRecordAggregates;
    }




    /***************************************************************************************
    Get the aggregated values for the given Master Records and the Rollup Settings
    LO_RollupSettings.AggregateFields__c containsIgnoreCase(substring)
    ***************************************************************************************/
    private static MAP<String,String> getAggregates_first_last_value( AggregateSettings__c LO_RollupSettings, Set< Id > LS_MasterRecordIds )
    {
        List<sObject> db_query_result = new List<sObject>();
        MAP<String,String> result_map = new MAP<String,String>();
        String str_orderby_order = 'ASC';
        /***********************************************
        RecordAggregator Settings
        ***********************************************/
        System.debug( 'Start RecordAggregator.getAggregates | LO_RollupSettings : ' + LO_RollupSettings + ' | LS_MasterRecordIds : ' + LS_MasterRecordIds );
        String LV_SlaveObjectName = LO_RollupSettings.SlaveObject__c;
        String LV_LookupField = LO_RollupSettings.LookupField__c;
        String LV_AggregateFields = LO_RollupSettings.AggregateFields__c;
        String LV_MasterFields = LO_RollupSettings.MasterFields__c;
        String LV_Criteria = LO_RollupSettings.Criteria__c;
        String LV_include_deleted = '';
        If(LO_RollupSettings.include_deleted__c == true)
        {
          LV_include_deleted = ' ALL ROWS';
        }

        // the LV_AggregateFields may contain
        String LV_AggregateFields_clean = LV_AggregateFields.remove(' ');

        /***********************************************
        to cover first and last change order
        first is ASC and last DESC
        ***********************************************/
        if(LV_AggregateFields_clean.containsIgnoreCase('last('))
        {
          str_orderby_order = 'DESC';
        }


        /***********************************************
        clean the LV_AggregateFields
        ***********************************************/
        LV_AggregateFields_clean = LV_AggregateFields_clean.removeStartIgnoreCase('first(');
        LV_AggregateFields_clean = LV_AggregateFields_clean.removeStartIgnoreCase('last(');
        LV_AggregateFields_clean = LV_AggregateFields_clean.remove(')');
        System.debug('### LV_AggregateFields_clean:'+LV_AggregateFields_clean);
        /***********************************************/

        /***********************************************
        prepare query fields
        ***********************************************/
        List<String> LV_AggregateFields_clean_list = LV_AggregateFields_clean.split(',');
        System.debug('### LV_AggregateFields_clean_list:'+LV_AggregateFields_clean_list);
        // the order by part
        String strOrderby = LV_AggregateFields_clean_list[0];
        // the new value
        String LV_AggregateFields_value = LV_AggregateFields_clean_list[1];
        /***********************************************/




        if( ( LV_Criteria != null ) && ( LV_Criteria != '' ) ){
            LV_Criteria = ' AND ( ' + LV_Criteria + ' )';
        }else{
            LV_Criteria = '';
        }

        List< String > LL_AggregateFields = LV_AggregateFields.split( ',' );
        List< String > LL_MasterFields = LV_MasterFields.split( ',' );

        String LV_AggregateFieldsQueryString = '';
        LV_AggregateFieldsQueryString = LV_AggregateFields_clean_list[0] +','+ LV_AggregateFields_clean_list[1];

        String LV_AggregateSlaveRecordsQuery = 'SELECT ' + LV_LookupField  + ', ' + LV_AggregateFieldsQueryString + ' FROM ' + LV_SlaveObjectName + ' WHERE ( ' + LV_LookupField  + ' IN: LS_MasterRecordIds )' + LV_Criteria + ' Order BY ' + strOrderby + ' '+ str_orderby_order +''+ LV_include_deleted +' ';//LIMIT 1
        System.debug('### LV_AggregateSlaveRecordsQuery:'+LV_AggregateSlaveRecordsQuery);
        db_query_result = Database.query( LV_AggregateSlaveRecordsQuery  );

        /***********************************************
        loop query result and prepare result map
        ***********************************************/
        if(db_query_result.size()>0)
        {
          for(sObject tmp_so:db_query_result)
          {
            //LV_LookupField contains the MasterId
            if(!result_map.containsKey(String.valueOf(tmp_so.get(LV_LookupField))))
            {
              System.debug('### tmp_so.get(LV_LookupField)' +tmp_so.get(LV_LookupField));
              System.debug('### tmp_so.get(LV_AggregateFields_clean_list[1]' +tmp_so.get(LV_AggregateFields_clean_list[1]));
              result_map.put(String.valueOf(tmp_so.get(LV_LookupField)),String.valueOf(tmp_so.get(LV_AggregateFields_clean_list[1])));
            }

          }
        }


        //System.debug('### End last' +db_query_result);
        System.debug('### result_map' +result_map);
        return result_map;
    }
/***************************************************************************************/




/***************************************************************************************
    //Update the Master Records with the aggregated values of the Slave Records
***************************************************************************************/
    private static List< SObject > updateMasterRecords( AggregateSettings__c LO_RollupSettings, Set< Id > PS_MasterRecordIds, List< AggregateResult > LL_Aggregates ){
        System.debug( 'Start RecordAggregator.updateMasterRecords | LO_RollupSettings : ' + LO_RollupSettings + ' | LL_Aggregates : ' + LL_Aggregates );

        String LV_MasterObjectName = LO_RollupSettings.MasterObject__c;
        String LV_LookupField = LO_RollupSettings.LookupField__c;
        String LV_AggregateFields = LO_RollupSettings.AggregateFields__c;
        String LV_MasterFields = LO_RollupSettings.MasterFields__c;

        List< String > LL_AggregateFields = LV_AggregateFields.split( ',' );
        List< String > LL_MasterFields = LV_MasterFields.split( ',' );

        List< Id > LL_MasterRecordIds = new List< Id >();
        for( AggregateResult LO_Aggregates: LL_Aggregates ){
            LL_MasterRecordIds.add( (id) LO_Aggregates.get( LV_LookupField  ) );
        }

        String LV_FieldsQueryString = '';
        String LV_Seperator = '';
        for( String LV_Field: LL_MasterFields ){
            LV_FieldsQueryString += LV_Seperator + LV_Field;
            LV_Seperator = ',';
        }

        String LV_MasterRecordsQueryString = 'SELECT ' + LV_FieldsQueryString + ' FROM ' + LV_MasterObjectName + ' WHERE Id IN: PS_MasterRecordIds ';
        System.debug( 'MasterRecordsQuery: ' + LV_MasterRecordsQueryString );
        Map< Id, SObject > LM_MasterRecords = new Map< Id, SObject >();
        for( SObject LO_Record: Database.query( LV_MasterRecordsQueryString ) ){
            for( String LV_MasterField: LL_MasterFields ){
                LO_Record.put( LV_MasterField, null );
            }
            LM_MasterRecords.put( LO_Record.Id, LO_Record );
        }
        System.debug( 'MasterRecords: ' + LM_MasterRecords );

        for( AggregateResult LO_Aggregates: LL_Aggregates ){
            for( Integer LV_Counter = 0; LV_Counter < LL_AggregateFields.size(); LV_Counter++ ){
                String LV_AggregateField = LL_AggregateFields.get( LV_Counter );
                String LV_MasterField = LL_MasterFields.get( LV_Counter );
                if( LM_MasterRecords.containsKey( (Id) LO_Aggregates.get( LV_LookupField ) ) ){
                    LM_MasterRecords.get( (Id) LO_Aggregates.get( LV_LookupField ) ).put( LV_MasterField, LO_Aggregates.get( LV_MasterField ) );
                }
            }
        }
        System.debug( 'End RecordAggregator.updateMasterRecords' );
        System.debug('### LM_MasterRecords.values()'+LM_MasterRecords.values());
        update( LM_MasterRecords.values() );
        return LM_MasterRecords.values();
    }


    /***************************************************************************************
        //Update the Master Records with the aggregated values of the Slave Records
    ***************************************************************************************/
        private static List< SObject > updateMasterRecords_first_last( AggregateSettings__c LO_RollupSettings, Set< Id > PS_MasterRecordIds, MAP<string,String> result_map){
            System.debug( 'Start RecordAggregator.updateMasterRecords | LO_RollupSettings : ' + LO_RollupSettings + ' | result_map : ' + result_map );

            String LV_MasterObjectName = LO_RollupSettings.MasterObject__c;
            String LV_LookupField = LO_RollupSettings.LookupField__c;
            String LV_AggregateFields = LO_RollupSettings.AggregateFields__c;
            String LV_MasterFields = LO_RollupSettings.MasterFields__c;



            //List< String > LL_AggregateFields = LV_AggregateFields.split( ',' );
            List< String > LL_MasterFields = LV_MasterFields.split( ',' );

            List< Id > LL_MasterRecordIds = new List< Id >();

            /***********************************************
            collect all master records
            ***********************************************/
            for( String tmp_str_master_id: result_map.keyset() ){
                LL_MasterRecordIds.add( (id) tmp_str_master_id);
            }

            String LV_FieldsQueryString = '';
            String LV_Seperator = '';
            for( String LV_Field: LL_MasterFields ){
                LV_FieldsQueryString += LV_Seperator + LV_Field;
                LV_Seperator = ',';
            }

            String LV_MasterRecordsQueryString = 'SELECT ' + LV_FieldsQueryString + ' FROM ' + LV_MasterObjectName + ' WHERE Id IN: PS_MasterRecordIds ';
            System.debug( 'MasterRecordsQuery: ' + LV_MasterRecordsQueryString );
            Map< Id, SObject > LM_MasterRecords = new Map< Id, SObject >();
            for( SObject LO_Record: Database.query( LV_MasterRecordsQueryString ) ){
                for( String LV_MasterField: LL_MasterFields ){
                  if(result_map.containsKey(LO_Record.Id))
                  {
                    LO_Record.put( LV_MasterField, result_map.get(LO_Record.Id) );
                  }
                  else
                  {
                    LO_Record.put( LV_MasterField, null );
                  }

                }
                LM_MasterRecords.put( LO_Record.Id, LO_Record );
            }
            System.debug( 'MasterRecords: ' + LM_MasterRecords );


            System.debug( 'End RecordAggregator.updateMasterRecords' );
            System.debug('### LM_MasterRecords.values()'+LM_MasterRecords.values());
            update( LM_MasterRecords.values() );
            return LM_MasterRecords.values();
        }
}