/**************************************************************************
Company: aquilliance GmbH
Author: Andreas Wolf
CreatedDate: 2017-1

**************************************************************************/
public with sharing class Budget_Trigger {
	// dont use Chatter
	//Budget_Trigger.isSpecialTest
	public static Boolean isSpecialTest = false;
	public Budget_Trigger() {}


//*****************************************************************************************************************************************
//******************************************** INSERT *************************************************************************************
//*****************************************************************************************************************************************

	public void doAfterInsert(List<Budget__c> list_bud){
		writeUserIDinOppHelperField(list_bud);

		User u = new User();
		try{u = [SELECT Id, FirstName, LastName FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
		System.debug('');}catch(Exception e){System.debug('### -Budget_Trigger-afterInsert- getUserInfo:'+e);}

		String recordTypeId_sekundaer_budget;
		try{recordTypeId_sekundaer_budget = [SELECT Id FROM RecordType WHERE DeveloperName = 'sekundaer_budget' AND SobjectType='budget__c' LIMIT 1].Id;
		System.debug('');}catch(Exception e){System.debug('### -Budget_Trigger-afterInsert- getSekundärBudgetRTid:'+e);}

		// prepare all relevant opportunitis
		Set<String> relevant_oppIds = new Set<String>();
		for(Budget__c tmp:list_bud)
		{
			if(tmp.RecordTypeId == recordTypeId_sekundaer_budget)
			{
				if(tmp.benutzer__c != u.Id)
				{
					if(!relevant_oppIds.contains(tmp.Opportunity__c))
					{
						relevant_oppIds.add(tmp.Opportunity__c);
					}
				}
			}
		}

		// Get all existing subscriptions for Opportunity Record --> so we dont create a duplicate
		List<EntitySubscription> list_existing_follows = new List<EntitySubscription>();
		Set<String> set_existing_follows = new Set<String>();
		try{list_existing_follows = [SELECT Id, subscriberid FROM EntitySubscription WHERE parentId IN: relevant_oppIds];
		System.debug('');}catch(Exception e){System.debug('### -Budget_Trigger-afterInsert- getExisitingSubscriptions:'+e);}
		System.debug('### -Budget_Trigger-afterInsert- list_existing_follows:'+list_existing_follows);
		for(EntitySubscription tmp:list_existing_follows)
		{
			set_existing_follows.add(tmp.subscriberid);
		}

		// Prepare Subscriptions for insert
		List<EntitySubscription> list_follows = new List<EntitySubscription>();
		Set<String> set_subscriptions = new Set<String>();
			// Prepare set to get all User Emails and Opportunity Infos
			Set<String> set_subscriptions_userIds = new Set<String>();
			Set<String> set_subscriptions_oppIds = new Set<String>();
		for(Budget__c tmp:list_bud)
		{
			if(tmp.RecordTypeId == recordTypeId_sekundaer_budget)
			{
				//if(tmp.benutzer__c != u.Id)
				//{
					if(!set_subscriptions.contains(tmp.Opportunity__c+' '+tmp.benutzer__c) && !set_existing_follows.contains(tmp.benutzer__c))
					{
						EntitySubscription follow = new EntitySubscription (
																	 parentId = tmp.Opportunity__C,
																	 subscriberid = tmp.benutzer__c);
						list_follows.add(follow);
						set_subscriptions.add(tmp.Opportunity__c+' '+tmp.benutzer__c);
							// Prepare set to get all User Emails and Opportunity Infos
							set_subscriptions_userIds.add(tmp.benutzer__c);
							set_subscriptions_oppIds.add(tmp.Opportunity__c);
					}
				//}
			}
		}

		// insert subscriptions
		System.debug('### -Budget_Trigger-afterInsert- list_follows: '+list_follows);
		try{upsert list_follows;
		System.debug('');}catch(Exception e){System.debug('### -Budget_Trigger-afterInsert- upsertFollows:'+e);}

		// Send Info Email to Users
		List<User> list_user = new List<User>();
		Map<String, User> map_users = new Map<String, User>();
		List<Opportunity__c> list_opps = new List<Opportunity__c>();
		Map<String, Opportunity__c> map_opps = new Map<String, Opportunity__c>();
		List<Budget__c> list_budgets = new List<Budget__c>();
		Map<String, List<Budget__c>> map_budgets = new Map<String, List<Budget__c>>();
		try{list_user = [SELECT Id, FirstName, LastName, Email FROM User WHERE Id IN: set_subscriptions_userIds];
		System.debug('');}catch(Exception e){System.debug('### -Budget_Trigger-afterInsert- getUserInfoForEmails:'+e);}
		try{list_opps = [SELECT Id, Name, Account__r.Name, Honorarumsatz__c FROM Opportunity__c WHERE Id IN: set_subscriptions_oppIds];
		System.debug('');}catch(Exception e){System.debug('### -Budget_Trigger-afterInsert- getOppInfoForEmails:'+e);}
		try{list_budgets = [SELECT Id, RecordType.DeveloperName, Benutzer__c, Benutzer__r.FirstName, Benutzer__r.LastName,
			honorarumsatz_absolut__c, opportunity__c, Opportunity__r.Name, Opportunity__r.Account__r.Name,
			Benutzer__r.Email
			FROM Budget__c WHERE Opportunity__c IN: set_subscriptions_oppIds];
		System.debug('');}catch(Exception e){System.debug('### -Budget_Trigger-afterInsert- getBudgetsInfoForEmails:'+e);}
		for(User tmp:list_user)
		{
			map_users.put(tmp.Id, tmp);
		}
		for(Opportunity__c tmp:list_opps)
		{
			map_opps.put(tmp.Id, tmp);
		}
		List<Budget__c> tmp_list;
		for(Budget__c tmp:list_budgets)
		{
			if(!map_budgets.containsKey(tmp.Opportunity__c))
			{
				tmp_list = new List<Budget__c>();
				tmp_list.add(tmp);
				map_budgets.put(tmp.Opportunity__c, tmp_list);
			}else{
				tmp_list = map_budgets.get(tmp.Opportunity__c);
				tmp_list.add(tmp);
				map_budgets.put(tmp.Opportunity__c, tmp_list);
			}
		}

		sendInfoEmail(map_users, map_opps, list_follows, u, map_budgets);
	}

	public void writeUserIDinOppHelperField(List<Budget__c> list_bud)
	{
		Map<String, Opportunity__c> map_opp_ids = new Map<String, Opportunity__c>();
		Map<String, List<Budget__c>> map_buds = new Map<String, List<Budget__c>>();
		List<Budget__c> tmp_list = new List<Budget__c>();
		for(Budget__c tmp:list_bud)
		{
			tmp_list = new List<Budget__c>();
			if(map_buds.containskey(tmp.opportunity__c))
			{
				tmp_list = map_buds.get(tmp.opportunity__c);
				tmp_list.add(tmp);
				map_buds.put(tmp.opportunity__c, tmp_list);
			}else{
				tmp_list.add(tmp);
				map_buds.put(tmp.opportunity__c, tmp_list);
			}

			if(!map_opp_ids.containsKEy(tmp.opportunity__c))
			{
				map_opp_ids.put(tmp.opportunity__c, new Opportunity__c(Id = tmp.opportunity__c));
			}
		}
		List<opportunity__c> list_opps = new List<opportunity__c>();
		try{list_opps = [SELECT Id, zz_budget_user_ids__c
			FROM Opportunity__c WHERE Id IN: map_opp_ids.KeySet()];
		System.debug('');}catch(Exception e){System.debug('### -Budget_Trigger-writeUserIDinOppHelperField- getOpps:'+e);}
		for(opportunity__c tmp:list_opps)
		{
			if(map_buds.containsKey(tmp.Id))
			{
				for(Budget__c tmp2:map_buds.get(tmp.Id))
				{
					if(tmp.zz_budget_user_ids__c != null && tmp2.benutzer__c != null)
					{
						if(!tmp.zz_budget_user_ids__c.contains(tmp2.benutzer__c))
						{
							tmp.zz_budget_user_ids__c += ' '+tmp2.benutzer__c;
						}
					}
					else
					{
						tmp.zz_budget_user_ids__c = tmp2.benutzer__c;
					}
				}
			}
		}

		try{update list_opps;
		System.debug('');}catch(Exception e){System.debug('### -Budget_Trigger-writeUserIDinOppHelperField- updateOpps:'+e);}
	}




//*****************************************************************************************************************************************
//******************************************** UPDATE *************************************************************************************
//*****************************************************************************************************************************************

	public void doAfterUpdate(List<Budget__c> list_bud_new, List<Budget__c> list_bud_old){
		// Check wether the assigned benutzer__c has changed
		Map<String, Budget__c> map_buds_old = new Map<String, Budget__c>();
		List<Budget__c> list_chatter_budgets = new List<Budget__c>();
		Map<String, Budget__c> map_chatter_budgets_old_values = new Map<String, Budget__c>();
		for(Budget__c tmp:list_bud_old)
		{
			map_buds_old.put(tmp.Id, tmp);
		}
		// WRITE NEW HELPER FIELD
		writeUserIDinOppHelperField(list_bud_new, map_buds_old);

		List<Budget__c> list_bud = new List<Budget__c>();
		Set<String> set_delete_subscription = new Set<String>();
		for(Budget__c tmp:list_bud_new)
		{
			if(map_buds_old.get(tmp.Id).Benutzer__c != tmp.Benutzer__c)
			{
				list_bud.add(tmp);
				set_delete_subscription.add(map_buds_old.get(tmp.Id).Benutzer__c);
			}
			// Only if honorarumsatz_absolut__c changed
			if(map_buds_old.get(tmp.Id).honorarumsatz_absolut__c != tmp.honorarumsatz_absolut__c)
			{
				list_chatter_budgets.add(tmp);
				map_chatter_budgets_old_values.put(tmp.Id, map_buds_old.get(tmp.Id));
			}
		}

		User u = new User();
		try{u = [SELECT Id, FirstName, LastName FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
		System.debug('');}catch(Exception e){System.debug('### -Budget_Trigger-afterUpdate- getUserInfo:'+e);}

		String recordTypeId_sekundaer_budget;
		try{recordTypeId_sekundaer_budget = [SELECT Id FROM RecordType WHERE DeveloperName = 'sekundaer_budget' AND SobjectType='budget__c' LIMIT 1].Id;
		System.debug('');}catch(Exception e){System.debug('### -Budget_Trigger-afterUpdate- getSekundärBudgetRTid:'+e);}

		// prepare all relevant opportunitis for budgets where the benutzer__C has changed
		Set<String> relevant_oppIds = new Set<String>();
		for(Budget__c tmp:list_bud)
		{
			if(tmp.RecordTypeId == recordTypeId_sekundaer_budget)
			{
				//if(tmp.benutzer__c != u.Id)
				//{
					if(!relevant_oppIds.contains(tmp.Opportunity__c))
					{
						relevant_oppIds.add(tmp.Opportunity__c);
					}
				//}
			}
		}

		// Get all existing subscriptions for Opportunity Record --> so we dont create a duplicate
		List<EntitySubscription> list_existing_follows = new List<EntitySubscription>();
		Set<String> set_existing_follows = new Set<String>();
		List<EntitySubscription> list_delete_existing_follows = new List<EntitySubscription>();
		try{list_existing_follows = [SELECT Id, subscriberid FROM EntitySubscription WHERE parentId IN: relevant_oppIds];
		System.debug('');}catch(Exception e){System.debug('### -Budget_Trigger-afterUpdate- getExisitingSubscriptions:'+e);}
		System.debug('### -Budget_Trigger-afterUpdate- list_existing_follows:'+list_existing_follows);
		for(EntitySubscription tmp:list_existing_follows)
		{
			set_existing_follows.add(tmp.subscriberid);
			// Check if the Subscription should be deleted because its replaced
			if(set_delete_subscription.contains(tmp.subscriberid))
			{
				list_delete_existing_follows.add(tmp);
			}
		}

		// Prepare Subscriptions for insert
		List<EntitySubscription> list_follows = new List<EntitySubscription>();
		Set<String> set_subscriptions = new Set<String>();
			// Prepare set to get all User Emails and Opportunity Infos
			Set<String> set_subscriptions_userIds = new Set<String>();
			Set<String> set_subscriptions_oppIds = new Set<String>();
		for(Budget__c tmp:list_bud)
		{
			if(tmp.RecordTypeId == recordTypeId_sekundaer_budget)
			{
				//if(tmp.benutzer__c != u.Id)
				//{
					if(!set_subscriptions.contains(tmp.Opportunity__c+' '+tmp.benutzer__c) && !set_existing_follows.contains(tmp.benutzer__c))
					{
						EntitySubscription follow = new EntitySubscription (
																	 parentId = tmp.Opportunity__C,
																	 subscriberid = tmp.benutzer__c);
						list_follows.add(follow);
						set_subscriptions.add(tmp.Opportunity__c+' '+tmp.benutzer__c);
							// Prepare set to get all User Emails and Opportunity Infos
							set_subscriptions_userIds.add(tmp.benutzer__c);
							set_subscriptions_oppIds.add(tmp.Opportunity__c);
					}
				//}
			}
		}

		// insert subscriptions
		System.debug('### -Budget_Trigger-afterUpdate- list_follows: '+list_follows);
		try{upsert list_follows;
		System.debug('');}catch(Exception e){System.debug('### -Budget_Trigger-afterUpdate- upsertFollows:'+e);}
		// delete subscriptions
		System.debug('### -Budget_Trigger-afterUpdate- list_delete_existing_follows: '+list_delete_existing_follows);
		try{delete list_delete_existing_follows;
		System.debug('');}catch(Exception e){System.debug('### -Budget_Trigger-afterUpdate- deleteReplacedUserFollows:'+e);}



		// Send Info Email to Users
		List<User> list_user = new List<User>();
		Map<String, User> map_users = new Map<String, User>();
		List<Opportunity__c> list_opps = new List<Opportunity__c>();
		Map<String, Opportunity__c> map_opps = new Map<String, Opportunity__c>();
		List<Budget__c> list_budgets = new List<Budget__c>();
		Map<String, List<Budget__c>> map_budgets = new Map<String, List<Budget__c>>();
		try{list_user = [SELECT Id, FirstName, LastName, Email FROM User WHERE Id IN: set_subscriptions_userIds];
		System.debug('');}catch(Exception e){System.debug('### -Budget_Trigger-afterUpdate- getUserInfoForEmails:'+e);}
		try{list_opps = [SELECT Id, Name, Account__r.Name, Honorarumsatz__c FROM Opportunity__c WHERE Id IN: set_subscriptions_oppIds];
		System.debug('');}catch(Exception e){System.debug('### -Budget_Trigger-afterUpdate- getOppInfoForEmails:'+e);}
		try{list_budgets = [SELECT Id, RecordType.DeveloperName, Benutzer__c, Benutzer__r.FirstName, Benutzer__r.LastName,
			honorarumsatz_absolut__c, opportunity__c, Opportunity__r.Name, Opportunity__r.Account__r.Name,
			Benutzer__r.Email
			FROM Budget__c WHERE Opportunity__c IN: set_subscriptions_oppIds];
		System.debug('');}catch(Exception e){System.debug('### -Budget_Trigger-afterUpdate- getBudgetsInfoForEmails:'+e);}
		for(User tmp:list_user)
		{
			map_users.put(tmp.Id, tmp);
		}
		for(Opportunity__c tmp:list_opps)
		{
			map_opps.put(tmp.Id, tmp);
		}
		List<Budget__c> tmp_list;
		for(Budget__c tmp:list_budgets)
		{
			if(!map_budgets.containsKey(tmp.Opportunity__c))
			{
				tmp_list = new List<Budget__c>();
				tmp_list.add(tmp);
				map_budgets.put(tmp.Opportunity__c, tmp_list);
			}else{
				tmp_list = map_budgets.get(tmp.Opportunity__c);
				tmp_list.add(tmp);
				map_budgets.put(tmp.Opportunity__c, tmp_list);
			}
		}

		sendInfoEmail(map_users, map_opps, list_follows, u, map_budgets);

		if(isSpecialTest == false)
		{
			List<ConnectApi.BatchInput> batchInputs = new List<ConnectApi.BatchInput>();
			ConnectApi.BatchInput tmp_batchInput = new ConnectApi.BatchInput(new ConnectApi.FeedItemInput());
			// Do Chatter Posts for changed Budgets
			for(Budget__c tmp : list_chatter_budgets)
			{
				tmp_batchInput = new ConnectApi.BatchInput(new ConnectApi.FeedItemInput());
				//chatter_info_prepare_chatter_message erzeugt die eigentliche meldung
				String changed_budget = ''+tmp.honorarumsatz_absolut__c;
				changed_budget = changed_budget.replace('.', ',')+' €';
				String old_budget = ''+map_chatter_budgets_old_values.get(tmp.Id).honorarumsatz_absolut__c;
				old_budget = old_budget.replace('.', ',')+' €';
				tmp_batchInput = chatter_info_prepare_chatter_message(tmp.Benutzer__c, tmp.Opportunity__c,'Für die Opportunity hat sich das Budget geändert: \n alter Betrag lautet: '+old_budget+'\n neuer Betrag lautet: '+changed_budget);
				batchInputs.add(tmp_batchInput);
			}

			//postet die chatter meldungen
			ConnectApi.ChatterFeeds.postFeedElementBatch(Network.getNetworkId(), batchinputs);
		}

		//########################################################
	}



	public void writeUserIDinOppHelperField(List<Budget__c> list_bud, Map<String, Budget__c> map_old)
	{
		Map<String, Opportunity__c> map_opp_ids = new Map<String, Opportunity__c>();
		Map<String, List<Budget__c>> map_buds = new Map<String, List<Budget__c>>();
		Map<String, List<Budget__c>> map_buds_old = new Map<String, List<Budget__c>>();
		List<Budget__c> tmp_list;
		for(Budget__c tmp:list_bud)
		{
			if(tmp.Benutzer__c != map_old.get(tmp.Id).Benutzer__c)
			{
				if(map_buds.containskey(tmp.opportunity__c))
				{
					tmp_list = map_buds.get(tmp.opportunity__c);
					tmp_list.add(tmp);
					map_buds.put(tmp.opportunity__c, tmp_list);
				}else{
					tmp_list = new List<Budget__c>();
					tmp_list.add(tmp);
					map_buds.put(tmp.opportunity__c, tmp_list);
				}

				if(map_buds_old.containskey(tmp.opportunity__c))
				{
					tmp_list = map_buds_old.get(tmp.opportunity__c);
					tmp_list.add(map_old.get(tmp.Id));
					map_buds_old.put(tmp.opportunity__c, tmp_list);
				}else{
					tmp_list = new List<Budget__c>();
					tmp_list.add(map_old.get(tmp.Id));
					map_buds_old.put(tmp.opportunity__c, tmp_list);
				}

				if(!map_opp_ids.containsKEy(tmp.opportunity__c))
				{
					map_opp_ids.put(tmp.opportunity__c, new Opportunity__c(Id = tmp.opportunity__c));
				}
			}
		}
		List<opportunity__c> list_opps = new List<opportunity__c>();
		try{list_opps = [SELECT Id, zz_budget_user_ids__c
			FROM Opportunity__c WHERE Id IN: map_opp_ids.KeySet()];
		System.debug('');}catch(Exception e){System.debug('### -Budget_Trigger-writeUserIDinOppHelperField-update- getOpps:'+e);}
		for(opportunity__c tmp:list_opps)
		{
			if(map_buds.containsKey(tmp.Id)){
				for(Budget__c tmp2:map_buds.get(tmp.Id))
				{
					if(tmp.zz_budget_user_ids__c != null)
					{
						if(!tmp.zz_budget_user_ids__c.contains(tmp2.benutzer__c))
						{
							tmp.zz_budget_user_ids__c += ' '+tmp2.benutzer__c;
						}
					}
					else
					{
						tmp.zz_budget_user_ids__c = tmp2.benutzer__c;
					}
				}
			}
			if(map_buds_old.containsKey(tmp.Id))
			{
				for(Budget__c tmp2:map_buds_old.get(tmp.Id))
				{
					if(tmp.zz_budget_user_ids__c != null && tmp2.Benutzer__c != null)
					{
						if(tmp.zz_budget_user_ids__c.contains(tmp2.benutzer__c))
						{
							tmp.zz_budget_user_ids__c = tmp.zz_budget_user_ids__c.replace(tmp2.benutzer__c, '');
						}
					}
				}
			}
		}

		try{update list_opps;
		System.debug('');}catch(Exception e){System.debug('### -Budget_Trigger-writeUserIDinOppHelperField-update- updateOpps:'+e);}
	}



//*****************************************************************************************************************************************
//******************************************** DELETE *************************************************************************************
//*****************************************************************************************************************************************

	public void doAfterDelete(List<Budget__c> list_bud){
		deleteUserIDinOppHelperField(list_bud);

		User u = new User();
		try{u = [SELECT Id, FirstName, LastName FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
		System.debug('');}catch(Exception e){System.debug('### -Budget_Trigger-afterDelete- getUserInfo:'+e);}

		String recordTypeId_sekundaer_budget;
		try{recordTypeId_sekundaer_budget = [SELECT Id FROM RecordType WHERE DeveloperName = 'sekundaer_budget' AND SobjectType='budget__c' LIMIT 1].Id;
		System.debug('');}catch(Exception e){System.debug('### -Budget_Trigger-afterDelete- getSekundärBudgetRTid:'+e);}


		List<EntitySubscription> list_follows = new List<EntitySubscription>();
		Set<String> set_subscriptions = new Set<String>();
		// prepare all relevant opportunitis for budgets
		Set<String> relevant_oppIds = new Set<String>();
		Set<String> relevant_benutzerIds = new Set<String>();
		for(Budget__c tmp:list_bud)
		{
			if(tmp.RecordTypeId == recordTypeId_sekundaer_budget)
			{
					if(!relevant_oppIds.contains(tmp.Opportunity__c))
					{
						relevant_oppIds.add(tmp.Opportunity__c);
					}
					if(!relevant_benutzerIds.contains(tmp.Benutzer__c))
					{
						relevant_benutzerIds.add(tmp.Benutzer__c);
					}
					if(!set_subscriptions.contains(tmp.Opportunity__c+' '+tmp.benutzer__c))
					{
						EntitySubscription follow = new EntitySubscription (
																	 parentId = tmp.Opportunity__C,
																	 subscriberid = tmp.benutzer__c);
						list_follows.add(follow);
						set_subscriptions.add(tmp.Opportunity__c+' '+tmp.benutzer__c);
					}
			}
		}

		// Get all existing subscriptions for Opportunity Record --> so we dont create a duplicate
		List<EntitySubscription> list_existing_follows = new List<EntitySubscription>();
		try{list_existing_follows = [SELECT Id, subscriberid, parentId FROM EntitySubscription WHERE parentId IN: relevant_oppIds AND subscriberid IN: relevant_benutzerIds];
		System.debug('');}catch(Exception e){System.debug('### -Budget_Trigger-afterDelete- getExisitingSubscriptions:'+e);}
		System.debug('### -Budget_Trigger-afterDelete- list_existing_follows:'+list_existing_follows);

		// delete subscriptions
		try{delete list_existing_follows;
		System.debug('');}catch(Exception e){System.debug('### -Budget_Trigger-afterDelete- deleteFollows:'+e);}

		// Send Info Email to Users
		List<User> list_user = new List<User>();
		Map<String, User> map_users = new Map<String, User>();
		List<Opportunity__c> list_opps = new List<Opportunity__c>();
		Map<String, Opportunity__c> map_opps = new Map<String, Opportunity__c>();
		List<Budget__c> list_budgets = new List<Budget__c>();
		Map<String, List<Budget__c>> map_budgets = new Map<String, List<Budget__c>>();
		try{list_user = [SELECT Id, FirstName, LastName, Email FROM User WHERE Id IN: relevant_benutzerIds];
		System.debug('');}catch(Exception e){System.debug('### -Budget_Trigger-afterDelete- getUserInfoForEmails:'+e);}
		try{list_opps = [SELECT Id, Name, Account__r.Name, Honorarumsatz__c FROM Opportunity__c WHERE Id IN: relevant_oppIds];
		System.debug('');}catch(Exception e){System.debug('### -Budget_Trigger-afterDelete- getOppInfoForEmails:'+e);}
		try{list_budgets = [SELECT Id, RecordType.DeveloperName, Benutzer__c, Benutzer__r.FirstName, Benutzer__r.LastName,
			honorarumsatz_absolut__c, opportunity__c, Opportunity__r.Name, Opportunity__r.Account__r.Name,
			Benutzer__r.Email
			FROM Budget__c WHERE Opportunity__c IN: relevant_oppIds];
		System.debug('');}catch(Exception e){System.debug('### -Budget_Trigger-afterDelete- getBudgetsInfoForEmails:'+e);}
		for(User tmp:list_user)
		{
			map_users.put(tmp.Id, tmp);
		}
		for(Opportunity__c tmp:list_opps)
		{
			map_opps.put(tmp.Id, tmp);
		}
		List<Budget__c> tmp_list;
		for(Budget__c tmp:list_budgets)
		{
			if(!map_budgets.containsKey(tmp.Opportunity__c))
			{
				tmp_list = new List<Budget__c>();
				tmp_list.add(tmp);
				map_budgets.put(tmp.Opportunity__c, tmp_list);
			}else{
				tmp_list = map_budgets.get(tmp.Opportunity__c);
				tmp_list.add(tmp);
				map_budgets.put(tmp.Opportunity__c, tmp_list);
			}
		}

		sendInfoEmail(map_users, map_opps, list_follows, u, map_budgets);
}

public void deleteUserIDinOppHelperField(List<Budget__c> list_bud)
{
	Map<String, Opportunity__c> map_opp_ids = new Map<String, Opportunity__c>();
	Map<String, List<Budget__c>> map_buds = new Map<String, List<Budget__c>>();
	List<Budget__c> tmp_list;
	for(Budget__c tmp:list_bud)
	{
			if(map_buds.containskey(tmp.opportunity__c))
			{
				tmp_list = map_buds.get(tmp.opportunity__c);
				tmp_list.add(tmp);
				map_buds.put(tmp.opportunity__c, tmp_list);
			}else{
				tmp_list = new List<Budget__c>();
				tmp_list.add(tmp);
				map_buds.put(tmp.opportunity__c, tmp_list);
			}

			if(!map_opp_ids.containsKEy(tmp.opportunity__c))
			{
				map_opp_ids.put(tmp.opportunity__c, new Opportunity__c(Id = tmp.opportunity__c));
			}
	}
	List<opportunity__c> list_opps = new List<opportunity__c>();
	try{list_opps = [SELECT Id, zz_budget_user_ids__c
		FROM Opportunity__c WHERE Id IN: map_opp_ids.KeySet()];
	System.debug('');}catch(Exception e){System.debug('### -Budget_Trigger-writeUserIDinOppHelperField-delete- getOpps:'+e);}
	for(opportunity__c tmp:list_opps)
	{
		if(map_buds.containsKey(tmp.Id)){
			for(Budget__c tmp2:map_buds.get(tmp.Id))
			{
				if(tmp.zz_budget_user_ids__c != null && tmp2.Benutzer__c != null)
				{
					if(tmp.zz_budget_user_ids__c.contains(tmp2.benutzer__c))
					{
						tmp.zz_budget_user_ids__c = tmp.zz_budget_user_ids__c.replace(tmp2.benutzer__c, '');
					}
				}
			}
		}
	}

	try{update list_opps;
	System.debug('');}catch(Exception e){System.debug('### -Budget_Trigger-writeUserIDinOppHelperField-delete- updateOpps:'+e);}
}


//*****************************************************************************************************************************************
//******************************************** UNDELETE *************************************************************************************
//*****************************************************************************************************************************************

	public void doAfterUnDelete(List<Budget__c> list_bud){
		for(Budget__c tmp:list_bud)
		{
			tmp.addError('Gelöschte Budget Datensätze können nicht wieder hergestellt werden!');
		}
	}



	public void sendInfoEmail(Map<String, User> map_users, Map<String, Opportunity__c> map_opps, List<EntitySubscription> list_follows, User u, Map<String, List<Budget__c>> map_budgets){
		// Get the domain for the opp link
		String urlInstance = String.valueof(System.URL.getSalesforceBaseURL()).replace('Url:[delegate=','').replace(']','');
		String[] domain = urlInstance.split('\\.');
		String puffer = domain[0];
		String theDomain = puffer.substring(0, puffer.length()-3);
		System.debug('### -Budget_Trigger-sendInfoEmail- theDomain: '+theDomain);

		// Save Emails in Map for sending only 1 email for opp
		Map<String, wrapper_mail> map_mails = new Map<String, wrapper_mail>();

		List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
		for(EntitySubscription tmp:list_follows)
		{
			String hu_gesamt = ''+map_opps.get(tmp.parentId).Honorarumsatz__c;
			hu_gesamt = hu_gesamt.replace('.', ',')+' €';

			// Prepare Budget Info Splitted
			String budget_amounts_splitted = '';
			Decimal primaer_summe = map_opps.get(tmp.parentId).Honorarumsatz__c;
			String primaer_string = '';
			Budget__c primaer_budget_info = new Budget__c();
			for(Budget__c tmp2:map_budgets.get(tmp.parentId))
			{
				if(tmp2.RecordType.DeveloperName == 'sekundaer_budget')
				{
					String hu_split = ''+tmp2.honorarumsatz_absolut__c;
					hu_split = hu_split.replace('.', ',')+' €';
					budget_amounts_splitted += '<br/>Sekundär: '+tmp2.Benutzer__r.FirstName+' '+tmp2.Benutzer__r.LastName+': '+hu_split+'';
					primaer_summe -= tmp2.honorarumsatz_absolut__c;
				}else if(tmp2.RecordType.DeveloperName == 'primaer_budget'){
					primaer_string = '<br/>Primär: '+tmp2.Benutzer__r.FirstName+' '+tmp2.Benutzer__r.LastName+': ';
					primaer_budget_info = tmp2;
				}
			}
			String primaer_summe_string = ''+primaer_summe;
			primaer_summe_string = primaer_summe_string.replace('.', ',')+' €';
			budget_amounts_splitted = primaer_string+primaer_summe_string+budget_amounts_splitted;

			List<String> toAddresses = new List<String>();
			if(map_users.get(tmp.subscriberid) != null)
			{
				toAddresses.add(''+map_users.get(tmp.subscriberid).Email);
			}

			Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
			mail.setSaveAsActivity(false);
			mail.setUseSignature(true);
			mail.setBccSender(false);
			mail.setToAddresses(toAddresses);
			if(map_users.get(tmp.subscriberid) != null){
				mail.setReplyTo(''+map_users.get(tmp.subscriberid).Email);
			}
			mail.setSubject('Budget Info für Opportunity: '+map_opps.get(tmp.parentId).Name);
			mail.setHtmlBody(''
			+'<p>Zur Info:</p><p>Opportunity: <a href="'+theDomain+'.lightning.force.com/one/one.app#/sObject/'
			+map_opps.get(tmp.parentId).Id+'/view">'+map_opps.get(tmp.parentId).Name+'</a><br/>Firma: '
			+map_opps.get(tmp.parentId).Account__r.Name+'<br/>Honorarumsatz Gesamt: '+hu_gesamt
			+'<br/> <b>Verteilung:</b>'
			+budget_amounts_splitted
			+'<br/><br/>Geändert von: '+u.FirstName+' '+u.LastName+'</p>');
			mail.setSenderDisplayName('fischerAppelt - Salesforce System');
			System.debug('### Email: '+mail);

			if(!map_mails.containsKey(tmp.parentId))
			{
				wrapper_mail tmp_wm = new wrapper_mail();
				tmp_wm.mail = mail;
				tmp_wm.toAddresses = toAddresses;
				tmp_wm.primaer_toAddress = primaer_budget_info.Benutzer__r.Email;
				map_mails.put(tmp.parentId, tmp_wm);
			}else{
				wrapper_mail tmp_wm = map_mails.get(tmp.parentId);
				tmp_wm.toAddresses.addAll(toAddresses);
				map_mails.put(tmp.parentId, tmp_wm);
			}
		}

		for(Opportunity__c tmp:map_opps.values())
		{
			if(map_mails.containsKEy(tmp.Id))
			{
				Messaging.SingleEmailMessage mail = map_mails.get(tmp.Id).mail;
				List<String> toAddresses = map_mails.get(tmp.Id).toAddresses;
				toAddresses.add(''+map_mails.get(tmp.Id).primaer_toAddress);
				mail.setToAddresses(toAddresses);
				allmsg.add(mail);
			}
		}

		List<Messaging.SendEmailResult> results = new List<Messaging.SendEmailResult>();
		results = Messaging.sendEmail(allmsg,false);
		for(Messaging.SendEmailResult result: results)
		{
				if(!result.isSuccess()) {
					System.debug('### Failed: '+result);
				}
		}
	}

	public class wrapper_mail{
		public Messaging.SingleEmailMessage mail {get;set;}
		public List<String> toAddresses {get;set;}
		public String primaer_toAddress {get;set;}
	}

	/***************************************************************************************
			Erweiterung für Chatter Info, erzeugt den Chatter Eintrag
	***************************************************************************************/
	public ConnectApi.BatchInput chatter_info_prepare_chatter_message(String mention_user_id, String record_Id, String short_text)
	{

		ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
		ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
		ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
		ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();

		messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
		//Mention user here
		mentionSegmentInput.id = mention_user_id;
		messageBodyInput.messageSegments.add(mentionSegmentInput);

		textSegmentInput.text = '\n'+short_text;
		messageBodyInput.messageSegments.add(textSegmentInput);

		feedItemInput.body = messageBodyInput;
		feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
		// datensatz an dem der chatter post hängt
		feedItemInput.subjectId = record_Id;

		ConnectApi.BatchInput batchInput = new ConnectApi.BatchInput(feedItemInput);

		return batchInput;
	}


	public static boolean run = true;
	public static boolean runOnce()
	{
			if(run)
			{
			 run=false;
			 system.debug('### runonce = ' + run);
			 return true;
			}
			else
			{
					system.debug('### runonce = ' + run);
					return run;
			}
	}

}