@isTest
private class Team_Management_Ext_Test {

	@isTest(SeeAllData=true) static void test_method_one() {
		Account tmp_acc = new Account();
		tmp_acc.Industry = 'Test';
		tmp_acc.Name = 'Test';

		insert tmp_acc;

		Team_Member__c tmp_tm = new Team_Member__c();
		tmp_tm.Account__c = tmp_acc.Id;
		tmp_tm.User__c = UserInfo.getUserId();
		tmp_tm.verantwortlich__c = true;

		insert tmp_tm;

		ApexPages.StandardController standard1 = new ApexPages.StandardController(tmp_acc);
		Team_Management_Ext con = new Team_Management_Ext(standard1);
		con.addNewMember();
		con.selected_member = '1';
		con.deleteMember_toggle();
		con.selected_member = '0';
		con.deleteMember_toggle();
		con.deleteMember_toggle();
		con.deleteMember();
		con.addNewMember();
		con.back();
		con.addNewMember();
		con.selected_member = '0';
		con.save();
		con.list_tm.add(new Team_Member__c());
		System.debug('### con.list_tm: '+con.list_tm);
		con.list_tm[1].User__c = UserInfo.getUserId();
		con.save();
	}
}