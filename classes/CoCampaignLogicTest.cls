/**************************************************************************
Company: aquilliance GmbH
Author: Simon Kuhnt
CreatedDate: 2017-08
**************************************************************************/
@isTest
private class CoCampaignLogicTest {

	@isTest static void createAndDeleteFollowingRecords() {
		Kampagne__c kampagne = new Kampagne__c(
		Name = 'kampagne1'
		);
		insert kampagne;

		Team_Member__c teamMemberForKampaign = new Team_Member__c();
		teamMemberForKampaign.Kampagne__c = kampagne.Id;
		teamMemberForKampaign.User__c = UserInfo.getUserId();
		insert teamMemberForKampaign;

		Team_Member__c teamMemberWithoutParent = new Team_Member__c();
		teamMemberWithoutParent.User__c = UserInfo.getUserId();
		insert teamMemberWithoutParent;


		kampagne.status__c = 'aktiv';
		update kampagne;
		System.assertNotEquals('',[SELECT Id FROM EntitySubscription WHERE ParentId =: teamMemberForKampaign.Kampagne__c AND Subscriberid =: teamMemberForKampaign.User__c LIMIT 1].Id,'Error: Insert teamMember for Kampaign doesnt work how it is supposed to.');
		kampagne.status__c = 'verworfen';
		update kampagne;
		System.assertEquals(new List<EntitySubscription>(),[SELECT Id FROM EntitySubscription WHERE ParentId =: teamMemberForKampaign.Kampagne__c AND Subscriberid =: teamMemberForKampaign.User__c LIMIT 1],'Error: Delete teamMember for Kampaign doesnt work how it is supposed to.');
	}
}