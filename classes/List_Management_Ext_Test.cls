/**************************************************************************
Company: aquilliance GmbH
Author: Andreas Wolf
CreatedDate: 2017-1

**************************************************************************/
@isTest
private class List_Management_Ext_Test {

	@isTest static void test_method_one() {
		Account tmp_acc = new Account();
		tmp_acc.Industry = 'Test';
		tmp_acc.Name = 'Test';

		insert tmp_acc;

		Team_Member__c tmp_tm = new Team_Member__c();
		tmp_tm.Account__c = tmp_acc.Id;
		tmp_tm.User__c = UserInfo.getUserId();

		insert tmp_tm;

		Contact tmp_con = new Contact();
		tmp_con.AccountId = tmp_acc.Id;
		tmp_con.FirstName = 'Max';
		tmp_con.LastName = 'Mustermann';
		tmp_con.Salutation = 'Mr.';
		tmp_con.Department = 'Test';
		tmp_con.Title = 'Test';

		insert tmp_con;

		Contact tmp_con2 = new Contact();
		tmp_con2.AccountId = tmp_acc.Id;
		tmp_con2.FirstName = 'Max2';
		tmp_con2.LastName = 'Mustermann2';
		tmp_con2.Salutation = 'Mr.';

		insert tmp_con2;
		Contact tmp_con3 = new Contact();
		tmp_con3.AccountId = tmp_acc.Id;
		tmp_con3.FirstName = 'Max3';
		tmp_con3.LastName = 'Mustermann3';
		tmp_con3.Salutation = 'Mr.';

		insert tmp_con3;
		Contact tmp_con4 = new Contact();
		tmp_con4.AccountId = tmp_acc.Id;
		tmp_con4.FirstName = 'Max4';
		tmp_con4.LastName = 'Mustermann4';
		tmp_con4.Salutation = 'Mr.';

		insert tmp_con4;

		Contact tmp_con5 = new Contact();
		tmp_con5.AccountId = tmp_acc.Id;
		tmp_con5.FirstName = 'Max5';
		tmp_con5.LastName = 'Mustermann5';
		tmp_con5.Salutation = 'Mr.';

		insert tmp_con5;

		Contact tmp_con6 = new Contact();
		tmp_con6.AccountId = tmp_acc.Id;
		tmp_con6.FirstName = 'Max6';
		tmp_con6.LastName = 'Mustermann6';
		tmp_con6.Salutation = 'Mr.';

		insert tmp_con6;

		Mandant__c tmp_man = new Mandant__c();
		tmp_man.Name = 'Test';
		insert tmp_man;

		String standardRT = [SELECT id FROM RecordType WHERE sObjectType = 'List__c' AND DeveloperName = 'Standard' LIMIT 1].Id;
		String internRT = [SELECT id FROM RecordType WHERE sObjectType = 'List__c' AND DeveloperName = 'Intern' LIMIT 1].Id;
		String externRT = [SELECT id FROM RecordType WHERE sObjectType = 'List__c' AND DeveloperName = 'Extern' LIMIT 1].Id;
		String privateRT = [SELECT id FROM RecordType WHERE sObjectType = 'List__c' AND DeveloperName = 'Private' LIMIT 1].Id;

		List<List__c> list_folders = new List<List__c>();
		List__c tmp_list = new List__c();
		tmp_list.Name = 'Something';
		tmp_list.RecordTypeId = internRT;
		tmp_list.Mandant__c = tmp_man.Id;
		list_folders.add(tmp_list);
		List__c tmp_list2 = new List__c();
		tmp_list2.Name = 'Something2';
		tmp_list2.Account__c = tmp_acc.Id;
		tmp_list2.RecordTypeId = externRT;
		list_folders.add(tmp_list2);
		List__c tmp_list3 = new List__c();
		tmp_list3.Name = 'Something3';
		tmp_list3.RecordTypeId = privateRT;
		list_folders.add(tmp_list3);
		insert list_folders;

		List<List__c> list_lists = new List<List__c>();
		List__c tmp_list4 = new List__c();
		tmp_list4.Name = 'Aomething';
		tmp_list4.Folder__c = tmp_list.Id;
		tmp_list4.RecordTypeId = standardRT;
		list_lists.add(tmp_list4);
		List__c tmp_list4_2 = new List__c();
		tmp_list4_2.Name = 'Something';
		tmp_list4_2.Folder__c = tmp_list.Id;
		tmp_list4_2.RecordTypeId = standardRT;
		list_lists.add(tmp_list4_2);
		List__c tmp_list5 = new List__c();
		tmp_list5.Name = 'Something';
		tmp_list5.Folder__c = tmp_list2.Id;
		tmp_list5.RecordTypeId = standardRT;
		list_lists.add(tmp_list5);
		List__c tmp_list5_2 = new List__c();
		tmp_list5_2.Name = 'Something';
		tmp_list5_2.Folder__c = tmp_list2.Id;
		tmp_list5_2.RecordTypeId = standardRT;
		list_lists.add(tmp_list5_2);
		List__c tmp_list6 = new List__c();
		tmp_list6.Name = 'Something';
		tmp_list6.Folder__c = tmp_list3.Id;
		tmp_list6.RecordTypeId = standardRT;
		list_lists.add(tmp_list6);
		List__c tmp_list6_2 = new List__c();
		tmp_list6_2.Name = 'Something';
		tmp_list6_2.Folder__c = tmp_list3.Id;
		tmp_list6_2.RecordTypeId = standardRT;
		list_lists.add(tmp_list6_2);

		insert list_lists;

		List<List_Member__c> list_lm = new List<List_Member__c>();
		List_Member__c tmp_lm = new List_Member__c();
		tmp_lm.Contact__c = tmp_con.Id;
		tmp_lm.List__c = tmp_list4.Id;
		list_lm.add(tmp_lm);

		List_Member__c tmp_lm2 = new List_Member__c();
		tmp_lm2.Contact__c = tmp_con.Id;
		tmp_lm2.List__c = tmp_list4_2.Id;
		list_lm.add(tmp_lm2);

		List_Member__c tmp_lm3 = new List_Member__c();
		tmp_lm3.Contact__c = tmp_con.Id;
		tmp_lm3.List__c = tmp_list5.Id;
		list_lm.add(tmp_lm3);

		List_Member__c tmp_lm4 = new List_Member__c();
		tmp_lm4.Contact__c = tmp_con.Id;
		tmp_lm4.List__c = tmp_list5_2.Id;
		list_lm.add(tmp_lm4);

		List_Member__c tmp_lm5 = new List_Member__c();
		tmp_lm5.Contact__c = tmp_con.Id;
		tmp_lm5.List__c = tmp_list6.Id;
		list_lm.add(tmp_lm5);

		List_Member__c tmp_lm6 = new List_Member__c();
		tmp_lm6.Contact__c = tmp_con.Id;
		tmp_lm6.List__c = tmp_list6_2.Id;
		list_lm.add(tmp_lm6);

		insert list_lm;

		ApexPages.StandardController standard1 = new ApexPages.StandardController(tmp_acc);
		List_Management_Ext con = new List_Management_Ext(standard1);
		con.goToListManagement_Acc();

		ApexPages.StandardController standard2 = new ApexPages.StandardController(tmp_con);
		List_Management_Ext con2 = new List_Management_Ext(standard2);
		con2.goToListManagement_Con();

		ApexPages.StandardController standard3 = new ApexPages.StandardController(tmp_man);
		List_Management_Ext con3 = new List_Management_Ext(standard3);
		con3.goToListManagement_Man();

	}
}