/**************************************************************************
Company: aquilliance GmbH
Author: Andreas Wolf
CreatedDate: 2017-1

**************************************************************************/
global class account_status_check_batch implements Database.Batchable<sObject> {

	String query;

	global account_status_check_batch() {
		query = 'SELECT Id, Name, sperrgrund__c FROM Account';
	}

	global Database.QueryLocator start(Database.BatchableContext BC){
		 return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext BC, List<Account> list_acc){
		// Prepare a Map with all AccountIds + Accounts
		Map<String, Account> map_accounts = new Map<String, Account>();
		for(Account tmp:list_acc)
		{
			map_accounts.put(tmp.Id, tmp);
		}

		// Get All Vertragsbeziehungen for relevant AccountIds
		List<Vertragsbeziehung__c> list_vb = new List<Vertragsbeziehung__c>();
		try{list_vb = [SELECT Id, Account__c, Vertragsbeziehung__c FROM Vertragsbeziehung__c WHERE Account__c IN: map_accounts.keyset()];
		System.debug('');}catch(Exception e){System.debug('### -account_status_check_batch- getVertragsbeziehungen: '+e);}

		// Prepare a Map with all AccountIds + related Vertragsbeziehungen
		Map<String, List<Vertragsbeziehung__c>> map_vb = new Map<String, List<Vertragsbeziehung__c>>();
		List<Vertragsbeziehung__c> tmp_list;
		for(Vertragsbeziehung__c tmp:list_vb)
		{
			if(!map_vb.containsKey(tmp.Account__C))
			{
				tmp_list = new List<Vertragsbeziehung__c>();
				tmp_list.add(tmp);
				map_vb.put(tmp.Account__c, tmp_list);
			}
			else
			{
				tmp_list = map_vb.get(tmp.Account__c);
				tmp_list.add(tmp);
				map_vb.put(tmp.Account__c, tmp_list);
			}
		}

		// Set Account Status
		for(Account tmp:list_acc)
		{
			System.debug('### tmp.sperrgrund__c: '+tmp.sperrgrund__c);
			if(tmp.sperrgrund__c == '' || tmp.sperrgrund__c == null)
			{
				if(map_vb.containsKey(tmp.Id))
				{
					// Check all Vertragbeziehungen for an active one
					Boolean isActive = false;
					for(Vertragsbeziehung__c tmp2:map_vb.get(tmp.Id))
					{
						if(tmp2.Vertragsbeziehung__c == 'Aktiv')
						{
							isActive = true;
						}
					}
					// If at least one is active --> Set Status to "Aktiver Kunde"
					if(isActive)
					{
						tmp.account_status__c = 'Aktiver Kunde';
					}
					else
					{
						// If no one is active --> Set Status to "Inaktiver Kunde"
						tmp.account_status__c = 'Inaktiver Kunde';
					}
				}
				else
				{
					// IF no Vertragsbeziehungen found --> Set Status to "Potenzieller Kunde"
					tmp.account_status__c = 'Potentieller Kunde';
				}
			}
			else
			{
				// IF Account sperrgrund__c not blank --> Set Status to "Gesperrter Kunde"
				tmp.account_status__c = 'Gesperrter Kunde';
			}
		}

		// try to update all accounts
		//update list_acc; // nor error handling
		Database.SaveResult[] srList = Database.update(list_acc, false);

		List<sObject> list_acc_update_try_2 = new List<sObject>();
			// Iterate through each returned result
			Integer i = 0;
			for (Database.SaveResult sr : srList)
			{
					if (sr.isSuccess()) {
							// Operation was successful
					}
					else {
							// Operation failed, so get all errors
							for(Database.Error err : sr.getErrors()) {
									System.debug('#### The following error has occurred.');
									System.debug('####'+err.getStatusCode() + ': ' + err.getMessage());
									System.debug('#### Account fields that affected this error: ' + err.getFields());
							}
							/****************************/
							list_acc_update_try_2.add(list_acc[i]);
					}
					i++;
			}
			try
			{
				update list_acc_update_try_2;
			}
			catch(Exception e)
			{
				System.debug('### -account_status_check_batch- Acc Update retry error'+e);
			}
	}

	global void finish(Database.BatchableContext BC){
		System.debug('### -account_status_check_batch- FINISHED!');
	}


}