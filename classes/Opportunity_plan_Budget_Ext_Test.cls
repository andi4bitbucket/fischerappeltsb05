/***************************************************************************
Company: aquilliance GmbH
Author: Andre Mergen
CreatedDate: 2016-12

***************************************************************************/
@isTest(SeeAllData=true)
private class Opportunity_plan_Budget_Ext_Test {

	/********************************************************
				create test Records
	*******************************************************
	@testSetup static void TheSetup()
	{
		Account tmp_Acc = new Account();
		tmp_Acc.Name = 'Test AQ';
		tmp_Acc.BillingStreet = 'Högerdamm 41';
		tmp_Acc.BillingCity = 'Hamburg';
		tmp_Acc.BillingPostalCode = '20097';
		tmp_Acc.BillingCountry = 'Germany';

		insert tmp_Acc;


		opportunity__c tmp_opp = new opportunity__c();
		tmp_opp.Name = 'TestOpp';
		tmp_opp.account__c = tmp_Acc.Id;
		tmp_opp.honorarumsatz__c = 1000.00;
		tmp_opp.projektstart__c = Date.Today();
		tmp_opp.projektende__c = Date.Today()+95;

		insert tmp_opp;
	}
*/

	@isTest static void test_method_with_existing()
	{
		Opportunity_Trigger.run = false;

		Test.StartTest();
		Account tmp_Acc = new Account();
		tmp_Acc.Name = 'Test AQ';
		tmp_Acc.BillingStreet = 'Högerdamm 41';
		tmp_Acc.BillingCity = 'Hamburg';
		tmp_Acc.BillingPostalCode = '20097';
		tmp_Acc.BillingCountry = 'Germany';

		insert tmp_Acc;


		opportunity__c tmp_opp = new opportunity__c();
		tmp_opp.Name = 'TestOpp';
		tmp_opp.account__c = tmp_Acc.Id;
		tmp_opp.honorarumsatz__c = 1000.00;
		tmp_opp.projektstart__c = Date.Today();
		tmp_opp.projektende__c = Date.Today()+95;

		insert tmp_opp;


		//Account tmp_acc = [SELECT Id, Name FROM Account WHERE Name = 'Test AQ' LIMIT 1];
		//opportunity__c tmp_opp = [SELECT Id, Name, account__c, honorarumsatz__c, OwnerId, projektstart__c, projektende__c FROM opportunity__c WHERE account__c =: tmp_acc.Id LIMIT 1];

		Integer nr_of_month = tmp_opp.projektstart__c.monthsBetween(tmp_opp.projektende__c)+1;

		/*########################################*/

		String RecordTypeId_sekundaer_budget = null;
		String RecordTypeId_monats_budget = null;

		budget__c tmp_bg = new budget__c();
		budget__c tmp_cbg = new budget__c(); // monats budget
		List<budget__c> tmp_list_cbg = new List<budget__c>();

		Date start_date = tmp_opp.projektstart__c.toStartOfMonth();



		try{
			RecordTypeId_sekundaer_budget = [SELECT Id FROM RecordType WHERE DeveloperName = 'primaer_budget' AND SobjectType='budget__c' LIMIT 1].Id;
		System.debug('');}catch(Exception e){System.debug('### error RecordTypeId_sekundaer_budget:'+e);}

		try{
			RecordTypeId_monats_budget = [SELECT Id FROM RecordType WHERE DeveloperName = 'monats_budget' AND SobjectType='budget__c' LIMIT 1].Id;
		System.debug('');}catch(Exception e){System.debug('### error RecordTypeId_monats_budget:'+e);}
		/***************************************/
		//neues budget
		tmp_bg.RecordTypeId = RecordTypeId_sekundaer_budget;
		tmp_bg.opportunity__c = tmp_opp.Id;
		tmp_bg.account__c = tmp_opp.account__c;
		tmp_bg.honorarumsatz_absolut__c = tmp_opp.honorarumsatz__c;
		tmp_bg.benutzer__c = tmp_opp.OwnerId;

		insert tmp_bg;

		Test.StopTest();

		//##########################################################
		Integer i = 0;
		for(i=0; i<nr_of_month;i++)
		{
			tmp_cbg = new budget__c(); // monats budget
			tmp_cbg.budget__c = tmp_bg.Id;
			tmp_cbg.RecordTypeId = RecordTypeId_monats_budget;
			tmp_cbg.honorarumsatz_absolut__c = (tmp_bg.honorarumsatz_absolut__c/nr_of_month);
			tmp_cbg.betreffender_monat__c = start_date.addMonths(i);
			tmp_cbg.Name = start_date.year() +'-'+ start_date.Month();

			tmp_list_cbg.add(tmp_cbg);
		}

		insert tmp_list_cbg;


		/*########################################*/

		// Controller Test
		PageReference pref = Page.Opportunity_plan_Budget;
		pref.getParameters().put('Id',tmp_opp.Id);
		Test.setCurrentPage(pref);

		ApexPages.StandardController opp = new ApexPages.StandardController(tmp_opp);
		Opportunity_plan_Budget_Ext opb = new Opportunity_plan_Budget_Ext(opp);

		opb.btn_nix();
	}


	@isTest static void test_method_with_one()
	{

		Test.StartTest();
		Account tmp_Acc = new Account();
		tmp_Acc.Name = 'Test AQ';
		tmp_Acc.BillingStreet = 'Högerdamm 41';
		tmp_Acc.BillingCity = 'Hamburg';
		tmp_Acc.BillingPostalCode = '20097';
		tmp_Acc.BillingCountry = 'Germany';

		insert tmp_Acc;


		opportunity__c tmp_opp = new opportunity__c();
		tmp_opp.Name = 'TestOpp';
		tmp_opp.account__c = tmp_Acc.Id;
		tmp_opp.honorarumsatz__c = 1000.00;
		tmp_opp.projektstart__c = Date.Today();
		tmp_opp.projektende__c = Date.Today()+95;

		insert tmp_opp;
		Test.StopTest();


		//Account tmp_acc = [SELECT Id, Name FROM Account WHERE Name = 'Test AQ' LIMIT 1];
		//opportunity__c tmp_opp = [SELECT Id, Name, account__c FROM opportunity__c WHERE account__c =: tmp_acc.Id LIMIT 1];

		// Controller Test
		PageReference pref = Page.Opportunity_plan_Budget;
		pref.getParameters().put('Id',tmp_opp.Id);
		Test.setCurrentPage(pref);

		ApexPages.StandardController opp = new ApexPages.StandardController(tmp_opp);
		Opportunity_plan_Budget_Ext opb = new Opportunity_plan_Budget_Ext(opp);

		opb.btn_nix();

	}

	@isTest static void test_method_new_and_save()
	{
		Test.StartTest();
		Opportunity_Trigger.run = false;

		Account tmp_Acc = new Account();
		tmp_Acc.Name = 'Test AQ';
		tmp_Acc.BillingStreet = 'Högerdamm 41';
		tmp_Acc.BillingCity = 'Hamburg';
		tmp_Acc.BillingPostalCode = '20097';
		tmp_Acc.BillingCountry = 'Germany';

		insert tmp_Acc;


		opportunity__c tmp_opp = new opportunity__c();
		tmp_opp.Name = 'TestOpp';
		tmp_opp.account__c = tmp_Acc.Id;
		tmp_opp.honorarumsatz__c = 1000.00;
		tmp_opp.projektstart__c = Date.Today();
		tmp_opp.projektende__c = Date.Today()+95;

		insert tmp_opp;
		Test.StopTest();

		Budget_Trigger.run = false;
		budget__c tmp_budget = new budget__c();
		//Account tmp_acc = [SELECT Id, Name FROM Account WHERE Name = 'Test AQ' LIMIT 1];
		//opportunity__c tmp_opp = [SELECT Id, Name, account__c FROM opportunity__c WHERE account__c =: tmp_acc.Id LIMIT 1];
		User currUser = [SELECT Id, ProfileId FROM User WHERE Id=: UserInfo.getUserId()];

		// Controller Test
		PageReference pref = Page.Opportunity_plan_Budget;
		pref.getParameters().put('Id',tmp_opp.Id);
		Test.setCurrentPage(pref);


		ApexPages.StandardController opp = new ApexPages.StandardController(tmp_opp);
		Opportunity_plan_Budget_Ext opb = new Opportunity_plan_Budget_Ext(opp);

		opb.add_new();

		tmp_budget = opb.list_wrapper_budgets[1].budget;
		tmp_budget.honorarumsatz_absolut__c = 500.00;
		tmp_budget.benutzer__c = currUser.Id;

		opb.calc_summe_budget_update_primary();

		opb.row_to_reset_linear = '1';
		opb.one_linear();

		opb.btn_edit_mode();
		opb.btn_show_save_budget_popup();
		opb.save_now();
		opb.calc_prozent();

		opb.delete_all_childs();
		opb.cancel_edit();
	}


	@isTest static void test_method_modify_month()
	{
		Test.StartTest();
		Account tmp_Acc = new Account();
		tmp_Acc.Name = 'Test AQ';
		tmp_Acc.BillingStreet = 'Högerdamm 41';
		tmp_Acc.BillingCity = 'Hamburg';
		tmp_Acc.BillingPostalCode = '20097';
		tmp_Acc.BillingCountry = 'Germany';

		insert tmp_Acc;


		opportunity__c tmp_opp = new opportunity__c();
		tmp_opp.Name = 'TestOpp';
		tmp_opp.account__c = tmp_Acc.Id;
		tmp_opp.honorarumsatz__c = 1000.00;
		tmp_opp.projektstart__c = Date.Today();
		tmp_opp.projektende__c = Date.Today()+95;

		insert tmp_opp;
		Test.StopTest();

		budget__c tmp_budget = new budget__c();
		budget__c tmp_budget_month = new budget__c();
		//Account tmp_acc = [SELECT Id, Name FROM Account WHERE Name = 'Test AQ' LIMIT 1];
		//opportunity__c tmp_opp = [SELECT Id, Name, account__c FROM opportunity__c WHERE account__c =: tmp_acc.Id LIMIT 1];
		User currUser = [SELECT Id, ProfileId FROM User WHERE Id=: UserInfo.getUserId()];

		// Controller Test
		PageReference pref = Page.Opportunity_plan_Budget;
		pref.getParameters().put('Id',tmp_opp.Id);
		Test.setCurrentPage(pref);

		ApexPages.StandardController opp = new ApexPages.StandardController(tmp_opp);
		Opportunity_plan_Budget_Ext opb = new Opportunity_plan_Budget_Ext(opp);

		opb.add_new();

		tmp_budget = opb.list_wrapper_budgets[1].budget;
		tmp_budget_month = opb.list_wrapper_budgets[1].list_child_budget[0];

		//
		tmp_budget.honorarumsatz_absolut__c = 500.00;
		tmp_budget.benutzer__c = currUser.Id;

		opb.calc_summe_budget_update_primary();


		tmp_budget_month.honorarumsatz_absolut__c = 1.00;
		opb.calc_summe_last_child_budget();

		opb.save_now();

	}

	@isTest static void test_method_delete()
	{
		Opportunity_Trigger.run = false;

		Test.StartTest();
		Account tmp_Acc = new Account();
		tmp_Acc.Name = 'Test AQ';
		tmp_Acc.BillingStreet = 'Högerdamm 41';
		tmp_Acc.BillingCity = 'Hamburg';
		tmp_Acc.BillingPostalCode = '20097';
		tmp_Acc.BillingCountry = 'Germany';

		insert tmp_Acc;


		opportunity__c tmp_opp = new opportunity__c();
		tmp_opp.Name = 'TestOpp';
		tmp_opp.account__c = tmp_Acc.Id;
		tmp_opp.honorarumsatz__c = 1000.00;
		tmp_opp.projektstart__c = Date.Today();
		tmp_opp.projektende__c = Date.Today()+95;

		insert tmp_opp;
		Test.StopTest();

		budget__c tmp_budget = new budget__c();
		budget__c tmp_budget_month = new budget__c();
		//Account tmp_acc = [SELECT Id, Name FROM Account WHERE Name = 'Test AQ' LIMIT 1];
		//opportunity__c tmp_opp = [SELECT Id, Name, account__c FROM opportunity__c WHERE account__c =: tmp_acc.Id LIMIT 1];
		User currUser = [SELECT Id, ProfileId FROM User WHERE Id=: UserInfo.getUserId()];

		// Controller Test
		PageReference pref = Page.Opportunity_plan_Budget;
		pref.getParameters().put('Id',tmp_opp.Id);
		Test.setCurrentPage(pref);

		ApexPages.StandardController opp = new ApexPages.StandardController(tmp_opp);
		Opportunity_plan_Budget_Ext opb = new Opportunity_plan_Budget_Ext(opp);

		opb.add_new();

		tmp_budget = opb.list_wrapper_budgets[1].budget;
		tmp_budget.honorarumsatz_absolut__c = 500.00;
		tmp_budget.benutzer__c = currUser.Id;

		opb.calc_summe_budget_update_primary();



		opb.save_now();

		opb.row_to_delete = '1';
		opb.btn_show_delete_budget_popup();
		opb.btn_delete_budget();
	}


}