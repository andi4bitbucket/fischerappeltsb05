/**************************************************************************
Company: aquilliance GmbH
Author: Andreas Wolf
CreatedDate: 2017-1

**************************************************************************/
public with sharing class Opportunity_Create_From_Ext {

	private final sObject mysObject;
	public String object_type {get;set;}

	public Account a {get;set;}
	public Contact c {get;set;}

	public Boolean notAllowed {get;set;}
	public RecordType standardRT {get;set;}
	public RecordType pitchRT {get;set;}
	public RecordType ausschreibungRT {get;set;}

	public String selected_rt {get;set;}

	public Opportunity_Create_From_Ext(ApexPages.StandardController stdController) {
		this.mysObject = (sObject)stdController.getRecord();
		object_type = checkPrefixForObjectName(mysObject.Id);
		getAllRTs();
		if(object_type == 'Account')
			run_Acc();
		if(object_type == 'Contact')
			run_Con();
	}

	public void getAllRTs()
	{
		standardRT = new RecordType();
		pitchRT = new RecordType();
		ausschreibungRT = new RecordType();
		try{standardRT=[SELECT Id, Name FROM RecordType WHERE sObjectType = 'Opportunity__c' AND DeveloperName = 'standard' LIMIT 1];
		System.debug('');}catch(Exception e){System.debug('### -Opportunity_Create_From_Ext- getStandardRTopp: '+e);}
		try{pitchRT=[SELECT Id, Name FROM RecordType WHERE sObjectType = 'Opportunity__c' AND DeveloperName = 'Pitch' LIMIT 1];
		System.debug('');}catch(Exception e){System.debug('### -Opportunity_Create_From_Ext- getPitchRTopp: '+e);}
		try{ausschreibungRT=[SELECT Id, Name FROM RecordType WHERE sObjectType = 'Opportunity__c' AND DeveloperName = 'oeffentliche_ausschreibung' LIMIT 1];
		System.debug('');}catch(Exception e){System.debug('### -Opportunity_Create_From_Ext- getAusschreibungRTopp: '+e);}
	}

	public void run_Acc(){
		a = new Account();
		notAllowed = false;
		try{a=[SELECT Id, Name, account_status__c FROM Account WHERE Id =: mysObject.Id];
		System.debug('');}catch(Exception e){System.debug('### -Opportunity_Create_From_Ext- getAccountInfo: '+e);}

		if(a.account_status__c == 'Gesperrter Kunde')
		{
			notAllowed=  true;
		}
	}

	public void run_Con(){
		c = new Contact();
		notAllowed = false;
		try{c=[SELECT Id, Name, Account.account_status__c, Account.Name, AccountId FROM Contact WHERE Id =: mysObject.Id];
		System.debug('');}catch(Exception e){System.debug('### -Opportunity_Create_From_Ext- getContactInfo: '+e);}

		if(c.Account.account_status__c == 'Gesperrter Kunde')
		{
			notAllowed=  true;
		}
	}

	public static String checkPrefixForObjectName(String recordId)
	{
		String objectName;
		String myIdPrefix = String.valueOf(recordId).substring(0,3);

		Map<String, Schema.SObjectType> gd =  Schema.getGlobalDescribe();
		//Loop through all the sObject types returned by Schema
		for(Schema.SObjectType stype : gd.values())
		{
				Schema.DescribeSObjectResult r = stype.getDescribe();
				String prefix = r.getKeyPrefix();
				//System.debug('Prefix is ' + prefix);
				//Check if the prefix matches with requested prefix
				if(prefix!=null && prefix.equals(myIdPrefix)){
						objectName = r.getName();
						//System.debug('Object Name! ' + objectName);
						break;
				}
		}
		return objectName;
	}

	public PageReference createOpportunity_Acc()
	{
		System.debug('### standardRT '+standardRT);
		System.debug('### selected_rt '+selected_rt);
		// Prepare Opportunity
		Opportunity__c prep = new Opportunity__c();
		if(object_type == 'Account')
		{
			prep.Name = a.Name+' - '+Date.today().format();
			prep.account__c = a.Id;
		}
		if(object_type == 'Contact')
		{
			prep.Name = c.Account.Name+' - '+Date.today().format();
			prep.account__c = c.AccountId;
		}
		prep.RecordTypeId = selected_rt;
		prep.projektstart__c = Date.today()+14;
		prep.projektende__c = Date.today()+28;

		if(selected_rt == standardRT.Id)
		{
			prep.Stage__c = 'Lead';
		}
		if(selected_rt == pitchRT.Id)
		{
			prep.Stage__c = 'Pitchteilnahme';
		}
		if(selected_rt == ausschreibungRT.Id)
		{
			prep.Stage__c = 'Verhandlung';
		}

		try{insert prep;
		System.debug('');}catch(Exception e){System.debug('### -Opportunity_Create_From_Ext- insertOpp: '+e);}

		if(prep.Id != null)
		{
			PageReference gotoPR = new ApexPages.StandardController(prep).edit();
			gotoPR.setRedirect(true);

			return gotoPR;
		}else{
			return null;
		}
	}

	// Method to go back to the account after the popup screen
	public PageReference backTo()
	{
		PageReference gotoPR;
		if(object_type == 'Account')
			gotoPR = new ApexPages.StandardController(a).view();
		if(object_type == 'Contact')
			gotoPR = new ApexPages.StandardController(c).view();

		gotoPR.setRedirect(true);

		return gotoPR;
	}
}