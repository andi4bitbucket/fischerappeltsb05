/**************************************************************************
Company: aquilliance GmbH
Author: Simon Kuhnt
CreatedDate: 2017-08
**************************************************************************/
public with sharing class CoCampaignLogic {

	public static void chatterFollowUpdate(Map<Id,kampagne__c> kampagneMapOld, List<kampagne__c> kampagneListNew){
		Set<Id> changedKampagnes = new Set<Id>();
		for (kampagne__c tmpKampagneNew: kampagneListNew){
			if(kampagneMapOld.containsKey(tmpKampagneNew.Id) && tmpKampagneNew.status__c != kampagneMapOld.get(tmpKampagneNew.Id).status__c){
				changedKampagnes.add(tmpKampagneNew.Id);
			}
		}
		List<Team_Member__c> changedTeamKampagnes = [SELECT Kampagne__c, Kampagne_Aktiv__c, User__c FROM Team_Member__c WHERE Kampagne__c =:changedKampagnes];
		System.debug(changedTeamKampagnes);
		List<EntitySubscription> listNewEntitySubscriptions = new List<EntitySubscription>();
		Set<Id> setUserIds = new Set<Id>();
		Set<Id> setParentIds = new Set<Id>();
		for (Team_Member__c tmpTeamKampagne : changedTeamKampagnes){
			if (tmpTeamKampagne.Kampagne_Aktiv__c){
				System.debug(tmpTeamKampagne);
				listNewEntitySubscriptions.add(addChatterSub(tmpTeamKampagne));
			}
			else if (!tmpTeamKampagne.Kampagne_Aktiv__c){
				System.debug(tmpTeamKampagne);
				setUserIds.add(tmpTeamKampagne.User__c);
				setParentIds.add(tmpTeamKampagne.Kampagne__c);
			}
		}
		System.debug(listNewEntitySubscriptions);
		deleteFollowingRecords(getFollowingRecords(setUserIds, setParentIds) );
		createFollowingRecords(listNewEntitySubscriptions);
	}

	public static EntitySubscription addChatterSub(Team_Member__c tmpTeamKampagne){
		EntitySubscription newEntitySubscription = new EntitySubscription();
		newEntitySubscription.SubscriberId = tmpTeamKampagne.User__c;
		newEntitySubscription.ParentId = tmpTeamKampagne.Kampagne__c;
		return newEntitySubscription;
	}

	public static void createFollowingRecords(List<EntitySubscription> listNewEntitySubscriptions)
	{
		try{
			insert listNewEntitySubscriptions;
		System.debug('');}catch(Exception e){
			System.debug('### -TeamMemberTriggerLogic-createFollowingRecords: '+e);
		}
	}

	public static List<EntitySubscription> getFollowingRecords(Set<Id> setUserIds, Set<Id> setParentIds)
	{
		List<EntitySubscription> listExistingEntitySubscriptions = new List<EntitySubscription>();
		try{listExistingEntitySubscriptions = [SELECT Id FROM EntitySubscription WHERE ParentId IN: setParentIds AND Subscriberid IN: setUserIds];
		System.debug('');}catch(Exception e){System.debug('### -TeamMemberTriggerLogic-getFollowingRecords: '+e);}
		return listExistingEntitySubscriptions;
	}

	public static void deleteFollowingRecords(List<EntitySubscription> listEntitySubscriptions)
	{
		try{
			delete listEntitySubscriptions;
		System.debug('');}catch(Exception e){
			System.debug('### -TeamMemberTriggerLogic-deleteFollowingRecords: '+e);
		}
	}


}