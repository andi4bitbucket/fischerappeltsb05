/**************************************************************************
Company: aquilliance GmbH
Author: Andreas Wolf
CreatedDate: 2017-4
**************************************************************************/
@isTest
private class TeamMemberTriggerLogic_Test {

	@isTest static void createAndDeleteFollowingRecords() {
		Account acc = new Account();
		acc.Name = 'Test';

		insert acc;

		Case__c cas = new Case__c();
		cas.Accountname__c = acc.Id;
		cas.Name = 'Test';

		insert cas;

		Opportunity__c opp = new Opportunity__c();

		opp.Account__c = acc.Id;
		opp.Name = 'Test';
		opp.Owner__c = UserInfo.getUserId();
		opp.Quelle__c = 'Kundenbeziehung';
		opp.Honorarumsatz__c = 200;
		opp.Fremdkosten__c = 200;

		insert opp;

		Kampagne__c kampagne = new Kampagne__c(
		Name = 'kampagne1',
		status__c = 'aktiv'
		);
		insert kampagne;

		Team_Member__c teamMemberForKampaign = new Team_Member__c();
		teamMemberForKampaign.Kampagne__c = kampagne.Id;
		teamMemberForKampaign.User__c = UserInfo.getUserId();

		insert teamMemberForKampaign;
		delete teamMemberForKampaign;

		List<Team_Member__c> listTeamMember = new List<Team_Member__c>();
		Team_Member__c teamMemberForAcc = new Team_Member__c();
		teamMemberForAcc.Account__c = acc.Id;
		teamMemberForAcc.User__c = UserInfo.getUserId();
		listTeamMember.add(teamMemberForAcc);
		teamMemberForAcc = new Team_Member__c();
		teamMemberForAcc.Account__c = acc.Id;
		teamMemberForAcc.User__c = UserInfo.getUserId();
		listTeamMember.add(teamMemberForAcc);

		insert listTeamMember;
		delete listTeamMember;

		List<Team_Member__c> listTeamMemberForCase = new List<Team_Member__c>();
		Team_Member__c teamMemberForCase = new Team_Member__c();
		teamMemberForCase.CO_Case__c = cas.Id;
		teamMemberForCase.User__c = UserInfo.getUserId();
		listTeamMemberForCase.add(teamMemberForCase);
		teamMemberForCase = new Team_Member__c();
		teamMemberForCase.CO_Case__c = cas.Id;
		teamMemberForCase.User__c = UserInfo.getUserId();
		listTeamMemberForCase.add(teamMemberForCase);

		insert listTeamMemberForCase;
		delete listTeamMemberForCase;

		Team_Member__c teamMemberForOpp = new Team_Member__c();
		teamMemberForOpp.Opportunity__c = opp.Id;
		teamMemberForOpp.User__c = UserInfo.getUserId();

		insert teamMemberForOpp;
		delete teamMemberForOpp;

		Team_Member__c teamMemberWithoutParent = new Team_Member__c();
		teamMemberWithoutParent.User__c = UserInfo.getUserId();

		insert teamMemberWithoutParent;
		delete teamMemberWithoutParent;
	}

	@isTest(SeeAllData=true) static void chatterPost() {
			recordType einreichungsTeamRt = [SELECT Id, DeveloperName FROM recordType WHERE DeveloperName = 'Einreichungsteam' LIMIT 1];
			recordType einreichungRt = [SELECT Id, DeveloperName FROM recordType WHERE DeveloperName = 'Einreichung' LIMIT 1];
			Team_Member__c einrichtung = new Team_Member__c(
				Einreichungsschluss__c = date.today(),
				RecordTypeId = einreichungRt.Id
			);
			insert einrichtung;
			Team_Member__c einrichtungTeam = new Team_Member__c(
				User__c = UserInfo.getUserId(),
				RecordTypeId = einreichungsTeamRt.Id,
				Team_Member_Einreichung__c = einrichtung.Id
			);
			insert einrichtungTeam;
	}

	@isTest(SeeAllData=true) static void chatterPost_Awardcase_Team_Status_geaendert()
	{

		Account acc = new Account();
		acc.Name = 'Test Account #5';
		acc.BillingStreet = 'Billing Str 1';
		acc.BillingCity = 'Hamburg';
		acc.BillingPostalCode= '20097';
		acc.BillingCountry = 'Germany';

		acc.ShippingStreet = 'Billing Str 2';
		acc.ShippingCity = 'Hamburg';
		acc.ShippingPostalCode= '20097';
		acc.ShippingCountry = 'Germany';


		Id rt_einreichung = [SELECT Id, DeveloperName FROM recordType WHERE DeveloperName = 'Einreichung' LIMIT 1].Id;

		Case__c cas = new Case__c();
		cas.Accountname__c = acc.Id;
		cas.Name = 'Awardcase';
		cas.awardfaehig__c = true;

		insert cas;



		Team_Member__c tm_Einreichung = new Team_Member__c();
		tm_Einreichung.Account__c = acc.Id;
		tm_Einreichung.Einreichungsschluss__c = date.today();
		tm_Einreichung.case_for_award__c = cas.Id;
		tm_Einreichung.Name = 'Einreichung';
		tm_Einreichung.Platzierung__c='	keine Platzierung';
		tm_Einreichung.Status__c = '';
		tm_Einreichung.RecordTypeId = rt_einreichung;

		insert tm_Einreichung;

		Test.StartTest();
		tm_Einreichung.Status__c = 'Vorbereitung';
		update tm_Einreichung;
		Test.StopTest();
	}

}