@isTest
private class Salesflash_Ext_Test {

	@isTest static void test_method_WITH_ACC() {
		Account tmp_acc = new Account();
		tmp_acc.Name = 'Test';
		tmp_acc.account_status__c = 'Gesperrter Kunde';

		insert tmp_acc;

		Salesflash__c tmp_sf = new Salesflash__c();
		tmp_sf.Name = 'Test';
		tmp_sf.account__c = tmp_acc.Id;
		tmp_sf.beschreibung__c = 'Test';

		insert tmp_sf;

		ApexPages.StandardController standard1 = new ApexPages.StandardController(tmp_sf);
		Salesflash_Ext con = new Salesflash_Ext(standard1);
		con.backTo();
		con.createOpportunity();
	}

	@isTest static void test_method_WITHOUT_ACC() {
		Salesflash__c tmp_sf = new Salesflash__c();
		tmp_sf.Name = 'Test';
		tmp_sf.beschreibung__c = 'Test';

		insert tmp_sf;

		ApexPages.StandardController standard1 = new ApexPages.StandardController(tmp_sf);
		Salesflash_Ext con = new Salesflash_Ext(standard1);
		con.createOpportunity();
	}

	@isTest static void test_method_FAIL() {
		Account tmp_Acc = new Account();
		tmp_Acc.Name = 'Test AQ';
		tmp_Acc.BillingStreet = 'Högerdamm 41';
		tmp_Acc.BillingCity = 'Hamburg';
		tmp_Acc.BillingPostalCode = '20097';
		tmp_Acc.BillingCountry = 'Germany';

		insert tmp_Acc;

		opportunity__c tmp_opp = new opportunity__c();
		tmp_opp.Name = 'TestOpp';
		tmp_opp.account__c = tmp_Acc.Id;
		tmp_opp.honorarumsatz__c = 1000.00;
		tmp_opp.projektstart__c = Date.Today();
		tmp_opp.projektende__c = Date.Today()+95;

		insert tmp_opp;

		Salesflash__c tmp_sf = new Salesflash__c();
		tmp_sf.Name = 'Test';
		tmp_sf.opportunity__c = tmp_opp.Id;
		tmp_sf.account__c = tmp_acc.Id;
		tmp_sf.beschreibung__c = 'Test';

		insert tmp_sf;

		ApexPages.StandardController standard1 = new ApexPages.StandardController(tmp_sf);
		Salesflash_Ext con = new Salesflash_Ext(standard1);
		con.createOpportunity();
	}

}