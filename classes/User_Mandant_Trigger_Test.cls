@isTest
private class User_Mandant_Trigger_Test {

	@isTest static void test_method_one() {

		String RecordTypeId_agentur = null;
		try{ RecordTypeId_agentur = [SELECT Id FROM RecordType WHERE DeveloperName = 'Agentur' AND SobjectType='user_mandant__c' LIMIT 1].Id;
		System.debug('');}catch(Exception e){System.debug('### agenturRT error:'+e);}

		Mandant__c tmp_man = new Mandant__c();
		tmp_man.Name = 'Test';
		insert tmp_man;

		User_mandant__c tmp_um = new User_mandant__c();
		tmp_um.mandant_agentur__c = tmp_man.Id;
		tmp_um.RecordTypeId = RecordTypeId_agentur;
		tmp_um.User__c = UserInfo.getUserId();

		insert tmp_um;

		User_mandant__c tmp_um2 = new User_mandant__c();
		tmp_um2.mandant_agentur__c = tmp_man.Id;
		tmp_um2.RecordTypeId = RecordTypeId_agentur;
		tmp_um2.User__c = UserInfo.getUserId();


		try{ insert tmp_um2;
		System.debug('');}catch(Exception e){System.debug('### 1st insert error:'+e);}

		Profile p = [SELECT Id FROM Profile WHERE Name='Standard User' OR Name='Standardbenutzer' LIMIT 1];
		User u = new User();
		u.Alias = 'standt';
		u.Email='info@aquilliance.de';
		u.EmailEncodingKey='UTF-8';
		u.LastName='Testing';
		u.LanguageLocaleKey='en_US';
		u.LocaleSidKey='en_US';
		u.ProfileId = p.Id;
		u.TimeZoneSidKey='America/Los_Angeles';
		u.UserName='info@aquilliance.de';

		insert u;

		User_mandant__c tmp_um3 = new User_mandant__c();
		tmp_um3.mandant_agentur__c = tmp_man.Id;
		tmp_um3.RecordTypeId = RecordTypeId_agentur;
		tmp_um3.User__c = u.id;

		insert tmp_um3;

		tmp_um3.User__c = UserInfo.getUserId();

		try{ update tmp_um3;
		System.debug('');}catch(Exception e){System.debug('### 2nd update error:'+e);}
	}
}