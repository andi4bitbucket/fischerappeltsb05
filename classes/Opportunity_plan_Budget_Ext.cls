/***************************************************************************
Company: aquilliance GmbH
Author: Andre Mergen
CreatedDate: 2016-12

***************************************************************************/
public with sharing class Opportunity_plan_Budget_Ext {

		public opportunity__c the_opp {get;set;}

		public List<wrapper_budget> list_wrapper_budgets {get;set;}

		public SET<Id> set_budget_ids = new SET<Id>();
		public string row_to_reset_linear {get;set;}
		public string row_to_delete {get;set;}
		public string show_popup {get;set;}
		public string show_box {get;set;}
		public Boolean show_box_value_too_high {get;set;}
		public Boolean show_box_value_too_low {get;set;}
		public Boolean show_box_not_unique_user {get;set;}
		public Boolean system_error {get;set;}
		public Boolean disable_save {get;set;}
		public Boolean edit_mode {get;set;}

		// muss nicht mehr get:set
		public MAP<Id, List<budget__c>> map_child_budget {get;set;}
		public List<budget__c> list_budget {get;set;}
		public Integer nr_of_month {get;set;}
		public Decimal summe_budget {get;set;}

    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public Opportunity_plan_Budget_Ext(ApexPages.StandardController stdController) {
				init();
        the_opp = (opportunity__c)stdController.getRecord();

				select_opp();
				select_budgets();
				select_monats_budgets();

				nr_of_month = the_opp.projektstart__c.monthsBetween(the_opp.projektende__c)+1;

				if(list_budget.size()<=0)
				{
					add_primary();
					//edit_mode = true;
				}
				generate_wrapper();
    }

		/*********************************************
		um die VAR´s zu initialisieren
		*********************************************/
		public void init()		{
			list_budget = new List<budget__c>();
			map_child_budget = new MAP<Id, List<budget__c>>();
			list_wrapper_budgets = new List<wrapper_budget>();
			summe_budget = 0.0;
			row_to_reset_linear = '';
			row_to_delete = '';
			show_popup = '';
			show_box = '';
			show_box_value_too_high = false;
			show_box_value_too_low = false;
			show_box_not_unique_user = false;
			system_error = false;
			disable_save = false;
			edit_mode = false;
		}

		/*********************************************
		select opp infos
		*********************************************/
		public void select_opp(){
			the_opp = [SELECT Id, Name,account__c, account__r.Name, honorarumsatz__c, honorarumsatz_gewichtet__c, projektstart__c, projektende__c, Owner__c, Owner__r.FirstName, Owner__r.LastName, stage__c,opportunity_art__c,
			zz_helper_start_end_date_changed__c FROM opportunity__c WHERE Id =:the_opp.Id];
		}

		/*********************************************
		select primär und sekundär budgets
		*********************************************/
		public void select_budgets()
		{
			String RecordTypeId = null;
			try{
				RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'monats_budget' AND SobjectType='budget__c' LIMIT 1].Id;
			System.debug('');}catch(Exception e){System.debug('### error select_budgets RecordTypeId:'+e);}

			list_budget = [SELECT Id,
			Name,
			account__r.Name,
			opportunity__c,
			RecordTypeId,
			budget__c,
			benutzer__c,
			benutzer__r.Name,
			honorarumsatz_absolut__c,
			honorarumsatz_gewichtet__c,
			betreffender_monat__c,
			kostenstelle__c,
			honorarumsatz_in_prozent__c,
			honorarumsatz_anteil__c
			FROM budget__c WHERE opportunity__c =:the_opp.Id AND RecordTypeId !=:RecordTypeId AND budget__c = null ORDER BY RecordTypeId];

			/**********************************************
			map vorbefüllen
			**********************************************/
			for(budget__c tmp_budget:list_budget)
			{
				// tmp_budget
				if(!map_child_budget.containsKey(tmp_budget.Id))
				{
					map_child_budget.put(tmp_budget.Id, new List<budget__c>());
					set_budget_ids.add(tmp_budget.Id);
				}
			}
		}

		/*********************************************
		select der monats budgets
		*********************************************/
		public void select_monats_budgets()
		{
			String RecordTypeId = null;
			List<budget__c> tmp_map_list_child_budget = new List<budget__c>();
			List<budget__c> tmp_list_child_budget = new List<budget__c>();
			try{
				RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'monats_budget' AND SobjectType='budget__c' LIMIT 1].Id;
			System.debug('');}catch(Exception e){System.debug('### error select_monats_budgets RecordTypeId:'+e);}

			try{
			tmp_list_child_budget = [SELECT Id,
			Name,
			opportunity__c,
			RecordTypeId,
			budget__c,
			benutzer__c,
			honorarumsatz_absolut__c,
			honorarumsatz_gewichtet__c,
			betreffender_monat__c,
			honorarumsatz_in_prozent__c,
			honorarumsatz_anteil__c
			FROM budget__c WHERE budget__c =:set_budget_ids AND RecordTypeId =:RecordTypeId ORDER BY betreffender_monat__c];
			System.debug('');}catch(Exception e){System.debug('### error tmp_list_child_budget:'+e);}


			/**********************************************
			map befüllen
			**********************************************/
			for(budget__c tmp_child_budget:tmp_list_child_budget)
			{
				// tmp liste zurücksetzten
				tmp_map_list_child_budget = new List<budget__c>();
				// wenn map key enthält
				if(map_child_budget.containsKey(tmp_child_budget.budget__c))
				{
					// hole liste zum parent
					tmp_map_list_child_budget = map_child_budget.get(tmp_child_budget.budget__c);
					// füge der liste den neuen eintrag hinzu
					tmp_map_list_child_budget.add(tmp_child_budget);
					// füge die liste wieder in die map ein
					map_child_budget.put(tmp_child_budget.budget__c, tmp_map_list_child_budget);
				}
			}
		}

		/*********************************************
		füg eine neue Zeile hinzu
		*********************************************/
public void add_new(){
	String RecordTypeId_sekundaer_budget = null;
	String RecordTypeId_monats_budget = null;

	budget__c tmp_bg = new budget__c();
	budget__c tmp_cbg = new budget__c(); // monats budget
	List<budget__c> tmp_list_cbg = new List<budget__c>();

	Date start_date = the_opp.projektstart__c.toStartOfMonth();



	try{
		RecordTypeId_sekundaer_budget = [SELECT Id FROM RecordType WHERE DeveloperName = 'sekundaer_budget' AND SobjectType='budget__c' LIMIT 1].Id;
	System.debug('');}catch(Exception e){System.debug('error add_new RecordTypeId_sekundaer_budget:'+e);}

	try{
		RecordTypeId_monats_budget = [SELECT Id FROM RecordType WHERE DeveloperName = 'monats_budget' AND SobjectType='budget__c' LIMIT 1].Id;
	System.debug('');}catch(Exception e){System.debug('error add_new RecordTypeId_monats_budget:'+e);}
	/***************************************/
	//neues budget
	tmp_bg.RecordTypeId = RecordTypeId_sekundaer_budget;
	tmp_bg.opportunity__c = the_opp.Id;
	tmp_bg.account__c = the_opp.account__c;
	tmp_bg.honorarumsatz_absolut__c = 0;
	tmp_bg.honorarumsatz_in_prozent__c = 0;
	tmp_bg.honorarumsatz_anteil__c = 0;

	/***************************************/
	// neuer temporäter wrapper
	wrapper_budget tmp_wb = new wrapper_budget();


		tmp_wb = new wrapper_budget();
		tmp_wb.isnew = true;
		tmp_wb.budget = tmp_bg;

		//##########################################################
		Integer i = 0;
		for(i=0; i<nr_of_month;i++)
		{
			tmp_cbg = new budget__c(); // monats budget
			tmp_cbg.RecordTypeId = RecordTypeId_monats_budget;
			tmp_cbg.honorarumsatz_absolut__c = 0;
			tmp_cbg.betreffender_monat__c = start_date.addMonths(i);
			tmp_cbg.Name = start_date.year() +'-'+ start_date.Month();

			tmp_list_cbg.add(tmp_cbg);
		}
		//##########################################################



		tmp_wb.list_child_budget = tmp_list_cbg;



		list_wrapper_budgets.add(tmp_wb);

		show_box = 'unsaved';
}

/*********************************************
speichert die budgets + monats budgets
*********************************************/
public Pagereference save_now(){
	// zeigt sysfehler panel
	system_error = false;
	Pagereference p = Apexpages.Currentpage();

	List<budget__c> list_new_budget = new List<budget__c>();
	List<budget__c> list_new_child_budget = new List<budget__c>();
	for(wrapper_budget tmp_wb:list_wrapper_budgets)
	{
		list_new_budget.add(tmp_wb.budget);
	}

	try{upsert list_new_budget;}catch(Exception e)	{String strMsg = e.getMessage(); ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, strMsg);	ApexPages.addMessage(msg); system_error = true;}

	/*********************************************/

	for(wrapper_budget tmp_wb:list_wrapper_budgets)
	{
		// child budget
		for(budget__c tmp_cbg:tmp_wb.list_child_budget)
		{

			tmp_cbg.budget__c = tmp_wb.budget.Id;
			list_new_child_budget.add(tmp_cbg);
		}
	}


	try{upsert list_new_child_budget;}catch(Exception e){String strMsg2 = e.getMessage();	ApexPages.Message msg2 = new ApexPages.Message(ApexPages.Severity.ERROR, strMsg2); ApexPages.addMessage(msg2); system_error = true;}

	show_box = '';

	show_popup = '';
	edit_mode = false;

	init();
	select_budgets();
	select_monats_budgets();
					generate_wrapper();

	return null;

}


/***********************************
	setzte einen auf linear zurück
***********************************/
public void one_linear(){

		wrapper_budget tmp_wb = new wrapper_budget();
		tmp_wb = list_wrapper_budgets[Integer.valueOf(row_to_reset_linear)];
		//	for(wrapper_budget tmp_wb:list_wrapper_budgets)
		//	{
		// child budget
		for(budget__c tmp_cbg:tmp_wb.list_child_budget)
		{
			tmp_cbg.budget__c = tmp_wb.budget.Id;
			tmp_cbg.honorarumsatz_absolut__c = (tmp_wb.budget.honorarumsatz_absolut__c/nr_of_month);
			//list_new_child_budget.add(tmp_cbg)
		}

	//	}
	//############################
	decimal tmp_summe_budget = 0.0;
	for(wrapper_budget tmp_wb_2:list_wrapper_budgets)
	{
	//if(tmp_wb.primary != true)
	//{
		tmp_summe_budget += tmp_wb_2.budget.honorarumsatz_absolut__c;
	//}
	}
	if(tmp_summe_budget != the_opp.honorarumsatz_gewichtet__c)//honorarumsatz__c
	{
		calc_summe_budget_update_primary();
	}


	check_error_too_low_too_high();

	//############################
	check_and_disable_save();
}

/**********************************************************************
setzte alle auf linear zurück, dazu werden die monats budgets gelöscht
und neu erstellt. Das ist wichtig, wenn sich das Opp Datum
geändert hat
**********************************************************************/
public void delete_all_childs(){
	// zeigt sysfehler panel
	system_error = false;
	List<budget__c> tmp_list_delete_child_budget = new List<budget__c>();
	Date start_date = the_opp.projektstart__c.toStartOfMonth();
	String RecordTypeId_monats_budget = null;
	budget__c tmp_cbg2 = new budget__c();
	List<budget__c> tmp_list_cbg = new List<budget__c>();

	try{
		RecordTypeId_monats_budget = [SELECT Id FROM RecordType WHERE DeveloperName = 'monats_budget' AND SobjectType='budget__c' LIMIT 1].Id;
	System.debug('');}catch(Exception e){System.debug('### error delete_all_childs RecordTypeId_monats_budget:'+e);}


	// delete old childs
	/***********************************************/
	for(wrapper_budget tmp_wb:list_wrapper_budgets)
	{
			// child budget
			for(budget__c tmp_cbg:tmp_wb.list_child_budget)
			{
				if(tmp_cbg.Id != null)
				{
					tmp_list_delete_child_budget.add(tmp_cbg);
				}
			}
			tmp_wb.list_child_budget = new List<budget__c>();
	}

	if(tmp_list_delete_child_budget.size()>0)
	{
		try{delete tmp_list_delete_child_budget;}catch(Exception e){String strMsg2 = e.getMessage(); ApexPages.Message msg2 = new ApexPages.Message(ApexPages.Severity.ERROR, strMsg2); ApexPages.addMessage(msg2); system_error = true;}
	}
	/***********************************************/



	// prepare new childs
	/***********************************************/

	for(wrapper_budget tmp_wb:list_wrapper_budgets)
	{
	//reset child
	tmp_wb.list_child_budget = new List<budget__c>();
	tmp_list_cbg = new List<budget__c>();
		// child budgets
	/*********/
		Integer i = 0;
		for(i=0; i<nr_of_month;i++)
		{
			tmp_cbg2 = new budget__c(); // monats budget
			tmp_cbg2.RecordTypeId = RecordTypeId_monats_budget;
			tmp_cbg2.honorarumsatz_absolut__c = (tmp_wb.budget.honorarumsatz_absolut__c/nr_of_month);
			tmp_cbg2.betreffender_monat__c = start_date.addMonths(i);
			tmp_cbg2.Name = start_date.year() +'-'+ start_date.Month();

			tmp_list_cbg.add(tmp_cbg2);
		}
		tmp_wb.list_child_budget = tmp_list_cbg;
	/*********/



		}
		the_opp.zz_helper_start_end_date_changed__c = false;

		save_now();

		try{update the_opp;}catch(Exception e){String strMsg2 = e.getMessage(); ApexPages.Message msg2 = new ApexPages.Message(ApexPages.Severity.ERROR, strMsg2);	ApexPages.addMessage(msg2); system_error = true;}

		/***********************************************/
}

public void add_primary(){
	String RecordTypeId_sekundaer_budget = null;
	String RecordTypeId_monats_budget = null;

	budget__c tmp_bg = new budget__c();
	budget__c tmp_cbg = new budget__c(); // monats budget
	List<budget__c> tmp_list_cbg = new List<budget__c>();

	Date start_date = the_opp.projektstart__c.toStartOfMonth();



	try{
		RecordTypeId_sekundaer_budget = [SELECT Id FROM RecordType WHERE DeveloperName = 'primaer_budget' AND SobjectType='budget__c' LIMIT 1].Id;
	System.debug('');}catch(Exception e){System.debug('### error RecordTypeId_sekundaer_budget:'+e);}

	try{
		RecordTypeId_monats_budget = [SELECT Id FROM RecordType WHERE DeveloperName = 'monats_budget' AND SobjectType='budget__c' LIMIT 1].Id;
	System.debug('');}catch(Exception e){System.debug('### error RecordTypeId_monats_budget:'+e);}
	/***************************************/
	//neues budget
	tmp_bg.RecordTypeId = RecordTypeId_sekundaer_budget;
	tmp_bg.opportunity__c = the_opp.Id;
	tmp_bg.account__c = the_opp.account__c;
	tmp_bg.honorarumsatz_absolut__c = the_opp.honorarumsatz_gewichtet__c;//honorarumsatz__c
	tmp_bg.benutzer__c = the_opp.Owner__c;
	tmp_bg.honorarumsatz_in_prozent__c = 100;
	tmp_bg.honorarumsatz_anteil__c = the_opp.honorarumsatz__c;

	/***************************************/
	// neuer temporäter wrapper
	wrapper_budget tmp_wb = new wrapper_budget();


		tmp_wb = new wrapper_budget();
		tmp_wb.isnew = true;
		tmp_wb.primary = true;
		tmp_wb.budget = tmp_bg;

		//##########################################################
		Integer i = 0;
		for(i=0; i<nr_of_month;i++)
		{
			tmp_cbg = new budget__c(); // monats budget
			tmp_cbg.RecordTypeId = RecordTypeId_monats_budget;
			tmp_cbg.honorarumsatz_absolut__c = (tmp_wb.budget.honorarumsatz_absolut__c/nr_of_month);
			tmp_cbg.betreffender_monat__c = start_date.addMonths(i);
			tmp_cbg.Name = start_date.year() +'-'+ start_date.Month();

			tmp_list_cbg.add(tmp_cbg);
		}
		//##########################################################



		tmp_wb.list_child_budget = tmp_list_cbg;



		list_wrapper_budgets.add(tmp_wb);

		show_box = 'unsaved';
		check_benutzer_name();
}


public void generate_wrapper(){
	System.debug('#### generate_wrapper');
	String RecordTypeId_primaer_budget = null;

	try{
		RecordTypeId_primaer_budget = [SELECT Id FROM RecordType WHERE DeveloperName = 'primaer_budget' AND SobjectType='budget__c' LIMIT 1].Id;
	}catch(Exception e){}

	wrapper_budget tmp_wb = new wrapper_budget();


	for(budget__c tmp_bg:list_budget)
	{
		tmp_wb = new wrapper_budget();

		if(tmp_bg.RecordTypeId == RecordTypeId_primaer_budget)
		{
			tmp_wb.primary = true;
		}
		else{
			tmp_wb.primary = false;
		}
		tmp_wb.isnew = false;
		tmp_wb.budget = tmp_bg;
		tmp_wb.list_child_budget = map_child_budget.get(tmp_bg.Id);

		list_wrapper_budgets.add(tmp_wb);
	}

	check_benutzer_name();
}

public void calc_summe_budget_update_primary(){
	system_error = false;
	System.debug('#### calc_summe_budget_update_primary');

	summe_budget = 0.0;
	Decimal summe_prozent = 0;
	for(wrapper_budget tmp_wb:list_wrapper_budgets)
	{
		if(tmp_wb.primary != true)
		{
			summe_budget += tmp_wb.budget.honorarumsatz_absolut__c;
			summe_prozent += tmp_wb.budget.honorarumsatz_in_prozent__c;
		}

	}


	//honorar
		for(wrapper_budget tmp_wb:list_wrapper_budgets)
		{
			if(tmp_wb.primary == true)
			{
				//tmp_wb.budget.honorarumsatz_absolut__c = (the_opp.honorarumsatz_gewichtet__c-summe_budget);//honorarumsatz__c
				tmp_wb.budget.honorarumsatz_in_prozent__c = (100-summe_prozent);//honorarumsatz__c

			}
		}

	show_box = 'unsaved';

		calc_summe_last_child_budget();

		check_error_too_low_too_high();

		//############################
			// prüft ob fehler vorliegt
	check_and_disable_save();
}


public void calc_summe_last_child_budget(){
	system_error = false;
	System.debug('#### calc_summe_last_child_budget');
	Decimal tmp_summe_budget = 0.0;
	Integer i = 1;



	for(wrapper_budget tmp_wb:list_wrapper_budgets)
	{

		i =1;
		tmp_summe_budget = 0.0;

		for(budget__c tmp_cbg:tmp_wb.list_child_budget)
		{

			if(i != tmp_wb.list_child_budget.size())
			{
				tmp_summe_budget += tmp_cbg.honorarumsatz_absolut__c;
			}
			else
			{
				tmp_cbg.honorarumsatz_absolut__c = (tmp_wb.budget.honorarumsatz_absolut__c-tmp_summe_budget);
			}


			i++;
		}
	}


	show_box = 'unsaved';

	check_error_too_low_too_high();
	// prüft ob fehler vorliegt
	check_and_disable_save();
}

public void calc_prozent()
{
	calc_summe_budget_update_primary();
	system_error = false;
	System.debug('#### calc prozent');


	for(wrapper_budget tmp_wb:list_wrapper_budgets)
	{
		if(tmp_wb.primary != true)
		{
			tmp_wb.budget.honorarumsatz_anteil__c = (the_opp.honorarumsatz__c/100)*tmp_wb.budget.honorarumsatz_in_prozent__c;
			tmp_wb.budget.honorarumsatz_absolut__c = ((the_opp.honorarumsatz_gewichtet__c/100)*tmp_wb.budget.honorarumsatz_in_prozent__c);
		}
		else
		{
			tmp_wb.budget.honorarumsatz_anteil__c = (the_opp.honorarumsatz__c/100)*tmp_wb.budget.honorarumsatz_in_prozent__c;
			tmp_wb.budget.honorarumsatz_absolut__c = ((the_opp.honorarumsatz_gewichtet__c/100)*tmp_wb.budget.honorarumsatz_in_prozent__c);
		}
	}


	calc_summe_last_child_budget();

}

public void btn_nix(){
	show_popup = '';
	row_to_delete = '';
}

public void btn_edit_mode(){
	edit_mode = true;
}

public void btn_show_delete_budget_popup(){
	calc_summe_budget_update_primary();
	//calc_summe_last_child_budget();
	//check_error_too_low_too_high();
	//check_and_disable_save();
	if(disable_save==false)
	{
		show_popup = 'delete';
	}
	show_popup = 'delete';
}

public void btn_show_save_budget_popup(){
	calc_summe_budget_update_primary();
	calc_summe_last_child_budget();
	check_error_too_low_too_high();
	check_and_disable_save();

	if(disable_save==false)
	{
			show_popup = 'save';
	}

}

public void btn_delete_budget(){
	system_error = false;
	List<budget__c> tmp_list_delete_budget_and_childs = new List<budget__c>();
	// delete old childs
	/***********************************************/
		wrapper_budget tmp_wb = list_wrapper_budgets[Integer.ValueOf(row_to_delete)];

				// child budget
				for(budget__c tmp_cbg:tmp_wb.list_child_budget)
				{
					if(tmp_cbg.Id != null)
					{
						tmp_list_delete_budget_and_childs.add(tmp_cbg);
					}
				}

				if(tmp_wb.budget.Id != null)
				{
					tmp_list_delete_budget_and_childs.add(tmp_wb.budget);
				}



		if(tmp_list_delete_budget_and_childs.size()>0)
		{
			try{delete tmp_list_delete_budget_and_childs;} catch(Exception e) { String strMsg2 = e.getMessage(); ApexPages.Message msg2 = new ApexPages.Message(ApexPages.Severity.ERROR, strMsg2); ApexPages.addMessage(msg2); system_error = true; }
		}

		list_wrapper_budgets.remove(Integer.ValueOf(row_to_delete));
	/***********************************************/
	show_popup = '';
	/***********************************************
	***********************************************/





	check_unique_user();

	//calc_summe_budget_update_primary();
calc_prozent();
	check_error_too_low_too_high();
	check_and_disable_save();
}

public void check_unique_user(){
	Set<Id> set_user_id  = new Set<Id>();
	show_box_not_unique_user = false;

	for(wrapper_budget tmp_wb:list_wrapper_budgets)
	{
		if(!set_user_id.contains(tmp_wb.budget.benutzer__c))
		{
			set_user_id.add(tmp_wb.budget.benutzer__c);
		}
		else{
			show_box_not_unique_user = true;
		}
	}


	// prüft ob fehler vorliegt
	check_and_disable_save();
}

public void check_error_too_low_too_high(){
	Boolean tmp_error = false;
	Boolean tmp_error_low = false;

	//honorar
		for(wrapper_budget tmp_wb:list_wrapper_budgets)
		{
				if(tmp_wb.budget.honorarumsatz_absolut__c<0)
				{
					if(tmp_wb.primary==true)
					{
						tmp_error = true;
					}

				}


			for(budget__c tmp_cbg:tmp_wb.list_child_budget)
			{
				if(tmp_cbg.honorarumsatz_absolut__c<0.0)
				{
					tmp_error_low = true;
				}
			}

	}


	if(tmp_error==true)
	{
		show_box_value_too_high = true;
	}
	else
	{
		show_box_value_too_high = false;
	}

	// zeige error box wenn werte minus
	if(tmp_error_low==true)
	{
		show_box_value_too_low = true;
		//show_box_value_too_high = false;
	}
	else
	{
		show_box_value_too_low = false;
	}
}

/******************************************
stop edit and go back
******************************************/
public void cancel_edit(){
	init();
	//the_opp = (opportunity__c)stdController.getRecord();

	select_opp();
	select_budgets();
	select_monats_budgets();

	nr_of_month = the_opp.projektstart__c.monthsBetween(the_opp.projektende__c)+1;

	if(list_budget.size()<=0)
	{
		add_primary();
		//edit_mode = true;
	}
	generate_wrapper();
}

/******************************************
	prüft ob ein Fehler vorliegt und disabled save
******************************************/
public void check_and_disable_save(){
	if(show_box_value_too_high == true || show_box_value_too_low == true || system_error == true || show_box_not_unique_user == true)
	{
		disable_save = true;
	}
	else{
		disable_save = false;
	}
}

/******************************************
ruft den namen des gewählten Users ab
******************************************/
public void check_benutzer_name(){
	Set<Id> set_user_ids = new Set<Id>();
	List<User> tmp_list = new List<User>();
	MAP<Id,User> map_user = new MAP<Id, User>();


	for(wrapper_budget tmp_wb:list_wrapper_budgets)
	{
		if(!set_user_ids.contains(tmp_wb.budget.benutzer__c))
		{
			set_user_ids.add(tmp_wb.budget.benutzer__c);
		}
	}

	tmp_list = [SELECT Id, Name FROM User WHERE Id IN:set_user_ids];
	map_user.putAll(tmp_list);

	for(wrapper_budget tmp_wb:list_wrapper_budgets)
	{
		if(map_user.containsKey(tmp_wb.budget.benutzer__c))
		{
			tmp_wb.str_benutzer_name = map_user.get(tmp_wb.budget.benutzer__c).Name;
		}
	}
}
		/******************************************
			wrapper class
			verwendet für list_wrapper_contacts
		******************************************/
		public class wrapper_budget
		{
			public String str_benutzer_name {get;set;}
			public boolean isnew {get;set;}
			public boolean primary {get;set;}
			public budget__c budget {get;set;}
			public List<budget__c> list_child_budget {get;set;}
		}

}