@isTest
private class Listing_Check_Ext_Test {

	@isTest static void test_method_one() {
		Account tmp_acc = new Account();
		tmp_acc.Industry = 'Test';
		tmp_acc.Name = 'Test';

		insert tmp_acc;

		Mandant__c tmp_man = new Mandant__c();
		tmp_man.Name = 'Test';
		insert tmp_man;

		User_mandant__c tmp_um = new User_mandant__c();
		tmp_um.mandant_agentur__c = tmp_man.Id;
		tmp_um.User__c = UserInfo.getUserId();

		insert tmp_um;

		Mandant__c tmp_man2 = new Mandant__c();
		tmp_man2.Name = 'Test';
		insert tmp_man2;

		opportunity__c tmp_opp = new opportunity__c();
		tmp_opp.Name = 'TestOpp';
		tmp_opp.account__c = tmp_Acc.Id;
		tmp_opp.honorarumsatz__c = 1000.00;
		tmp_opp.projektstart__c = Date.Today();
		tmp_opp.projektende__c = Date.Today()+95;
		tmp_opp.mandant__c = tmp_man.Id;
		tmp_opp.vertragsfuehrende_agentur__c = tmp_man2.Id;

		insert tmp_opp;



		ApexPages.StandardController standard1 = new ApexPages.StandardController(tmp_acc);
		Listing_Check_Ext con = new Listing_Check_Ext(standard1);

		ApexPages.StandardController standard2 = new ApexPages.StandardController(tmp_opp);
		Listing_Check_Ext con2 = new Listing_Check_Ext(standard2);
	}
}