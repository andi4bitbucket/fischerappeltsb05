/**************************************************************************
Company: aquilliance GmbH
Author: Andreas Wolf
CreatedDate: 2017-8
**************************************************************************/
global class ReminderOpportunityBatch implements Database.Batchable<sObject> {

	String query;
	Date overdueNextStepDate;
	Date nearCloseDate;

	global ReminderOpportunityBatch() {
		overdueNextStepDate = Date.today().addDays(-7);
		nearCloseDate = Date.today().addDays(14);
		query = 'SELECT Id, Next_Step__c, Schlusstermin__c FROM Opportunity__c WHERE Next_Step__c =: overdueNextStepDate OR Schlusstermin__c =: nearCloseDate';
	}

	global Database.QueryLocator start(Database.BatchableContext BC){
		System.debug('###-ReminderOpportunityBatch-start EXECUTED!');
		return Database.getQueryLocator(query);
	}

  global void execute(Database.BatchableContext BC, List<sObject> scope){
		System.debug('###-ReminderOpportunityBatch-execute EXECUTED!');
		List<ConnectApi.BatchInput> allChatterPosts = new List<ConnectApi.BatchInput>();
		ConnectApi.BatchInput tmp_batchInput;

		for(SObject tmp : scope)
		{
			Opportunity__c opp = (Opportunity__c) tmp;
			tmp_batchInput = new ConnectApi.BatchInput(new ConnectApi.FeedItemInput());
			if(opp.Next_Step__c == overdueNextStepDate){
				tmp_batchInput = generateChatterMessage('', opp.Id, 'Der letzte Schritt ist schon 7 Tage her!');
				allChatterPosts.add(tmp_batchInput);
			}
			if(opp.Schlusstermin__c == nearCloseDate){
				tmp_batchInput = generateChatterMessage('', opp.Id, 'In 14 Tagen ist der Schlusstermin angesetzt!');
				allChatterPosts.add(tmp_batchInput);
			}
		}
		postChatterMessages(allChatterPosts);
	}

	global void finish(Database.BatchableContext BC){
		System.debug('###-ReminderOpportunityBatch-finish EXECUTED!');
	}

	private ConnectApi.BatchInput generateChatterMessage(String mention_user_id, String record_Id, String short_text)
	{
		ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
		ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
		ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
		ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();

		messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();

		// if(mention_user_id != '')
		// {
		// 	//Mention user here
		// 	mentionSegmentInput.id = mention_user_id;
		// 	messageBodyInput.messageSegments.add(mentionSegmentInput);
		// }

		textSegmentInput.text = '\n'+short_text;
		messageBodyInput.messageSegments.add(textSegmentInput);

		feedItemInput.body = messageBodyInput;
		feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
		// datensatz an dem der chatter post hängt
		feedItemInput.subjectId = record_Id;

		ConnectApi.BatchInput batchInput = new ConnectApi.BatchInput(feedItemInput);

		return batchInput;
	}

	private void postChatterMessages(List<ConnectApi.BatchInput> batchinputs)
	{
		try{
			ConnectApi.ChatterFeeds.postFeedElementBatch(Network.getNetworkId(), batchinputs);
		System.debug('');}catch(Exception e){
			System.debug('###-ReminderOpportunityBatch-postChatterMessages: '+e);
		}
	}

}