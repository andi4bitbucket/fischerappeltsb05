/**************************************************************************
Company: aquilliance GmbH
Author: Andreas Wolf
CreatedDate: 2017-1

**************************************************************************/
global class account_status_check_schedulable implements Schedulable {
	/***********************************************
		to Start open execute anonymous

		account_status_check_schedulable aoa = new account_status_check_schedulable();
		system.schedule('Check Account Status', aoa.CRON_EXP_everyday_2am, aoa);
	***********************************************/
	public String CRON_EXP_everyday_2am =  '0 0 2 * * ?';

	public account_status_check_schedulable() {}

	global void execute(SchedulableContext sc) {
		run();
	}

	public void run(){
		account_status_check_batch b = new account_status_check_batch();
		database.executebatch(b);
	}

}