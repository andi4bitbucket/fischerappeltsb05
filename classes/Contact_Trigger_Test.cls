/**************************************************************************
Company: aquilliance GmbH
Author: Andre Mergen
CreatedDate: 2017-01

**************************************************************************/
@isTest
private class Contact_Trigger_Test {
	/********************************************************
				create test Records
	********************************************************/
	@testSetup static void TheSetup()
	{
		String acc_RecordTypeId = null;

		try{
			acc_RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'kunde' AND SobjectType='Account' LIMIT 1].Id;
		System.debug('');}catch(Exception e){System.debug('### error acc_RecordTypeId:'+e);}

		Account tmp_Acc = new Account();
		tmp_Acc.Name 							= 'Test Account';
		tmp_Acc.BillingStreet 		= 'Högerdamm 41';
		tmp_Acc.BillingCity				= 'Hamburg';
		tmp_Acc.BillingPostalCode = '20097';
		tmp_Acc.BillingCountry 		= 'Germany';
		tmp_Acc.RecordTypeId 			= acc_RecordTypeId;
		tmp_Acc.account_status__c = 'Gesperrter Kunde';

		insert tmp_Acc;

		Contact tmp_Con = new Contact();
		tmp_Con.AccountId 				= tmp_Acc.Id;
		tmp_con.Salutation 				= 'Herr';
		tmp_con.Firstname 				= 'Chuck';
		tmp_con.Lastname 					= 'Borris';
		tmp_con.MailingStreet 		= 'Högerdamm 41';
		tmp_con.MailingCity 			= 'Hamburg';
		tmp_con.MailingPostalCode	= '20097';
		tmp_con.MailingCountry 		= 'Germany';

		insert tmp_con;

	}

	@isTest static void test_method_Gesperrt()
	{
		Contact tmp_con = new Contact();
		tmp_con = [SELECT Id, Firstname, Lastname FROM Contact WHERE Lastname = 'Borris'];

		tmp_con.kontakt_status__c = 'Gesperrt';

		update tmp_con;
	}

	@isTest static void test_method_Inaktiv() {
		Contact tmp_con = new Contact();
		tmp_con = [SELECT Id, Firstname, Lastname FROM Contact WHERE Lastname = 'Borris'];

		tmp_con.kontakt_status__c = 'Inaktiv';

		update tmp_con;
	}

	@isTest static void test_method_sperrgrund()
	{
		Contact tmp_con = new Contact();
		tmp_con = [SELECT Id, Firstname, Lastname FROM Contact WHERE Lastname = 'Borris'];

		tmp_con.sperrgrund__c = 'verstorben';

		update tmp_con;
	}

	@isTest static void test_method_optout_status_vor_sperrung()
	{
		Contact tmp_con = new Contact();
		tmp_con = [SELECT Id, Firstname, Lastname FROM Contact WHERE Lastname = 'Borris'];

		tmp_con.HasOptedOutOfEmail = true;

		update tmp_con;
	}


	@isTest static void test_method_HasOptedOutOfEmail()
	{
		Contact tmp_con = new Contact();
		tmp_con = [SELECT Id, Firstname, Lastname FROM Contact WHERE Lastname = 'Borris'];

		tmp_con.kontakt_status__c = 'Inaktiv';
		tmp_con.optout_status_vor_sperrung__c  = 'gesperrt';

		update tmp_con;
		tmp_con.kontakt_status__c = '';
		update tmp_con;
	}

}