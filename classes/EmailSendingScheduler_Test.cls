/**************************************************************************
Company: aquilliance GmbH
Author: Andreas Wolf
CreatedDate: 2017-4
**************************************************************************/
@isTest
private class EmailSendingScheduler_Test {

	@testSetup static void theSetup()
	{
		// block opp trigger
		//Opportunity_Trigger.dontrun = true;

		Account acc = new Account();
		acc.Name = 'Test AQ';
		acc.BillingStreet = 'Högerdamm 41';
		acc.BillingCity = 'Hamburg';
		acc.BillingPostalCode = '20097';
		acc.BillingCountry = 'Germany';

		insert acc;

		String oppPitchRTid = [SELECT Id FROM RecordType WHERE sObjectType = 'Opportunity__c' AND DeveloperName = 'Pitch' LIMIT 1].Id;

		List<opportunity__c> listOpps = new List<opportunity__c>();
		// Won Opp
		opportunity__c opp = new opportunity__c();
		opp.Name = 'TestOpp';
		opp.account__c = acc.Id;
		opp.Stage__C = 'Gewinn';
		opp.honorarumsatz__c = 1000.00;
		opp.wahrscheinlichkeit__c = 50;
		opp.fremdkosten__c = 100;
		opp.projektstart__c = Date.Today();
		opp.projektende__c = Date.Today()+95;
		opp.zz_stage_date__c = Date.Today()-95;
		opp.zz_open_activities__c = 0;
		opp.schlusstermin__c = Date.Today()-5;
		opp.opportunity_art__c = 'Test';
		listOpps.add(opp);
		// Lost Opp
		opp = new opportunity__c();
		opp.Stage__C = 'Verlust';
		opp.opportunity_art__c = 'Test';
		opp.honorarumsatz__c = 1000.00;
		opp.wahrscheinlichkeit__c = 50;
		opp.nichtgewinngrund__c = 'Aufwand';
		opp.erlaeuterung_nichtgewinngrund__c = 'Test';
		listOpps.add(opp);
		// Pitch Opp
		opp = new opportunity__c();
		opp.Stage__C = 'Test';
		opp.Status__c = 'open';
		opp.honorarumsatz__c = 1000.00;
		opp.wahrscheinlichkeit__c = 50;
		opp.opportunity_art__c = 'Test';
		opp.RecordTypeId = oppPitchRTid;
		opp.nichtgewinngrund__c = '';
		opp.erlaeuterung_nichtgewinngrund__c = '';
		opp.next_step__c = Date.today()+20;
		opp.next_step_beschreibung__c = 'Test';
		listOpps.add(opp);

		insert listOpps;
	}

	@isTest static void testEmailSendingScheduler()
	{
		Test.startTest();
		 EmailSendingScheduler con = new EmailSendingScheduler();

		 // Schedule the test job
		 String CRON_EXP = '0 0 0 15 3 ? 2022';
		 String jobId = System.schedule('TESTING-ONLY 444', CRON_EXP, con);

		 // Get the information from the CronTrigger API object
		 CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];

		 // Verify the expressions are the same
		 System.assertEquals(CRON_EXP, ct.CronExpression);

		 // Verify the job has not run
		 System.assertEquals(0, ct.TimesTriggered);

		 // Verify the next time the job will run
		 System.assertEquals('2022-03-15 00:00:00', String.valueOf(ct.NextFireTime));

		Test.stopTest();
	}
}