/**************************************************************************
Company: aquilliance GmbH
Author: Andre Mergen
CreatedDate: 2017-01

**************************************************************************/
public with sharing class Account_Trigger
{

	/***************************************************************************************
	set Acc Name gesperrt
	***************************************************************************************/
	public void set_acc_name_gesperrt(List<Account> new_accounts,MAP<Id, Account> old_map_accounts)
	{
		String acc_RecordTypeId_Kunde = null;
		//id set von gesperrten accounts
		SET<Id> set_acc_ids = new SET<Id>();
		SET<Id> set_acc_ids_nicht_mehr_gesperrt = new SET<Id>();
		List<Contact> upd_list_contact = new List<Contact>();
		List<Contact> tmp_list_contact = new List<Contact>();

		try{
			acc_RecordTypeId_Kunde = [SELECT Id FROM RecordType WHERE DeveloperName = 'kunde' AND SobjectType='Account' LIMIT 1].Id;
		System.debug('');}catch(Exception e){System.debug('### error acc_RecordTypeId_Kunde:'+e);}


		for(Account tmp_new_acc:new_accounts)
		{

			/******************************************
				gesperrter kunde
			******************************************/
			if(tmp_new_acc.RecordTypeId == acc_RecordTypeId_Kunde)
			{

				if(tmp_new_acc.sperrgrund__c != null && tmp_new_acc.sperrgrund__c != '')
				{
					if(tmp_new_acc.account_status__c != 'Gesperrter Kunde')
					{
						tmp_new_acc.zz_status_vor_sperrung_acc__c = tmp_new_acc.account_status__c;
					}
					tmp_new_acc.account_status__c = 'Gesperrter Kunde';
				}
				else{
					if(tmp_new_acc.zz_status_vor_sperrung_acc__c != '' && tmp_new_acc.zz_status_vor_sperrung_acc__c != null && tmp_new_acc.zz_status_vor_sperrung_acc__c != 'Gesperrter Kunde')
					{
						tmp_new_acc.account_status__c = tmp_new_acc.zz_status_vor_sperrung_acc__c;
					}
				}


				if(tmp_new_acc.account_status__c == 'Gesperrter Kunde' && tmp_new_acc.Name.contains('[gesperrt]')==false)
				{
					tmp_new_acc.Name = tmp_new_acc.Name + ' [gesperrt]';
				}
				if(tmp_new_acc.account_status__c != 'Gesperrter Kunde' && tmp_new_acc.Name.contains('[gesperrt]')==true)
				{
					tmp_new_acc.Name = tmp_new_acc.Name.replace(' [gesperrt]','');
					tmp_new_acc.Name = tmp_new_acc.Name.trim();
				}

				/******************************************
					sammel vorhandene gesperrte Kunden
					um deren Kontakte zu updaten
				******************************************/
				if(tmp_new_acc.Id != null && tmp_new_acc.account_status__c == 'Gesperrter Kunde')
				{
					if(!set_acc_ids.contains(tmp_new_acc.Id))
					{
						set_acc_ids.add(tmp_new_acc.Id);
					}
				}
				if(tmp_new_acc.Id != null && (tmp_new_acc.account_status__c != 'Gesperrter Kunde'  && old_map_accounts.get(tmp_new_acc.Id).account_status__c == 'Gesperrter Kunde'))
				{
					if(!set_acc_ids_nicht_mehr_gesperrt.contains(tmp_new_acc.Id))
					{
						set_acc_ids_nicht_mehr_gesperrt.add(tmp_new_acc.Id);
					}
				}
				/*System.debug('### gesperrter kunde: '+tmp_new_acc);
				System.debug('### gesperrter kunde - RT: Kunde');
				System.debug('### gesperrter kunde - Name: '+tmp_new_acc.Name);
				System.debug('### gesperrter kunde - tmp_new_acc.account_status__c: '+tmp_new_acc.account_status__c);
				System.debug('### gesperrter kunde - tmp_new_acc.Name.contains([gesperrt]): '+tmp_new_acc.Name.contains('[gesperrt]'));
				System.debug('### #####################################################');*/
			}
		}

		System.debug('### set_acc_ids: '+set_acc_ids);
		if(set_acc_ids.size()>0)
		{
			tmp_list_contact = new List<Contact>();
			tmp_list_contact = update_contacts(set_acc_ids,'Gesperrt durch Firma', true);

			upd_list_contact.addAll(tmp_list_contact);
		}

		if(set_acc_ids_nicht_mehr_gesperrt.size()>0)
		{
			tmp_list_contact = new List<Contact>();
			tmp_list_contact = update_contacts(set_acc_ids_nicht_mehr_gesperrt,'', false);

			upd_list_contact.addAll(tmp_list_contact);
		}

		if(upd_list_contact.size()>0)
		{
			System.debug('### upd_list_contact: '+upd_list_contact);
			update upd_list_contact;
		}

	}

	/******************************************
					prepare contact update
	******************************************/
	public List<Contact> update_contacts(SET<Id> set_acc_ids,String sperr_status, Boolean sperren)
	{
		List<Contact> list_contact = new List<Contact>();
		if(sperren)
			list_contact = [SELECT Id, AccountId, Lastname, sperrgrund__c FROM Contact WHERE AccountId IN:set_acc_ids AND kontakt_status__c!='Gesperrt'];
		else
			list_contact = [SELECT Id, AccountId, Lastname, sperrgrund__c FROM Contact WHERE AccountId IN:set_acc_ids AND sperrgrund__c = 'Gesperrt durch Firma'];

		for(Contact tmp_con:list_contact)
		{
			tmp_con.sperrgrund__c = sperr_status;
		}


		return list_contact;
	}

	/***************************************************************************************
			Chatter Info erzeugt Chatter Meldungen für gesperrte Accounts,
			mit offenen Opp´s und Projekten
	***************************************************************************************/
	public void chatter_info(List<Account> new_accounts,MAP<Id, Account> map_old_acc)
	{
		SET<Id> set_acc_ids_gesperrt = new SET<Id>();
		SET<Id> set_acc_ids_nichtmehrgesperrt = new SET<Id>();
		List<Opportunity__c> list_opps_gesperrt = new List<Opportunity__c>();
		List<Opportunity__c> list_opps_nicht_mehr_gesperrt = new List<Opportunity__c>();

		List<Projekte__c> list_projects_gesperrt = new List<Projekte__c>();
		List<Projekte__c> list_projects_nicht_mehr_gesperrt = new List<Projekte__c>();

		// loop all Accs
		for(Account tmp_new_acc:new_accounts)
		{
			//if: wenn acc neu gesperrt wurde und es vorher noch nicht war
			//elseif: wenn acc vorher gesperrt war und es jezt nicht mehr ist.
			if(map_old_acc!=null && map_old_acc.containsKey(tmp_new_acc.Id))
			{
				if(tmp_new_acc.account_status__c == 'Gesperrter Kunde' && map_old_acc.get(tmp_new_acc.Id).account_status__c !=tmp_new_acc.account_status__c)
				{
					set_acc_ids_gesperrt.add(tmp_new_acc.Id);
				}
				else if(map_old_acc.get(tmp_new_acc.Id).account_status__c == 'Gesperrter Kunde' && map_old_acc.get(tmp_new_acc.Id).account_status__c != tmp_new_acc.account_status__c )
				{
					set_acc_ids_nichtmehrgesperrt.add(tmp_new_acc.Id);
				}
			}
		}

		/******************************************
			select opps
		******************************************/
		try{list_opps_gesperrt = [SELECT Id, Name, account__c, status__c, OwnerId FROM Opportunity__c WHERE account__c IN:set_acc_ids_gesperrt AND status__c != 'closed'];}catch(Exception e){System.debug('### select list_opps_gesperrt:'+e);}
		try{list_opps_nicht_mehr_gesperrt = [SELECT Id, Name, account__c, status__c, OwnerId FROM Opportunity__c WHERE account__c IN:set_acc_ids_nichtmehrgesperrt AND status__c != 'closed'];}catch(Exception e){System.debug('### select list_opps_gesperrt:'+e);}

		/******************************************
			select projects Projekte__c
		******************************************/
		try{list_projects_gesperrt = [SELECT Id, Name, account__c, isclosed__c, OwnerId FROM Projekte__c WHERE account__c IN:set_acc_ids_gesperrt AND isclosed__c = false];}catch(Exception e){System.debug('### select list_projects_gesperrt:'+e);}
		try{list_projects_nicht_mehr_gesperrt = [SELECT Id, Name, account__c, isclosed__c, OwnerId FROM Projekte__c WHERE account__c IN:set_acc_ids_nichtmehrgesperrt AND isclosed__c = false];}catch(Exception e){System.debug('### select list_projects_nicht_mehr_gesperrt:'+e);}

		//########################################################
		List<ConnectApi.BatchInput> batchInputs = new List<ConnectApi.BatchInput>();
		ConnectApi.BatchInput tmp_batchInput = new ConnectApi.BatchInput(new ConnectApi.FeedItemInput());


		/******************************************
		###########################################
												OPP´s
		###########################################
		******************************************/
		/******************************************
			loop offene opps von gesperrten acc´s
		******************************************/
		for(Opportunity__c tmp_opp : list_opps_gesperrt)
		{
			tmp_batchInput = new ConnectApi.BatchInput(new ConnectApi.FeedItemInput());
			//chatter_info_prepare_chatter_message erzeugt die eigentliche meldung
			tmp_batchInput = chatter_info_prepare_chatter_message(tmp_opp.OwnerId, tmp_opp.Id,'Der Account wurde gesperrt !');
			batchInputs.add(tmp_batchInput);
		}

		/******************************************
			loop offene opps von nicht
			mehr gesperrten acc´s
		******************************************/
		for(Opportunity__c tmp_opp : list_opps_nicht_mehr_gesperrt)
		{
			tmp_batchInput = new ConnectApi.BatchInput(new ConnectApi.FeedItemInput());
			//chatter_info_prepare_chatter_message erzeugt die eigentliche meldung
			tmp_batchInput = chatter_info_prepare_chatter_message(tmp_opp.OwnerId, tmp_opp.Id,'Der Account wurde entsperrt !');
			batchInputs.add(tmp_batchInput);
		}

		/******************************************
		###########################################
												PROJECT´s
		###########################################
		******************************************/
		/******************************************
			loop offene projects von gesperrten acc´s
		******************************************/
		for(Projekte__c tmp_pro : list_projects_gesperrt)
		{
			tmp_batchInput = new ConnectApi.BatchInput(new ConnectApi.FeedItemInput());
			//chatter_info_prepare_chatter_message erzeugt die eigentliche meldung
			tmp_batchInput = chatter_info_prepare_chatter_message(tmp_pro.OwnerId, tmp_pro.Id,'Der Account wurde gesperrt !');
			batchInputs.add(tmp_batchInput);
		}

		/******************************************
			loop offene projects von nicht
			mehr gesperrten acc´s
		******************************************/
		for(Projekte__c tmp_pro : list_projects_nicht_mehr_gesperrt)
		{
			tmp_batchInput = new ConnectApi.BatchInput(new ConnectApi.FeedItemInput());
			//chatter_info_prepare_chatter_message erzeugt die eigentliche meldung
			tmp_batchInput = chatter_info_prepare_chatter_message(tmp_pro.OwnerId, tmp_pro.Id,'Der Account wurde entsperrt !');
			batchInputs.add(tmp_batchInput);
		}

		//postet die chatter meldungen
		ConnectApi.ChatterFeeds.postFeedElementBatch(Network.getNetworkId(), batchinputs);
		//########################################################
	}

	/***************************************************************************************
			Erweiterung für Chatter Info, erzeugt den Chatter Eintrag
	***************************************************************************************/
	public ConnectApi.BatchInput chatter_info_prepare_chatter_message(String mention_user_id, String record_Id, String short_text)
	{

		ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
		ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
		ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
		ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();

		messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
		//Mention user here
		mentionSegmentInput.id = mention_user_id;
		messageBodyInput.messageSegments.add(mentionSegmentInput);

		textSegmentInput.text = '\n'+short_text;
		messageBodyInput.messageSegments.add(textSegmentInput);

		feedItemInput.body = messageBodyInput;
		feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
		// datensatz an dem der chatter post hängt
		feedItemInput.subjectId = record_Id;

		ConnectApi.BatchInput batchInput = new ConnectApi.BatchInput(feedItemInput);

		return batchInput;
	}

	public void setSearchableTextFields(List<Account> listAcc)
	{
		for(Account tmp:listAcc)
		{
			if(tmp.branche__c != NULL)
				tmp.zz_branche__c = tmp.branche__c.left(255);
			if(tmp.IsPersonAccount)
			{
				if(tmp.skills__c != null)
					tmp.skill_tags__c = tmp.skills__c.left(255);
			}
		}
	}

}