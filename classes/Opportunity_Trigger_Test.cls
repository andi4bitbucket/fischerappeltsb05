/**************************************************************************
Company: aquilliance GmbH
Author: Andreas Wolf & Andre Mergen
CreatedDate: 2017-1

**************************************************************************/
@isTest

private class Opportunity_Trigger_Test {
	/********************************************************
				create test Records
	********************************************************/
	@testSetup static void TheSetup()
	{
		Budget_Trigger.isSpecialTest = true;

		Profile p = [SELECT Id FROM Profile WHERE Name='Standard User' LIMIT 1];
		List<User> listUsers = new List<User>();
		User u = new User(Alias = 'standt', Email='standarduser@aquilliance.com',
					EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='de',
					LocaleSidKey='de', ProfileId = p.Id,
					TimeZoneSidKey='Europe/Berlin', UserName='standarduser@aquilliance.com');
		listUsers.add(u);
		User u2 = new User(Alias = 'stand2', Email='standarduser2@aquilliance.com',
					EmailEncodingKey='UTF-8', LastName='Testing2', LanguageLocaleKey='de',
					LocaleSidKey='de', ProfileId = p.Id,
					TimeZoneSidKey='Europe/Berlin', UserName='standarduser2@aquilliance.com');
		listUsers.add(u2);
		User u3 = new User(Alias = 'stand3', Email='standarduser3@aquilliance.com',
					EmailEncodingKey='UTF-8', LastName='Testing3', LanguageLocaleKey='de',
					LocaleSidKey='de', ProfileId = p.Id,
					TimeZoneSidKey='Europe/Berlin', UserName='standarduser3@aquilliance.com');
		listUsers.add(u3);
		insert listUsers;

		Account tmp_acc = new Account();
		tmp_acc.Name = 'Test AQ';
		tmp_acc.BillingStreet = 'Högerdamm 41';
		tmp_acc.BillingCity = 'Hamburg';
		tmp_acc.BillingPostalCode = '20097';
		tmp_acc.BillingCountry = 'Germany';

		insert tmp_acc;

		opportunity__c tmp_opp = new opportunity__c();
		tmp_opp.Name = 'MainTestOpp';
		tmp_opp.account__c = tmp_acc.Id;
		tmp_opp.Stage__c = 'open';
		tmp_opp.honorarumsatz__c = 200000.00;
		tmp_opp.Opportunity_Art__c = 'Neukundengeschäft';
		tmp_opp.fremdkosten__c = 100;
		tmp_opp.Owner__c = UserInfo.getUserId();
		tmp_opp.Quelle_person__c = u3.Id;
		tmp_opp.projektstart__c = Date.Today();
		tmp_opp.projektende__c = Date.Today()+95;
		tmp_opp.zz_stage_date__c = Date.Today()-95;
		tmp_opp.zz_open_activities__c = 0;
		tmp_opp.schlusstermin__c = Date.Today()-300;
		tmp_opp.leistungssegmente__c = 'Test1; Test2;';

		insert tmp_opp;
	}

	@isTest static void test_method_one()
	{
		Budget_Trigger.isSpecialTest = true;
		Account acc = new Account();
		acc.Name = 'Test AQ1';
		acc.BillingStreet = 'Högerdamm 41';
		acc.BillingCity = 'Hamburg';
		acc.BillingPostalCode = '20097';

		insert acc;


		opportunity__c opp = new opportunity__c();
		opp.Name = 'TestOpp';
		opp.account__c = acc.Id;
		opp.Stage__C = 'Test';
		opp.honorarumsatz__c = 1000.00;
		opp.fremdkosten__c = 100;
		opp.Owner__c = UserInfo.getUserId();
		opp.projektstart__c = Date.Today();
		opp.projektende__c = Date.Today()+95;
		opp.zz_stage_date__c = Date.Today()-95;
		opp.zz_open_activities__c = 0;
		opp.schlusstermin__c = Date.Today()-300;
		opp.leistungssegmente__c = 'Test1; Test2;';

		insert opp;
		delete opp;
	}


	@isTest static void test_newOppStageGewinn()
	{
		Budget_Trigger.isSpecialTest = true;
		Account acc = new Account();
		acc.Name = 'Test AQ2';
		acc.BillingStreet = 'Högerdamm 41';
		acc.BillingCity = 'Hamburg';
		acc.BillingPostalCode = '20097';
		acc.BillingCountry = 'Germany';

		insert acc;


		opportunity__c opp = new opportunity__c();
		opp.Name = 'TestOpp';
		opp.account__c = acc.Id;
		opp.Stage__C = 'Gewinn';
		opp.honorarumsatz__c = 1000.00;
		opp.fremdkosten__c = 100;
		opp.projektstart__c = Date.Today();
		opp.projektende__c = Date.Today()+95;
		opp.zz_stage_date__c = Date.Today()-95;
		opp.zz_open_activities__c = 0;
		opp.schlusstermin__c = Date.Today()-300;

		insert opp;
	}

	@isTest static void test_updateOppStageGewinn()
	{
		Budget_Trigger.isSpecialTest = true;
		Account acc = new Account();
		acc.Name = 'Test AQ3';
		acc.BillingStreet = 'Högerdamm 41';
		acc.BillingCity = 'Hamburg';
		acc.BillingPostalCode = '20097';
		acc.BillingCountry = 'Germany';

		insert acc;


		opportunity__c opp = new opportunity__c();
		opp.Name = 'TestOpp';
		opp.account__c = acc.Id;
		opp.Stage__C = 'Test';
		opp.honorarumsatz__c = 1000.00;
		opp.fremdkosten__c = 100;
		opp.projektstart__c = Date.Today();
		opp.projektende__c = Date.Today()+95;
		opp.zz_stage_date__c = Date.Today()-95;
		opp.zz_open_activities__c = 0;
		opp.schlusstermin__c = Date.Today()-300;

		insert opp;


		Test.startTest();
		opp.stage__c = 'Gewinn';
		update opp;
		Test.stopTest();
	}

	@isTest static void tm_OppChangeHonorarumsatz()
	{
		Budget_Trigger.isSpecialTest = true;
			Account tmp_Acc = new Account();
			tmp_Acc.Name = 'Test AQ4';
			tmp_Acc.BillingStreet = 'Högerdamm 41';
			tmp_Acc.BillingCity = 'Hamburg';
			tmp_Acc.BillingPostalCode = '20097';
			tmp_Acc.BillingCountry = 'Germany';

			insert tmp_Acc;


			opportunity__c tmp_opp = new opportunity__c();
			tmp_opp.Name = 'TestOpp';
			tmp_opp.account__c = tmp_Acc.Id;
			tmp_opp.Stage__C = 'Test';
			tmp_opp.honorarumsatz__c = 1000.00;
			tmp_opp.fremdkosten__c = 100;
			tmp_opp.projektstart__c = Date.Today();
			tmp_opp.projektende__c = Date.Today()+95;
			tmp_opp.zz_stage_date__c = Date.Today()-95;
			tmp_opp.zz_open_activities__c = 0;
			tmp_opp.schlusstermin__c = Date.Today()-300;
			tmp_opp.wahrscheinlichkeit__c = 1;
			tmp_opp.Stage__c = 'Gewinn';

			insert tmp_opp;

			Test.startTest();

			tmp_opp.honorarumsatz__c = tmp_opp.honorarumsatz__c + 1000;
			update tmp_opp;
			Test.stopTest();
		}

		@isTest static void tm_OppChangeWahrscheinlichkeit() {
			Budget_Trigger.isSpecialTest = true;
			Account tmp_Acc = new Account();
			tmp_Acc.Name = 'Test AQ5';
			tmp_Acc.BillingStreet = 'Högerdamm 41';
			tmp_Acc.BillingCity = 'Hamburg';
			tmp_Acc.BillingPostalCode = '20097';
			tmp_Acc.BillingCountry = 'Germany';

			insert tmp_Acc;


			opportunity__c tmp_opp = new opportunity__c();
			tmp_opp.Name = 'TestOpp';
			tmp_opp.account__c = tmp_Acc.Id;
			tmp_opp.Stage__C = 'Test';
			tmp_opp.honorarumsatz__c = 1000.00;
			tmp_opp.fremdkosten__c = 100;
			tmp_opp.projektstart__c = Date.Today();
			tmp_opp.projektende__c = Date.Today()+95;
			tmp_opp.zz_stage_date__c = Date.Today()-95;
			tmp_opp.zz_open_activities__c = 0;
			tmp_opp.schlusstermin__c = Date.Today()-300;
			tmp_opp.wahrscheinlichkeit__c = 1;
			tmp_opp.Stage__c = 'Gewinn';

			insert tmp_opp;

			Test.startTest();

			tmp_opp.wahrscheinlichkeit__c = 0.8;
			update tmp_opp;
			Test.stopTest();
		}


		@isTest static void tm_OppChangeWahrscheinlichkeitWithSekundaerBudget()
		{
			Budget_Trigger.isSpecialTest = true;
			Test.StartTest();
			Opportunity_Trigger.run = false;

			Account tmp_Acc = new Account();
			tmp_Acc.Name = 'Test AQ6';
			tmp_Acc.BillingStreet = 'Högerdamm 41';
			tmp_Acc.BillingCity = 'Hamburg';
			tmp_Acc.BillingPostalCode = '20097';
			tmp_Acc.BillingCountry = 'Germany';

			insert tmp_Acc;


			opportunity__c tmp_opp = new opportunity__c();
			tmp_opp.Name = 'TestOpp';
			tmp_opp.account__c = tmp_Acc.Id;
			tmp_opp.Stage__C = 'Test';
			tmp_opp.honorarumsatz__c = 1000.00;
			tmp_opp.fremdkosten__c = 100;
			tmp_opp.projektstart__c = Date.Today();
			tmp_opp.projektende__c = Date.Today()+95;
			tmp_opp.zz_stage_date__c = Date.Today()-95;
			tmp_opp.zz_open_activities__c = 0;
			tmp_opp.schlusstermin__c = Date.Today()-300;
			tmp_opp.wahrscheinlichkeit__c = 0.5;

			insert tmp_opp;


			Budget_Trigger.run = false;
			budget__c tmp_budget = new budget__c();
			//Account tmp_acc = [SELECT Id, Name FROM Account WHERE Name = 'Test AQ' LIMIT 1];
			//opportunity__c tmp_opp = [SELECT Id, Name, account__c FROM opportunity__c WHERE account__c =: tmp_acc.Id LIMIT 1];
			User currUser = [SELECT Id, ProfileId FROM User WHERE Id=: UserInfo.getUserId()];

			// Controller Test
			PageReference pref = Page.Opportunity_plan_Budget;
			pref.getParameters().put('Id',tmp_opp.Id);
			Test.setCurrentPage(pref);


			ApexPages.StandardController opp = new ApexPages.StandardController(tmp_opp);
			Opportunity_plan_Budget_Ext opb = new Opportunity_plan_Budget_Ext(opp);

			opb.add_new();

			tmp_budget = opb.list_wrapper_budgets[1].budget;
			tmp_budget.honorarumsatz_absolut__c = 500.00;
			tmp_budget.benutzer__c = currUser.Id;

			opb.calc_summe_budget_update_primary();

			opb.row_to_reset_linear = '1';
			opb.one_linear();
			opb.save_now();

			Test.StopTest();

			Opportunity_Trigger.run = true;
			tmp_opp.wahrscheinlichkeit__c = 0.6;
			update tmp_opp;
		}

		@isTest static void tm_OppChangeHonorarumsatzWithSekundaerBudget()
		{
			Budget_Trigger.isSpecialTest = true;
			Test.StartTest();
			Opportunity_Trigger.run = false;

			Account tmp_Acc = new Account();
			tmp_Acc.Name = 'Test AQ7';
			tmp_Acc.BillingStreet = 'Högerdamm 41';
			tmp_Acc.BillingCity = 'Hamburg';
			tmp_Acc.BillingPostalCode = '20097';
			tmp_Acc.BillingCountry = 'Germany';

			insert tmp_Acc;


			opportunity__c tmp_opp = new opportunity__c();
			tmp_opp.Name = 'TestOpp';
			tmp_opp.account__c = tmp_Acc.Id;
			tmp_opp.Stage__C = 'Test';
			tmp_opp.honorarumsatz__c = 1000.00;
			tmp_opp.fremdkosten__c = 100;
			tmp_opp.projektstart__c = Date.Today();
			tmp_opp.projektende__c = Date.Today()+95;
			tmp_opp.zz_stage_date__c = Date.Today()-95;
			tmp_opp.zz_open_activities__c = 0;
			tmp_opp.schlusstermin__c = Date.Today()-300;
			tmp_opp.wahrscheinlichkeit__c = 0.5;

			insert tmp_opp;


			Budget_Trigger.run = false;
			budget__c tmp_budget = new budget__c();
			//Account tmp_acc = [SELECT Id, Name FROM Account WHERE Name = 'Test AQ' LIMIT 1];
			//opportunity__c tmp_opp = [SELECT Id, Name, account__c FROM opportunity__c WHERE account__c =: tmp_acc.Id LIMIT 1];
			User currUser = [SELECT Id, ProfileId FROM User WHERE Id=: UserInfo.getUserId()];

			// Controller Test
			PageReference pref = Page.Opportunity_plan_Budget;
			pref.getParameters().put('Id',tmp_opp.Id);
			Test.setCurrentPage(pref);


			ApexPages.StandardController opp = new ApexPages.StandardController(tmp_opp);
			Opportunity_plan_Budget_Ext opb = new Opportunity_plan_Budget_Ext(opp);

			opb.add_new();

			tmp_budget = opb.list_wrapper_budgets[1].budget;
			tmp_budget.honorarumsatz_absolut__c = 500.00;
			tmp_budget.benutzer__c = currUser.Id;

			opb.calc_summe_budget_update_primary();

			opb.row_to_reset_linear = '1';
			opb.one_linear();
			opb.save_now();

			Test.StopTest();

			tmp_opp.honorarumsatz__c = tmp_opp.honorarumsatz__c + 1000;
			update tmp_opp;
		}

/**********************STAGE PART*************************/
		//Lead
		@isTest static void test_Stage_UPD_Lead()
		{
			Budget_Trigger.isSpecialTest = true;
			Opportunity__c tmp_opp = new Opportunity__c();
			tmp_opp = [SELECT Id, Name, Stage__c FROM Opportunity__c WHERE Name = 'MainTestOpp'];

			tmp_opp.Stage__c = 'Lead';
			update tmp_opp;
		}

		//Anfrage_allgemein
		@isTest static void test_Stage_UPD_Anfrage_allgemein()
		{
			Budget_Trigger.isSpecialTest = true;
			Opportunity__c tmp_opp = new Opportunity__c();
			tmp_opp = [SELECT Id, Name, Stage__c FROM Opportunity__c WHERE Name = 'MainTestOpp'];

			tmp_opp.Stage__c = 'Anfrage_allgemein';
			update tmp_opp;
		}

		//Proposal_allgemein
		@isTest static void test_Stage_UPD_Proposal_allgemein()
		{
			Budget_Trigger.isSpecialTest = true;
			Opportunity__c tmp_opp = new Opportunity__c();
			tmp_opp = [SELECT Id, Name, Stage__c FROM Opportunity__c WHERE Name = 'MainTestOpp'];

			tmp_opp.Stage__c = 'Proposal_allgemein';
			update tmp_opp;
		}

		//Anfrage_konkret
		@isTest static void test_Stage_UPD_Anfrage_konkret()
		{
			Budget_Trigger.isSpecialTest = true;
			Opportunity__c tmp_opp = new Opportunity__c();
			tmp_opp = [SELECT Id, Name, Stage__c FROM Opportunity__c WHERE Name = 'MainTestOpp'];

			tmp_opp.Stage__c = 'Anfrage_konkret';
			update tmp_opp;
		}

		//Termin_Chemistry_Meeting
		@isTest static void test_Stage_UPD_Termin_Chemistry_Meeting()
		{
			Budget_Trigger.isSpecialTest = true;
			Opportunity__c tmp_opp = new Opportunity__c();
			tmp_opp = [SELECT Id, Name, Stage__c FROM Opportunity__c WHERE Name = 'MainTestOpp'];

			tmp_opp.Stage__c = 'Termin_Chemistry_Meeting';
			update tmp_opp;
		}

		//Konzeptpraesentation
		@isTest static void test_Stage_UPD_Konzeptpraesentation()
		{
			Budget_Trigger.isSpecialTest = true;
			Opportunity__c tmp_opp = new Opportunity__c();
			tmp_opp = [SELECT Id, Name, Stage__c FROM Opportunity__c WHERE Name = 'MainTestOpp'];

			tmp_opp.Stage__c = 'Konzeptpraesentation';
			update tmp_opp;
		}

		//Proposal_konkret
		@isTest static void test_Stage_UPD_Proposal_konkret()
		{
			Budget_Trigger.isSpecialTest = true;
			Opportunity__c tmp_opp = new Opportunity__c();
			tmp_opp = [SELECT Id, Name, Stage__c FROM Opportunity__c WHERE Name = 'MainTestOpp'];

			tmp_opp.Stage__c = 'Proposal_konkret';
			update tmp_opp;
		}

		//Pitch_gewonnen
		@isTest static void test_Stage_UPD_Pitch_gewonnen()
		{
			Budget_Trigger.isSpecialTest = true;
			Opportunity__c tmp_opp = new Opportunity__c();
			tmp_opp = [SELECT Id, Name, Stage__c FROM Opportunity__c WHERE Name = 'MainTestOpp'];

			tmp_opp.Stage__c = 'Pitch_gewonnen';
			update tmp_opp;
		}

		//Verhandlung
		@isTest static void test_Stage_UPD_Verhandlung()
		{
			String oppRecordTypeId_Pitch = '';
			try{
				oppRecordTypeId_Pitch = [SELECT Id FROM RecordType WHERE DeveloperName = 'Pitch' AND SobjectType='Opportunity__c' LIMIT 1].Id;
			System.debug('');}catch(Exception e){System.debug('### error Opp RecordTypeId Pitch:'+e);}

			Budget_Trigger.isSpecialTest = true;
			Opportunity__c tmp_opp = new Opportunity__c();
			tmp_opp = [SELECT Id, Name, Stage__c FROM Opportunity__c WHERE Name = 'MainTestOpp'];

			tmp_opp.Stage__c = 'Verhandlung';
			tmp_opp.RecordTypeId = oppRecordTypeId_Pitch;
			update tmp_opp;
		}

		//Gewinn
		@isTest static void test_Stage_UPD_Gewinn()
		{
			Budget_Trigger.isSpecialTest = true;
			Opportunity__c tmp_opp = new Opportunity__c();
			tmp_opp = [SELECT Id, Name, Stage__c FROM Opportunity__c WHERE Name = 'MainTestOpp'];

			tmp_opp.Stage__c = 'Gewinn';
			update tmp_opp;
		}
/********************** STAGE PART ENDE *************************/

		@isTest static void test_changeFollowingOwnerUser()
		{
			Budget_Trigger.isSpecialTest = true;

			Opportunity__c opp = [SELECT Id FROM Opportunity__c WHERE Name = 'MainTestOpp' LIMIT 1];
			User u = [SELECT Id FROM User WHERE UserName = 'standarduser@aquilliance.com' LIMIT 1];
			User u2 = [SELECT Id FROM User WHERE UserName = 'standarduser2@aquilliance.com' LIMIT 1];

			opp.Owner__c = u.Id;
			opp.Quelle_person__c = u2.Id;
			update opp;

			delete opp;
		}

}