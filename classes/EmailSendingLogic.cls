/**************************************************************************
Company: aquilliance GmbH
Author: Andreas Wolf
CreatedDate: 2017-4
**************************************************************************/
public with sharing class EmailSendingLogic {

	private Set<Id> setUserIds;
	private Set<Id> setRoleIds;
	private List<opportunity__c> listOppWonThisWeek;
	private List<opportunity__c> listOppLostThisWeek;
	private List<opportunity__c> listOppPitch;
	private List<opportunity__c> listOppNewTop;
	private String htmlStringWonOpps;
	private String htmlStringLostOpps;
	private String htmlStringOppPitch;
	private String htmlStringNewTopOpps;

	public EmailSendingLogic(){}

	public void sendOppReportsToGroup()
	{
		getReportData();
		prepareHtmlStrings();
		System.debug('### sendOppReportsToGroup called!');
		List<Messaging.SingleEmailMessage> emailMessages = new List<Messaging.SingleEmailMessage>();
		for(String emailAddress:getRecipientEmailAddresses())
		{
			emailMessages.add(createEmail(emailAddress));
		}
		sendEmails(emailMessages);
	}

	public List<String> getRecipientEmailAddresses()
	{
		List<String> listRecipientEmailAddresses = new List<String>();
		getGroupMemberUserIds(getGroups('fischerAppelt_Manager', new Set<Id>())[0].Id);
		for(User tmp:getUsers(setUserIds, setRoleIds))
		{
			System.debug('### tmp.Email: '+tmp.Email);
			listRecipientEmailAddresses.add(tmp.Email);
		}
		return listRecipientEmailAddresses;
	}

	public List<User> getUsers(Set<Id> setUserIds, Set<Id> setRoleIds)
	{
		List<User> listUsers = new List<User>();
		try{
			listUsers = [SELECT Id, Email FROM User WHERE Id IN: setUserIds OR (UserRoleId IN: setRoleIds AND Id NOT IN:setUserIds)];
		System.debug('');}catch(Exception e){
			System.debug('###-EmailSendingLogic-getUsers: '+e);
		}
		return listUsers;
	}

	public void getGroupMemberUserIds(String groupId)
	{
		List<GroupMember> listGroupMember = new List<GroupMember>();
		try{
			listGroupMember = [SELECT Id, UserOrGroupId, Group.RelatedId, Group.Type FROM GroupMember WHERE GroupId =: groupId];
		System.debug('');}catch(Exception e){
			System.debug('###-EmailSendingLogic-getGroupMember: '+e);
		}
		System.debug('### listGroupMember: '+listGroupMember);
		setUserIds = new Set<Id>();
		setRoleIds = new Set<Id>();
		Set<Id> setRoleGroupIds = new Set<Id>();
		for(GroupMember tmp:listGroupMember)
		{
			System.debug('### tmp.UserOrGroupId: '+tmp.UserOrGroupId);
			if(String.valueOf(tmp.UserOrGroupId).substring(0,3) == '00G')
			{
				setRoleGroupIds.add(tmp.UserOrGroupId);
			}
			else
			{
				if(!setUserIds.contains(tmp.UserOrGroupId))
					setUserIds.add(tmp.UserOrGroupId);
			}
		}
		System.debug('### setUserIds: '+setUserIds);

		for(Group tmp:getGroups('', setRoleGroupIds))
		{
			if(!setRoleIds.contains(tmp.RelatedId))
				setRoleIds.add(tmp.RelatedId);
		}
		System.debug('### setRoleIds: '+setRoleIds);
	}

	public List<Group> getGroups(String groupName, Set<Id> setGroupIds)
	{
		List<Group> listGroup = new List<Group>();
		try{
			listGroup = [SELECT Id, RelatedId FROM Group WHERE DeveloperName =: groupName OR Id IN: setGroupIds];
		System.debug('');}catch(Exception e){
			System.debug('###-EmailSendingLogic-getGroups: '+e);
		}
		return listGroup;
	}

	public Messaging.SingleEmailMessage createEmail(String emailAddress)
  {
		List<String> toAddresses = new List<String>();
    toAddresses.add(emailAddress);

		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
	  mail.setSaveAsActivity(false);
	  mail.setUseSignature(false);
	  mail.setBccSender(false);
	  mail.setToAddresses(toAddresses);
	  mail.setSubject('The Good, the Bad and the Ugly');
	  mail.setHtmlBody('<head><style>td, th{border:1px solid #A4A4A4; padding: 3px;}a{text-decoration:none;}</style></head>'
	  +'<p style="margin-top:0 !important;"><b>Sugar for us all!</b></p>'
		+'<p>WON-Opportunites aus dieser Woche</p>'
		+'<table style="border:1px solid #A4A4A4;border-collapse:collapse; margin-bottom: 35px;">'
		+'<tr>'
		+'<th>Opportunity Name</th>'
		+'<th>Firmenname</th>'
		+'<th>Owner</th>'
		+'<th>Agentur</th>'
		+'<th>Honorarumsatz</th>'
		+'<th>Art</th>'
		+'</tr>'
		+htmlStringWonOpps
	  +'</table>'
		+'<p><b>Here\'s what\'s not so sweet...!</b></p>'
		+'<p>LOST-Opportunites aus dieser Woche</p>'
		+'<table style="border:1px solid #A4A4A4;border-collapse:collapse; margin-bottom: 35px;">'
		+'<tr>'
		+'<th>Opportunity Name</th>'
		+'<th>Firmenname</th>'
		+'<th>Owner</th>'
		+'<th>Agentur</th>'
		+'<th>Honorarumsatz</th>'
		+'<th>Art</th>'
		+'<th>Verlustgrund</th>'
		+'<th>Erläuterung</th>'
		+'</tr>'
		+htmlStringLostOpps
		+'</table>'
		+'<p><b>We\'ll keep our fingers crossed</b></p>'
		+'<p>Running Pitches</p>'
		+'<table style="border:1px solid #A4A4A4;border-collapse:collapse; margin-bottom: 35px;">'
		+'<tr>'
		+'<th>Opportunity Name</th>'
		+'<th>Firmenname</th>'
		+'<th>Owner</th>'
		+'<th>Agentur</th>'
		+'<th>Wahrscheinlichkeit(%)</th>'
		+'<th>Honorarumsatz</th>'
		+'<th>Art</th>'
		+'<th>Next Step</th>'
		+'<th>Beschreibung</th>'
		+'</tr>'
		+htmlStringOppPitch
		+'</table>'
		+'<p><b>New big fish on the hook!</b></p>'
		+'<p>Neue Top-Opportunities aus dieser Woche</p>'
		+'<table style="border:1px solid #A4A4A4;border-collapse:collapse; margin-bottom: 35px;">'
		+'<tr>'
		+'<th>Opportunity Name</th>'
		+'<th>Firmenname</th>'
		+'<th>Owner</th>'
		+'<th>Agentur</th>'
		+'<th>Honorarumsatz</th>'
		+'<th>Stage</th>'
		+'<th>Wahrscheinlichkeit(%)</th>'
		+'<th>Art</th>'
		+'<th>Next Step</th>'
		+'</tr>'
		+htmlStringNewTopOpps
		+'</table>');
	  mail.setSenderDisplayName('fischerAppelt - Salesforce');
	  System.debug('### Email: '+mail);

    return mail;
  }

  public void sendEmails(List<Messaging.SingleEmailMessage> emailMessages)
  {
    List<Messaging.SendEmailResult> results = new List<Messaging.SendEmailResult>();
    if(emailMessages.size() > 0)
    {
      try{
				Messaging.sendEmail(emailMessages, true);
      System.debug('');}catch(Exception e){
				System.debug('### -EmailSendingExt-sendEmails: '+e);
			}
    }
  }

	public void getReportData()
	{
		listOppWonThisWeek = getOpportunities('won');
		listOppLostThisWeek = getOpportunities('lost');
		listOppPitch = getOpportunities('pitch');
		listOppNewTop = getOpportunities('top');
		System.debug('### listOppWonThisWeek: '+listOppWonThisWeek);
		System.debug('### listOppLostThisWeek: '+listOppLostThisWeek);
		System.debug('### listOppPitch: '+listOppPitch);
		System.debug('### listOppNewTop: '+listOppNewTop);
	}

	public List<opportunity__c> getOpportunities(String reportType)
	{
		List<opportunity__c> listOpps = new List<opportunity__c>();
		String query = 'SELECT Id, Name, Owner.FirstName, Owner.LastName, honorarumsatz__c, opportunity_art__c,';
		query += ' schlusstermin__c, account__c, account__r.Name, mandant__c, mandant__r.Name, nichtgewinngrund__c,';
		query += ' erlaeuterung_nichtgewinngrund__c, wahrscheinlichkeit__c, next_step__c, next_step_beschreibung__c,';
		query += ' stage__c, CreatedDate';
		query += ' FROM opportunity__c';
		query += ' WHERE ID != null';

		Date startDate = Date.today()-7;
		Date endDate;
		if(Test.isRunningTest())
		 endDate = Date.today()+1;
		else
		 endDate = Date.today();

		System.debug('###-EmailSendingLogic-getOpportunities startDate: '+startDate+'    EndDate: '+endDate);

		if(reportType == 'won')
		{
			query += ' AND schlusstermin__c >=: startDate';
			query += ' AND schlusstermin__c <=: endDate';
			query += ' AND stage__c = \'Gewinn\'';
			query += ' ORDER BY honorarumsatz__c DESC';
		}
		if(reportType == 'lost')
		{
			query += ' AND schlusstermin__c >=: startDate';
			query += ' AND schlusstermin__c <=: endDate';
			query += ' AND stage__c = \'Verlust\'';
			query += ' ORDER BY honorarumsatz__c DESC';
		}
		if(reportType == 'pitch')
		{
			query += ' AND RecordType.DeveloperName = \'Pitch\'';
			query += ' AND Status__c = \'open\'';
			query += ' ORDER BY honorarumsatz__c DESC';
		}
		if(reportType == 'top')
		{
			query += ' AND CreatedDate >=: startDate';
			query += ' AND CreatedDate <=: endDate';
			query += ' ORDER BY honorarumsatz_gewichtet__c DESC';
			query += ' LIMIT 25';
		}
		System.debug('###-EmailSendingLogic-getOpportunities query: '+query);
		listOpps = Database.query(query);
		return listOpps;
	}

	public void prepareHtmlStrings()
	{
		String urlInstance = String.valueof(System.URL.getSalesforceBaseURL()).replace('Url:[delegate=','').replace(']','');
		String[] domain = urlInstance.split('\\.');
		//String puffer = domain[0];
		String theDomain = domain[0]; //puffer.substring(0, puffer.length()-3);

		System.debug('### urlInstance: '+urlInstance);
		System.debug('### theDomain: '+theDomain);

		htmlStringWonOpps = '';
		htmlStringLostOpps = '';
		htmlStringOppPitch = '';
		htmlStringNewTopOpps = '';
		for(Opportunity__c tmp:listOppWonThisWeek)
		{
			String art = '';
			if(tmp.opportunity_art__c != null && tmp.opportunity_art__c != '')
				art = tmp.opportunity_art__c;

			htmlStringWonOpps += '<tr>';
			htmlStringWonOpps += '<td><a href="'+theDomain+'.lightning.force.com/one/one.app#/sObject/'+tmp.Id+'/view">'+tmp.Name+'</a></td>';
			htmlStringWonOpps += '<td>'+tmp.account__r.Name+'</td>';
			htmlStringWonOpps += '<td>'+tmp.Owner.FirstName+' '+tmp.Owner.LastName+'</td>';
			htmlStringWonOpps += '<td>'+tmp.mandant__r.Name+'</td>';
			htmlStringWonOpps += '<td>'+tmp.honorarumsatz__c.format()+' €</td>';
			htmlStringWonOpps += '<td>'+art+'</td>';
			htmlStringWonOpps += '</tr>';
		}
		for(Opportunity__c tmp:listOppLostThisWeek)
		{
			String art = '';
			if(tmp.opportunity_art__c != null && tmp.opportunity_art__c != '')
				art = tmp.opportunity_art__c;

			String nowingrund = '';
			if(tmp.nichtgewinngrund__c != null && tmp.nichtgewinngrund__c != '')
				nowingrund = tmp.nichtgewinngrund__c;

			String nowingrunddesc = '';
			if(tmp.erlaeuterung_nichtgewinngrund__c != null && tmp.erlaeuterung_nichtgewinngrund__c != '')
				nowingrunddesc = tmp.erlaeuterung_nichtgewinngrund__c;


			htmlStringLostOpps += '<tr>';
			htmlStringLostOpps += '<td><a href="'+theDomain+'.lightning.force.com/one/one.app#/sObject/'+tmp.Id+'/view">'+tmp.Name+'</a></td>';
			htmlStringLostOpps += '<td>'+tmp.account__r.Name+'</td>';
			htmlStringLostOpps += '<td>'+tmp.Owner.FirstName+' '+tmp.Owner.LastName+'</td>';
			htmlStringLostOpps += '<td>'+tmp.mandant__r.Name+'</td>';
			htmlStringLostOpps += '<td>'+tmp.honorarumsatz__c.format()+' €</td>';
			htmlStringLostOpps += '<td>'+art+'</td>';
			htmlStringLostOpps += '<td>'+nowingrund+'</td>';
			htmlStringLostOpps += '<td>'+nowingrunddesc+'</td>';
			htmlStringLostOpps += '</tr>';
		}
		for(Opportunity__c tmp:listOppPitch)
		{
			String art = '';
			if(tmp.opportunity_art__c != null && tmp.opportunity_art__c != '')
				art = tmp.opportunity_art__c;

			String nextstep = '';
			if(tmp.next_step__c != null )
				nextstep = tmp.next_step__c.format();

			String nextstepdesc = '';
			if(tmp.next_step_beschreibung__c != null && tmp.next_step_beschreibung__c != '')
				nextstepdesc = tmp.next_step_beschreibung__c;


			htmlStringOppPitch += '<tr>';
			htmlStringOppPitch += '<td><a href="'+theDomain+'.lightning.force.com/one/one.app#/sObject/'+tmp.Id+'/view">'+tmp.Name+'</a></td>';
			htmlStringOppPitch += '<td>'+tmp.account__r.Name+'</td>';
			htmlStringOppPitch += '<td>'+tmp.Owner.FirstName+' '+tmp.Owner.LastName+'</td>';
			htmlStringOppPitch += '<td>'+tmp.mandant__r.Name+'</td>';
			htmlStringOppPitch += '<td>'+tmp.wahrscheinlichkeit__c+'</td>';
			htmlStringOppPitch += '<td>'+tmp.honorarumsatz__c.format()+' €</td>';
			htmlStringOppPitch += '<td>'+art+'</td>';
			htmlStringOppPitch += '<td>'+nextstep+'</td>';
			htmlStringOppPitch += '<td>'+nextstepdesc+'</td>';
			htmlStringOppPitch += '</tr>';
		}
		for(Opportunity__c tmp:listOppNewTop)
		{
			String art = '';
			if(tmp.opportunity_art__c != null && tmp.opportunity_art__c != '')
				art = tmp.opportunity_art__c;

			String nextstep = '';
			if(tmp.next_step__c != null )
				nextstep = tmp.next_step__c.format();

			htmlStringNewTopOpps += '<tr>';
			htmlStringNewTopOpps += '<td><a href="'+theDomain+'.lightning.force.com/one/one.app#/sObject/'+tmp.Id+'/view">'+tmp.Name+'</a></td>';
			htmlStringNewTopOpps += '<td>'+tmp.account__r.Name+'</td>';
			htmlStringNewTopOpps += '<td>'+tmp.Owner.FirstName+' '+tmp.Owner.LastName+'</td>';
			htmlStringNewTopOpps += '<td>'+tmp.mandant__r.Name+'</td>';
			htmlStringNewTopOpps += '<td>'+tmp.honorarumsatz__c.format()+' €</td>';
			htmlStringNewTopOpps += '<td>'+tmp.stage__c+'</td>';
			htmlStringNewTopOpps += '<td>'+tmp.wahrscheinlichkeit__c+'</td>';
			htmlStringNewTopOpps += '<td>'+art+'</td>';
			htmlStringNewTopOpps += '<td>'+nextstep+'</td>';
			htmlStringNewTopOpps += '</tr>';
		}
	}
}