/**************************************************************************
Company: aquilliance GmbH
Author: Andre Mergen
CreatedDate: 2016-12

**************************************************************************/
@isTest
private class Account_Address_Sync_to_Contact_EXT_Test {


	/********************************************************
				create test Records
	********************************************************/
	@testSetup static void TheSetup()
	{
		Account tmp_Acc = new Account();
		tmp_Acc.Name 								= 'Test AQ';
		tmp_Acc.BillingStreet 			= 'Högerdamm 41';
		tmp_Acc.BillingCity 				= 'Hamburg';
		tmp_Acc.BillingPostalCode 	= '20097';
		tmp_Acc.BillingCountry 			= 'Germany';

		tmp_Acc.ShippingStreet 			= 'Högerdamm 41';
		tmp_Acc.ShippingCity 				= 'Hamburg';
		tmp_Acc.ShippingPostalCode 	= '20097';
		tmp_Acc.ShippingCountry 		= 'Germany';


		insert tmp_Acc;

		List<Contact> list_new_Contacts = new List<Contact>();

		Contact tmp_Con_1 = new Contact();
		tmp_Con_1.AccountId = tmp_Acc.Id;
		tmp_Con_1.Salutation = 'Herr';
		tmp_Con_1.Firstname = 'Chuck';
		tmp_Con_1.Lastname = 'Blue';
		tmp_Con_1.MailingStreet = 'Blumen Str. 1';
		tmp_Con_1.MailingCity = 'Hamburg';
		tmp_Con_1.MailingPostalCode = '20097';
		tmp_Con_1.MailingCountry = 'Germany';
		list_new_Contacts.add(tmp_Con_1);

		Contact tmp_Con_2 = new Contact();
		tmp_Con_2.AccountId = tmp_Acc.Id;
		tmp_Con_2.Salutation = 'Herr';
		tmp_Con_2.Firstname = 'Borris';
		tmp_Con_2.Lastname = 'Norris';
		tmp_Con_2.MailingStreet = 'Traum Str. 1';
		tmp_Con_2.MailingCity = 'Hamburg';
		tmp_Con_2.MailingPostalCode = '20097';
		tmp_Con_2.MailingCountry = 'Germany';
		list_new_Contacts.add(tmp_Con_2);

		Contact tmp_Con_3 = new Contact();
		tmp_Con_3.AccountId = tmp_Acc.Id;
		tmp_Con_3.Salutation = 'Frau';
		tmp_Con_3.Firstname = 'Lola';
		tmp_Con_3.Lastname = 'Flora';
		tmp_Con_3.MailingStreet = 'Högerdamm 41';
		tmp_Con_3.MailingCity = 'Hamburg';
		tmp_Con_3.MailingPostalCode = '20097';
		tmp_Con_3.MailingCountry = 'Germany';
		list_new_Contacts.add(tmp_Con_3);

		insert list_new_Contacts;
	}


	@isTest static void test_method_one()
	{
		Account tmp_acc = [SELECT Id, Name FROM Account WHERE Name = 'Test AQ' LIMIT 1];

		// Controller Test
		PageReference pref = Page.Account_Address_Sync_to_Contact;
		pref.getParameters().put('Id',tmp_acc.Id);
		Test.setCurrentPage(pref);

		ApexPages.StandardController acc = new ApexPages.StandardController(tmp_acc);
		Account_Address_Sync_to_Contact_EXT aasc = new Account_Address_Sync_to_Contact_EXT(acc);

		aasc.btn_select_all();
		aasc.btn_deselect_all();
		aasc.btn_sync_selected();
		aasc.btn_show_conf_pop();
		aasc.btn_nix();
	}


	@isTest static void test_method_two()
	{
		Account tmp_acc = [SELECT Id, Name FROM Account WHERE Name = 'Test AQ' LIMIT 1];

		// Controller Test
		PageReference pref = Page.Account_Address_Sync_to_Contact;
		pref.getParameters().put('Id',tmp_acc.Id);
		Test.setCurrentPage(pref);

		ApexPages.StandardController acc = new ApexPages.StandardController(tmp_acc);
		Account_Address_Sync_to_Contact_EXT aasc = new Account_Address_Sync_to_Contact_EXT(acc);

		aasc.btn_select_all();
		aasc.btn_sync_selected();
	}

}