/**************************************************************************
Company: aquilliance GmbH
Author: Andreas Wolf & Andre Mergen
CreatedDate: 2017-1
**************************************************************************/
public with sharing class Opportunity_Trigger
{
		private User u;
		private String relevantChatterGroupId;
		private String oppRecordTypeId_Pitch;
		private Map<Id, Opportunity__c> mapOppsForGettingCustomerNumber;

		public Opportunity_Trigger(){
			mapOppsForGettingCustomerNumber = new Map<Id, Opportunity__c>();
		}

		public static boolean run = true;
		public static boolean runOnce()
		{
			if(run)
			{
			 	run=false;
			 	system.debug('### runonce = ' + run);
			 	return true;
			}
			else
			{
				system.debug('### runonce = ' + run);
				return run;
			}
		}

		/******************************************
			wenn eine neue Opp erstellt wird, soll
			immer auch ein neues Budget erstellt werden.
		******************************************/
		public void after_insert_create_budget(List<Opportunity__c> list_opps)
		{
			// Class aus dem Opp Budgetplaner
			Opportunity_plan_Budget_Ext con;
			ApexPages.StandardController standard;
			for(Opportunity__c tmp: list_opps)
			{
				standard = new ApexPages.StandardController(tmp);
				con = new Opportunity_plan_Budget_Ext(standard);
				con.save_now();
			}
		}

		/******************************************
		 HonorarumsatzGewichtet
		******************************************/
		public void afterUpdateOppChangeBudget(List<Opportunity__c> list_opps,MAP<Id, Opportunity__c> map_old_opps)
		{
			// vars
			Set<Id> set_oppId = new Set<Id>();
			Map<Id, Opportunity__c> map_opp = new Map<Id, Opportunity__c>();
			List<budget__c> list_primaerBudgets = new List<budget__c>();
			List<budget__c> list_sekundaerBudgets = new List<budget__c>();
			// key opp
			Map<Id, budget__c> map_primaerBudgets = new Map<Id, budget__c>();
			Map<Id, List<budget__c>> map_sekundaerBudgets = new Map<Id, List<budget__c>>();

			List<budget__c> list_monatsBudgets = new List<budget__c>();
			// key primaer_budget
			Map<Id, List<budget__c>> map_monatsBudgets = new Map<Id, List<budget__c>>();

			Set<Id> set_primaerBudgetId = new Set<Id>();
			Set<Id> set_sekundaerBudgetId = new Set<Id>();
			List<budget__c> list_update_allBudgets = new List<budget__c>();

			//tmp vars, used in loops etc
			List<budget__c> tmp_list_monatsBudgets = new List<budget__c>();

			set_oppId = (new Map<Id, Opportunity__c>(list_opps)).keySet();
			map_opp = (new Map<Id, Opportunity__c>(list_opps));

			/***************************************************************************
			select primaerbudget
			***************************************************************************/
			list_primaerBudgets = selectBudgetByOppAndRecordTypeName('primaer_budget',set_oppId);
			set_primaerBudgetId = (new Map<Id, budget__c>(list_primaerBudgets)).keySet();
			for(budget__c tmp_primaerBudget:list_primaerBudgets)
			{
				if(!map_primaerBudgets.containsKey(tmp_primaerBudget.opportunity__c))
				{
						map_primaerBudgets.put(tmp_primaerBudget.opportunity__c,tmp_primaerBudget);
				}
			}

			/***************************************************************************
			select sekundaer_budget
			***************************************************************************/
			list_sekundaerBudgets = selectBudgetByOppAndRecordTypeName('sekundaer_budget',set_oppId);
			set_sekundaerBudgetId = (new Map<Id, budget__c>(list_sekundaerBudgets)).keySet();
			//add month entries to map
			for(budget__c tmp_sekundaerBudget:list_sekundaerBudgets)
			{
				tmp_list_monatsBudgets = new List<budget__c>();
				if(map_sekundaerBudgets.containsKey(tmp_sekundaerBudget.opportunity__c))
				{
					tmp_list_monatsBudgets = map_sekundaerBudgets.get(tmp_sekundaerBudget.opportunity__c);
				}
				tmp_list_monatsBudgets.add(tmp_sekundaerBudget);


				map_sekundaerBudgets.put(tmp_sekundaerBudget.opportunity__c, tmp_list_monatsBudgets);
			}

			/***************************************************************************
			select monats budgets
			***************************************************************************/
			list_monatsBudgets = selectMonatsBudget(set_primaerBudgetId, set_sekundaerBudgetId);
			//add month entries to map
			for(budget__c tmp_monatsBudget:list_monatsBudgets)
			{
				tmp_list_monatsBudgets = new List<budget__c>();
				if(map_monatsBudgets.containsKey(tmp_monatsBudget.budget__c))
				{
					tmp_list_monatsBudgets = map_monatsBudgets.get(tmp_monatsBudget.budget__c);
				}
				tmp_list_monatsBudgets.add(tmp_monatsBudget);


				map_monatsBudgets.put(tmp_monatsBudget.budget__c,tmp_list_monatsBudgets);
			}

			for(Opportunity__c tmp_opp:list_opps)
			{
				if(map_old_opps.containsKey(tmp_opp.Id))
				{
					// beträge erhöhen
					if(tmp_opp.wahrscheinlichkeit__c != map_old_opps.get(tmp_opp.Id).wahrscheinlichkeit__c && map_old_opps.get(tmp_opp.Id).honorarumsatz_gewichtet__c>0)
					{
						//calculatePercentalBudgetChange(Opportunity__c opp, Opportunity__c old_opp, budget__c primaer_budget, List<budget__c> list_sekundaerBudgets)
						list_update_allBudgets.addAll(calculatePercentalBudgetChange(tmp_opp, map_old_opps.get(tmp_opp.Id), map_primaerBudgets.get(tmp_opp.Id), map_sekundaerBudgets.get(tmp_opp.Id), map_monatsBudgets));
					}
					// linear neu berechnen
					else if(tmp_opp.honorarumsatz_gewichtet__c != map_old_opps.get(tmp_opp.Id).honorarumsatz_gewichtet__c)
					{
						//calculateLinearBudget(Opportunity__c opp, budget__c primaer_budget, List<budget__c> list_monatsBudgets, List<budget__c> list_sekundaerBudgets)
						// returned dann list für update
						list_update_allBudgets.addAll(calculateLinearBudget(tmp_opp, map_primaerBudgets.get(tmp_opp.Id), map_monatsBudgets.get(map_primaerBudgets.get(tmp_opp.Id).Id), map_sekundaerBudgets.get(tmp_opp.Id)));
					}
				}
			}

			if(list_update_allBudgets.size()>0)
			{
				try{
					update list_update_allBudgets;
				System.debug('');}catch(Exception e){
					System.debug('###-Opportunity_Trigger-afterUpdateOppChangeBudget-update allBudgets: '+e);
				}
			}
		}

		private List<budget__c> calculateLinearBudget(Opportunity__c opp, budget__c primaer_budget, List<budget__c> list_monatsBudgets, List<budget__c> list_sekundaerBudgets)
		{
			//init vars
			Integer nr_of_month = 0;
			Decimal summe_honorarumsatz_absolut = 0.0;
			List<budget__c> list_update_allBudgets = new List<budget__c>();
			nr_of_month = opp.projektstart__c.monthsBetween(opp.projektende__c)+1;
			if(list_sekundaerBudgets != null && list_sekundaerBudgets.size()>0)
			{
				for(budget__c tmp_sekundaerBudgets:list_sekundaerBudgets)
				{
					summe_honorarumsatz_absolut = summe_honorarumsatz_absolut + tmp_sekundaerBudgets.honorarumsatz_absolut__c;
				}
			}

			primaer_budget.honorarumsatz_absolut__c = primaer_budget.honorarumsatz_gewichtet__c-summe_honorarumsatz_absolut;
			list_update_allBudgets.add(primaer_budget);

			for(budget__c tmp_monatsBudget:list_monatsBudgets)
			{
				tmp_monatsBudget.honorarumsatz_absolut__c = primaer_budget.honorarumsatz_absolut__c/nr_of_month;
			}
			list_update_allBudgets.addAll(list_monatsBudgets);
			return list_update_allBudgets;
		}

		private List<budget__c> calculatePercentalBudgetChange(Opportunity__c opp, Opportunity__c old_opp, budget__c primaer_budget, List<budget__c> list_sekundaerBudgets, Map<Id, List<budget__c>> map_monatsBudgets)
		{
			List<budget__c> list_monatsBudgets = new List<budget__c>();
			List<budget__c> list_updateBudgets = new List<budget__c>();
			Decimal percent = 0.0;

			if(old_opp.honorarumsatz_gewichtet__c>0){percent = opp.honorarumsatz_gewichtet__c / old_opp.honorarumsatz_gewichtet__c;}

			primaer_budget.honorarumsatz_absolut__c = primaer_budget.honorarumsatz_absolut__c * percent;
			list_updateBudgets.add(primaer_budget);
			/***************************************************************************
					loop primaer monatsBudget
			***************************************************************************/
			if(map_monatsBudgets.containsKey(primaer_budget.Id))
			{
				for(budget__c tmp_monatsBudget:map_monatsBudgets.get(primaer_budget.Id))
				{
						tmp_monatsBudget.honorarumsatz_absolut__c = tmp_monatsBudget.honorarumsatz_absolut__c * percent;
						list_updateBudgets.add(tmp_monatsBudget);
				}
			}
			/***************************************************************************
			sekundaerBudget
			***************************************************************************/
			if(list_sekundaerBudgets!=null && list_sekundaerBudgets.size()>0)
			{
				for(budget__c tmp_sekundaerBudget:list_sekundaerBudgets)
				{
					tmp_sekundaerBudget.honorarumsatz_absolut__c = tmp_sekundaerBudget.honorarumsatz_absolut__c * percent;
					list_updateBudgets.add(tmp_sekundaerBudget);
					/***************************************************************************
					loop sekundär monatsBudget
					***************************************************************************/
					if(map_monatsBudgets.containsKey(tmp_sekundaerBudget.Id))
					{
						for(budget__c tmp_monatsBudget:map_monatsBudgets.get(tmp_sekundaerBudget.Id))
						{
								tmp_monatsBudget.honorarumsatz_absolut__c = tmp_monatsBudget.honorarumsatz_absolut__c * percent;
								list_updateBudgets.add(tmp_monatsBudget);
						}
					}
				}
			}

			return list_updateBudgets;
		}

		private List<budget__c> selectBudgetByOppAndRecordTypeName(String recordTypeDeveloperName, SET<Id> set_oppId)
		{
			List<budget__c> list_budget = new List<budget__c>();

			String recordTypeId_primaer_budget = '';
			try{
			RecordTypeId_primaer_budget = [SELECT Id FROM RecordType WHERE DeveloperName =: recordTypeDeveloperName AND SobjectType='budget__c' LIMIT 1].Id;
			System.debug('');}catch(Exception e){System.debug('### error RecordTypeId_primaer_budget:'+e);}

			list_budget = [SELECT Id,
			Name,
			opportunity__c,
			RecordTypeId,
			budget__c,
			benutzer__c,
			benutzer__r.Name,
			honorarumsatz_absolut__c,
			honorarumsatz_gewichtet__c,
			betreffender_monat__c,
			kostenstelle__c
			FROM budget__c WHERE opportunity__c IN:set_oppId AND RecordTypeId =:RecordTypeId_primaer_budget AND budget__c = null];

			return list_budget;
		}

		public List<budget__c> selectMonatsBudget(SET<Id> set_primaerBudgetId, SET<Id> set_sekundaerBudgetId)
		{
			SET<Id> set_allBudgetId = new SET<Id>();
			set_allBudgetId.addAll(set_primaerBudgetId);
			set_allBudgetId.addAll(set_sekundaerBudgetId);
			List<budget__c> list_child_budget = new List<budget__c>();

			String RecordTypeId_monats_budget = '';
			try{
				RecordTypeId_monats_budget = [SELECT Id FROM RecordType WHERE DeveloperName = 'monats_budget' AND SobjectType='budget__c' LIMIT 1].Id;
			System.debug('');}catch(Exception e){System.debug('### error select_monats_budgets RecordTypeId:'+e);}

			try{
			list_child_budget = [SELECT Id,
			Name,
			opportunity__c,
			RecordTypeId,
			budget__c,
			benutzer__c,
			honorarumsatz_absolut__c,
			betreffender_monat__c
			FROM budget__c WHERE budget__c IN:set_allBudgetId AND RecordTypeId =:RecordTypeId_monats_budget ORDER BY betreffender_monat__c];
			System.debug('');}catch(Exception e){System.debug('### error list_child_budget:'+e);}

			return list_child_budget;
		}

	//******************************************** INSERT *************************************************************************************
		public void createFollowingOwnerUser(Map<ID, Opportunity__c> mapOpps)
		{
			// Get all existing subscriptions for Opportunity Record --> so we dont create a duplicate
			List<EntitySubscription> list_existing_follows = new List<EntitySubscription>();
			try{list_existing_follows = [SELECT Id, subscriberid FROM EntitySubscription WHERE parentId IN: mapOpps.KeySet()];
			System.debug('');}catch(Exception e){System.debug('### -Opportunity_Trigger-createFollowingOwnerUser- getExisitingSubscriptions:'+e);}
			System.debug('### -Opportunity_Trigger-createFollowingOwnerUser- list_existing_follows:'+list_existing_follows);

			Set<String> set_existing_follows = new Set<String>();
			for(EntitySubscription tmp:list_existing_follows)
			{
				set_existing_follows.add(tmp.subscriberid);
			}

			List<EntitySubscription> list_follows = new List<EntitySubscription>();
			Set<String> set_subscriptions = new Set<String>();
			for(Opportunity__c tmp:mapOpps.values())
			{
				// Owner
				if(!set_subscriptions.contains(tmp.Id+' '+tmp.Owner__c) && !set_existing_follows.contains(tmp.Owner__c))
				{
					EntitySubscription follow = new EntitySubscription();
					follow.parentId = tmp.Id;
					follow.subscriberid = tmp.Owner__c;

					list_follows.add(follow);
					set_subscriptions.add(tmp.Id+' '+tmp.Owner__c);
				}
				// Quelle Person
				if(!set_subscriptions.contains(tmp.Id+' '+tmp.Quelle_person__c) && !set_existing_follows.contains(tmp.Quelle_person__c) && tmp.Quelle_person__c != null)
				{
					EntitySubscription follow = new EntitySubscription();
					follow.parentId = tmp.Id;
					follow.subscriberid = tmp.Quelle_person__c;

					list_follows.add(follow);
					set_subscriptions.add(tmp.Id+' '+tmp.Quelle_person__c);
				}
			}

			System.debug('### -Opportunity_Trigger-createFollowingOwnerUser- list_follows: '+list_follows);
			try{upsert list_follows;
			System.debug('');}catch(Exception e){System.debug('### -Opportunity_Trigger-createFollowingOwnerUser- upsertFollows:'+e);}
		}

	//******************************************** UPDATE *************************************************************************************
		public void changeFollowingOwnerUser(Map<ID, Opportunity__c> mapNewOpps, Map<ID, Opportunity__c> mapOldOpps){
			// Check wether the assigned Owner__c has changed
			List<Opportunity__c> listOpps = new List<Opportunity__c>();
			Set<String> setSubscriptionsForDeletion = new Set<String>();
			for(Opportunity__c tmp : mapNewOpps.values())
			{
				// Owner
				if(mapOldOpps.get(tmp.Id).Owner__c != tmp.Owner__c && tmp.Owner__c != mapOldOpps.get(tmp.Id).Quelle_person__c)
				{
					listOpps.add(tmp);
					if(!setSubscriptionsForDeletion.contains(mapOldOpps.get(tmp.Id).Owner__c))
						setSubscriptionsForDeletion.add(mapOldOpps.get(tmp.Id).Owner__c);
				}
				// Quelle Person
				if(mapOldOpps.get(tmp.Id).Quelle_person__c != tmp.Quelle_person__c && tmp.Quelle_person__c != mapOldOpps.get(tmp.Id).Owner__c)
				{
					listOpps.add(tmp);
					if(!setSubscriptionsForDeletion.contains(mapOldOpps.get(tmp.Id).Quelle_person__c))
						setSubscriptionsForDeletion.add(mapOldOpps.get(tmp.Id).Quelle_person__c);
				}
			}

			// Get all existing subscriptions for Opportunity Record --> so we dont create a duplicate
			List<EntitySubscription> list_existing_follows = new List<EntitySubscription>();
			try{list_existing_follows = [SELECT Id, subscriberid FROM EntitySubscription WHERE parentId IN: mapNewOpps.KeySet()];
			System.debug('');}catch(Exception e){System.debug('### -Opportunity_Trigger-changeFollowingOwnerUser- getExisitingSubscriptions:'+e);}
			System.debug('### -Opportunity_Trigger-changeFollowingOwnerUser- list_existing_follows:'+list_existing_follows);

			Set<String> set_existing_follows = new Set<String>();
			List<EntitySubscription> list_delete_existing_follows = new List<EntitySubscription>();
			for(EntitySubscription tmp:list_existing_follows)
			{
				set_existing_follows.add(tmp.subscriberid);
				// Check if the Subscription should be deleted because its replaced
				if(setSubscriptionsForDeletion.contains(tmp.subscriberid))
				{
					list_delete_existing_follows.add(tmp);
				}
			}

			List<EntitySubscription> list_follows = new List<EntitySubscription>();
			Set<String> set_subscriptions = new Set<String>();
			for(Opportunity__c tmp:listOpps)
			{
				// Owner
				if(!set_subscriptions.contains(tmp.Id+' '+tmp.Owner__c) && !set_existing_follows.contains(tmp.Owner__c))
				{
					EntitySubscription follow = new EntitySubscription();
					follow.parentId = tmp.Id;
					follow.subscriberid = tmp.Owner__c;

					list_follows.add(follow);
					set_subscriptions.add(tmp.Id+' '+tmp.Owner__c);
				}
				// Quelle Person
				if(!set_subscriptions.contains(tmp.Id+' '+tmp.Quelle_person__c) && !set_existing_follows.contains(tmp.Quelle_person__c) && tmp.Quelle_person__c != null)
				{
					EntitySubscription follow = new EntitySubscription();
					follow.parentId = tmp.Id;
					follow.subscriberid = tmp.Quelle_person__c;

					list_follows.add(follow);
					set_subscriptions.add(tmp.Id+' '+tmp.Quelle_person__c);
				}
			}

			System.debug('### -Opportunity_Trigger-changeFollowingOwnerUser- list_follows: '+list_follows);
			try{upsert list_follows;
			System.debug('');}catch(Exception e){System.debug('### -Opportunity_Trigger-createFollowingOwnerUser- upsertFollows:'+e);}
			// delete subscriptions
			System.debug('### -Opportunity_Trigger-changeFollowingOwnerUser- list_delete_existing_follows: '+list_delete_existing_follows);
			try{delete list_delete_existing_follows;
			System.debug('');}catch(Exception e){System.debug('### -Opportunity_Trigger-changeFollowingOwnerUser- deleteReplacedUserFollows:'+e);}
		}

		// 8.2017 - Andreas Wolf
		/**************************************************************************
			Prüft, ob bei insert/update einer Opp irgendwelche Chatter Posts erstellt werden müssen
		**************************************************************************/
		public void afterInsertCheckIFcreateChatterPosts(Map<Id, Opportunity__c> mapOpps)
		{
			System.debug('###-Opportunity_Trigger-afterInsertCheckIFcreateChatterPosts EXECUTED!');
			u = [SELECT Id, FirstName, LastName, Email FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
			relevantChatterGroupId = getChatterGroupID('New Business');
			Map<Id, Opportunity__c> mapTopOpp = new Map<Id, Opportunity__c>();

			for(Opportunity__c tmp : mapOpps.values())
			{
				// Top-Opportunity
				if(tmp.Honorarumsatz__c > 100000)
				{
					mapTopOpp.put(tmp.Id, tmp);
				}
			}

			List<ConnectApi.BatchInput> allChatterPosts = new List<ConnectApi.BatchInput>();
			allChatterPosts.addAll(prepareChatterMessagesForRelevantOpps(mapOpps, 'NewOpp'));
			allChatterPosts.addAll(prepareChatterMessagesForRelevantOpps(mapTopOpp, 'NewTopOpp'));

			postChatterMessages(allChatterPosts, 'afterInsertCheckIFcreateChatterPosts');
		}

		public void afterUpdateCheckIFcreateChatterPosts(List<Opportunity__c> listOpps, Map<Id, Opportunity__c> mapOldOpps)
		{
			System.debug('###-Opportunity_Trigger-afterUpdateCheckIFcreateChatterPosts EXECUTED!');
			u = [SELECT Id, FirstName, LastName, Email FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
			relevantChatterGroupId = getChatterGroupID('New Business');
			Map<Id, Opportunity__c> mapWonOpp = new Map<Id, Opportunity__c>();
			Map<Id, Opportunity__c> mapWonOppNewCustomer = new Map<Id, Opportunity__c>();
			Map<Id, Opportunity__c> mapWonOppHU = new Map<Id, Opportunity__c>();
			Map<Id, Opportunity__c> mapWonOppForCampaign = new Map<Id, Opportunity__c>();

			for(Opportunity__c tmp : listOpps)
			{
				// Old Values from Stage and Status should not be the following
				if(mapOldOpps.get(tmp.Id).Stage__c != 'Gewinn')
				{
					// Won Opportunity
					if(tmp.Stage__c == 'Gewinn')
					{
						mapWonOpp.put(tmp.Id, tmp);
						// New Customer
						if(tmp.Opportunity_Art__c == 'Neukundengeschäft')
						{
							mapWonOppNewCustomer.put(tmp.Id, tmp);
						}
						// Over 50.000 HU
						if(tmp.Honorarumsatz__c > 50000)
						{
							mapWonOppHU.put(tmp.Id, tmp);
						}
						// Post to related campaign
						if(tmp.Kampagne__c != null)
						{
							mapWonOppForCampaign.put(tmp.Id, tmp);
						}
					}
				}
			}

			List<ConnectApi.BatchInput> allChatterPosts = new List<ConnectApi.BatchInput>();
			allChatterPosts.addAll(prepareChatterMessagesForRelevantOpps(mapWonOpp, 'WonOpp'));
			allChatterPosts.addAll(prepareChatterMessagesForRelevantOpps(mapWonOppNewCustomer, 'WonOppNewCustomer'));
			allChatterPosts.addAll(prepareChatterMessagesForRelevantOpps(mapWonOppHU, 'WonOppHU'));
			allChatterPosts.addAll(prepareChatterMessagesForRelevantOpps(mapWonOppForCampaign, 'WonOppForCampaign'));

			postChatterMessages(allChatterPosts, 'afterUpdateCheckIFcreateChatterPosts');
		}

		public String getChatterGroupID(String name)
		{
			try{
				return [SELECT Id FROM CollaborationGroup WHERE Name =: name LIMIT 1].Id;
			System.debug('');}catch(Exception e){
				System.debug('###-Opportunity_Trigger-getChatterGroupID: '+e);
			}
			return '';
		}

		private List<ConnectApi.BatchInput> prepareChatterMessagesForRelevantOpps(Map<Id, Opportunity__c> mapOpps, String type)
		{
			List<ConnectApi.BatchInput> batchInputs = new List<ConnectApi.BatchInput>();
			ConnectApi.BatchInput tmp_batchInput;
			for(Opportunity__c tmp : mapOpps.values())
			{
				tmp_batchInput = new ConnectApi.BatchInput(new ConnectApi.FeedItemInput());
				tmp_batchInput = generateChatterMessageForType(type, tmp);
				batchInputs.add(tmp_batchInput);
			}
			return batchInputs;
		}

		private ConnectApi.BatchInput generateChatterMessageForType(String type, Opportunity__c opp)
		{
			if(type == 'NewOpp')
				return generateChatterMessage('', opp.Account__c, u.FirstName+' '+u.LastName+' hat bei '+opp.zz_account_name__c+' eine neue Opportunity erstellt: '+opp.Name);
			if(type == 'NewTopOpp')
				return generateChatterMessage('', relevantChatterGroupId, 'New Big Fish on the hook! '+opp.Name);
			if(type == 'WonOpp')
				return generateChatterMessage('', opp.Account__c, 'Gratulation! Opportunity \''+opp.Name+'\' wurde gewonnen!');
			if(type == 'WonOppNewCustomer')
				return generateChatterMessage('', relevantChatterGroupId, 'Gratulation! Neukundengeschäft Opportunity \''+opp.Name+'\' wurde beim Kunden '+opp.zz_account_name__c+' gewonnen!');
			if(type == 'WonOppHU')
				return generateChatterMessage('', relevantChatterGroupId, 'Gratulation! Opportunity \''+opp.Name+'\' (mit Honorarumsatz > 50.000) wurde beim Kunden '+opp.zz_account_name__c+' gewonnen!');
			if(type == 'WonOppForCampaign')
				return generateChatterMessage('', opp.Kampagne__c, 'Gratulation! Opportunity \''+opp.Name+'\' wurde gewonnen!');

			return new ConnectApi.BatchInput(new ConnectApi.FeedItemInput());
		}

		private ConnectApi.BatchInput generateChatterMessage(String mention_user_id, String record_Id, String short_text)
		{
			ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
			ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
			ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
			ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
			messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();

			// if(mention_user_id != '')
			// {
			// 	mentionSegmentInput.id = mention_user_id;
			// 	messageBodyInput.messageSegments.add(mentionSegmentInput);
			// }

			textSegmentInput.text = '\n'+short_text;
			messageBodyInput.messageSegments.add(textSegmentInput);

			feedItemInput.body = messageBodyInput;
			feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
			feedItemInput.subjectId = record_Id; // datensatz an dem der chatter post hängt

			ConnectApi.BatchInput batchInput = new ConnectApi.BatchInput(feedItemInput);

			return batchInput;
		}

		private void postChatterMessages(List<ConnectApi.BatchInput> batchinputs, String startingMethod)
		{
			try{
				ConnectApi.ChatterFeeds.postFeedElementBatch(Network.getNetworkId(), batchinputs);
			System.debug('');}catch(Exception e){
				System.debug('###-Opportunity_Trigger-'+startingMethod+'-postChatterMessages: '+e);
			}
		}

		// Controller Method for all Before Insert Opp Logic
		public void beforeInsertController(List<Opportunity__c> listOpps)
		{
			oppRecordTypeId_Pitch = '';
			try{oppRecordTypeId_Pitch = [SELECT Id FROM RecordType WHERE DeveloperName = 'Pitch' AND SobjectType='Opportunity__c' LIMIT 1].Id;
			System.debug('');}catch(Exception e){System.debug('###-Opportunity_Trigger-beforeInsertController: '+e);}

			for(Opportunity__c tmp : listOpps)
			{
				// New Methods from Process Builder migrated
				beforeInsertUpdateSetMandantFromOwner(tmp);  // ALWAYS OVERWRITE Opp.Mandant__c --> Opp.Owner__r.zz_OwnerAgenturId__c
				beforeInsertUpdateSetVertragsfuehrendeAgenturFromOwner(tmp); // If Opp.Vertragsfuehrende_agentur__c == null, dann --> Opp.Owner__r.zz_OwnerAgenturId__c
				beforeInsertUpdateSetStageDate(tmp, null);  // Directly Set Opp.zz_stage_date__c
				beforeInsertUpdateChangeStatusAndSalesLevel(tmp, null); // Directly Set Opp.saleslevel__c AND Opp.Status__c

				// Old Methods
				setSearchableTextFields(tmp); // Writes specific fields in text fields so they are searchable
				beforeInsertUpdateChangeWahrscheinlichkeit(tmp, null); // If Opp.Stage__c changed, change Opp.wahrscheinlichkeit__c
				// EXECUTE AFTER beforeInsertUpdateChangeWahrscheinlichkeit
				beforeInsertUpdateCalcHonorarumsatzGewichtet(tmp, null); // If Changed Values, recalculate Opp.honorarumsatz_gewichtet_currency__c
			}
		}
		// Controller Method for all Before Update Opp Logic
		public void beforeUpdateController(List<Opportunity__c> listOpps, Map<Id, Opportunity__c> mapOldOpps)
		{
			oppRecordTypeId_Pitch = '';
			try{oppRecordTypeId_Pitch = [SELECT Id FROM RecordType WHERE DeveloperName = 'Pitch' AND SobjectType='Opportunity__c' LIMIT 1].Id;
			System.debug('');}catch(Exception e){System.debug('###-Opportunity_Trigger-beforeInsertUpdateController: '+e);}

			for(Opportunity__c tmp : listOpps)
			{
				// New Methods (partially from Process Builder integrated)
				beforeInsertUpdateSetMandantFromOwner(tmp);   // ALWAYS OVERWRITE Opp.Mandant__c --> Opp.Owner__r.zz_OwnerAgenturId__c
				beforeInsertUpdateSetVertragsfuehrendeAgenturFromOwner(tmp); // If Opp.Vertragsfuehrende_agentur__c == null, dann --> Opp.Owner__r.zz_OwnerAgenturId__c
				beforeInsertUpdateSetStageDate(tmp, mapOldOpps.get(tmp.Id)); // If Opp.Stage__c changed, Set Opp.zz_stage_date__c
				beforeInsertUpdateChangeStatusAndSalesLevel(tmp, mapOldOpps.get(tmp.Id));  // If Opp.Stage__c changed, Set Opp.saleslevel__c AND Opp.Status__c
				beforeUpdateSavePriorStageValue(tmp, mapOldOpps.get(tmp.Id)); // If Opp.Stage__c changed, Set Old Stage value for Opp.zz_last_stage__c
				beforeUpdateResetEmailSentBool(tmp, mapOldOpps.get(tmp.Id)); // If Opp.Stage__c changed AND Opp.zz_email_sent__c = true ,Resets the Opp.zz_email_sent__c Boolean to FALSE
				beforeUpdateSetSourcePerson(tmp, mapOldOpps.get(tmp.Id)); // If the Opp.Owner__c changed, save the old owner value in Opp.Quelle_person__c
				beforeUpdateCheckIfStartOrEndDateChangedAndSetHelperField(tmp, mapOldOpps.get(tmp.Id)); // If opp.projektstart__c OR opp.projektende__c changed, set Opp.zz_helper_start_end_date_changed__c Boolean TRUE

				// Old Methods
				setSearchableTextFields(tmp); // Writes specific fields in text fields so they are searchable
				beforeInsertUpdateChangeWahrscheinlichkeit(tmp, mapOldOpps.get(tmp.Id)); // If Opp.Stage__c changed, change Opp.wahrscheinlichkeit__c
				// EXECUTE AFTER beforeInsertUpdateChangeWahrscheinlichkeit
				beforeInsertUpdateCalcHonorarumsatzGewichtet(tmp, mapOldOpps.get(tmp.Id)); // If Changed Values, recalculate Opp.honorarumsatz_gewichtet_currency__c
			}
		}

		private void beforeInsertUpdateSetVertragsfuehrendeAgenturFromOwner(Opportunity__c opp)
		{
			if(opp.Vertragsfuehrende_agentur__c == null && opp.zz_OwnerAgenturId__c != null)
			{
				opp.Vertragsfuehrende_agentur__c = opp.zz_OwnerAgenturId__c;
			}
		}

		private void beforeUpdateCheckIfStartOrEndDateChangedAndSetHelperField(Opportunity__c opp, Opportunity__c oldOpp)
		{
			if(opp.projektstart__c != oldOpp.projektstart__c || opp.projektende__c != oldOpp.projektende__c)
				opp.zz_helper_start_end_date_changed__c = true;
		}

		private void beforeInsertUpdateSetMandantFromOwner(Opportunity__c opp)
		{
			if(opp.zz_OwnerAgenturId__c != opp.Mandant__c && opp.zz_OwnerAgenturId__c != null)
			{
				opp.Mandant__c = opp.zz_OwnerAgenturId__c;
			}
		}

		private void beforeUpdateSavePriorStageValue(Opportunity__c opp, Opportunity__c oldOpp)
		{
			if(opp.Stage__c != oldOpp.Stage__c)
			{
				opp.zz_last_stage__c = oldOpp.Stage__c;
			}
		}

		private void beforeInsertUpdateSetStageDate(Opportunity__c opp, Opportunity__c oldOpp)
		{
			if(oldOpp != null)
			{
				if(opp.Stage__c != oldOpp.Stage__c)
				{
					opp.zz_stage_date__c = Date.today();
				}
			}
			else
			{
				opp.zz_stage_date__c = Date.today();
			}
		}

		private void beforeUpdateResetEmailSentBool(Opportunity__c opp, Opportunity__c oldOpp)
		{
			if(opp.Stage__c != oldOpp.Stage__c && opp.zz_email_sent__c)
			{
				opp.zz_email_sent__c = false;
			}
		}

		private void beforeUpdateSetSourcePerson(Opportunity__c opp, Opportunity__c oldOpp)
		{
			if(opp.Owner__c != oldOpp.Owner__c)
			{
				opp.Quelle_person__c = oldOpp.Owner__c;
			}
		}

		private void beforeInsertUpdateChangeStatusAndSalesLevel(Opportunity__c opp, Opportunity__c oldOpp)
		{
			if(opp.Id == NULL || opp.stage__c != oldOpp.stage__c)
			{
				if(opp.stage__c == 'Lead' || opp.stage__c == 'Anfrage_allgemein' || opp.stage__c == 'Proposal_allgemein' || opp.stage__c == 'Teilnahme_in_Pruefung' || opp.stage__c == 'Teilnahmeantrag_erstellen' )
				{
					opp.status__c = 'open';
					opp.saleslevel__c = 'Low';
				}
				else if(opp.stage__c == 'Anfrage_konkret' || opp.stage__c == 'Termin_Chemistry_Meeting' || opp.stage__c == 'Konzeptpraesentation' || opp.stage__c == 'Pitchteilnahme' || opp.stage__c == 'Praesentation_2_Runde' || opp.stage__c == 'Wettbewerbsteilnahme' || opp.stage__c == 'Wettbewerbsteilnahme_2_Runde')
				{
					opp.status__c = 'open';
					opp.saleslevel__c = 'Mid';
				}
				else if(opp.stage__c == 'Proposal_konkret' || opp.stage__c == 'Proposal_nach_Pitch' || opp.stage__c == 'Pitch_gewonnen' || opp.stage__c == 'Verhandlung')
				{
					opp.status__c = 'open';
					opp.saleslevel__c = 'High';
				}
				else if(opp.stage__c == 'Gewinn')
				{
					opp.status__c = 'closed';
					opp.saleslevel__c = 'Won';
					opp.schlusstermin__c = Date.today();
				}
				else if(opp.stage__c == 'Verlust' || opp.stage__c == 'keine_Teilnahme')
				{
					opp.status__c = 'closed';
					opp.saleslevel__c = 'Declined';
					opp.schlusstermin__c = Date.today();
				}
			}
		}

		private void beforeInsertUpdateChangeWahrscheinlichkeit(Opportunity__c opp, Opportunity__c oldOpp)
		{
			if(opp.Id == NULL || opp.stage__c != oldOpp.stage__c)
			{
				if(opp.stage__c == 'Lead' || opp.stage__c == 'Verlust' || opp.stage__c == 'keine_Teilnahme')
				{
					opp.wahrscheinlichkeit__c = 0.0;
				}
				else if(opp.stage__c == 'Anfrage_allgemein')
				{
					opp.wahrscheinlichkeit__c = 5;
				}
				else if(opp.stage__c == 'Proposal_allgemein')
				{
					opp.wahrscheinlichkeit__c = 10;
				}
				else if(opp.stage__c == 'Anfrage_konkret' || opp.stage__c == 'Wettbewerbsteilnahme')
				{
					opp.wahrscheinlichkeit__c = 15;
				}
				else if(opp.stage__c == 'Termin_Chemistry_Meeting' || opp.stage__c == 'Pitchteilnahme')
				{
					opp.wahrscheinlichkeit__c = 20;
				}
				else if(opp.stage__c == 'Konzeptpraesentation')
				{
					opp.wahrscheinlichkeit__c = 25;
				}
				else if(opp.stage__c == 'Proposal_konkret' || opp.stage__c == 'Proposal_nach_Pitch')
				{
					opp.wahrscheinlichkeit__c = 60;
				}
				else if(opp.stage__c == 'Pitch_gewonnen')
				{
					opp.wahrscheinlichkeit__c = 80;
				}
				else if(opp.stage__c == 'Verhandlung' && opp.RecordTypeId == oppRecordTypeId_Pitch)
				{
					opp.wahrscheinlichkeit__c = 90;
				}
				else if(opp.stage__c == 'Gewinn')
				{
					opp.wahrscheinlichkeit__c = 100;
				}
			}
		}

		private void beforeInsertUpdateCalcHonorarumsatzGewichtet(Opportunity__c opp, Opportunity__c oldOpp)
		{
			if(opp.Id == NULL || (opp.honorarumsatz__c != oldOpp.honorarumsatz__c || opp.wahrscheinlichkeit__c != oldOpp.wahrscheinlichkeit__c))
			{
				if(opp.honorarumsatz__c != NULL && opp.wahrscheinlichkeit__c != NULL)
				{
					opp.honorarumsatz_gewichtet_currency__c = ((opp.honorarumsatz__c/100) * opp.wahrscheinlichkeit__c);
				}
			}
		}

		private void setSearchableTextFields(Opportunity__c opp)
		{
			if(opp.zz_searchOwnerHelper__c != NULL)
				opp.zz_searchOwnerName__c = opp.zz_searchOwnerHelper__c.left(255);
			if(opp.Leadteam_neu__c != NULL)
				opp.zz_searchLeadteam__c = opp.Leadteam_neu__c.left(255);
			if(opp.leistungssegmente__c != NULL)
				opp.zz_searchLeistungssegmente__c = opp.leistungssegmente__c.left(255);
		}

		// Controller Method for After Insert Opp Logic
		public void afterInsertController(List<Opportunity__c> listOpps)
		{
			for(Opportunity__c tmp : listOpps)
			{
				afterInsertUpdateSetKundennummerFromVertragsfuehrendeAgentur(tmp, null); // if null get and set Opp.Kundennummer__c
			}

			if(mapOppsForGettingCustomerNumber.size() > 0)
				getAndSetKundennummer(mapOppsForGettingCustomerNumber); // If necessary get Kundennummern, set on opp and update opps
		}
		
		// Controller Method for After Update Opp Logic
		public void afterUpdateController(List<Opportunity__c> listOpps, Map<Id, Opportunity__c> mapOldOpps)
		{
			for(Opportunity__c tmp : listOpps)
			{
				afterInsertUpdateSetKundennummerFromVertragsfuehrendeAgentur(tmp, mapOldOpps.get(tmp.Id));  // If Opp.Vertragsfuehrende_agentur__c changed, get and set Opp.Kundennummer__c
			}

			if(mapOppsForGettingCustomerNumber.size() > 0)
				getAndSetKundennummer(mapOppsForGettingCustomerNumber); // If necessary get Kundennummern, set on opp and update opps
		}

		private void afterInsertUpdateSetKundennummerFromVertragsfuehrendeAgentur(Opportunity__c opp, Opportunity__c oldOpp)
		{
			if(oldOpp != null)
			{
				if(opp.Vertragsfuehrende_agentur__c != oldOpp.Vertragsfuehrende_agentur__c)
				{
					mapOppsForGettingCustomerNumber.put(opp.Id, opp);
				}
			}
			else
			{
				if(opp.Kundennummer__c == null)
				{
					mapOppsForGettingCustomerNumber.put(opp.Id, opp);
				}
			}
		}

		private void getAndSetKundennummer(Map<ID, Opportunity__c> mapOpps)
		{
			Map<String, Vertragsbeziehung__c> mapAccAgenturIdANDvertragsbeziehung = new Map<String, Vertragsbeziehung__c>();
			Map<ID, Opportunity__c> mapAccIDandOpp = new Map<ID, Opportunity__c>();
			for(Opportunity__c tmp : mapOpps.values())
			{
				if(tmp.Vertragsfuehrende_agentur__c != null)
				{
					mapAccAgenturIdANDvertragsbeziehung.put(tmp.Vertragsfuehrende_agentur__c, null);
					mapAccIDandOpp.put(tmp.Account__c, tmp);
				}
			}
			mapAccAgenturIdANDvertragsbeziehung = getVertragsbeziehungen(mapAccAgenturIdANDvertragsbeziehung.KeySet(), mapAccIDandOpp.KeySet());
			List<Opportunity__c> listOppsForUpdate = new List<Opportunity__c>();
			for(Opportunity__c tmp : mapOpps.values())
			{
				Opportunity__c tmpOpp = new Opportunity__c();
				tmpOpp.Id = tmp.Id;

				if(mapAccAgenturIdANDvertragsbeziehung.containsKey(tmp.Account__c+''+tmp.Vertragsfuehrende_agentur__c))
				{
					tmpOpp.Kundennummer__c = mapAccAgenturIdANDvertragsbeziehung.get(tmp.Account__c+''+tmp.Vertragsfuehrende_agentur__c).account_nummer__c;
				}
				else
				{
					tmpOpp.Kundennummer__c = tmp.zz_VertragsAgenturKuerzel__c;
				}
				listOppsForUpdate.add(tmpOpp);
			}
			updateOpps(listOppsForUpdate);
		}

		private void updateOpps(List<Opportunity__c> listOpps)
		{
			try{
				update listOpps;
			System.debug('');}catch(Exception e){
				System.debug('-Opportunity_Trigger-getAndSetKundennummer-updateOpps: '+e);
			}
		}

		private Map<String, Vertragsbeziehung__c> getVertragsbeziehungen(Set<String> setAgenturIds, Set<ID> setAccIds)
		{
			Map<String, Vertragsbeziehung__c> mapVertragsbeziehungen = new Map<String, Vertragsbeziehung__c>();
			List<Vertragsbeziehung__c> listVertragsbeziehungen = new List<Vertragsbeziehung__c>();
			try{
				listVertragsbeziehungen = [SELECT Id, Account__c, mandant__c, account_nummer__c FROM Vertragsbeziehung__c WHERE mandant__c IN:setAgenturIds AND account__c IN:setAccIds AND vertragsbeziehung__c = 'Aktiv'];
			System.debug('');}catch(Exception e){
				System.debug('###-Opportunity_Trigger-getAndSetKundennummer-getVertragsbeziehungen: '+e);
			}
			for(Vertragsbeziehung__c tmp : listVertragsbeziehungen)
			{
				mapVertragsbeziehungen.put(tmp.Account__c+''+tmp.Mandant__c, tmp);
			}
			return mapVertragsbeziehungen;
		}
}
