@isTest
private class ReminderSchedulable_Test {

	@isTest static void test_Scheduler() {
		Test.startTest();
			ReminderSchedulable con = new ReminderSchedulable();
		 	// Schedule the test job
		 	String CRON_EXP = '0 0 0 15 3 ? 2022';
		 	String jobId = System.schedule('TESTING 555', CRON_EXP, con);
		 	// Get the information from the CronTrigger API object
		 	CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
		 	// Verify the expressions are the same
		 	System.assertEquals(CRON_EXP, ct.CronExpression);
		 	// Verify the job has not run
		 	System.assertEquals(0, ct.TimesTriggered);
		 	// Verify the next time the job will run
		 	System.assertEquals('2022-03-15 00:00:00', String.valueOf(ct.NextFireTime));
		Test.stopTest();
	}
}