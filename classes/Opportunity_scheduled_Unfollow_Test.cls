@isTest
private class Opportunity_scheduled_Unfollow_Test {

	@isTest static void test_method_one() {
		Account tmp_Acc = new Account();
		tmp_Acc.Name = 'Test AQ';
		tmp_Acc.BillingStreet = 'Högerdamm 41';
		tmp_Acc.BillingCity = 'Hamburg';
		tmp_Acc.BillingPostalCode = '20097';
		tmp_Acc.BillingCountry = 'Germany';

		insert tmp_Acc;


		opportunity__c tmp_opp = new opportunity__c();
		tmp_opp.Name = 'TestOpp';
		tmp_opp.account__c = tmp_Acc.Id;
		tmp_opp.honorarumsatz__c = 1000.00;
		tmp_opp.projektstart__c = Date.Today();
		tmp_opp.projektende__c = Date.Today()+95;
		tmp_opp.stage__c = 'Verlust';
		tmp_opp.status__c = 'closed';
		tmp_opp.nichtgewinngrund__c = 'Aufwand';
		tmp_opp.erlaeuterung_nichtgewinngrund__c = 'Test';
		tmp_opp.zz_unfollowed__c = false;

		insert tmp_opp;

		Test.startTest();
		 Opportunity_scheduled_Unfollow con = new Opportunity_scheduled_Unfollow();

		 // Schedule the test job
		 String CRON_EXP = '0 0 0 15 3 ? 2022';
		 String jobId = System.schedule('TESTING 444xxx', CRON_EXP, con);

		 // Get the information from the CronTrigger API object
		 CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];

		 // Verify the expressions are the same
		 System.assertEquals(CRON_EXP, ct.CronExpression);

		 // Verify the job has not run
		 System.assertEquals(0, ct.TimesTriggered);

		 // Verify the next time the job will run
		 System.assertEquals('2022-03-15 00:00:00', String.valueOf(ct.NextFireTime));

		Test.stopTest();
	}
}