/***************************************************************************
Company: aquilliance GmbH
Author: Mattis Weiler
CreatedDate: 2017-01
***************************************************************************/

global class Opportunity_Reminder_Scheduled implements Schedulable {
	/***********************************************
	to Start open execute anonymous

	Opportunity_Reminder_Scheduled ors = new Opportunity_Reminder_Scheduled();
	system.schedule('Opportunity Reminder', ors.CRON_EXP_everyday_6am, ors);
	***********************************************/
	public String CRON_EXP_everyday_6am =  '0 0 6 * * ?';

	global void execute(SchedulableContext sc) {
		run();
	}

	public Opportunity_Reminder_Scheduled(){}

	public void run(){
		List<Opportunity__c> list_opp = new List<Opportunity__c>();
		try{list_opp = [SELECT id, zz_reminder_sent__c, zz_email_sent__c, zz_send_email__c, zz_send_reminder__c FROM Opportunity__c WHERE zz_send_email__c = true OR zz_send_reminder__c = true];
		System.debug('');}catch(Exception e){System.debug('### -Opportunity_Reminder_Scheduled- getOpps: '+e);}
		for(Opportunity__c tmp: list_opp)
		{
			if(tmp.zz_send_email__c)
				tmp.zz_email_sent__c = true;
			if(tmp.zz_send_reminder__c)
				tmp.zz_reminder_sent__c = true;
		}

		// DML statement
		Database.SaveResult[] srList = Database.update(list_opp, false);

		// Iterate through each returned result
		for (Database.SaveResult sr : srList) {
		    if (sr.isSuccess()) {
		        // Operation was successful, so get the ID of the record that was processed
		        System.debug('### Successfully updated opportunity. Opportunity ID: ' + sr.getId());
		    }
		    else {
		        // Operation failed, so get all errors
		        for(Database.Error err : sr.getErrors()) {
		            System.debug('### The following error has occurred.');
		            System.debug(err.getStatusCode() + ': ' + err.getMessage());
		            System.debug('### Opportunity fields that affected this error: ' + err.getFields());
		        }
		    }
		}
	}
}
