/**************************************************************************
Company: aquilliance GmbH
Author: Andreas Wolf
CreatedDate: 2017-4
**************************************************************************/
global class EmailSendingScheduler implements Schedulable {
	/***********************************************
		to Start open execute anonymous

		EmailSendingScheduler ess = new EmailSendingScheduler();
		system.schedule('Send Opportunity Report Data', ess.CRON_EXP_every_friday_16pm, ess);
	***********************************************/
	public String CRON_EXP_every_friday_16pm =  '0 0 16 ? * FRI';

	public EmailSendingScheduler() {}

	global void execute(SchedulableContext sc) {
		run();
	}

	public void run(){
		EmailSendingLogic logic = new EmailSendingLogic();
		logic.sendOppReportsToGroup();
	}
}