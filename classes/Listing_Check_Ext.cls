/**************************************************************************
Company: aquilliance GmbH
Author: Andreas Wolf
CreatedDate: 2017-1

**************************************************************************/
public with sharing class Listing_Check_Ext {

	private final sObject mysObject;
	public String object_type {get;set;}

	public List<Listings__c> list_listings {get;set;}
	public Integer list_listings_size {get;set;}

	public String theDomain {get;set;}

    public Listing_Check_Ext(ApexPages.StandardController stdController) {
        this.mysObject = (sObject)stdController.getRecord();
				init();
				getRelevantListings();
    }

		public void init(){
			list_listings = new List<Listings__c>();
			list_listings_size = 0;

			// get Domain for Record Links
			String urlInstance = String.valueof(System.URL.getSalesforceBaseURL()).replace('Url:[delegate=','').replace(']','');
			String[] domain = urlInstance.split('\\.');
			String puffer = domain[0];
			theDomain = puffer.substring(0, puffer.length()-3);
			System.debug('### -Listing_Check_Ext- theDomain: '+theDomain);

			object_type = checkPrefixForObjectName(mysObject.Id);
			System.debug('### -Listing_Check_Ext- object_type: '+object_type);
		}

		public static String checkPrefixForObjectName(String recordId)
		{
			String objectName;
			String myIdPrefix = String.valueOf(recordId).substring(0,3);

			Map<String, Schema.SObjectType> gd =  Schema.getGlobalDescribe();
			//Loop through all the sObject types returned by Schema
			for(Schema.SObjectType stype : gd.values())
			{
					Schema.DescribeSObjectResult r = stype.getDescribe();
					String prefix = r.getKeyPrefix();
					//System.debug('Prefix is ' + prefix);
					//Check if the prefix matches with requested prefix
					if(prefix!=null && prefix.equals(myIdPrefix)){
							objectName = r.getName();
							//System.debug('Object Name! ' + objectName);
							break;
					}
			}
			return objectName;
		}

		public void getRelevantListings()
		{
			String relevantAccountId = '';
			String relevantAgenturId = '';
			String relevantAgenturId2 = '';
			if(object_type == 'Account')
			{
				relevantAccountId = mysObject.Id;
				// Get User-Mandant Relation
				User_mandant__c tmp_um = new User_mandant__c();
				try{tmp_um = [SELECT id, mandant_agentur__c FROM user_mandant__c WHERE User__c =: UserInfo.getUserId() LIMIT 1];
				System.debug('');}catch(Exception e){System.debug('### -Listing_Check_Ext- getUserMandantInfo: '+e);}
				if(tmp_um.mandant_agentur__c != null){
					relevantAgenturId = tmp_um.mandant_agentur__c;
				}
			}
			if(object_type == 'Opportunity__c')
			{
				// Get Account from Opp
				Opportunity__c tmp_opp = new Opportunity__c();
				try{tmp_opp = [SELECT id, Account__c, mandant__c, vertragsfuehrende_agentur__c FROM Opportunity__c WHERE id =: mysObject.Id LIMIT 1];
				System.debug('');}catch(Exception e){System.debug('### -Listing_Check_Ext- getOppInfo: '+e);}
				relevantAccountId = tmp_opp.Account__c;

				if(tmp_opp.mandant__c != null)
				{
					relevantAgenturId = tmp_opp.mandant__c;
				}
				if(tmp_opp.vertragsfuehrende_agentur__c != null)
				{
					relevantAgenturId2 = tmp_opp.vertragsfuehrende_agentur__c;
				}
			}

			// Get active listings
			try{list_listings = [SELECT id, Name, mandant__c, mandant__r.Name FROM Listings__c
				WHERE (mandant__c =: relevantAgenturId OR mandant__c =: relevantAgenturId2) AND account__c =: relevantAccountId AND status__c = 'Aktiv'];
			System.debug('');}catch(Exception e){System.debug('### -Listing_Check_Ext- getListings: '+e);}
			list_listings_size = list_listings.size();
		}

}