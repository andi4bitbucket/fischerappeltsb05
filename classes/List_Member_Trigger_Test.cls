/**************************************************************************
Company: aquilliance GmbH
Author: Andreas Wolf
CreatedDate: 2017-1

**************************************************************************/
@isTest
private class List_Member_Trigger_Test {

	@isTest static void test_method_one() {
		Account tmp_acc = new Account();
    tmp_acc.Name = 'Test';

    insert tmp_acc;

    Contact tmp_con = new Contact();
    tmp_con.AccountId = tmp_acc.Id;
    tmp_con.LastName = 'Mustermann';
		tmp_con.Salutation = 'Mr.';

    insert tmp_con;

		String standardRT = [SELECT id FROM RecordType WHERE sObjectType = 'List__c' AND DeveloperName = 'Standard' LIMIT 1].Id;

		List__c tmp_list = new List__c();
		tmp_list.Name = 'Something';
		tmp_list.RecordTypeId = standardRT;

		insert tmp_list;

		List_Member__c tmp_lm = new List_Member__c();
		tmp_lm.Contact__c = tmp_con.Id;
		tmp_lm.List__c = tmp_list.Id;

		insert tmp_lm;
		try{update tmp_lm;}catch(Exception e){System.debug('### -test-: '+e);}
		delete tmp_lm;
		try{undelete tmp_lm;}catch(Exception e){System.debug('### -test-: '+e);}

	}
}