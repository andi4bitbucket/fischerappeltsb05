/**************************************************************************
Company: aquilliance GmbH
Author: Andreas Wolf
CreatedDate: 2017-4
**************************************************************************/
public with sharing class CO_CaseTriggerLogic {

	private User u;

	public CO_CaseTriggerLogic() {}

	public void setSearchableTextFields(List<Case__c> listCases)
	{
		for(Case__c tmp:listCases)
		{
			if(tmp.zz_searchOwnerHelper__c != NULL)
				tmp.zz_searchOwnerName__c = tmp.zz_searchOwnerHelper__c.left(255);
			if(tmp.branche__c != NULL)
				tmp.zz_searchBranch__c = tmp.branche__c.left(255);
			if(tmp.leistungssegmente__c != NULL)
				tmp.zz_searchLeistungssegmente__c = tmp.leistungssegmente__c.left(255);
		}
	}

	/**************************************************************************
		Prüft, ob bei insert/update einer Opp irgendwelche Chatter Posts erstellt werden müssen
	**************************************************************************/
	public void afterInsertCheckIFcreateChatterPosts(Map<Id, Case__c> mapCases)
	{
		System.debug('###-CO_CaseTriggerLogic-afterInsertCheckIFcreateChatterPosts EXECUTED!');
		u = [SELECT Id, FirstName, LastName, Email FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
		Map<Id, Case__c> mapRelevantCases = new Map<Id, Case__c>();
		for(Case__c tmp : mapCases.values())
		{
			if(tmp.Accountname__c != null)
			{
				mapRelevantCases.put(tmp.Id, tmp);
			}
		}
		List<ConnectApi.BatchInput> allChatterPosts = new List<ConnectApi.BatchInput>();
		allChatterPosts.addAll(prepareChatterMessagesForRelevantCases(mapRelevantCases));

		postChatterMessages(allChatterPosts, 'afterInsertCheckIFcreateChatterPosts');
	}

	private List<ConnectApi.BatchInput> prepareChatterMessagesForRelevantCases(Map<Id, Case__c> mapCases)
	{
		List<ConnectApi.BatchInput> batchInputs = new List<ConnectApi.BatchInput>();
		ConnectApi.BatchInput tmp_batchInput;
		for(Case__c tmp : mapCases.values())
		{
			tmp_batchInput = new ConnectApi.BatchInput(new ConnectApi.FeedItemInput());
			tmp_batchInput = generateChatterMessage('', tmp.Accountname__c, u.FirstName+' '+u.LastName+' hat bei '+tmp.zz_AccountName__c+' einen neuen Case erstellt: '+tmp.Name);
			batchInputs.add(tmp_batchInput);
		}
		return batchInputs;
	}

	private ConnectApi.BatchInput generateChatterMessage(String mention_user_id, String record_Id, String short_text)
	{
		ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
		ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
		ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
		ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();

		messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();

		// if(mention_user_id != '')
		// {
		// 	//Mention user here
		// 	mentionSegmentInput.id = mention_user_id;
		// 	messageBodyInput.messageSegments.add(mentionSegmentInput);
		// }

		textSegmentInput.text = '\n'+short_text;
		messageBodyInput.messageSegments.add(textSegmentInput);

		feedItemInput.body = messageBodyInput;
		feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
		// datensatz an dem der chatter post hängt
		feedItemInput.subjectId = record_Id;

		ConnectApi.BatchInput batchInput = new ConnectApi.BatchInput(feedItemInput);

		return batchInput;
	}

	private void postChatterMessages(List<ConnectApi.BatchInput> batchinputs, String startingMethod)
	{
		try{
			ConnectApi.ChatterFeeds.postFeedElementBatch(Network.getNetworkId(), batchinputs);
		System.debug('');}catch(Exception e){
			System.debug('###-CO_CaseTriggerLogic-'+startingMethod+'-postChatterMessages: '+e);
		}
	}
}