/**************************************************************************
Company: aquilliance GmbH
Author: Andreas Wolf
CreatedDate: 2017-1

**************************************************************************/
public with sharing class List_Member_Trigger {
	public List_Member_Trigger() {

	}

	public void doAfterInsert(List<List_Member__c> list_lm)
	{
		List<Contact> list_con = new List<Contact>();
		Map<String, String> map_cons = new Map<String, String>();
		for(List_Member__c tmp : list_lm)
		{
			map_cons.put(tmp.Contact__c, tmp.List__c);
		}
		try{list_con = [SELECT id, Member_of_Lists__c FROM Contact WHERE Id IN: map_cons.keyset()];
		System.debug('');}catch(Exception e){System.debug('### -List_Member_Trigger- ins_getCons: '+e);}
		for(Contact tmp : list_con)
		{
			tmp.Member_of_Lists__c += ' '+map_cons.get(tmp.Id);
			tmp.Member_of_Lists__c = tmp.Member_of_Lists__c.replace('null', '');
		}
		try{update list_con;
		System.debug('');}catch(Exception e){System.debug('### -List_Member_Trigger- ins_updateCons: '+e);}
	}

	public void doAfterDelete(List<List_Member__c> list_lm)
	{
		List<Contact> list_con = new List<Contact>();
		Map<String, String> map_cons = new Map<String, String>();
		for(List_Member__c tmp : list_lm)
		{
			map_cons.put(tmp.Contact__c, tmp.List__c);
		}
		try{list_con = [SELECT id, Member_of_Lists__c FROM Contact WHERE Id IN: map_cons.keyset()];
		System.debug('');}catch(Exception e){System.debug('### -List_Member_Trigger- del_getCons: '+e);}
		for(Contact tmp : list_con)
		{
			if(tmp.Member_of_Lists__c != null)
				tmp.Member_of_Lists__c = tmp.Member_of_Lists__c.replace(map_cons.get(tmp.Id), '');
		}
		try{update list_con;
		System.debug('');}catch(Exception e){System.debug('### -List_Member_Trigger- del_UpdateCons: '+e);}
	}

	public void doAfterUndelete(List<List_Member__c> list_lm)
	{
		for(List_Member__c tmp : list_lm)
		{
			tmp.addError('List-Member records cannot be undeleted');
		}
	}

	public void doAfterUpdate(List<List_Member__c> list_lm)
	{
		for(List_Member__c tmp : list_lm)
		{
			tmp.addError('List-Member records cannot be updated');
		}
	}
}