/**************************************************************************
Company: aquilliance GmbH
Author: Andreas Wolf
CreatedDate: 2017-8
**************************************************************************/
global class ReminderContactBirthdayBatch implements Database.Batchable<sObject> {

	String query;
	Date birthdayReminderDate;

	global ReminderContactBirthdayBatch() {
		birthdayReminderDate = Date.today().addDays(3);

		query = 'SELECT Id, Birthdate, FirstName, LastName FROM Contact WHERE Birthdate =: birthdayReminderDate';
	}

	global Database.QueryLocator start(Database.BatchableContext BC){
		System.debug('###-ReminderContactBirthdayBatch-start EXECUTED!');
		return Database.getQueryLocator(query);
	}

  global void execute(Database.BatchableContext BC, List<sObject> scope){
		System.debug('###-ReminderContactBirthdayBatch-execute EXECUTED!');
		List<ConnectApi.BatchInput> allChatterPosts = new List<ConnectApi.BatchInput>();
		ConnectApi.BatchInput tmp_batchInput;

		for(SObject tmp : scope)
		{
			Contact con = (Contact) tmp;
			tmp_batchInput = new ConnectApi.BatchInput(new ConnectApi.FeedItemInput());
			tmp_batchInput = generateChatterMessage('', con.Id, con.FirstName+' '+con.LastName+' hat in 3 Tagen Geburtstag!');
			allChatterPosts.add(tmp_batchInput);
		}
		postChatterMessages(allChatterPosts);
	}

	global void finish(Database.BatchableContext BC){
		System.debug('###-ReminderContactBirthdayBatch-finish EXECUTED!');
	}

	private ConnectApi.BatchInput generateChatterMessage(String mention_user_id, String record_Id, String short_text)
	{
		ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
		ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
		ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
		ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();

		messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();

		// if(mention_user_id != '')
		// {
		// 	//Mention user here
		// 	mentionSegmentInput.id = mention_user_id;
		// 	messageBodyInput.messageSegments.add(mentionSegmentInput);
		// }

		textSegmentInput.text = '\n'+short_text;
		messageBodyInput.messageSegments.add(textSegmentInput);

		feedItemInput.body = messageBodyInput;
		feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
		// datensatz an dem der chatter post hängt
		feedItemInput.subjectId = record_Id;

		ConnectApi.BatchInput batchInput = new ConnectApi.BatchInput(feedItemInput);

		return batchInput;
	}

	private void postChatterMessages(List<ConnectApi.BatchInput> batchinputs)
	{
		try{
			ConnectApi.ChatterFeeds.postFeedElementBatch(Network.getNetworkId(), batchinputs);
		System.debug('');}catch(Exception e){
			System.debug('###-ReminderContactBirthdayBatch-postChatterMessages: '+e);
		}
	}

}