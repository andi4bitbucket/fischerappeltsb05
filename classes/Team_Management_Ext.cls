/**************************************************************************
Company: aquilliance GmbH
Author: Andreas Wolf
CreatedDate: 2017-1

**************************************************************************/
public with sharing class Team_Management_Ext {

	private final sObject mysObject;
	public User u {get;set;}
	public String object_type {get;set;}

	// RecordTypes
	public String accountRT {get;set;} // Customer Team
	public String opportunityRT {get;set;} // Project Team

	// Member
	public List<Team_Member__c> list_tm {get;set;}
	public Map<String, Team_Member__c> map_member {get;set;}
	public Boolean hasVerantwortlichen {get;set;}

	// new Member
	public Boolean createMemberModus {get;set;}

	// delete Member
	public String selected_member {get;set;}
	public Boolean tryDeletion {get;set;}

	// Errors
	public String err_user {get;set;}

		public Team_Management_Ext(ApexPages.StandardController stdController) {
				this.mysObject = (sObject)stdController.getRecord();
				init();
				try{u = [SELECT id, FirstName, LastName, Email, Phone, Profile.Name FROM User WHERE id =: UserInfo.getUserId() LIMIT 1];
				System.debug('');}catch(Exception e){System.debug('### -Team_Management_Ext- getUserInfo: '+e);}

				object_type = checkPrefixForObjectName(mysObject.Id);
				System.debug('### -Team_Management_Ext- object_type: '+object_type);

				// Get Member
				list_tm = getExistingMember();

				//if(object_type == 'Account')
					//run();
				//if(object_type == 'Opportunity__c')
					//run_Mandant();
		}

		public void init()
		{
			u = new User();
			list_tm = new List<Team_Member__c>();
			map_member = new Map<String, Team_Member__c>();
			getRecordTypeIds();
			err_user = '';
			createMemberModus = false;
			hasVerantwortlichen = false;
			selected_member = '';
			tryDeletion = false;
		}

		public void addNewMember(){
			//if(object_type == 'Account')
				list_tm.add(new Team_Member__c(Account__c = mysObject.Id, RecordTypeId = accountRT));
			//if(object_type == 'Opportunity__c')
				//run_Mandant();
			createMemberModus = true;
			err_user = '';
		}

		public void deleteMember_toggle(){
			if(!tryDeletion)
			{
				if(!Test.isRunningTest())
					selected_member = Apexpages.currentPage().getParameters().get('member_number');

				if(list_tm[Integer.valueOf(selected_member)].Id == null)
				{
					list_tm.remove(Integer.valueOf(selected_member));
					createMemberModus = false;
				}
				else
				{
					tryDeletion = true;
				}
			}else{
				tryDeletion = false;
			}
		}

		public void deleteMember(){
			Boolean succeeded = false;
			Team_Member__c tmp = list_tm[Integer.valueOf(selected_member)];

			// Prepare Chatter Post
			String recordId;
			//if(object_type == 'Account')
				recordId = tmp.Account__c;
			//if(object_type == 'Opportunity__c')
				//run_Mandant();

			List<ConnectApi.BatchInput> batchInputs = new List<ConnectApi.BatchInput>();
			ConnectApi.BatchInput tmp_batchInput = new ConnectApi.BatchInput(new ConnectApi.FeedItemInput());
			tmp_batchInput = chatter_info_prepare_chatter_message(tmp.User__c, recordId, 'Sie wurden von '+u.FirstName+' '+u.LastName+' aus dem Team entfernt!');
			batchInputs.add(tmp_batchInput);

			try{
				list_tm.remove(Integer.valueOf(selected_member));
				map_member.remove(tmp.User__c);
				if(tmp.verantwortlich__c)
				{
					hasVerantwortlichen = false;
				}
				delete tmp;
				tryDeletion = false;
				succeeded = true;
			System.debug('');}catch(Exception e){System.debug('### -Team_Management_Ext- deleteMember: '+e);}

			if(succeeded)
			{
				Account tmp_acc = new Account();
				try{
					tmp_acc = [SELECT Id, zz_team_user_ids__c FROM Account WHERE Id =: tmp.Account__c];
				System.debug('');}catch(Exception e){System.debug('### -Team_Management_Ext- getAccInfo: '+e);}

				if(tmp_acc.zz_team_user_ids__c != null)
				{
					tmp_acc.zz_team_user_ids__c = tmp_acc.zz_team_user_ids__c.replace(tmp.User__c, '');
				}

				try{
					update tmp_acc;
				System.debug('');}catch(Exception e){System.debug('### -Team_Management_Ext- updateAccount: '+e);}

				//postet die chatter meldung
				ConnectApi.ChatterFeeds.postFeedElementBatch(Network.getNetworkId(), batchinputs);
			}
		}

		public void back(){
			list_tm.remove(list_tm.size()-1);
			createMemberModus = false;
		}

		public void save(){
			Boolean succeeded = false;
			Team_Member__c tmp = list_tm[list_tm.size()-1];
			if(map_member.containsKey(tmp.User__c))
			{
				err_user = 'Der gewählte User ist bereits im Team.';
			}
			else if(tmp.User__c == null)
			{
				err_user = 'Bitte wählen Sie erst einen User aus.';
			}else{
				err_user = '';
			}

			if(err_user == '')
			{
				try{
					insert tmp;
					list_tm.clear();
					list_tm = getExistingMember();
					createMemberModus = false;
					succeeded = true;
				System.debug('');}catch(Exception e){System.debug('### -Team_Management_Ext- insertNewMember: '+e);}
			}

			// Prepare Chatter Post and update user id to account helper field
			if(succeeded)
			{
				Account tmp_acc = new Account();
				try{
					tmp_acc = [SELECT Id, zz_team_user_ids__c FROM Account WHERE Id =: tmp.Account__c];
				System.debug('');}catch(Exception e){System.debug('### -Team_Management_Ext- getAccInfo: '+e);}

				if(tmp_acc.zz_team_user_ids__c != null)
				{
					if(!tmp_acc.zz_team_user_ids__c.contains(tmp.User__c)){
						tmp_acc.zz_team_user_ids__c += ' '+tmp.User__c;
					}
				}else{
					tmp_acc.zz_team_user_ids__c = tmp.User__c;
				}

				try{
					update tmp_acc;
				System.debug('');}catch(Exception e){System.debug('### -Team_Management_Ext- updateAccount: '+e);}

				// Chatter Post
				String recordId;
				//if(object_type == 'Account')
					recordId = tmp.Account__c;
				//if(object_type == 'Opportunity__c')
					//run_Mandant();
				List<ConnectApi.BatchInput> batchInputs = new List<ConnectApi.BatchInput>();
				ConnectApi.BatchInput tmp_batchInput = new ConnectApi.BatchInput(new ConnectApi.FeedItemInput());
				tmp_batchInput = chatter_info_prepare_chatter_message(tmp.User__c, recordId, 'Sie wurden von '+u.FirstName+' '+u.LastName+' zum Team hinzugefügt!');
				batchInputs.add(tmp_batchInput);
				//postet die chatter meldung
				ConnectApi.ChatterFeeds.postFeedElementBatch(Network.getNetworkId(), batchinputs);
			}
		}

		public List<Team_Member__c> getExistingMember(){
			List<Team_Member__c> tmp_list = new List<Team_Member__c>();
			if(object_type == 'Account')
			{
				try{tmp_list = [SELECT id, User__c, User__r.FirstName, User__r.LastName, verantwortlich__c, Account__c, Account__r.zz_team_user_ids__c, Opportunity__c FROM Team_Member__c WHERE Account__c =: mysObject.Id];
				System.debug('');}catch(Exception e){System.debug('### -Team_Management_Ext- getAccTeamMember: '+e);}
			}
			for(Team_Member__c tmp:tmp_list)
			{
				map_member.put(tmp.User__c, tmp);
				if(tmp.verantwortlich__c)
				{
					hasVerantwortlichen = true;
				}
			}
			//System.debug('### map_member: '+map_member);
			//System.debug('### hasVerantwortlichen: '+hasVerantwortlichen);

			return tmp_list;
		}

		public static String checkPrefixForObjectName(String recordId)
		{
			String objectName;
			String myIdPrefix = String.valueOf(recordId).substring(0,3);

			Map<String, Schema.SObjectType> gd =  Schema.getGlobalDescribe();
			//Loop through all the sObject types returned by Schema
			for(Schema.SObjectType stype : gd.values())
			{
					Schema.DescribeSObjectResult r = stype.getDescribe();
					String prefix = r.getKeyPrefix();
					//System.debug('Prefix is ' + prefix);
					//Check if the prefix matches with requested prefix
					if(prefix!=null && prefix.equals(myIdPrefix)){
							objectName = r.getName();
							//System.debug('Object Name! ' + objectName);
							break;
					}
			}
			return objectName;
		}

		public void getRecordTypeIds(){
			try{accountRT = [SELECT id FROM RecordType WHERE sObjectType = 'Team_Member__c' AND DeveloperName = 'Customer_Team' LIMIT 1].Id;
			System.debug('');}catch(Exception e){System.debug('### -Team_Management_Ext- getCustomerTeamRTid: '+e);}
			try{opportunityRT = [SELECT id FROM RecordType WHERE sObjectType = 'Team_Member__c' AND DeveloperName = 'Project_Team' LIMIT 1].Id;
			System.debug('');}catch(Exception e){System.debug('### -Team_Management_Ext- getProjectTeamRTid: '+e);}
		}

		/***************************************************************************************
				Erweiterung für Chatter Info, erzeugt den Chatter Eintrag
		***************************************************************************************/
		public ConnectApi.BatchInput chatter_info_prepare_chatter_message(String mention_user_id, String record_Id, String short_text)
		{

			ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
			ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
			ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
			ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();

			messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
			//Mention user here
			mentionSegmentInput.id = mention_user_id;
			messageBodyInput.messageSegments.add(mentionSegmentInput);

			textSegmentInput.text = '\n'+short_text;
			messageBodyInput.messageSegments.add(textSegmentInput);

			feedItemInput.body = messageBodyInput;
			feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
			// datensatz an dem der chatter post hängt
			feedItemInput.subjectId = record_Id;

			ConnectApi.BatchInput batchInput = new ConnectApi.BatchInput(feedItemInput);

			return batchInput;
		}
}