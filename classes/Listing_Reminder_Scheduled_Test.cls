/***************************************************************************
Company: aquilliance GmbH
Author: Mattis Weiler
CreatedDate: 2017-01

***************************************************************************/
@isTest
private class Listing_Reminder_Scheduled_Test {

  @isTest static void test_method_one() {
    Account tmp_Acc = new Account();
    tmp_Acc.Name = 'Test AQ';
    tmp_Acc.BillingStreet = 'Högerdamm 41';
    tmp_Acc.BillingCity = 'Hamburg';
    tmp_Acc.BillingPostalCode = '20097';
    tmp_Acc.BillingCountry = 'Germany';

    insert tmp_Acc;


    Listings__c tmp_listing = new Listings__c();
    tmp_listing.Name = 'TestOpp';
    tmp_listing.Account__c = tmp_Acc.Id;
    tmp_listing.startzeitpunkt__c = Date.TODAY()-1000;
    tmp_listing.endzeitpunkt__c = Date.TODAY()+300;
    tmp_listing.status__c = 'Aktiv';


    insert tmp_listing;


    Test.startTest();
    Listing_Reminder_Scheduled con = new Listing_Reminder_Scheduled();

     // Schedule the test job
     String CRON_EXP = '0 0 0 15 3 ? 2022';
     String jobId = System.schedule('TESTING 444', CRON_EXP, con);

     // Get the information from the CronTrigger API object
     CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];

     // Verify the expressions are the same
     System.assertEquals(CRON_EXP, ct.CronExpression);

     // Verify the job has not run
     System.assertEquals(0, ct.TimesTriggered);

     // Verify the next time the job will run
     System.assertEquals('2022-03-15 00:00:00', String.valueOf(ct.NextFireTime));

     Test.stopTest();

  }
}