/**************************************************************************
Company: aquilliance GmbH
Author: Andreas Wolf & Andre Mergen
CreatedDate: 2017-1

**************************************************************************/
@isTest
private class Budget_Trigger_Test {

	@isTest(SeeAllData=true) static void test_method_one() {
		Account tmp_Acc = new Account();
		tmp_Acc.Name = 'Test AQ';
		tmp_Acc.BillingStreet = 'Högerdamm 41';
		tmp_Acc.BillingCity = 'Hamburg';
		tmp_Acc.BillingPostalCode = '20097';
		tmp_Acc.BillingCountry = 'Germany';

		insert tmp_Acc;


		Opportunity__c tmp_opp = new Opportunity__c();
		tmp_opp.Name = 'TestOpp';
		tmp_opp.account__c = tmp_Acc.Id;
		tmp_opp.Stage__C = 'Test';
		tmp_opp.honorarumsatz__c = 1000.00;
		tmp_opp.fremdkosten__c = 100;
		tmp_opp.projektstart__c = Date.Today();
		tmp_opp.projektende__c = Date.Today()+95;
		tmp_opp.zz_stage_date__c = Date.Today()-95;
		tmp_opp.zz_open_activities__c = 0;
		tmp_opp.schlusstermin__c = Date.Today()-300;

		insert tmp_opp;

		Profile p = [SELECT Id FROM Profile WHERE Name='Standard User' OR Name='Standardbenutzer' LIMIT 1];
    User u = new User();
    u.Alias = 'standt';
    u.Email='info@aquilliance.de';
    u.EmailEncodingKey='UTF-8';
    u.LastName='Testing';
    u.LanguageLocaleKey='en_US';
    u.LocaleSidKey='en_US';
    u.ProfileId = p.Id;
    u.TimeZoneSidKey='America/Los_Angeles';
    u.UserName='info@aquilliance.de';

		insert u;

		User u2 = new User();
		u2.Alias = 'standt2';
		u2.Email='info@aquilliance.de2';
		u2.EmailEncodingKey='UTF-8';
		u2.LastName='Testing2';
		u2.LanguageLocaleKey='en_US';
		u2.LocaleSidKey='en_US';
		u2.ProfileId = p.Id;
		u2.TimeZoneSidKey='America/Los_Angeles';
		u2.UserName='info@aquilliance.de2';

		insert u2;

		// Generate Budgets
		String RecordTypeId_primaer_budget = null;
		try{
			RecordTypeId_primaer_budget = [SELECT Id FROM RecordType WHERE DeveloperName = 'primaer_budget' AND SobjectType='budget__c' LIMIT 1].Id;
		System.debug('');}catch(Exception e){System.debug('### error RecordTypeId_primaer_budget:'+e);}
		String RecordTypeId_sekundaer_budget = null;
		try{
			RecordTypeId_sekundaer_budget = [SELECT Id FROM RecordType WHERE DeveloperName = 'sekundaer_budget' AND SobjectType='budget__c' LIMIT 1].Id;
		System.debug('');}catch(Exception e){System.debug('### error RecordTypeId_sekundaer_budget:'+e);}

		// Budget__c tmp_bg = new Budget__c();
		// tmp_bg.RecordTypeId = RecordTypeId_primaer_budget;
		// tmp_bg.opportunity__c = tmp_opp.Id;
		// tmp_bg.account__c = tmp_opp.account__c;
		// tmp_bg.honorarumsatz_absolut__c = tmp_opp.honorarumsatz__c;
		// tmp_bg.benutzer__c = u.id;
		//
		// insert tmp_bg;

		Test.startTest();

			Budget__c tmp_bg2 = new Budget__c();
			tmp_bg2.RecordTypeId = RecordTypeId_sekundaer_budget;
			tmp_bg2.opportunity__c = tmp_opp.Id;
			tmp_bg2.account__c = tmp_opp.account__c;
			tmp_bg2.honorarumsatz_absolut__c = tmp_opp.honorarumsatz__c;
			tmp_bg2.benutzer__c = tmp_opp.OwnerId;

			insert tmp_bg2;

			tmp_bg2.benutzer__c = u2.id;
			tmp_bg2.honorarumsatz_absolut__c = 500;

			update tmp_bg2;

			delete tmp_bg2;

		Test.stopTest();

		try{undelete tmp_bg2;}catch(Exception e){}
	}
}