/**************************************************************************
Company: aquilliance GmbH
Author: Andre Mergen
CreatedDate: 2017-01

SeeAllData ist wegen den Chatter Post´s nötig
**************************************************************************/
@IsTest(SeeAllData=true)
private class Account_Trigger_Test {
	/********************************************************
				create test Records
	*******************************************************
	@testSetup static void TheSetup()
	{
		String acc_RecordTypeId = null;

		try{
			acc_RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'kunde' AND SobjectType='Account' LIMIT 1].Id;
		System.debug('');}catch(Exception e){System.debug('### error acc_RecordTypeId:'+e);}

		Account tmp_Acc = new Account();
		tmp_Acc.Name = 'Test Account';
		tmp_Acc.BillingStreet = 'Högerdamm 41';
		tmp_Acc.BillingCity = 'Hamburg';
		tmp_Acc.BillingPostalCode = '20097';
		tmp_Acc.BillingCountry = 'Germany';
		tmp_Acc.RecordTypeId = acc_RecordTypeId;
		tmp_Acc.account_status__c = 'Gesperrter Kunde';

		insert tmp_Acc;

		Contact tmp_Con = new Contact();
		tmp_Con.AccountId 				= tmp_Acc.Id;
		tmp_con.Salutation 				= 'Herr';
		tmp_con.Firstname 				= 'Chuck';
		tmp_con.Lastname 					= 'Borris';
		tmp_con.MailingStreet 		= 'Högerdamm 41';
		tmp_con.MailingCity 			= 'Hamburg';
		tmp_con.MailingPostalCode	= '20097';
		tmp_con.MailingCountry 		= 'Germany';

		insert tmp_con;
	}
	*/

	@isTest static void test_method_one() {
		String acc_RecordTypeId = null;

		try{
			acc_RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Freelancer' AND SobjectType='Account' LIMIT 1].Id;
		System.debug('');}catch(Exception e){System.debug('### error acc_RecordTypeId:'+e);}


		Account tmp_Acc = new Account();
		tmp_Acc.Firstname = 'Test AQ';
		tmp_Acc.Lastname = 'Test AQ';
		tmp_Acc.BillingStreet = 'Högerdamm 41';
		tmp_Acc.BillingCity = 'Hamburg';
		tmp_Acc.BillingPostalCode = '20097';
		tmp_Acc.BillingCountry = 'Germany';
		tmp_Acc.RecordTypeId = acc_RecordTypeId;
		tmp_Acc.skills__c = 'Skill 1';
		tmp_Acc.branche__c = 'Test1; Test2;';

		insert tmp_Acc;
	}

	/******************************************
		gesperrter kunde
	******************************************/
	@isTest static void test_method_gesperrt() {

		String acc_RecordTypeId = null;

		try{
			acc_RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'kunde' AND SobjectType='Account' LIMIT 1].Id;
		System.debug('');}catch(Exception e){System.debug('### error acc_RecordTypeId:'+e);}


		Account tmp_Acc = new Account();
		tmp_Acc.Name = 'Test Account';
		tmp_Acc.BillingStreet = 'Högerdamm 41';
		tmp_Acc.BillingCity = 'Hamburg';
		tmp_Acc.BillingPostalCode = '20097';
		tmp_Acc.BillingCountry = 'Germany';
		tmp_Acc.RecordTypeId = acc_RecordTypeId;
		tmp_Acc.skills__c = 'Skill 1';
		tmp_Acc.account_status__c = 'Gesperrter Kunde';

		insert tmp_Acc;
	}

	/******************************************
		nicht mehr gesperrter kunde
	******************************************/
	@isTest static void test_method_vorhandenen_Acc_sperren() {
		Test.StartTest();
		String acc_RecordTypeId = null;

		try{
			acc_RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'kunde' AND SobjectType='Account' LIMIT 1].Id;
		System.debug('');}catch(Exception e){System.debug('### error acc_RecordTypeId:'+e);}

		Account tmp_Acc = new Account();
		tmp_Acc.Name = 'Test Account #123456#';
		tmp_Acc.BillingStreet = 'Högerdamm 41';
		tmp_Acc.BillingCity = 'Hamburg';
		tmp_Acc.BillingPostalCode = '20097';
		tmp_Acc.BillingCountry = 'Germany';
		tmp_Acc.RecordTypeId = acc_RecordTypeId;
		//tmp_Acc.account_status__c = 'Gesperrter Kunde';

		insert tmp_Acc;

		Contact tmp_Con = new Contact();
		tmp_Con.AccountId 				= tmp_Acc.Id;
		tmp_con.Salutation 				= 'Herr';
		tmp_con.Firstname 				= 'Chuck';
		tmp_con.Lastname 					= 'Borris';
		tmp_con.MailingStreet 		= 'Högerdamm 41';
		tmp_con.MailingCity 			= 'Hamburg';
		tmp_con.MailingPostalCode	= '20097';
		tmp_con.MailingCountry 		= 'Germany';

		insert tmp_con;

		Test.StopTest();


		Account tmp_Acc2 = new Account();
		tmp_Acc2 = [SELECT Id, Name FROM Account WHERE Name like 'Test Account #123456#%' LIMIT 1];


		tmp_Acc2.account_status__c = 'Gesperrter Kunde';
		update tmp_Acc2;
		//Test.stopTest();
	}

	/******************************************
		nicht mehr gesperrter kunde
	******************************************/
	@isTest static void test_method_vorhandenen_Acc_sperren_mit_opp_etc() {
		Test.StartTest();
		String acc_RecordTypeId = null;

		try{
			acc_RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'kunde' AND SobjectType='Account' LIMIT 1].Id;
		System.debug('');}catch(Exception e){System.debug('### error acc_RecordTypeId:'+e);}

		Account tmp_Acc = new Account();
		tmp_Acc.Name = 'Test Account #123456#';
		tmp_Acc.BillingStreet = 'Högerdamm 41';
		tmp_Acc.BillingCity = 'Hamburg';
		tmp_Acc.BillingPostalCode = '20097';
		tmp_Acc.BillingCountry = 'Germany';
		tmp_Acc.RecordTypeId = acc_RecordTypeId;
		//tmp_Acc.account_status__c = 'Gesperrter Kunde';

		insert tmp_Acc;

		Contact tmp_Con = new Contact();
		tmp_Con.AccountId 				= tmp_Acc.Id;
		tmp_con.Salutation 				= 'Herr';
		tmp_con.Firstname 				= 'Chuck';
		tmp_con.Lastname 					= 'Borris';
		tmp_con.MailingStreet 		= 'Högerdamm 41';
		tmp_con.MailingCity 			= 'Hamburg';
		tmp_con.MailingPostalCode	= '20097';
		tmp_con.MailingCountry 		= 'Germany';

		insert tmp_con;

		opportunity__c tmp_opp = new opportunity__c();
		tmp_opp.Name = 'TestOpp';
		tmp_opp.account__c = tmp_Acc.Id;
		tmp_opp.Stage__C = 'Test';
		tmp_opp.honorarumsatz__c = 1000.00;
		tmp_opp.fremdkosten__c = 100;
		tmp_opp.projektstart__c = Date.Today();
		tmp_opp.projektende__c = Date.Today()+95;
		tmp_opp.zz_stage_date__c = Date.Today()-95;
		tmp_opp.zz_open_activities__c = 0;
		tmp_opp.schlusstermin__c = Date.Today()-300;

		insert tmp_opp;

		Projekte__c tmp_pro = new Projekte__c();
		tmp_pro.Name = 'Test';
		tmp_pro.account__c = tmp_Acc.Id;

		insert tmp_pro;

		Test.StopTest();


		Account tmp_Acc2 = new Account();
		tmp_Acc2 = [SELECT Id, Name FROM Account WHERE Name like 'Test Account #123456#%' LIMIT 1];


		tmp_Acc2.account_status__c = 'Gesperrter Kunde';
		update tmp_Acc2;
		//Test.stopTest();
	}

	/******************************************
		nicht mehr gesperrter kunde
	******************************************/
	@isTest static void test_method_nicht_mehr_gesperrt() {

		Test.StartTest();
		String acc_RecordTypeId = null;

		try{
			acc_RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'kunde' AND SobjectType='Account' LIMIT 1].Id;
		System.debug('');}catch(Exception e){System.debug('### error acc_RecordTypeId:'+e);}

		Account tmp_Acc = new Account();
		tmp_Acc.Name = 'Test Account #123456#';
		tmp_Acc.BillingStreet = 'Högerdamm 41';
		tmp_Acc.BillingCity = 'Hamburg';
		tmp_Acc.BillingPostalCode = '20097';
		tmp_Acc.BillingCountry = 'Germany';
		tmp_Acc.RecordTypeId = acc_RecordTypeId;
		tmp_Acc.account_status__c = 'Gesperrter Kunde';

		insert tmp_Acc;

		Contact tmp_Con = new Contact();
		tmp_Con.AccountId 				= tmp_Acc.Id;
		tmp_con.Salutation 				= 'Herr';
		tmp_con.Firstname 				= 'Chuck';
		tmp_con.Lastname 					= 'Borris';
		tmp_con.MailingStreet 		= 'Högerdamm 41';
		tmp_con.MailingCity 			= 'Hamburg';
		tmp_con.MailingPostalCode	= '20097';
		tmp_con.MailingCountry 		= 'Germany';

		insert tmp_con;


		Test.StopTest();


		Account tmp_Acc2 = new Account();
		tmp_Acc2 = [SELECT Id, Name FROM Account WHERE Name like 'Test Account%' LIMIT 1];


		tmp_Acc2.account_status__c = 'Inaktiver Kunde';
		update tmp_Acc2;
		//Test.stopTest();
	}

	/******************************************
		nicht mehr gesperrter kunde
	******************************************/
	@isTest static void test_method_nicht_mehr_gesperrt_mit_opp_etc() {

		Test.StartTest();
		String acc_RecordTypeId = null;

		try{
			acc_RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'kunde' AND SobjectType='Account' LIMIT 1].Id;
		System.debug('');}catch(Exception e){System.debug('### error acc_RecordTypeId:'+e);}

		Account tmp_Acc = new Account();
		tmp_Acc.Name = 'Test Account #123456#';
		tmp_Acc.BillingStreet = 'Högerdamm 41';
		tmp_Acc.BillingCity = 'Hamburg';
		tmp_Acc.BillingPostalCode = '20097';
		tmp_Acc.BillingCountry = 'Germany';
		tmp_Acc.RecordTypeId = acc_RecordTypeId;
		tmp_Acc.account_status__c = 'Gesperrter Kunde';

		insert tmp_Acc;

		Contact tmp_Con = new Contact();
		tmp_Con.AccountId 				= tmp_Acc.Id;
		tmp_con.Salutation 				= 'Herr';
		tmp_con.Firstname 				= 'Chuck';
		tmp_con.Lastname 					= 'Borris';
		tmp_con.MailingStreet 		= 'Högerdamm 41';
		tmp_con.MailingCity 			= 'Hamburg';
		tmp_con.MailingPostalCode	= '20097';
		tmp_con.MailingCountry 		= 'Germany';

		insert tmp_con;

		opportunity__c tmp_opp = new opportunity__c();
		tmp_opp.Name = 'TestOpp';
		tmp_opp.account__c = tmp_Acc.Id;
		tmp_opp.Stage__C = 'Test';
		tmp_opp.honorarumsatz__c = 1000.00;
		tmp_opp.fremdkosten__c = 100;
		tmp_opp.projektstart__c = Date.Today();
		tmp_opp.projektende__c = Date.Today()+95;
		tmp_opp.zz_stage_date__c = Date.Today()-95;
		tmp_opp.zz_open_activities__c = 0;
		tmp_opp.schlusstermin__c = Date.Today()-300;

		insert tmp_opp;

		Projekte__c tmp_pro = new Projekte__c();
		tmp_pro.Name = 'Test';
		tmp_pro.account__c = tmp_Acc.Id;

		insert tmp_pro;

		Test.StopTest();


		Account tmp_Acc2 = new Account();
		tmp_Acc2 = [SELECT Id, Name FROM Account WHERE Name like 'Test Account%' LIMIT 1];


		tmp_Acc2.account_status__c = 'Inaktiver Kunde';
		update tmp_Acc2;
		//Test.stopTest();
	}

}