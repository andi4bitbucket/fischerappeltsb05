/***************************************************************************
Company: aquilliance GmbH
Author: Andre Mergen
CreatedDate: 2016-12

***************************************************************************/
public with sharing class Opportunity_Budget_View_Ext {

	public opportunity__c the_opp {get;set;}
public List<wrapper_budget> list_wrapper_budgets {get;set;}
public SET<Id> set_budget_ids = new SET<Id>();





// muss nicht mehr get:set
public MAP<Id, List<budget__c>> map_child_budget {get;set;}
public List<budget__c> list_budget {get;set;}
public boolean no_budgets {get;set;}





	// The extension constructor initializes the private member
	// variable mysObject by using the getRecord method from the standard
	// controller.
	public Opportunity_Budget_View_Ext(ApexPages.StandardController stdController) {
		init();
		the_opp = (opportunity__c)stdController.getRecord();
		select_opp();
		select_budgets();
		select_monats_budgets();
		generate_wrapper();

		if(list_wrapper_budgets.size()<=0)
		{
			no_budgets = true;
		}
	}

	/*********************************************
	um die VAR´s zu initialisieren
	*********************************************/
	public void init()
	{
		list_budget = new List<budget__c>();
		map_child_budget = new MAP<Id, List<budget__c>>();
		list_wrapper_budgets = new List<wrapper_budget>();
		no_budgets = false;
	}



	/*********************************************
	select opp infos
	*********************************************/
	public void select_opp()
	{
		the_opp = [SELECT Id, Name,account__c, honorarumsatz__c, projektstart__c, projektende__c, OwnerId, stage__c,opportunity_art__c,
		zz_helper_start_end_date_changed__c FROM opportunity__c WHERE Id =:the_opp.Id];
	}


	/*********************************************
	select primär und sekundär budgets
	*********************************************/
	public void select_budgets()
	{
		String RecordTypeId = null;
		try{
			RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'monats_budget' AND SobjectType='budget__c' LIMIT 1].Id;
		System.debug('');}catch(Exception e){System.debug('### error select_budgets RecordTypeId:'+e);}
try{
		list_budget = [SELECT Id,
		Name,
		opportunity__c,
		RecordTypeId,
		budget__c,
		benutzer__c,
		benutzer__r.Name,
		honorarumsatz_absolut__c,
		betreffender_monat__c,
		kostenstelle__c
		FROM budget__c WHERE opportunity__c =:the_opp.Id AND RecordTypeId !=:RecordTypeId AND budget__c = null ORDER BY RecordTypeId];
	System.debug('');}catch(Exception e){System.debug('### error select_budgets:'+e);}
		/**********************************************
		map vorbefüllen
		**********************************************/
		for(budget__c tmp_budget:list_budget)
		{
			// tmp_budget
			if(!map_child_budget.containsKey(tmp_budget.Id))
			{
				map_child_budget.put(tmp_budget.Id, new List<budget__c>());
				set_budget_ids.add(tmp_budget.Id);
			}
		}
	}

	/*********************************************
	select der monats budgets
	*********************************************/
	public void select_monats_budgets()
	{
		String RecordTypeId = null;
		List<budget__c> tmp_map_list_child_budget = new List<budget__c>();
		List<budget__c> tmp_list_child_budget = new List<budget__c>();
		try{
			RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'monats_budget' AND SobjectType='budget__c' LIMIT 1].Id;
		System.debug('');}catch(Exception e){System.debug('### error select_monats_budgets RecordTypeId:'+e);}

		try{
		tmp_list_child_budget = [SELECT Id,
		Name,
		opportunity__c,
		RecordTypeId,
		budget__c,
		benutzer__c,
		honorarumsatz_absolut__c,
		betreffender_monat__c
		FROM budget__c WHERE budget__c =:set_budget_ids AND RecordTypeId =:RecordTypeId ORDER BY betreffender_monat__c];
		System.debug('');}catch(Exception e){System.debug('### error tmp_list_child_budget:'+e);}


		/**********************************************
		map befüllen
		**********************************************/
		for(budget__c tmp_child_budget:tmp_list_child_budget)
		{
			// tmp liste zurücksetzten
			tmp_map_list_child_budget = new List<budget__c>();
			// wenn map key enthält
			if(map_child_budget.containsKey(tmp_child_budget.budget__c))
			{
				// hole liste zum parent
				tmp_map_list_child_budget = map_child_budget.get(tmp_child_budget.budget__c);
				// füge der liste den neuen eintrag hinzu
				tmp_map_list_child_budget.add(tmp_child_budget);
				// füge die liste wieder in die map ein
				map_child_budget.put(tmp_child_budget.budget__c, tmp_map_list_child_budget);
			}
		}
	}



	public void btn_nix()
	{

	}


	public void generate_wrapper()
	{
		System.debug('#### generate_wrapper');
		String RecordTypeId_primaer_budget = null;

		try{
			RecordTypeId_primaer_budget = [SELECT Id FROM RecordType WHERE DeveloperName = 'primaer_budget' AND SobjectType='budget__c' LIMIT 1].Id;
		}catch(Exception e){}

		wrapper_budget tmp_wb = new wrapper_budget();


		for(budget__c tmp_bg:list_budget)
		{
			tmp_wb = new wrapper_budget();

			if(tmp_bg.RecordTypeId == RecordTypeId_primaer_budget)
			{
				tmp_wb.primary = true;
			}
			else{
				tmp_wb.primary = false;
			}
			tmp_wb.isnew = false;
			tmp_wb.budget = tmp_bg;
			tmp_wb.list_child_budget = map_child_budget.get(tmp_bg.Id);

			list_wrapper_budgets.add(tmp_wb);
		}


	}


	/******************************************
		wrapper class
		verwendet für list_wrapper_contacts
	******************************************/
	public class wrapper_budget
	{
		public boolean isnew {get;set;}
		public boolean primary {get;set;}
		public budget__c budget {get;set;}
		public List<budget__c> list_child_budget {get;set;}
	}
}