/**************************************************************************
Company: aquilliance GmbH
Author: Andreas Wolf
CreatedDate: 2017-4
Edit: 2017-08 Simon Kuhnt
**************************************************************************/
public with sharing class TeamMemberTriggerLogic {

	public TeamMemberTriggerLogic() {}

	public void createFollowingRecordsForTeamMemberUsers(List<Team_Member__c> listTeamMember)
	{
		List<EntitySubscription> listNewEntitySubscriptions = new List<EntitySubscription>();
		for(Team_Member__c tmp:listTeamMember)
		{
			String parentRecordId = getRelevantParentRecordId(tmp);
			if(parentRecordId != ''){
				EntitySubscription newEntitySubscription = new EntitySubscription();
				newEntitySubscription.SubscriberId = tmp.User__c;
				newEntitySubscription.ParentId = parentRecordId;
				listNewEntitySubscriptions.add(newEntitySubscription);
			}
		}

		createFollowingRecords(listNewEntitySubscriptions);
	}

	public String getRelevantParentRecordId(Team_Member__c teamMember)
	{
		if(teamMember.Account__c != null)
			return teamMember.Account__c;
		if(teamMember.CO_Case__c != null)
			return teamMember.CO_Case__c;
		if(teamMember.Opportunity__c != null)
			return teamMember.Opportunity__c;
		if(teamMember.Kampagne__c != null && teamMember.Kampagne_Aktiv__c){
			return teamMember.Kampagne__c;
		}



		return '';
	}

	public void createFollowingRecords(List<EntitySubscription> listNewEntitySubscriptions)
	{
		try{
			insert listNewEntitySubscriptions;
		System.debug('');}catch(Exception e){
			System.debug('### -TeamMemberTriggerLogic-createFollowingRecords: '+e);
		}
	}

	public void deleteFollowingRecordsForTeamMemberUsers(List<Team_Member__c> listTeamMember)
	{
		Set<String> setParentRecordIds = new Set<String>();
		Set<String> setUserIds = new Set<String>();
		for(Team_Member__c tmp:listTeamMember)
		{
			if(!setUserIds.contains(tmp.User__c))
				setUserIds.add(tmp.User__c);

			String parentRecordId = getRelevantParentRecordId(tmp);
			if(!setParentRecordIds.contains(parentRecordId) && parentRecordId != '')
				setParentRecordIds.add(parentRecordId);
		}
		List<EntitySubscription> listEntitySubscriptions = new List<EntitySubscription>();
		deleteFollowingRecords(getFollowingRecords(setUserIds, setParentRecordIds));
	}

	public List<EntitySubscription> getFollowingRecords(Set<String> setUserIds, Set<String> setParentIds)
	{
		List<EntitySubscription> listExistingEntitySubscriptions = new List<EntitySubscription>();
		try{listExistingEntitySubscriptions = [SELECT Id FROM EntitySubscription WHERE ParentId IN: setParentIds AND Subscriberid IN: setUserIds];
		System.debug('');}catch(Exception e){System.debug('### -TeamMemberTriggerLogic-getFollowingRecords: '+e);}
		return listExistingEntitySubscriptions;
	}

	public void deleteFollowingRecords(List<EntitySubscription> listEntitySubscriptions)
	{
		try{
			delete listEntitySubscriptions;
		System.debug('');}catch(Exception e){
			System.debug('### -TeamMemberTriggerLogic-deleteFollowingRecords: '+e);
		}
	}

	public void setSearchableTextFieldsForMember(List<Team_Member__c> listTeamMember)
	{
		Map<Id, List<Team_Member__c>> mapAccIdsListTeamMember = new Map<Id, List<Team_Member__c>>();
		Map<Id, List<Team_Member__c>> mapCaseIdsListTeamMember = new Map<Id, List<Team_Member__c>>();

		List<Team_Member__c> maplistTeamMember;
		for(Team_Member__c tmp:listTeamMember)
		{
			maplistTeamMember = new List<Team_Member__c>();
			if(tmp.Account__c != NULL && tmp.zz_searchUserNameHelper__c != '')
			{
				if(!mapAccIdsListTeamMember.containsKey(tmp.Account__c))
				{
					maplistTeamMember.add(tmp);
					mapAccIdsListTeamMember.put(tmp.Account__c, maplistTeamMember);
				}else{
					maplistTeamMember = mapAccIdsListTeamMember.get(tmp.Account__c);
					maplistTeamMember.add(tmp);
					mapAccIdsListTeamMember.put(tmp.Account__c, maplistTeamMember);
				}
			}
			if(tmp.CO_Case__c != NULL && tmp.zz_searchUserNameHelper__c != '')
			{
				if(!mapCaseIdsListTeamMember.containsKey(tmp.CO_Case__c))
				{
					maplistTeamMember.add(tmp);
					mapCaseIdsListTeamMember.put(tmp.CO_Case__c, maplistTeamMember);
				}else{
					maplistTeamMember = mapCaseIdsListTeamMember.get(tmp.CO_Case__c);
					maplistTeamMember.add(tmp);
					mapCaseIdsListTeamMember.put(tmp.CO_Case__c, maplistTeamMember);
				}
			}
		}
		List<Account> listAccForUpdate = getAccountInfos(mapAccIdsListTeamMember);
		for(Account tmpAcc:listAccForUpdate)
		{
			for(Team_Member__c tmpTM: mapAccIdsListTeamMember.get(tmpAcc.Id))
			{
				if(tmpAcc.zz_searchTeamMember__c != null)
					tmpAcc.zz_searchTeamMember__c += ' '+tmpTM.zz_searchUserNameHelper__c;
				else
					tmpAcc.zz_searchTeamMember__c = tmpTM.zz_searchUserNameHelper__c;
			}
		}
		List<Case__c> listCasesForUpdate = getCaseInfos(mapCaseIdsListTeamMember);
		for(Case__c tmpCase:listCasesForUpdate)
		{
			for(Team_Member__c tmpTM: mapCaseIdsListTeamMember.get(tmpCase.Id))
			{
				if(tmpCase.zz_searchTeamMember__c != null)
					tmpCase.zz_searchTeamMember__c += ' '+tmpTM.zz_searchUserNameHelper__c;
				else
					tmpCase.zz_searchTeamMember__c = tmpTM.zz_searchUserNameHelper__c;
			}
		}
		updateAccs(listAccForUpdate);
		updateCases(listCasesForUpdate);
	}

	public void removeSearchableTextFieldsForMember(List<Team_Member__c> listTeamMember)
	{
		Map<Id, List<Team_Member__c>> mapAccIdsListTeamMember = new Map<Id, List<Team_Member__c>>();
		Map<Id, List<Team_Member__c>> mapCaseIdsListTeamMember = new Map<Id, List<Team_Member__c>>();
		List<Team_Member__c> maplistTeamMember;
		for(Team_Member__c tmp:listTeamMember)
		{
			maplistTeamMember = new List<Team_Member__c>();
			if(tmp.Account__c != NULL && tmp.zz_searchUserNameHelper__c != '')
			{
				if(!mapAccIdsListTeamMember.containsKey(tmp.Account__c))
				{
					maplistTeamMember.add(tmp);
					mapAccIdsListTeamMember.put(tmp.Account__c, maplistTeamMember);
				}else{
					maplistTeamMember = mapAccIdsListTeamMember.get(tmp.Account__c);
					maplistTeamMember.add(tmp);
					mapAccIdsListTeamMember.put(tmp.Account__c, maplistTeamMember);
				}
			}
			if(tmp.CO_Case__c != NULL && tmp.zz_searchUserNameHelper__c != '')
			{
				if(!mapCaseIdsListTeamMember.containsKey(tmp.CO_Case__c))
				{
					maplistTeamMember.add(tmp);
					mapCaseIdsListTeamMember.put(tmp.CO_Case__c, maplistTeamMember);
				}else{
					maplistTeamMember = mapCaseIdsListTeamMember.get(tmp.CO_Case__c);
					maplistTeamMember.add(tmp);
					mapCaseIdsListTeamMember.put(tmp.CO_Case__c, maplistTeamMember);
				}
			}
		}
		List<Account> listAccForUpdate = getAccountInfos(mapAccIdsListTeamMember);
		for(Account tmpAcc:listAccForUpdate)
		{
			for(Team_Member__c tmpTM: mapAccIdsListTeamMember.get(tmpAcc.Id))
			{
				if(tmpAcc.zz_searchTeamMember__c != null)
				{
					if(tmpAcc.zz_searchTeamMember__c.contains(tmpTM.zz_searchUserNameHelper__c))
						tmpAcc.zz_searchTeamMember__c = tmpAcc.zz_searchTeamMember__c.replace(tmpTM.zz_searchUserNameHelper__c, '');
				}
			}
		}
		List<Case__c> listCasesForUpdate = getCaseInfos(mapCaseIdsListTeamMember);
		for(Case__c tmpCase:listCasesForUpdate)
		{
			for(Team_Member__c tmpTM: mapCaseIdsListTeamMember.get(tmpCase.Id))
			{
				if(tmpCase.zz_searchTeamMember__c != null)
				{
					if(tmpCase.zz_searchTeamMember__c.contains(tmpTM.zz_searchUserNameHelper__c))
						tmpCase.zz_searchTeamMember__c = tmpCase.zz_searchTeamMember__c.replace(tmpTM.zz_searchUserNameHelper__c, '');
				}
			}
		}
		updateAccs(listAccForUpdate);
		updateCases(listCasesForUpdate);
	}

	public void postOnChatterEinrichtungsTeamInsert(List<Team_Member__c> newTeamMember){
		Set<Id> einreichungSet = new Set<Id>();
		Set<Id> userSet = new Set<Id>();
		List<Team_Member__c> einreichungTeamMembers = new List<Team_Member__c>();
		List<ConnectApi.BatchInput> batchInputs = new List<ConnectApi.BatchInput>();
		Map<Id, User> userMap = new Map<Id, User>();
		Map<Id, Team_Member__c> einreichungMap = new Map<Id, Team_Member__c>();

		for (Team_Member__c tmpMember : newTeamMember){
			if (tmpMember.Team_Member_Einreichung__c != null){
					einreichungSet.add(tmpMember.Team_Member_Einreichung__c);
					einreichungTeamMembers.add(tmpMember);
					userSet.add(tmpMember.User__c);
			}
		}
		userMap = new Map<Id,User>([SELECT Id, Name FROM User WHERE Id = :userSet]);
		einreichungMap = new Map<Id,Team_Member__c>([SELECT Id, Einreichungsschluss__c, award_lookup__c, zz_helper_Award_Name__c, case_for_award__c FROM Team_Member__c WHERE Id = :einreichungSet]);

		for (Team_Member__c tmpTeamEinreichung: einreichungTeamMembers){
			User tmpUser = userMap.get(tmpTeamEinreichung.User__c);
			Team_Member__c tmpEinreichung = einreichungMap.get(tmpTeamEinreichung.Team_Member_Einreichung__c);
			if (tmpUser.Name != null && tmpEinreichung.Id != null){
				String chatterText = 'Hallo '+ tmpUser.Name +' willkommmen im Award Team';
				if (tmpEinreichung.case_for_award__c != null){
					chatterText += ' für den Case mit der Id: '+ tmpEinreichung.case_for_award__c + ' und';
				}
				if (tmpEinreichung.zz_helper_Award_Name__c != null){
					chatterText += ' für den Award: '+tmpEinreichung.zz_helper_Award_Name__c;
					if (tmpEinreichung.Einreichungsschluss__c != null){
						chatterText += ' mit dem Einreichungsschluss '+tmpEinreichung.Einreichungsschluss__c.format();
					}
				}
				batchInputs.add(prepareChatterPost(tmpUser.Id, tmpEinreichung.Id, chatterText));
			}
		}
		ConnectApi.ChatterFeeds.postFeedElementBatch(Network.getNetworkId(), batchinputs);
	}

	private ConnectApi.BatchInput prepareChatterPost(Id mention_user_id, Id record_Id, String short_text)
	{
		ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
		ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
		ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
		ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();

		messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
		//Mention user here
		mentionSegmentInput.id = mention_user_id;
		messageBodyInput.messageSegments.add(mentionSegmentInput);


		textSegmentInput.text = '\n'+short_text;
		messageBodyInput.messageSegments.add(textSegmentInput);

		feedItemInput.body = messageBodyInput;
		feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;

		feedItemInput.subjectId = record_Id;

		ConnectApi.BatchInput batchInput = new ConnectApi.BatchInput(feedItemInput);

		return batchInput;
	}


	public List<Account> getAccountInfos(Map<Id, List<Team_Member__c>> accountIds)
	{
		List<Account> listAccs = new List<Account>();
		try{
			listAccs = [SELECT Id, zz_searchTeamMember__c FROM Account WHERE Id IN: accountIds.KeySet()];
		System.debug('');}catch(Exception e){
			System.debug('###-TeamMemberTriggerLogic-getAccountInfos: '+e);
		}
		return listAccs;
	}

	public void updateAccs(List<Account> listAccs)
	{
		try{
			update listAccs;
		System.debug('');}catch(Exception e)		{
			System.debug('###-TeamMemberTriggerLogic-updateAccs: '+e);
		}
	}

	public List<Case__c> getCaseInfos(Map<Id, List<Team_Member__c>> caseIds)
	{
		List<Case__c> listCases = new List<Case__c>();
		try{
			listCases = [SELECT Id, zz_searchTeamMember__c FROM Case__c WHERE Id IN: caseIds.KeySet()];
		System.debug('');}catch(Exception e){
			System.debug('###-TeamMemberTriggerLogic-getCaseInfos: '+e);
		}
		return listCases;
	}

	public void updateCases(List<Case__c> listCases)
	{
		try{
			update listCases;
		System.debug('');}catch(Exception e)		{
			System.debug('###-TeamMemberTriggerLogic-updateCases: '+e);
		}
	}




	/***************************************************************************************
			Erweiterung für Chatter Info, erzeugt den Chatter Eintrag
	***************************************************************************************/
	public void create_chatter_post_on_Case_after_status_change(List<Team_Member__c> list_tm, MAP<Id, Team_Member__c> old_map_tm)
	{
		List<ConnectApi.BatchInput> batchInputs = new List<ConnectApi.BatchInput>();
		ConnectApi.BatchInput tmp_batchInput = new ConnectApi.BatchInput(new ConnectApi.FeedItemInput());

		for(Team_Member__c tmp_tm:list_tm)
		{
			// reset
			tmp_batchInput = new ConnectApi.BatchInput(new ConnectApi.FeedItemInput());

			if(tmp_tm.case_for_award__c != null && tmp_tm.Status__c != old_map_tm.get(tmp_tm.Id).Status__c)
			{
				tmp_batchInput = chatter_info_prepare_chatter_message(tmp_tm.OwnerId, tmp_tm.case_for_award__c,'Award Status geändert zu "'+tmp_tm.Status__c+'": Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam');
				batchInputs.add(tmp_batchInput);
			}
		}


		//postet die chatter meldungen
		if(batchInputs.size()>0)
		{
			ConnectApi.ChatterFeeds.postFeedElementBatch(Network.getNetworkId(), batchInputs);
		}

	}

	/***************************************************************************************
			Erweiterung für Chatter Info, erzeugt den Chatter Eintrag
	***************************************************************************************/
	private ConnectApi.BatchInput chatter_info_prepare_chatter_message(String mention_user_id, String record_Id, String short_text)
	{

		ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
		ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
		ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
		ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();

		messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
		//Mention user here
		mentionSegmentInput.id = mention_user_id;
		messageBodyInput.messageSegments.add(mentionSegmentInput);

		textSegmentInput.text = '\n'+short_text;
		messageBodyInput.messageSegments.add(textSegmentInput);

		feedItemInput.body = messageBodyInput;
		feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
		// datensatz an dem der chatter post hängt
		feedItemInput.subjectId = record_Id;

		ConnectApi.BatchInput batchInput = new ConnectApi.BatchInput(feedItemInput);

		return batchInput;
	}

}