/**************************************************************************
Company: aquilliance GmbH
Author: Andreas Wolf
CreatedDate: 2017-8
**************************************************************************/
@isTest
private class ReminderContactBirthdayBatch_Test {

	@isTest static void test_ReminderContactBirthdayBatch() {
		Account acc = new Account();
		acc.Name 								= 'Test AQ';
		acc.BillingStreet 			= 'Högerdamm 41';
		acc.BillingCity 				= 'Hamburg';
		acc.BillingPostalCode 	= '20097';
		acc.BillingCountry 			= 'Germany';
		acc.ShippingStreet 			= 'Högerdamm 41';
		acc.ShippingCity 				= 'Hamburg';
		acc.ShippingPostalCode 	= '20097';
		acc.ShippingCountry 		= 'Germany';
		insert acc;

		Contact con = new Contact();
		con.AccountId = acc.Id;
		con.Firstname = 'Chuck';
		con.Lastname = 'Blue';
		con.Salutation = 'Mr.';
		con.MailingStreet = 'Blumen Str. 1';
		con.MailingCity = 'Hamburg';
		con.MailingPostalCode = '20097';
		con.MailingCountry = 'Germany';
		con.Birthdate = Date.today().addDays(3);
		insert con;

		ReminderContactBirthdayBatch b1 = new ReminderContactBirthdayBatch();
		database.executebatch(b1);
	}
}