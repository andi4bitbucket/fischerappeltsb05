/**************************************************************************
Company: aquilliance GmbH
Author: Andreas Wolf
CreatedDate: 2017-1

**************************************************************************/
@isTest
private class List_Management_Contact_Start_Test {

	@isTest static void test_method_one() {
		Account tmp_acc = new Account();
		tmp_acc.Name = 'Test';
		tmp_acc.Industry = 'Test';

		insert tmp_acc;

		Contact tmp_con = new Contact();
		tmp_con.AccountId = tmp_acc.Id;
		tmp_con.FirstName = 'Max';
		tmp_con.LastName = 'Mustermann';
		tmp_con.Salutation = 'Mr.';
		tmp_con.Title = 'Test';
		tmp_con.Department = 'Test';

		insert tmp_con;

    // Controller Test
    PageReference pref = Page.List_Management_Contact_Start;
    pref.getParameters().put('id',tmp_con.Id);
    Test.setCurrentPage(pref);

    ApexPages.StandardController con = new ApexPages.StandardController(tmp_con);
    List_Management_Contact_Start t = new List_Management_Contact_Start(con);
    t.nextStep();
	}

}