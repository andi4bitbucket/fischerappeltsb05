/**************************************************************************
Company: aquilliance GmbH
Author: Andreas Wolf
CreatedDate: 2017-1

**************************************************************************/
public with sharing class List_Management_Contact_Start {
		public Contact c_START {get;set;}
		public Contact c {get;set;}

    public List_Management_Contact_Start(ApexPages.StandardController stdController) {
        this.c_START = (Contact)stdController.getRecord();
    }

    public PageReference nextStep()
    {
			c = new Contact();
      try{c = [SELECT Id, FirstName, LastName, Title, Department, Account.Name, Account.Branche__c FROM Contact WHERE Id =: c_START.Id LIMIT 1];
      System.debug('');}catch(Exception e){System.debug('### -List_Management_Contact_Start- getContact: '+e);}

			String firstName = '';
			String lastName = '';
			String accName = '';
			String accBranch = '';
			String department = '';
			String function = '';

			if(c.FirstName != null)
				firstName = c.FirstName;
			if(c.LastName != null)
				lastName = c.LastName;
			if(c.Account.Name != null)
				accName = c.Account.Name;
			if(c.Account.Branche__c != null)
				accBranch = c.Account.Branche__c;
			if(c.Department != null)
				department = c.Department;
			if(c.Title != null)
				function = c.Title;


      return new PageReference('/apex/List_Management?first_name='+firstName+'&last_name='+lastName+'&depa='+department+'&func='+function+'&acc_name='+accName+'&acc_branch='+accBranch+'&search=true');
    }

}