/**************************************************************************
Company: aquilliance GmbH
Author: Andreas Wolf
CreatedDate: 2017-8
**************************************************************************/
global class ReminderSchedulable implements Schedulable {
	/***********************************************
		to Start open execute anonymous

		ReminderSchedulable rs = new ReminderSchedulable();
		system.schedule('Check For Reminders', rs.CRON_EXP_everyday_4am, rs);
	***********************************************/
	public String CRON_EXP_everyday_4am =  '0 0 4 * * ?';

	public ReminderSchedulable() {}

	global void execute(SchedulableContext sc) {
		run();
	}

	public void run(){
		// Checks wether a contact has birthday in 3 days and posts to chatter
		ReminderContactBirthdayBatch b1 = new ReminderContactBirthdayBatch();
		database.executebatch(b1);
		// Checks wether the Next_Date__c of an Opportunity is 7 days in the past or Close_Date__c is in 14 days and posts to chatter
		ReminderOpportunityBatch b2 = new ReminderOpportunityBatch();
		database.executebatch(b2);
	}
}