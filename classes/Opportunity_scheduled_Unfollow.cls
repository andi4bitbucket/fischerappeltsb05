/**************************************************************************
Company: aquilliance GmbH
Author: Andreas Wolf
CreatedDate: 2017-1

**************************************************************************/
global class Opportunity_scheduled_Unfollow implements Schedulable {
/***********************************************
to Start open execute anonymous

Opportunity_scheduled_Unfollow con = new Opportunity_scheduled_Unfollow();
system.schedule('Delete Closed Opp Follower', con.CRON_EXP_everyday_3am, con);
***********************************************/
	public String CRON_EXP_everyday_3am =  '0 0 3 * * ?';

	global void execute(SchedulableContext sc) {
		run();
	}

	public void run(){
		Boolean deleteSuccess = false;

		List<opportunity__c> list_opps = new List<opportunity__c>();
		try{list_opps = [SELECT Id, status__c, zz_unfollowed__c FROM Opportunity__c WHERE
			status__c = 'closed' AND zz_unfollowed__c = false];
		System.debug('');}catch(Exception e){System.debug('### -Opportunity_scheduled_Unfollow-run- getOpps:'+e);}

		Set<String> set_parent_ids = new Set<String>();
		for(Opportunity__c tmp:list_opps)
		{
			set_parent_ids.add(tmp.Id);
			tmp.zz_unfollowed__c = true;
		}

		// Get all related Salesflashes
		List<Salesflash__c> list_sf = new List<Salesflash__c>();
		try{list_sf = [SELECT Id FROM Salesflash__c WHERE
			Opportunity__c IN: set_parent_ids];
		System.debug('');}catch(Exception e){System.debug('### -Opportunity_scheduled_Unfollow-run- getSalesflashes:'+e);}
		for(Salesflash__c tmp:list_sf)
		{
			set_parent_ids.add(tmp.Id);
		}

		List<EntitySubscription> list_existing_follows = new List<EntitySubscription>();
		try{list_existing_follows = [SELECT Id FROM EntitySubscription WHERE parentId IN: set_parent_ids];
		System.debug('');}catch(Exception e){System.debug('### -Opportunity_scheduled_Unfollow-run- getExisitingSubscriptions:'+e);}

		try{delete list_existing_follows; deleteSuccess = true;
		System.debug('');}catch(Exception e){System.debug('### -Opportunity_scheduled_Unfollow-run- deleteAllUserFollows:'+e);}

		if(deleteSuccess)
		{
			try{update list_opps;
			System.debug('');}catch(Exception e){System.debug('### -Opportunity_scheduled_Unfollow-run- updateOpps:'+e);}
		}
	}
}