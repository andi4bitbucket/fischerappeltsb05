trigger Task_Trigger on Task (after insert, after update, after delete, after undelete) {
    
    if( Trigger.isInsert || Trigger.isupdate || Trigger.isUndelete ){
        RecordAggregator.aggregate( Trigger.new, Trigger.old );
    }
    if( Trigger.isDelete ){
        RecordAggregator.aggregate( Trigger.new, Trigger.old );
    }

}