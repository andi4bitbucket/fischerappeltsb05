/**************************************************************************
Company: aquilliance GmbH
Author: Andreas Wolf
CreatedDate: 2017-4
Edit: 2017-08 Simon Kuhnt
**************************************************************************/
trigger TeamMember on Team_Member__c (before insert, before delete, after insert, after delete, after Update) {

  TeamMemberTriggerLogic logic = new TeamMemberTriggerLogic();

  if(Trigger.isBefore)
  {
    if(Trigger.isInsert)
    {
      logic.setSearchableTextFieldsForMember(Trigger.new);
    }
    if(Trigger.isDelete)
    {
      logic.removeSearchableTextFieldsForMember(Trigger.old);
    }
  }

  if(Trigger.isAfter)
  {
    if(Trigger.isInsert)
    {
      logic.createFollowingRecordsForTeamMemberUsers(Trigger.new);
      logic.postOnChatterEinrichtungsTeamInsert(Trigger.new);
    }
    if(Trigger.isDelete)
    {
      logic.deleteFollowingRecordsForTeamMemberUsers(Trigger.old);
    }
    if(Trigger.isUpdate)
    {
      logic.deleteFollowingRecordsForTeamMemberUsers(Trigger.old);
      logic.createFollowingRecordsForTeamMemberUsers(Trigger.new);
      logic.create_chatter_post_on_Case_after_status_change(Trigger.new, Trigger.oldmap);
    }
  }
}