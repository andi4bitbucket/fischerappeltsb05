/**************************************************************************
Company: aquilliance GmbH
Author: Andreas Wolf
CreatedDate: 2017-4
**************************************************************************/
trigger CO_Case on Case__c (before insert, before update, after insert) {

  CO_CaseTriggerLogic logic = new CO_CaseTriggerLogic();

  if(Trigger.isBefore)
  {
    if(Trigger.isInsert || Trigger.isUpdate)
    {
      logic.setSearchableTextFields(Trigger.new);
    }
  }
  if(Trigger.isAfter)
  {
    if(Trigger.isInsert)
    {
      logic.afterInsertCheckIFcreateChatterPosts(Trigger.newMap);
    }
  }

}