/**************************************************************************
Company: aquilliance GmbH
Author: Andreas Wolf
CreatedDate: 2017-1

**************************************************************************/
trigger List_Member on List_Member__c (
	after insert,
	after update,
	after delete,
	after undelete) {

		List_Member_Trigger con = new List_Member_Trigger();
		if(Trigger.isAfter && Trigger.isInsert)
		{
	    con.doAfterInsert(Trigger.new);
			RecordAggregator.aggregate( Trigger.new, Trigger.old );
		}
		if(Trigger.isAfter && Trigger.isUpdate)
		{
			con.doAfterUpdate(Trigger.new);
		}
		if(Trigger.isAfter && Trigger.isDelete)
		{
			con.doAfterDelete(Trigger.old);
			RecordAggregator.aggregate( Trigger.new, Trigger.old );
		}
		if(Trigger.isAfter && Trigger.isUndelete)
		{
			con.doAfterUndelete(Trigger.new);
		}
}