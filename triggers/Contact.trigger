/**************************************************************************
Company: aquilliance GmbH
Author: Andre Mergen
CreatedDate: 2017-01

**************************************************************************/
trigger Contact on Contact (before insert, before update)
{
  Contact_Trigger cont = new Contact_Trigger();

  if(Trigger.isBefore)
  {
    if(Trigger.isUpdate || Trigger.isInsert)
    {
      //cont.set_name_status(Trigger.new,Trigger.oldMap);
      cont.set_name_sperrgrund(Trigger.new,Trigger.oldMap);
      cont.set_email_opt_out(Trigger.new,Trigger.oldMap);
    }

  }
}