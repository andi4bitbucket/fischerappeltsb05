trigger User_Mandant on User_Mandant__c (
	before insert,
	before update,
	before delete,
	after insert,
	after update,
	after delete,
	after undelete) {

		User_Mandant_Trigger con = new User_Mandant_Trigger();

		if (Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)) {
	    	con.check_for_doubles(Trigger.new);
		}
}