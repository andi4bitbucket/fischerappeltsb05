/**************************************************************************
Company: aquilliance GmbH
Author: Andreas Wolf & Andre Mergen
CreatedDate: 2017-1
**************************************************************************/
trigger Opportunity on Opportunity__c (before insert, before update, after insert, after update, after delete, after undelete)
{
  Opportunity_Trigger ot = new Opportunity_Trigger();
  
  // BEFORE
  if(Trigger.isBefore)
  {
    if(Trigger.isInsert)
    {
      ot.beforeInsertController(Trigger.new);   // starts to loop opps and executes various methods in one for loop
    }
    if(Trigger.isUpdate)
    {
      ot.beforeUpdateController(Trigger.new, Trigger.oldMap); // starts to loop opps and executes various methods in one for loop
    }
  }
  // AFTER
  if(Trigger.isAfter)
  {
    if(Trigger.isInsert)
    {
      ot.after_insert_create_budget(Trigger.new); // for each new opp there has to be a budget
      ot.createFollowingOwnerUser(Trigger.newMap); //  Owner__c and Quelle_Person__c should follow Opp
      ot.afterInsertCheckIFcreateChatterPosts(Trigger.newMap);  // check whether chatter posts should be created for this opp
      ot.afterInsertController(Trigger.new); // starts to loop opps and executes various methods in one for loop
    }
    if(Trigger.isUpdate)
    {
      if(Opportunity_Trigger.runOnce())
      {
        ot.changeFollowingOwnerUser(Trigger.newMap, Trigger.oldMap); // If the Owner__c or the Quelle_Person__c changed, change the Users who follow the opp
        ot.afterUpdateOppChangeBudget(Trigger.new, Trigger.oldMap);  // If the wahrscheinlichkeit__c changed, calculate all Budgets again
        ot.afterUpdateCheckIFcreateChatterPosts(Trigger.new, Trigger.oldMap); // check whether chatter posts should be created for this opp
        ot.afterUpdateController(Trigger.new, Trigger.oldMap); // starts to loop opps and executes various methods in one for loop
        RecordAggregator.aggregate( Trigger.new, Trigger.old );
      }
    }
    if( Trigger.isInsert || Trigger.isUndelete ){
        RecordAggregator.aggregate( Trigger.new, Trigger.old );
    }
    if( Trigger.isDelete ){
        RecordAggregator.aggregate( Trigger.new, Trigger.old );
    }
  }
}
