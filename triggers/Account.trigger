/**************************************************************************
Company: aquilliance GmbH
Author: Andre Mergen
CreatedDate: 2017-01

**************************************************************************/
trigger Account on Account (before insert, before update, after insert, after update)
{
  Account_Trigger at = new Account_Trigger();

  if(Trigger.isBefore)
  {
    if(Trigger.isUpdate || Trigger.isInsert)
    {
      at.setSearchableTextFields(Trigger.new);
      at.set_acc_name_gesperrt(Trigger.new, Trigger.oldMap);
    }
  }

  if(Trigger.isAfter)
  {
    if(Trigger.isUpdate)
    {
      at.chatter_info(Trigger.new, Trigger.oldMap);
    }
  }
}