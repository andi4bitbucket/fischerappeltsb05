/**************************************************************************
Company: aquilliance GmbH
Author: Simon Kuhnt
CreatedDate: 2017-08
**************************************************************************/
trigger CoCampaign on kampagne__c (after Update) {

  if(Trigger.isAfter)
  {
    if(Trigger.isUpdate)
    {
      CoCampaignLogic.chatterFollowUpdate(Trigger.oldMap, Trigger.new);
    }
  }
}