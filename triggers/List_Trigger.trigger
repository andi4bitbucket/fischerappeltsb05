/**************************************************************************
Company: aquilliance GmbH
Author: Andreas Wolf
CreatedDate: 2017-1

**************************************************************************/
trigger List_Trigger on List__c (after insert, after update, after delete, after undelete) {

    if( Trigger.isInsert || Trigger.isupdate || Trigger.isUndelete ){
        RecordAggregator.aggregate( Trigger.new, Trigger.old );
    }
    if( Trigger.isDelete ){
        RecordAggregator.aggregate( Trigger.new, Trigger.old );
    }

}