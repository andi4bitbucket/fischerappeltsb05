/**************************************************************************
Company: aquilliance GmbH
Author: Andreas Wolf
CreatedDate: 2017-1

**************************************************************************/
trigger Budget on Budget__c (
	after insert,
	after update,
	after delete,
	after undelete) {

		Budget_Trigger con = new Budget_Trigger();

		System.debug('### Budget_Trigger run.');
		if(Trigger.isAfter)
		{
			if(Budget_Trigger.runOnce())
			{
				if(Trigger.isAfter && Trigger.isInsert)
				{
					con.doAfterInsert(Trigger.new);
				}

				if(Trigger.isAfter && Trigger.isUpdate)
				{
					con.doAfterUpdate(Trigger.new, Trigger.old);
				}

				if(Trigger.isAfter && Trigger.isDelete)
				{
					con.doAfterDelete(Trigger.old);
				}

				if(Trigger.isAfter && Trigger.isUndelete)
				{
					con.doAfterUnDelete(Trigger.new);
				}
			}
		}

}